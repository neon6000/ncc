/************************************************************************
*
*	ncc.h - NCC Core Interfaces
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#ifndef _NCC_H
#define _NCC_H

#include <stdio.h>
#include "base.h"
#include "msg.h"

/* files */

typedef struct File {
	char*  fname;
	FILE*  fstream;
	int    line;
	BOOL    eof;
	Buffer* pushback;
}File;

typedef BOOL(*file_push_t) (File*);
typedef File* (*file_pop_t) (void);
typedef size_t(*file_depth_t) (void);
typedef File* (*file_get_current_t) (void);
typedef int(*file_peek_t) (IN int index);
typedef int(*file_get_t) (void);
typedef void(*file_unget_t) (IN int c);
typedef void(*file_close_t)(IN File*);
typedef File* (*file_open_t)(IN char*);

typedef struct FileManager {
	file_push_t push;
	file_pop_t pop;
	file_depth_t depth;
	file_get_current_t current;
	file_peek_t peek;
	file_get_t get;
	file_unget_t unget;
	file_close_t close;
	file_open_t open;
}FileManager;

extern FileManager* GetFileManager(void);

typedef struct Location {
	File* src;
	unsigned int line;
}Location;

typedef enum MessagePriority {
	MSG_ERROR,
	MSG_WARN
}MessagePriority;

#define NccErrort(t,msg,...) NccError(MSG_ERROR, ((t.source) ? (t.source->fname) : "null"), t.line, msg, __VA_ARGS__);

extern PUBLIC NccError(IN MessagePriority pri, IN char* filename, IN int line, IN char* msg, ...);

/* translator ----------------- */

typedef struct Block Block;

typedef BOOL(*translate_init_t)(void);
typedef Block*(*translate_t)(IN File* file);

typedef struct Translator {
	translate_init_t init;
	translate_t      gen;
}Translator;

extern Translator* GetTranslator(void);

typedef struct CodeGenerator {
	void(*init)(char*);
	void(*write)(Block*);
	void(*flush)(void);
}CodeGenerator;

extern CodeGenerator* GetCodeGeneratorDot(void);

PUBLIC BOOL InitFrontEnd(void);
PUBLIC BOOL Parse(IN char* fname, OUT struct Function* fc);
PUBLIC BOOL NextParse(OUT struct Function* fc);

/* target capabilities ------- */

typedef struct CodeGeneratorCaps {
	uint32_t maxIntRegister;
	uint32_t maxFloatRegister;
}CodeGeneratorCaps;

/*
extern Cache _vectors;
extern Cache _buffers;
extern Cache _symbols;
extern Cache _types;
extern Cache _astNodes;
extern Cache _ilBlocks;
extern Cache _ilInstrs;
extern Cache _ilRegs;
*/

#endif

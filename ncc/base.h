/************************************************************************
*
*	base.h - Base types.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#ifndef BASE_H
#define BASE_H

#define IN
#define OUT
#define OPTIONAL
#define PRIVATE static
#define PUBLIC

#ifndef NULL
#ifdef __cplusplus
#define NULL    0
#else  /* __cplusplus */
#define NULL    ((void *)0)
#endif  /* __cplusplus */
#endif  /* NULL */

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long long uint64_t;
typedef signed char sint8_t;
typedef signed short sint16_t;
typedef signed int sint32_t;
typedef signed long long sint64_t;
typedef char int8_t;
typedef short int16_t;
typedef int int32_t;
typedef long long int64_t;
typedef int bool_t;
typedef bool_t BOOL;
#define TRUE 1
#define FALSE 0
#define INVALID_HANDLE ((unsigned int)0)
typedef unsigned int handle_t;
typedef unsigned int index_t;
typedef unsigned int offset_t;
typedef unsigned int size_t;
typedef void* addr_t;
typedef uint16_t wchar_t;
typedef uint32_t handle_t;

#define ON(v,n)    (((v)&(n))==(n))
#define ONANY(v,n) (((v)&(n))==(v))
#define OFF(A,B)   (!ON(A,B)) 
#define CLEAR(v,n) ((v) &= ~(n))
#define SET(v,n)   ((v) |= (n))
#define FLIP(v,n)  (ON((v),(n)) ? CLEAR((v),(n)) : SET((v),(n)))

/* allocator */

typedef void(*object_init_t)(IN void*);

typedef struct Cache {
	char*  name;
	size_t objSize;
	size_t totalSize;
	size_t numAllocs;
	void*  freeStack;
	object_init_t ctor;
}Cache;

typedef BOOL(*alloc_callback_t)(IN void* object, IN size_t size,
	IN OPTIONAL char* tag, IN OPTIONAL Cache* cache);
typedef BOOL(*alloc_cache_t)(IN Cache* cache, IN void* object);

extern Cache* NewCache(IN size_t size, IN OPTIONAL object_init_t);
extern Cache* NewCacheEx(IN char* name, IN size_t size, IN OPTIONAL object_init_t);
extern void*  AllocObject(IN Cache* cache);
extern void   FreeObject(IN void* p);
extern void*  Alloc(IN size_t size);
extern void*  AllocTag(IN char* tag, IN size_t size);
extern void*  Realloc(IN void* memory, IN size_t size);
extern void   Free(IN void* p);
extern void   FreeCacheObjects(IN Cache* cache);
extern void   FreeCache(IN Cache* cache);
extern void   VisitGlobalAlloc(IN alloc_callback_t callback);
extern void   VisitCache(IN Cache* cache, alloc_cache_t callback);
extern void   MmInit(void);

/* buffer */

typedef struct Buffer {
	size_t elementCount;
	size_t allocCount;
	uint8_t* data;
}Buffer;

PUBLIC Buffer*  InitBuffer(IN Buffer* r);
PUBLIC Buffer*  NewBuffer(void);
PUBLIC void     BufferFree(IN Buffer* r);
PUBLIC void*    BufferFreePreserve(IN Buffer* r);
PUBLIC uint8_t* GetBufferData(IN Buffer* b);
PUBLIC size_t   GetBufferLength(IN Buffer* b);
PUBLIC void     BufferWrite(IN Buffer* b, IN char c);
PUBLIC void     BufferAppend(IN Buffer* b, IN char* s, IN int len);
PUBLIC void     BufferPrintf(IN Buffer* b, IN char* fmt, ...);
PUBLIC void     BufferPush(IN Buffer* b, IN char c);
PUBLIC char     BufferPop(IN Buffer* b);

typedef struct Vector {
	uint8_t* data;
	size_t elementCount;
	size_t numAllocations;
	size_t elementSize;
	BOOL   newFlag; // TRUE if NewVector, FALSE if InitVector. Used by FreeVector.
}Vector;

extern Vector* InitVector(IN Vector* v, IN size_t elementSize);
extern Vector* NewVector(IN size_t elementSize);
extern void    ExtendVector(IN Vector* v, IN size_t numAllocations);
extern Vector* CopyVector(IN Vector* v);
extern void    VectorPush(IN Vector* v, IN void* e);
extern void    VectorPop(IN Vector* v, OUT void* data);
extern void    VectorGet(IN Vector* v, IN index_t index, OUT void* data);
extern void    VectorSet(IN Vector* v, IN index_t index, OUT void* data);
extern void    VectorFirstElement(IN Vector* v, OUT void* data);
extern BOOL    VectorLastElement(IN Vector* v, OUT void* data);
extern Vector* NewReverseVector(IN Vector* v);
extern void    VectorAppend(IN Vector* v, IN Vector* a);
extern void*   VectorData(IN Vector* v);
extern size_t  VectorLength(IN Vector* v);
extern void    FreeVector(IN Vector* v);
extern void*   VectorFirst(IN Vector* v);
extern void*   VectorNext(IN Vector* v, IN void* p);
extern BOOL    VectorContains(IN Vector* in, IN void* p);
extern BOOL    VectorPushUnique(IN Vector* in, IN void* p);
extern BOOL    VectorSetElement(IN Vector* in, IN void* p, IN void* data);

typedef struct Set {
	struct Set* next;
	char* s;
}Set;
typedef Set StringSet;

extern Set* SetInsert(IN Set* k, IN char* s);
extern BOOL SetFind(IN Set* k, IN char* s);
extern Set* SetUnion(IN Set* a, IN Set* b);
extern Set* SetIntersect(IN Set* a, IN Set* b);

typedef void* list_element_t;

typedef struct ListNode {
	struct ListNode* next;
	struct ListNode* prev;
	list_element_t data;
}ListNode;

typedef struct List {
	BOOL duplicates;
	ListNode* first;
	ListNode* last;
}List;

typedef void list_free_callback_t(ListNode* node);

extern List*     ListInitialize(IN List* root, IN BOOL duplicates);
extern ListNode* ListAdd(IN List* root, IN list_element_t data);
extern size_t    ListSize(IN List* root);
extern BOOL      ListRemove(IN List* root, IN ListNode* node);
extern ListNode* ListGetFirst(IN List* root, IN list_element_t data);
extern ListNode* ListGetNext(IN ListNode* node);
extern BOOL      ListRemove(IN List* root, IN ListNode* node);
extern void      ListFree(IN List* list, IN OPTIONAL list_free_callback_t callback);

typedef list_element_t bucket_element_t;
typedef void* bucket_key_t;

typedef struct BucketItem {
	bucket_key_t key;
	bucket_element_t value;
}BucketItem;

typedef struct BucketChain {
	List bucketItemList;
}BucketChain;

typedef BOOL compare_key_callback_t(bucket_key_t a, bucket_key_t b);
typedef BOOL compare_value_callback_t(list_element_t a, list_element_t b);
typedef unsigned long compute_hash_callback_t(bucket_key_t key);

typedef struct Map {
	long numberOfBuckets;
	long numberOfElements;
	BucketChain* buckets;
	BucketChain  nullBucket; //key=null
	struct Map* parent;
	List keys;
	compare_key_callback_t* compareKey;
	compare_value_callback_t* compareValue;
	compute_hash_callback_t* computeHash;
}Map;

/* default Map callbacks. */
extern unsigned long MapHashString(IN bucket_key_t key);
extern unsigned long MapHashPointer(IN const void *pointer);

extern BOOL MapComparePointer(const void *pointer1, const void *pointer2);
extern BOOL MapCompareString(const char *pointer1, const char *pointer2);

extern Map* NewMap(void);
extern Map* NewMapEx(IN Map* parent);
extern Map* NewMapNumeric(void);
extern list_element_t MapGet(IN Map* Map, IN bucket_key_t key);
extern BOOL MapContainsKey(IN Map* Map, IN bucket_key_t key);
extern BOOL MapPut(IN Map* Map, IN bucket_key_t key, IN bucket_element_t value);
extern void MapRemove(IN Map* Map, IN bucket_key_t key);
extern void MapFree(IN Map* Map);
extern BOOL MapEmpty(IN Map* Map);
extern long MapElementCount(IN Map* Map);
extern long MapBucketCount(IN Map* Map);
extern Map* MapGetParent(IN Map* map);
extern Map* MapSetParent(IN Map* map, IN Map* parent);
extern List* MapGetKeys(IN Map* Map);

// string pool

typedef struct String {
	int   refcount;
	char* value;
}String;

extern size_t  Align         (IN size_t addr, IN int align);
extern String* InternString  (IN char* string);
extern char*   intToStr      (IN int x);

#endif

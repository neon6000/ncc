/************************************************************************
*
*	map.c - Hash map
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <assert.h>
#include <string.h>
#include <malloc.h>
#include "base.h"

/* default hash string function. */
PUBLIC unsigned long MapHashString(IN bucket_key_t key) {

	const unsigned char *str = (const unsigned char *)key;
	unsigned long hash = 5381;
	int c;

	/* djb2 algorithm */
	while ((c = *str++) != '\0')
		hash = hash * 33 + c;

	return hash;
}

/* default hash number function. */
PUBLIC unsigned long MapHashNumber(IN bucket_key_t key) {

	const unsigned int i = (unsigned int)key;
	return i;
}

/* default hash pointer function. */
PUBLIC unsigned long MapHashPointer(const void *pointer) {

	return ((unsigned long)pointer) >> 4;
}

/* default key compare function. */
PUBLIC BOOL MapComparePointer(const void *pointer1, const void *pointer2) {

	return (pointer1 == pointer2);
}

/* default key compare function. */
PUBLIC BOOL MapCompareNumber(const unsigned int num1, const unsigned int num2) {

	return (num1 == num2);
}

/* default string compare function. */
PUBLIC BOOL MapCompareString(const char *pointer1, const char *pointer2) {

	if (strlen(pointer1) != strlen(pointer2))
		return FALSE;
	if (strcmp(pointer1, pointer2) != 0)
		return FALSE;
	return TRUE;
}

/* create map */
PRIVATE Map* _MapCreate(IN unsigned long numberOfBuckets, IN compare_key_callback_t* compareKey,
	IN compare_value_callback_t* compareValue, IN compute_hash_callback_t* computeHash) {

	Map* d;
	unsigned int i;

	assert(numberOfBuckets > 0);
	d = AllocTag("MAP", sizeof(Map));
	if (!d)
		return NULL;
	d->buckets = Realloc(NULL, numberOfBuckets * sizeof (BucketChain));
	if (!d->buckets) {
		Free(d);
		return NULL;
	}
	d->parent = NULL;
	d->numberOfBuckets = numberOfBuckets;
	d->numberOfElements = 0;
	d->compareKey = compareKey;
	d->compareValue = compareValue;
	d->computeHash = computeHash;
	ListInitialize(&d->keys,FALSE);
	for (i = 0; i < numberOfBuckets; i++)
		ListInitialize(&d->buckets[i].bucketItemList, FALSE);
	ListInitialize(&d->nullBucket.bucketItemList, TRUE);
	return d;
}

/* creates new Map. */
PUBLIC Map* NewMap(void) {

	return _MapCreate(100, MapCompareString, MapComparePointer, MapHashString);
}

/* creates new Map. */
PUBLIC Map* NewMapEx(IN Map* parent) {

	Map* map = _MapCreate(100, MapCompareString, MapComparePointer, MapHashString);
	map->parent = parent;
	return map;
}

/* creates new Map. */
PUBLIC Map* NewMapNumeric(void) {

	Map* map = _MapCreate(200, MapCompareNumber, MapComparePointer, MapHashNumber);
	return map;
}

/* returns parent. */
PUBLIC Map* MapGetParent(IN Map* map) {

	return map->parent;
}

/* sets new parent. */
PUBLIC Map* MapSetParent(IN Map* map, IN Map* parent) {

	Map* oldParent;

	oldParent = map->parent;
	map->parent = parent;
	return oldParent;
}

/* release resources. */
PUBLIC void MapFree(IN Map* Map) {

	int i;
	BucketChain b;

	assert(Map != NULL);

	for (i = 0; i < Map->numberOfBuckets; i++) {
		b = Map->buckets[i];
		ListFree(&b.bucketItemList, NULL);
	}
	ListFree(&Map->keys, NULL);
	Free(Map->buckets);
	Free(Map);
}

/* given a key, return the value associated with the key. */
PUBLIC list_element_t MapGet(IN Map* Map, IN bucket_key_t key) {

	long hash;
	List* bucket;
	ListNode* currentBucketItem;

	if (!Map)
		return NULL;

	hash = Map->computeHash(key) % Map->numberOfBuckets;
	bucket = &Map->buckets[hash].bucketItemList;

	/* look into the bucket and find the item with the same key. */
	for (currentBucketItem = bucket->first; currentBucketItem; currentBucketItem = currentBucketItem->next) {

		BucketItem* b = (BucketItem*)currentBucketItem->data;
		if (!b) continue;
		if (Map->compareKey(key, b->key))
			return b->value;
	}

	return NULL;
}

/* test if key is in Map. */
PUBLIC BOOL MapContainsKey(IN Map* Map, IN bucket_key_t key) {

	return MapGet(Map, key) != NULL;
}

/* put an item in the Map. */
PUBLIC BOOL MapPut(IN Map* Map, IN bucket_key_t key, IN bucket_element_t value) {

	long hash;
	List* bucketItemList;
	BucketItem* b;
	ListNode* currentBucketItem;

	assert(value != NULL);

	// NULL keys are stored in a separate bucket and may contain
	// duplicates. Used primarily for temporary symbols:
	if (!key) {

		b = AllocTag("MAPITEM", sizeof(BucketItem));
		if (!b)
			return FALSE;
		b->key = key;
		b->value = value;

		ListAdd(&Map->nullBucket.bucketItemList, b);
		Map->numberOfElements++;
		return TRUE;
	}

	hash = Map->computeHash(key) % Map->numberOfBuckets;
	bucketItemList = &Map->buckets[hash].bucketItemList;

	/* if its already in the bucket, we need to update it. */
	for (currentBucketItem = bucketItemList->first; currentBucketItem; currentBucketItem = currentBucketItem->next) {

		BucketItem* b = (BucketItem*)currentBucketItem->data;
		if (!b) continue;
		if (Map->compareKey(key, b->key)) {

			/* update it. */
			b->value = value;
			b->key = key;
			return TRUE;
		}
	}

	/* if its not in this bucket, add it. */
	b = AllocTag("MAPTEM", sizeof(BucketItem));
	if (!b)
		return FALSE;
	b->key = key;
	b->value = value;

	/* add item to bucket. */
	ListAdd(bucketItemList, b);
	Map->numberOfElements++;

	/* add key to list. */
	ListAdd(&Map->keys, key);

	return TRUE;
}

/* given a key, remove item from Map associated with the key. */
PUBLIC void MapRemove(IN Map* Map, IN bucket_key_t key) {

	long hash;
	List* bucket;
	ListNode* currentBucketItem;

	assert(key != NULL);

	hash = Map->computeHash(key) % Map->numberOfBuckets;
	bucket = &Map->buckets[hash].bucketItemList;

	/* look into bucket and locate item with key. */
	for (currentBucketItem = bucket->first; currentBucketItem; currentBucketItem = currentBucketItem->next) {

		BucketItem* b = (BucketItem*)currentBucketItem->data;
		if (!b) continue;
		if (Map->compareKey(key, b->key)) {

			/* remove item from bucket. */
			ListRemove(bucket, currentBucketItem);
			Free(b);
			Map->numberOfElements--;

			/* if values still associated with this key, thats a bug */
			if (MapGet(Map, key))
				__debugbreak();

			/* remove key and return */
			ListRemove(&Map->keys, key);
			return;
		}
	}
}

/* test if Map is empty. */
PUBLIC BOOL MapEmpty(IN Map* Map) {

	return Map->numberOfElements == 0;
}

/* return number of elements in Map. */
PUBLIC long MapElementCount(IN Map* Map) {

	return Map->numberOfElements;
}

/* return number of buckets in Map. */
PUBLIC long MapBucketCount(IN Map* Map) {

	return Map->numberOfBuckets;
}

/* returns list of keys. */
PUBLIC List* MapGetKeys(IN Map* Map) {

	return &Map->keys;
}

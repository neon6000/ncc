/************************************************************************
*
*	strpool.c - String pool
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This component implements a pool for literals and identifiers.
The intent is to lower the memory footprint and speed up string
comparisions during the compilation process.
*/

#include "base.h"
#include <malloc.h>
#include <stdio.h>

char* intToStr(IN int x) {

	char* buffer = malloc( sizeof (char) * sizeof (int) * 4 + 1);
	if (buffer)
		sprintf(buffer, "%d", x);
	return buffer;
}

size_t Align(IN size_t addr, IN int align) {

	return (addr + (align - 1)) & -align;
}

PRIVATE String* NewString(IN char* value) {

	String* s = malloc(sizeof(String));
	s->refcount = 0;
	s->value = value;
	return s;
}

// https://github.com/tvcutsem/tinyc/blob/ec4ebe6f97595c412e49c2279b826089da0bfaa2/syntaxtree/number.h

PRIVATE Map* _internTable;

PUBLIC String* InternString(IN char* string) {

	String* s;
	
	if (!_internTable)
		_internTable = NewMap();

	s = MapGet(_internTable, string);
	if (!s) {
		s = NewString(string);
		MapPut(_internTable, string, s);
	}
	s->refcount++;
	return s;
}

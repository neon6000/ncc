/************************************************************************
*
*	icode.h - Intermediate Code
*
*   Copyright(c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

// https://cs.nyu.edu/courses/fall10/G22.2130-001/lecture9.pdf
// https://cs.nyu.edu/courses/fall10/G22.2130-001/lecture10.pdf

#include "base.h"
#include "ncc.h"

#ifndef IL_H
#define IL_H

typedef enum DataType {
	// invalid ---------
	DT_INVALID  = 0x00000000,
	// integer type ----
	DT_INTEGER  = 0x00000100,
	DT_BOOL     = 0x00000101,
	DT_CHAR     = 0x00000102,
	DT_SHORT    = 0x00000103,
	DT_INT      = 0x00000104,
	DT_LONG     = 0x00000105,
	DT_LLONG    = 0x00000106,
	// floating point ---
	DT_FLT      = 0x00001000,
	DT_FLOAT    = 0x00001001,
	DT_DOUBLE   = 0x00001002,
	DT_LDOUBLE  = 0x00001003,
	// void ---
	DT_VOID     = 0x00010000,
	// aggregate ---
	DT_PTR      = 0x00100001,
	DT_ARRAY    = 0x00100002,
	DT_STRUCT   = 0x00100003,
	DT_UNION    = 0x00100004,
	DT_ENUM     = 0x00100005,
	DT_FUNCTION = 0x00100006,
	DT_BLOCK    = 0x00100007
}DataType;

typedef enum StorageClass {
	S_NONE,
	S_AUTO     = 1,
	S_REGISTER = 2,
	S_STATIC   = 4,
	S_EXTERN   = 8,
	S_TYPEDEF  = 16
}StorageClass;

typedef enum TypeQualifier {
	Q_NONE,
	Q_CONST    = 1,
	Q_VOLATILE = 2
}TypeQualifier;

typedef struct Type {
	DataType      dt;
	int           size;
	int           align;
	BOOL          isUnsigned;
	StorageClass  sc;
	TypeQualifier tq;
	int           bitfield;
	struct {
		struct Type* type;
	}pointer;
	struct {
		struct Type* type;
		int len;
	}array;
	struct {
		char* tag;
		Map*  fields;
		int   offset;
	}struc;
	struct {
		struct Type* rettype;
		Vector* params;
		BOOL    hasva;
	}function;
	struct Type*  parent;
}Type;

extern Type* VoidType        (void);
extern Type* FloatType       (void);
extern Type* DoubleType      (void);
extern Type* LongDoubleType  (void);
extern Type* IntType         (void);
extern Type* BoolType        (void);
extern Type* PointerType     (IN Type* to);
extern Type* ArrayType       (IN Type* type, IN int count);
extern Type* BlockType       (void);
extern void  PrintType       (IN Type* type, IN FILE* stream);

#define IsVoidType(type)          (ON(type->dt, DT_VOID))
#define IsIntType(type)           (ON(type->dt, DT_INTEGER))
#define IsFltType(type)           (ON(type->dt, DT_FLOAT))
#define IsIntOrFltType(type)      (ON(type->dt, DT_FLOAT)||ON(type->dt,DT_INTEGER))
#define IsPtrType(type)           (type->dt==DT_PTR)
#define IsStructOrUnionType(type) (type->dt==DT_STRUCT||type->dt==DT_UNION)
#define IsArrayType(type)         (type->dt==DT_ARRAY)
#define IsFunctionType(type)      (type->dt==DT_FUNCTION)
#define IsConstType(type)         (ON(type->tq, Q_CONST))
#define IsVolatileType(type)      (ON(type->tq, Q_VOLATILE))
#define IsBlockType(type)         (type->dt==DT_BLOCK)
#define IsCompositeType(type)     (type->dt == DT_ARRAY || type->dt == DT_FUNCTION || type->dt == DT_PTR)
#define IsCompleteType(type)      (type->size > 0)

// comparison codes for ICMP
#define ICMP_EQ   1
#define ICMP_NE   2
#define ICMP_UGT  3
#define ICMP_UGE  4
#define ICMP_ULT  5
#define ICMP_ULE  6
#define ICMP_SGT  7
#define ICMP_SGE  8
#define ICMP_SLT  9
#define ICMP_SLE  10

// comparison codes for FCMP
#define FCMP_FALSE   1
#define FCMP_OEQ     2
#define FCMP_OGT     3
#define FCMP_OGE     4
#define FCMP_OLT     5
#define FCMP_OLE     6
#define FCMP_ONE     7
#define FCMP_ORD     8
#define FCMP_UEQ     9
#define FCMP_UGT    10
#define FCMP_UGE    11
#define FCMP_ULT    12
#define FCMP_ULE    13
#define FCMP_UNE    14
#define FCMP_UNO    15
#define FCMP_TRUE   16

// calling conventions
#define CCONV_CDECL   1
#define CCONV_STDCALL 2

// dll storage class
#define DLL_IMPORT    1
#define DLL_EXPORT    2

// linkage types
#define LINKAGE_PRIVATE       1
#define LINKAGE_INTERNAL      2
#define LINKAGE_COMMON        3
#define LINKAGE_EXTERNAL      4

// thread local storage
#define TLS_GENERALDYNAMIC    1
#define TLS_LOCALDYNAMIC      2
#define TLS_INITIALEXEC       3
#define TLS_LOCALEXEC         4

// comdat's:
#define COMDAT_ANY            1
#define COMDAT_EXACTMATCH     2
#define COMDAT_LARGEST        3
#define COMDAT_NODUPLICATES   4
#define COMDAT_SAMESIZE       5

typedef enum Mnemonic {
	// control flow ---
	I_RET,
	I_JMP,
	I_IF,
	I_SWITCH,
	I_CALL,
	// performance ---
	I_UNREACHABLE,
	// memory access ---
	I_ALLOCA,
	I_LOAD,
	I_STORE,
	I_VA_ARG,
	// arithmitic ---
	I_FNEG,
	I_ADD,
	I_FADD,
	I_SUB,
	I_FSUB,
	I_MUL,
	I_FMUL,
	I_UDIV,
	I_UREM,
	I_SDIV,
	I_SREM,
	I_FDIV,
	I_FREM,
	I_SHL,
	I_LSHR, // logical shift right
	I_ASHR, // arithmitic shift right
	I_AND,
	I_OR,
	I_XOR,
	// conversions ---
	I_TRUNC,
	I_ZEXT,
	I_SEXT,
	I_FPTRUNC,
	I_FPEXT,
	I_FPTOUI,
	I_FPTOSI,
	I_UITOFP,
	I_SITOFP,
	I_PTRTOINT,
	I_INTTOPTR,
	// comparisions ---
	I_ICMP,
	I_FCMP
}Mnemonic;

typedef struct Context {
	uint32_t curGlobalName;
	uint32_t curLocalName;
}Context;

// Value kinds:
#define VALUE_COMMON 1
#define VALUE_GLOBAL 2
#define VALUE_BLOCK  3

// Named values represent user-defined variables.
// Unnamed values represent compiler-generated intermediates.
// Unnamed constants are stored as a literal or an aggregate type.
typedef struct Value {
	Type*    type;
	int      kind;
	int      align;
	String*  name;
	uint32_t virt;
	String*  literal;
	Vector   elements;
}Value;

#define IsValueNamed(value)     (value->name==NULL)
#define IsValueAggregate(value) (value->elements.elementCount > 0)

extern  Value* NamedValueBasic     (IN Type* type, IN String* literal);
extern  Value* NamedValueAggregate (IN Type* type, IN Vector elements);
extern  Value* NamedValue          (IN Type* type, IN String* name);
extern  Value* UnnamedValue        (IN Context* ctx, IN Type* type);
extern  Value* TypeValue           (IN Type* type);
extern  void   PushValue           (IN Value* value, IN Value* toPush);
extern  Value* PopValue            (IN Value* value);

typedef struct Instr Instr;
typedef struct Block Block;

typedef struct Code {
	Instr* first;
	Instr* last;
}Code;

// Basic blocks are BLOCK-type values representing
// a single linear path of execution.
typedef struct Block {
	Value    val;
	Code     code;
	uint32_t name;   // <--
	char*    label;  // <--

	// liveness -------
	Vector   defregs;
	Vector   inregs;
	Vector   outregs;
	Vector   predicate;
	Vector   successor;
	struct Block* next;
}Block;

typedef struct Instr {
	Location     loc;
	Mnemonic     mnemonic;
	int          cmpOp;
	int          cconv;
	Value*       target;
	Value*       ops[3];
	Vector*      parms;   // call
	String*      imm;     // emitJmpImm
	struct Instr* next;
}Instr;

extern Block*      NewBlock(void);
extern Code*       Addi(IN Code* code, IN Instr* ins);
extern Instr*      NewIns(IN Mnemonic m);

extern Instr*  InsRet();
extern Instr*  InsJmp();
extern Instr*  InsIf();
extern Instr*  InsSwitch         (IN Value* cond, IN Block* defCase);
extern Instr*  InsSwitchCase     (IN Instr* insSwitch, IN Value* cond, IN Block* jmp);
extern Instr*  InsCall           (IN Value* target, IN Value* calling, IN Vector* parms);
extern Instr*  InsUnreachable();
extern Instr*  InsAlloca();
extern Instr*  InsLoad();
extern Instr*  InsStore();
extern Instr*  InsVaArg();
extern Instr*  InsFNeg();
extern Instr*  InsAdd();
extern Instr*  InsFAdd();
extern Instr*  InsSub();
extern Instr*  InsFSub();
extern Instr*  InsMul();
extern Instr*  InsFMul();
extern Instr*  InsUDiv();
extern Instr*  InsURem();
extern Instr*  InsSDiv();
extern Instr*  InsSRem();
extern Instr*  InsUDiv();
extern Instr*  InsFDiv();
extern Instr*  InsFRem();
extern Instr*  InsShl();
extern Instr*  InsLShr();
extern Instr*  InsAShr();
extern Instr*  InsAnd();
extern Instr*  InsOr();
extern Instr*  InsXor();
extern Instr*  InsTrunc();
extern Instr*  InsZExt();
extern Instr*  InsSExt();
extern Instr*  InsFPTrunc();
extern Instr*  InsFPEct();
extern Instr*  InsFPToUI();
extern Instr*  InsFPToSI();
extern Instr*  InsUIToFP();
extern Instr*  InsSIToFP();
extern Instr*  InsPTRToINT();
extern Instr*  InsINTToPTR();
extern Instr*  InsICMP();
extern Instr*  InsFCMP();

// User-defined globals are named values.
// Unnamed variables are compiler generated (like C-strings.)
// These are always pointer-to-memory types.
typedef struct GlobalVariable {
	Value     val;
	String*   section;
	int       dllStorageClass;
	int       linkage;
}GlobalVariable;

extern Value*  NamedGlobal               (IN Type* type, IN String* name);
extern Value*  UnnamedGlobal             (IN Context* ctx, IN Type* type);
extern Value*  NamedGlobalBasic          (IN Type* type, IN String* name, IN String* literal);
extern Value*  UnnamedGlobalBasic        (IN Context* ctx, IN Type* type, IN String* literal);
extern Value*  NamedGlobalAggregate      (IN Type* type, IN String* name, IN Vector elements);
extern Value*  UnnamedGlobalAggregate    (IN Context* ctx, IN Type* type, IN Vector elements);
extern Value*  SetGlobalSection          (IN GlobalVariable* var, IN String* section);
extern Value*  SetGlobalLinkage          (IN GlobalVariable* var, IN int linkage);
extern Value*  SetGlobalStorageClassDLL  (IN GlobalVariable* var, IN int dllStorageClass);

// Functions are always Named values since they can
// be referenced in code as Pointer-to-function type.
typedef struct Function {
	Value   val;
	int     linkage;
	char*   name;    // <--
	Vector* lvars;
	Vector  blocks;
	Block*  entry;
	File*   source;
}Function;

extern Function*       ILNewFunction (IN String* name, IN Vector* lvars);


#endif

// http://www.cs.cmu.edu/~fp/courses/15411-f08/lectures/09-ssa.pdf
// http://pages.cs.wisc.edu/~fischer/cs701.f08/lectures/Lecture23.4up.pdf
// https://godoc.org/golang.org/x/tools/go/ssa

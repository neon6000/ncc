/************************************************************************
*
*	live.c - Liveness Analysis
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "base.h"
#include "ncc.h"
#include "icode.h"

#if 0
void VisitInternal(IN Block* block, IN Vector* visit, void(*visit_t)(IN Block*)) {

	Instr* ins;

	if (VectorContains(visit, block))
		return;
	VectorPush(visit, block);

	visit_t(block);
	ins = block->code.last;
	if (ins->out[0])
		VisitInternal(ins->out[0], visit, visit_t);
	if (ins->out[1])
		VisitInternal(ins->out[1], visit, visit_t);
}

void Visit(IN Block* block, IN void(*visit_t)(IN Block*)) {

	Vector visit;
	InitVector(&visit, sizeof(Block*));
	VisitInternal(block, &visit, visit_t);
	FreeVector(&visit);
}

void SetSuccessors(IN Block* block) {

	Instr* ins;

	if (VectorLength(&block->successor))
		return;
	ins = block->code.last;
	if (ins->out[0]) {
		VectorPush(&ins->out[0]->predicate, block);
		VectorPush(&block->successor, ins->out[0]);
		SetSuccessors(ins->out[0]);
	}
	if (ins->out[1]) {
		VectorPush(&ins->out[1]->predicate, block);
		VectorPush(&block->successor, ins->out[1]);
		SetSuccessors(ins->out[1]);
	}
}

void SetDefinedRegisters(IN Block* block) {

	Instr* ins;
	for (ins = block->code.first; ins; ins = ins->next) {
		if (ins->mnemonic == I_JMP)
			continue;
		VectorPushUnique(&block->defregs, ins->target);
	}
}

void PropagateRegister(IN Block* block, IN Value* r) {

	Block* pred;

	if (VectorContains(&block->defregs, r))
		return;

	if (!VectorPushUnique(&block->inregs, r))
		return;

	for (pred = VectorFirst(&block->predicate);
		pred;
		pred = VectorNext(&block->predicate, pred)) {

		if (VectorPushUnique(&pred->outregs, r))
			PropagateRegister(pred, r);
	}
}

void PropagateRegisters(IN Block* block) {

	Instr* ins;
	for (ins = block->code.first; ins; ins = ins->next) {
		if (ins->ops[0]) PropagateRegister (block, ins->ops[0]);
		if (ins->ops[1]) PropagateRegister (block, ins->ops[1]);
	}
}

Function* Liveness(IN Function* fc) {

	SetSuccessors(fc->entry);
	Visit(fc->entry, SetDefinedRegisters);
	Visit(fc->entry, PropagateRegisters);
	return fc;
}
#endif

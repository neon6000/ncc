/************************************************************************
*
*	pproc.c - Preprocessor.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* this is a work in progress ---------------------- */
// http://dev-docs.atariforge.org/files/Megamax_Laser_C.pdf

/*
	This component implements the preprocessor. It reads tokens from the scanner
	and either processes them (in case of preprocessor directives), expands them
	(in case of macro names) or returns them unmodified to the parser.
*/

// 6.4.7 header names

#include <assert.h>
#include <malloc.h>
#include <string.h>
#include "base.h"
#include "ncc.h"
#include "icode.h"
#include "c.h"

PRIVATE Token _current;
PRIVATE Scanner* _scan;
PRIVATE FileManager* _fm;
PRIVATE SymbolTable* _symbols;

extern AstNode* constant_expression(void);

typedef enum MacroType {
	MACRO_OBJECT,
	MACRO_FUNCTION
}MacroType;

/* macro. */
typedef struct Macro {
	MacroType type;
	Vector* body;
	int operandCount;
	BOOL isVariableArgument;
}Macro;

typedef Map MacroTable;
MacroTable* _macros;

PRIVATE Macro* NewObjectLikeMacro(IN Vector* tokens) {

	Macro* m = Alloc(sizeof(Macro));
	m->type = MACRO_OBJECT;
	m->body = tokens;
	m->operandCount = 0;
	m->isVariableArgument = FALSE;
	return m;
}

PRIVATE Macro* NewFunctionLikeMacro(IN Vector* tokens, IN int opcount, IN BOOL varargs) {

	Macro* m = Alloc(sizeof(Macro));
	m->type = MACRO_FUNCTION;
	m->body = tokens;
	m->operandCount = opcount;
	m->isVariableArgument = varargs;
	return m;
}

PRIVATE Token* NewMacroArgToken (IN int position, IN BOOL vararg) {

	Token* tok = Alloc(sizeof(Token));
	memset(tok, 0, sizeof(Token));
	tok->type = TMACROARG;
	tok->location = position;
	tok->vararg = vararg;
	return tok;
}

PUBLIC BOOL MacroPut(IN char* name, IN Macro* macro) {

	if (!_macros) {
		_macros = NewMap();
		if (!_macros)
			return FALSE;
	}

	if (MapPut(_macros, name, macro))
		return TRUE;
	return FALSE;
}

PUBLIC Macro* MacroGet(IN char* name) {

	return (Macro*)MapGet(_macros, name);
}

PRIVATE AstNode* ProcessConstExpression(IN Vector* tokens) {

	Vector* _stream = NewReverseVector(tokens);
	AstNode* r;
	Token c;

	c = _current;
	_scan->pushTokStream(&_stream);

	_current = _scan->get();
	r = constant_expression();

	_scan->popTokStream();
	_current = c;

	return r;
}

PRIVATE Vector* ReadConstExprLine (void) {

	Vector* v = NewVector(sizeof(Token));
	Token tok;

	while (TRUE) {
		tok = _scan->get();
		if (tok.type == TEOL || tok.type == TEOF)
			return v;
		// if (is_ident(tok, "defined")) {
		//	vec_push(r, read_defined_op());
		else if (tok.type == TIDENTIFIER)
			; // c11 6.10.1.4
		else
			VectorPush(v, &tok);
	}
}

PRIVATE BOOL ProcessIf(void) {

	AstNode* expr;
	_scan->get();

	expr = ProcessConstExpression(ReadConstExprLine());
	
	if (!expr) {
		printf("\n\r*** ProcessIf: expression expected");
		return FALSE;
	}

	printf("\n\r--#if--");
	return TRUE;
}

PRIVATE BOOL ProcessElif(void) {

	AstNode* expr;
	_scan->get();

	expr = ProcessConstExpression(ReadConstExprLine());

	if (!expr) {
		printf("\n\r*** ProcessElif: expression expected");
		return FALSE;
	}

	printf("\n\r--#elif--");
	return TRUE;
}

PRIVATE BOOL ProcessIfdef(void) {

	Token tok;
	_scan->get();
	tok = _scan->get();
	if (tok.type != TIDENTIFIER) {
		printf("\n\r*** ProcessIfdef: identifier expected");
		return FALSE;
	}

	printf("\n\r--#ifdef %s--", tok.val.s);
	return TRUE;
}

PRIVATE BOOL ProcessIfndef(void) {

	Token tok;
	_scan->get();
	tok = _scan->get();
	if (tok.type != TIDENTIFIER) {
		printf("\n\r*** ProcessIfndef: identifier expected");
		return FALSE;
	}

	printf("\n\r--#ifndef %s--", tok.val.s);
	return TRUE;
}

PRIVATE BOOL ProcessElse(void) {

	_scan->get();
	printf("\n\r--#else--");
	return TRUE;
}

PRIVATE BOOL ProcessEndif(void) {

	_scan->get();
	printf("\n\r--#endif--");
	return TRUE;
}

PRIVATE BOOL ProcessInclude(void) {

	Token tok;
	File* f;

	_scan->get();	
	tok = _scan->ppheader();

	if (tok.type != THEADER) {
		Error(PPROC_HEADERNAME);
		return FALSE;
	}

	f = _fm->open(tok.val.s);
	if (!f) {
		Fatal(FILENOTFOUND, tok.val.s);
		return TRUE;
	}

	_fm->push(f);
	return TRUE;
}

PRIVATE void ProcessFunctionLikeMacroOperands(OUT Map* args) {

	int location = 0;
	_scan->get(); // consume '('
	Token tok = _scan->get();
	while (TRUE) {
		if (tok.type == TSPACE) {
			tok = _scan->get();
			continue;
		}
		else if (tok.type == TEOL || tok.type == TEOF) {
			printf("\n\r*** ProcessFunctionLikeMacroOperands: premature end of line");
			break;
		}
		else if (tok.type == OP_RPAREN && location == 0) {
			break;
		}
		else if (tok.type != TIDENTIFIER) {
			printf("\n\r*** ProcessFunctionLikeMacroOperands: identifier expected");
			break;
		}

		MapPut(args, tok.val.s, NewMacroArgToken(location++, FALSE));

		tok = _scan->get();
		if (tok.type == TSPACE)
			tok = _scan->get();
		if (tok.type == OP_RPAREN)
			break;
		else if (tok.type != OP_COMMA) {
			printf("\n\r*** ProcessFunctionLikeMacroOperands: expected ','");
			break;
		}
		tok = _scan->get();
	}
}

PRIVATE Vector* ProcessFunctionLikeMacroBody(IN Map* args) {

	Vector* r = NewVector(sizeof(Token));
	Token tok;

	while (TRUE) {
		tok = _scan->get();
		if (tok.type == TEOL || tok.type == TEOF)
			return r;
		else if (tok.type == TIDENTIFIER) {
			Token* arg = (Token*) MapGet(args, tok.val.s);
			if (arg) {
				VectorPush(r, arg);
				continue;
			}
		}
		VectorPush(r, &tok);
	}
}

PRIVATE BOOL ProcessFunctionLikeMacro(IN char* name) {

	Map* args;
	Vector* body;
	Macro* m;

	args = NewMap();
	if (!args)
		return FALSE;

	ProcessFunctionLikeMacroOperands(args);
	body = ProcessFunctionLikeMacroBody(args);
	m = NewFunctionLikeMacro(body, args->numberOfElements, FALSE);
	MapFree(args);
	MacroPut(name, m);
	return TRUE;
}

PRIVATE BOOL ProcessObjectLikeMacro(IN char* name) {

	Vector* body;
	Macro* m;
	Token tok;

	body = NewVector(sizeof(Token));
	if (_scan->peek(1).type == TSPACE)
		_scan->get();
	while (TRUE) {
		tok = _scan->get();
		if (tok.type == TEOL || tok.type == TEOF)
			break;
		VectorPush(body, &tok);
	}
	m = NewObjectLikeMacro(body);
	MacroPut(name, m);
	return TRUE;
}

PRIVATE BOOL ProcessDefine(void) {

	char* name;
	_scan->get();
	if (_scan->peek(1).type != TIDENTIFIER) {
		printf("\n\r** ProcessDefine: identifier expected");
		return FALSE;
	}
	name = _scan->get().val.s;
	if (_scan->peek(1).type == OP_LPAREN)
		return ProcessFunctionLikeMacro(name);
	else
		return ProcessObjectLikeMacro(name);
}

PRIVATE BOOL ProcessUndef(void) {

	Token tok;
	_scan->get();
	tok = _scan->get();
	if (tok.type != TIDENTIFIER) {
		printf("\n\r*** ProcessUndef: identifier expected");
		return FALSE;
	}

	printf("\n\r--#undef %s--", tok.val.s);
	return TRUE;
}

PRIVATE BOOL ProcessLine(void) {

	Token lineno;
	Token fname;
	_scan->get();
	lineno = _scan->get();
	if (_scan->peek(1).type == TSPACE)
		_scan->get();
	fname = _scan->peek(1);
	switch (fname.type) {
	case TEOF:
	case TEOL:
		printf("\n\r--#line %s--", lineno.val.s);
		return TRUE;
	case TSTRING:
		_scan->get();
		printf("\n\r--#line %s source: %s--", lineno.val.s, fname.val.s);
		return TRUE;
	default:
		printf("\n\r*** ProcessLine: expected string");
		return FALSE;
	};
}

PRIVATE BOOL ProcessError(void) {

	return FALSE;
}

PRIVATE BOOL ProcessPragma(void) {

	return FALSE;
}

PRIVATE void ProcessDirective (void) {

	BOOL isSuccess = FALSE;
	Token tok = _scan->get();

	if (tok.type == TSPACE)
		tok = _scan->get();

	if (tok.type == TEOL || tok.type == TEOF)
		return;
	if (tok.type != TIDENTIFIER)
		goto err;
	if (!strcmp(tok.val.s, "if")) isSuccess = ProcessIf();
	else if (!strcmp(tok.val.s, "ifdef")) isSuccess = ProcessIfdef();
	else if (!strcmp(tok.val.s, "ifndef")) isSuccess = ProcessIfndef();
	else if (!strcmp(tok.val.s, "elif"))isSuccess = ProcessElif();
	else if (!strcmp(tok.val.s, "else")) isSuccess = ProcessElse();
	else if (!strcmp(tok.val.s, "endif")) isSuccess = ProcessEndif();
	else if (!strcmp(tok.val.s, "include")) isSuccess = ProcessInclude();
	else if (!strcmp(tok.val.s, "define")) isSuccess = ProcessDefine();
	else if (!strcmp(tok.val.s, "undef")) isSuccess = ProcessUndef();
	else if (!strcmp(tok.val.s, "line")) isSuccess = ProcessLine();
	else if (!strcmp(tok.val.s, "error")) isSuccess = ProcessError();
	else if (!strcmp(tok.val.s, "pragma")) isSuccess = ProcessPragma();
	else goto err;

	if (! isSuccess)
		goto err;
	else
		return;

err:
	printf("\n\r*** ProcessDirective: invalid preprocessor directive");

	// skip over rest of line and ignore it:
	while (tok.type != TEOL)
		tok = _scan->get();
}

PRIVATE Token RegetToken(void) {

	return _current;
}

PRIVATE void UngetToken(IN Token tok) {

	_scan->unget(tok);
	_current = tok;
}

PRIVATE void UngetTokens(IN Vector* tokens) {

	unsigned int i;
	Token tok;
	for (i = 0; i < VectorLength(tokens); i++) {
		VectorGet(tokens, i, &tok);
		UngetToken(tok);
	}
}

PRIVATE Vector* AddHideSet(IN Vector* tokens, IN Set* hideset) {

	unsigned int i;
	Vector* r;
	Token tok;

	r = NewVector(sizeof(Token));
	for (i = 0; i < VectorLength(tokens); i++) {
		VectorGet(tokens, i, &tok);
		tok.hideset = SetUnion(tok.hideset, hideset);
		VectorPush(r, &tok);
	}
	return r;
}

Token ReadExpand();

PRIVATE Vector* Expand(IN Vector* tokens, IN Token tok) {

	Vector* r;
	Token ntok;
	Vector* t = NewReverseVector(tokens);

	_scan->pushTokStream(&t);
	r = NewVector(sizeof(Token));
	while (TRUE) {
		ntok = ReadExpand();
		if (ntok.type == TEOF)
			break;
		VectorPush(r, &ntok);
	}
	_scan->popTokStream();
	return r;
}

PRIVATE Vector* subst(IN Macro* macro, IN Vector* args, IN Set* hideset) {

	unsigned int i;
	Vector* r;
	Token tok;

	r = NewVector(sizeof(Token));
	for (i = 0; i < VectorLength(macro->body); i++) {

		VectorGet(macro->body, i, &tok);
		if (tok.type == TMACROARG) {
			TokenVector* arg = NULL;
			VectorGet(args, tok.location, &arg);
			VectorAppend(r, Expand(arg, tok));
			continue;
		}
		VectorPush(r, &tok);
	}

	return AddHideSet(r,hideset);
}

PRIVATE TokenVector* ReadArgument(IN Macro* macro, OUT BOOL* isLast, IN TokenType delim, IN BOOL acceptNewLine) {

	TokenVector* tokens;
	Token tok;

	*isLast = FALSE;
	tokens = NewVector(sizeof(Token));

	while (TRUE) {

		tok = _scan->get();
		if (tok.type == TEOF || tok.type == TEOL) {
			printf("\n\r*** ReadArgument: Unexpected EOL reached while reading macro operands");
			__debugbreak();
			return tokens;
		}
		if (tok.type == delim) {
			_scan->unget(tok);
			*isLast = TRUE;
			return tokens;
		}
		if (tok.type == OP_COMMA) {
			return tokens;
		}
		VectorPush(tokens, &tok);
	}
}

PRIVATE Vector* ReadArgumentList(IN Macro* macro, IN TokenType delim, IN BOOL acceptNewLine) {

	TokenVector* operand;
	Vector* operands;
	BOOL last;

	last = FALSE;
	operands = NewVector(sizeof(TokenVector*));

	// if empty parameter list, return empty vector:
	if (macro->operandCount == 0 && _scan->peek(1).type == delim)
		return operands;

	while (!last) {
		operand = ReadArgument(macro, &last, delim, acceptNewLine);
		VectorPush(operands, &operand);
	}

	return operands;
}

PRIVATE Token ReadExpand() {

	Vector* tokens;
	Keyword* kw;
	Set* hideset;
	Macro* macro;
	Token rparen;
	Vector* args;
	char* name;
	Token tok;

	tok = _scan->get();
	if (tok.type != TIDENTIFIER || tok.type == TYPENAME)
		return tok;
	name = tok.val.s;

	kw = _scan->keyword(name);

	if (kw) {
		tok.type = kw->t;
//		if (tok.type == K_WHILE)
//			__debugbreak();
		tok.val.s = NULL;
		return tok;
	}

	macro = MacroGet(name);
	if (!macro)
		return tok;
	else if (SetFind(tok.hideset, name))
		return tok;

	switch (macro->type) {

	case MACRO_OBJECT:

		hideset = SetInsert(tok.hideset, name);
		tokens = subst(macro, NULL, hideset);
		UngetTokens(NewReverseVector(tokens));
		FreeVector(tokens);
		return ReadExpand();

	case MACRO_FUNCTION:

		if (_scan->peek(1).type != OP_LPAREN)
			return tok; // is this right?
		_scan->get(); // consume '('
		args = ReadArgumentList(macro, OP_RPAREN, FALSE);
		rparen = _scan->peek(1);
		if (rparen.type != OP_RPAREN) {
			printf("\n\r*** ReadExpand: missing ')'");
			__debugbreak();
		}
		_scan->get(); // consume ')'
		hideset = SetInsert(SetIntersect(tok.hideset,rparen.hideset), name);
		tokens = subst(macro, args, hideset);
		UngetTokens(NewReverseVector(tokens));
		FreeVector(tokens);
		return ReadExpand();
	};
	__debugbreak();
	return tok;
}

PRIVATE Token PeekToken(IN int index);
PRIVATE Token GetToken(void) {

	Token tok;
	Token lookahead;

	if (!_scan) {
		_scan = GetScanner();
		_fm = GetFileManager();
		_symbols = GetSymbolTable();
	}

	while (TRUE) {

		tok = _scan->peek(1);

		if (tok.type == OP_HASH) {
			_scan->get();
			ProcessDirective();
			continue;
		}
		else if (tok.type == TIDENTIFIER) {
			tok = ReadExpand();
			break;
		}
		else if (tok.type == TEOL || tok.type == TSPACE) {
			_scan->get();
			continue;
		}
		else {
			_scan->get();
			break;
		}
	}

	// 6.4.5 para 7: This pair of adjacent character string literals ... produces
	// a single character string literal:
	lookahead = tok;
	while (lookahead.type == TSTRING) {
		lookahead = GetToken();
		if (lookahead.type != TSTRING) {
			UngetToken(lookahead);
			break;
		}
		tok.val.s = realloc(tok.val.s, strlen(tok.val.s) + strlen(lookahead.val.s) + sizeof(char));
		strcat(tok.val.s, lookahead.val.s);
	}
	_current = tok;
	return _current;
}

PRIVATE Token PeekToken(IN int index) {

	Token _pushback[10];
	int i;
	Token cur = _current;

	assert(index != 0);
	assert(index < 10);

	for (i = 0; i < index; i++)
		_pushback[i] = GetToken();
	while (i--)
		UngetToken(_pushback[i]);

	_current = cur;
	return _pushback[index-1];
}

PRIVATE char* TokenToString(IN Token token) {

	return _scan->str(token);
}

PRIVATE Preprocessor _pproc = {

	GetToken, RegetToken, PeekToken, UngetToken, TokenToString
};

PUBLIC Preprocessor* GetPreprocessor(void) {

	return &_pproc;
}

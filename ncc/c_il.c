/************************************************************************
*
*	il.c - Intermediate Language translation
*
*   Copyright(c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This is a work in progress ------------------------- */

/*n
	This component is responsible for translating the AST into NCC IL.
*/

#include <malloc.h>
#include <string.h>
#include "ncc.h"
#include "icode.h"
#include "c.h"

// dot -Tpdf -o ../../../test.pdf ../../../test.dot

//https://cs.nyu.edu/courses/fall10/G22.2130-001/lecture10.pdf
//file:///C:/Users/Michael/Desktop/NEPTUNE/NCC/ncc/ncc/test/test.png
//https://cs.wellesley.edu/~cs240/f18/slides/x86-control.pdf
//https://www.isi.edu/~pedro/Teaching/CSCI565-Spring10/Lectures/IntermCodeGen.part2.6p.pdf

// current virtual register name:
PRIVATE int    _regName;

// current basic block:
PRIVATE Block* _block;

// list of basic blocks in this function.
// "unreachable code" is any basic block with no "in" edge.
PRIVATE Vector* _bbList;

PUBLIC Block* NewBlock(void) {

	static uint32_t i = 0;
	Block* block = malloc(sizeof(Block));
	memset(block, 0, sizeof(Block));
	InitVector(&block->successor, sizeof(Block*));
	InitVector(&block->predicate, sizeof(Block*));
	InitVector(&block->defregs, sizeof(Value));
	InitVector(&block->inregs, sizeof(Value));
	InitVector(&block->outregs, sizeof(Value));
	block->name = i++;
	VectorPush(_bbList, &block);
	return block;
}

PRIVATE uint32_t NextReg(void) {

	return ++_regName;
}

PRIVATE void PrevReg(void) {

	if (_regName == 0)
		__debugbreak();
	_regName--;
}

Value* EmitConstant(IN String* value, IN Type* type) {

	Value* reg = malloc(sizeof(Value));
	reg->type = type;
	reg->virt = NextReg();
	reg->name = value;
	return reg;
}

Value* Reg() {

	Value* reg = malloc(sizeof(Value));
	reg->type = NULL;
	reg->name = NULL;
	reg->virt = NextReg();
	return reg;
}

PRIVATE Instr* Emit2(IN Mnemonic m, IN Value* a, IN Value* b) {

	Instr* i = NewIns(m);
	i->target = a;
	i->ops[0] = b;
	Addi(&_block->code, i);
	return i;
}

PRIVATE Instr* Emit3(IN Mnemonic m, IN Value* a, IN Value* b, IN Value* c) {

	Instr* i = NewIns(m);
	i->target = a;
	i->ops[0] = b;
	i->ops[1] = c;
	Addi(&_block->code, i);
	return i;
}

PRIVATE void EmitCmpi(IN Value* target, IN int op, IN Value* left, IN Value* right) {

	Instr* i;
	BOOL isUns;

	if (left->type->dt != right->type->dt)
		__debugbreak(); // bugcheck

	isUns = left->type->isUnsigned;
	i = Emit3(I_ICMP, target, left, right);

	switch (op) {
	case OP_ISEQ: i->cmpOp = ICMP_EQ; break;
	case OP_ISNOTEQ: i->cmpOp = ICMP_NE; break;
	case OP_LTE:
		i->cmpOp = isUns ? ICMP_ULE : ICMP_SLE;
		break;
	case OP_LESS:
		i->cmpOp = isUns ? ICMP_ULT : ICMP_SLT;
		break;
	case OP_GTE:
		i->cmpOp = isUns ? ICMP_UGE : ICMP_SGE;
		break;
	case OP_GREATER:
		i->cmpOp = isUns ? ICMP_UGT : ICMP_SGT;
		break;
	}
}

PRIVATE void EmitCmpf(IN Value* target, IN int op, IN Value* left, IN Value* right) {

	Instr* i;

	if (left->type->dt != right->type->dt)
		__debugbreak(); // bugcheck

	i = Emit3(I_FCMP, target, left, right);

	switch (op) {
	case OP_ISEQ: i->cmpOp = FCMP_UEQ; break;
	case OP_ISNOTEQ: i->cmpOp = FCMP_UNE; break;
	case OP_LTE: i->cmpOp = FCMP_ULE; break;
	case OP_LESS: i->cmpOp = FCMP_ULT; break;
	case OP_GTE: i->cmpOp = FCMP_UGE; break;
	case OP_GREATER: i->cmpOp = FCMP_UGT; break;
	}
}

PRIVATE void EmitCmp(IN Value* target, IN int op, IN Value* left, IN Value* right) {

	if (IsFltType(left->type))
		EmitCmpf(target, op, left, right);
	else
		EmitCmpi(target, op, left, right);
}

PRIVATE Instr* EmitIf(IN Value* cond, IN Block* then, IN Block* els) {
	 
	Instr* i = NewIns(I_IF);
	i->ops[0] = cond;
	i->ops[1] = (Value*)then;
	i->ops[2] = (Value*)els;
	Addi(&_block->code, i);
	return i;
}

PRIVATE Instr* EmitRet(IN Value* r) {

	Instr* i = NewIns(I_RET);
	i->ops[0] = r;
	Addi(&_block->code, i);
	return i;
}

PRIVATE Instr* EmitJmp(IN Block* to) {

	Instr* i = NewIns(I_JMP);
	if (!to)
		__debugbreak();
	i->ops[0] = (Value*)to;
	Addi(&_block->code, i);
	return i;
}

PRIVATE Instr* EmitJmpImm(IN Block* to, IN String* value) {

	Instr* i = NewIns(I_JMP);
	i->ops[0] = (Value*)to;
	i->imm = value;
	Addi(&_block->code, i);
	return i;
}

PRIVATE Value* EmitExpr(IN AstNode* node);

PRIVATE Value* EmitLval(IN AstNode* node) {

	Value* r;

	r = NULL;
	switch (node->kind) {
	case AST_VARIABLE:
		if (!node->var.symbol)
			__debugbreak();
		return node->var.symbol;
	case AST_DEREF:
		return EmitExpr(node->deref.node);
	case AST_STRUCT_REF:
		r = Reg();
		Emit3(I_ADD, r, EmitLval(node->struc.struc), EmitConstant(InternString("0"), IntType())); // "offset"
		return r;
	default:
		__debugbreak();
	};
}

PRIVATE Instr* EmitLoad(IN Value* dest, IN Value* src) {

	Instr* i = NewIns(I_LOAD);
	i->target = dest;
	i->ops[0] = src;
	Addi(&_block->code, i);
	return i;
}

PRIVATE Value* EmitCall(IN Value* r, IN Vector* parms) {

	Instr* i = NewIns(I_CALL);
	i->target = Reg(); // return value
	i->ops[0] = r; // expression result (i.e. function/ptr to function, etc)
	i->parms = parms;
	Addi(&_block->code, i);
	return i->target;
}

PRIVATE Value* EmitTernOp(IN AstNode* node) {

	Value* cond;
	Block* end;
	Block* trueCase;
	Block* falseCase;

	end = NewBlock();
	trueCase = NewBlock();
	falseCase = NewBlock();

	cond = EmitExpr(node->ternary.one);
	EmitIf(cond, trueCase, falseCase);

	_block = trueCase;
	EmitExpr(node->ternary.two);
	EmitJmp(end);

	_block = falseCase;
	EmitExpr(node->ternary.three);
	EmitJmp(end);

	_block = end;
	return Reg();
}

PRIVATE void EmitCast(IN Value* target, IN Type* from, IN Type* to, IN Value* data) {

	if (IsIntType(from) && IsIntType(to)) {
		if (to->size < from->size)
			Emit2(I_TRUNC, target, data);
		else if (to->size > from->size) {
			if (to->isUnsigned)
				Emit2(I_ZEXT, target, data);
			else
				Emit2(I_SEXT, target, data);
		}
		target->type = to;
	}
	else if (IsFltType(from) && IsFltType(to)) {
		if (to->size < from->size)
			Emit2(I_FPTRUNC, target, data);
		else if (to->size > from->size)
			Emit2(I_FPEXT, target, data);
		target->type = to;
	}
	else if (IsFltType(from) && IsIntType(to)) {
		if (to->isUnsigned)
			Emit2(I_FPTOUI, target, data);
		else
			Emit2(I_FPTOSI, target, data);
		target->type = to;
	}
	else if (IsIntType(from) && IsFltType(to)) {
		if (from->isUnsigned)
			Emit2(I_UITOFP, target, data);
		else
			Emit2(I_SITOFP, target, data);
		target->type = to;
	}
	else if (IsPtrType(from) && IsIntType(to)) {
		Emit2(I_PTRTOINT, target, data);
		target->type = to;
	}
	else if (IsIntType(from) && IsPtrType(to)) {
		Emit2(I_INTTOPTR, target, data);
		target->type = to;
	}
	else {
		// bugcheck
		printf("*** c_il.c: EmitCast: unknown cast bugcheck");
		__debugbreak();
	}
}

PRIVATE Value* EmitUnOp(IN AstNode* node) {

	Value* r;
	Value* a;

	switch (node->unary.op) {
	case OP_COMPL:
		r = Reg();
		Emit3(I_XOR, r, EmitExpr(node->unary.left), EmitConstant(InternString("-1"), IntType()));
		return r;
	case OP_NOT:
		r = Reg();
		EmitCmp(r, OP_ISEQ, EmitExpr(node->unary.left), EmitConstant(InternString("0"), IntType()));
		return r;
	case OP_INC:
		r = EmitExpr(node->unary.left);
		a = Reg();
		Emit3(I_ADD, a, r, EmitConstant(InternString("1"), IntType()));
		Emit2(I_STORE, r, a);
		return a;
	case OP_DEC:
		r = EmitExpr(node->unary.left);
		a = Reg();
		Emit3(I_SUB, a, r, EmitConstant(InternString("1"), IntType()));
		Emit2(I_STORE, r, a);
		return a;
	case OP_POSTINC:
		r = EmitExpr(node->unary.left);
		a = Reg();
		Emit3(I_ADD, a, r, EmitConstant(InternString("1"), IntType()));
		Emit2(I_STORE, r, a);
		return r;
	case OP_POSTDEC:
		r = EmitExpr(node->unary.left);
		a = Reg();
		Emit3(I_SUB, a, r, EmitConstant(InternString("1"), IntType()));
		Emit2(I_STORE, r, a);
		return r;
	case OP_CAST:
		r = Reg();
		EmitCast(r, node->unary.left->type, node->type, EmitExpr(node->unary.left));
		return r;
	default:
		Fatal(MESSAGE, "Internal: EmitUnOp unknown AST kind");
	}
}

PRIVATE Value* EmitBinOp(IN AstNode* node) {

	Mnemonic m;
	Value* target;
	Value* lval;
	Block* end;
	Block* b1;
	Block* failCase;
	Block* passCase;
	Instr* i;

	switch (node->bin.op) {
	//
	// assignment operators:
	//
	case OP_EQ:
		target = EmitExpr(node->bin.right);
		Emit2(I_STORE, EmitLval(node->bin.left), target);
		return target;
	case OP_ADD_ASSIGN:
		target = EmitExpr(node->bin.right);
		lval = EmitLval(node->bin.left);
		i = Emit2(I_ADD, lval, target);
		Emit2(I_STORE, EmitLval(node->bin.left), i->ops[0]);
		return target;
	case OP_SUB_ASSIGN:
		target = EmitExpr(node->bin.right);
		lval = EmitLval(node->bin.left);
		i = Emit2(I_SUB, lval, target);
		Emit2(I_STORE, EmitLval(node->bin.left), i->ops[0]);
		return target;
	case OP_MUL_ASSIGN:
		target = EmitExpr(node->bin.right);
		lval = EmitLval(node->bin.left);
		i = Emit2(I_MUL, lval, target);
		Emit2(I_STORE, EmitLval(node->bin.left), i->ops[0]);
		return target;
	case OP_DIV_ASSIGN:
		target = EmitExpr(node->bin.right);
		lval = EmitLval(node->bin.left);
		i = Emit2(I_UDIV, lval, target);
		Emit2(I_STORE, EmitLval(node->bin.left), i->ops[0]);
		return target;
	case OP_MOD_ASSIGN:
		target = EmitExpr(node->bin.right);
		lval = EmitLval(node->bin.left);
		i = Emit2(I_UREM, lval, target);
		Emit2(I_STORE, EmitLval(node->bin.left), i->ops[0]);
		return target;
	case OP_AND_ASSIGN:
		target = EmitExpr(node->bin.right);
		lval = EmitLval(node->bin.left);
		i = Emit2(I_AND, lval, target);
		Emit2(I_STORE, EmitLval(node->bin.left), i->ops[0]);
		return target;
	case OP_XOR_ASSIGN:
		target = EmitExpr(node->bin.right);
		lval = EmitLval(node->bin.left);
		i = Emit2(I_XOR, lval, target);
		Emit2(I_STORE, EmitLval(node->bin.left), i->ops[0]);
		return target;
	case OP_OR_ASSIGN:
		target = EmitExpr(node->bin.right);
		lval = EmitLval(node->bin.left);
		i = Emit2(I_OR, lval, target);
		Emit2(I_STORE, EmitLval(node->bin.left), i->ops[0]);
		return target;
	case OP_LEFT_ASSIGN:
		target = EmitExpr(node->bin.right);
		lval = EmitLval(node->bin.left);
		i = Emit2(I_SHL, lval, target);
		Emit2(I_STORE, EmitLval(node->bin.left), i->ops[0]);
		return target;
	case OP_RIGHT_ASSIGN:
		target = EmitExpr(node->bin.right);
		lval = EmitLval(node->bin.left);
		i = Emit2(I_ASHR, lval, target);
		Emit2(I_STORE, EmitLval(node->bin.left), i->ops[0]);
		return target;
	case OP_ISEQ:
	case OP_ISNOTEQ:
	case OP_LTE:
	case OP_LESS:
	case OP_GTE:
	case OP_GREATER:
		target = Reg();
		EmitCmp(target, node->bin.op, EmitExpr(node->bin.left), EmitExpr(node->bin.right));
		target->type = node->type;
		return target;
	case OP_PLUS: m = I_ADD; break;
	case OP_MINUS: m = I_SUB; break;
	case OP_DIV: m = I_UDIV; break;
	case OP_MODULO: m = I_UREM; break;
	case OP_AND: m = I_AND; break;
	case OP_OR: m = I_OR; break;
	case OP_XOR: m = I_XOR; break;
	case OP_SHIFT_LEFT: m = I_SHL; break;
	case OP_SHIFT_RIGHT: m = I_ASHR; break;
	case OP_COMMA:
		EmitExpr(node->bin.left);
		return EmitExpr(node->bin.right);
	case OP_LOG_AND:
		end = NewBlock();
		b1 = NewBlock();
		failCase = NewBlock();
		passCase = NewBlock();

		EmitIf(EmitExpr(node->bin.left), b1, failCase);

		_block = b1;
		EmitIf(EmitExpr(node->bin.right), passCase, failCase);

		_block = failCase;
		EmitJmpImm(end, InternString("0"));

		_block = passCase;
		EmitJmpImm(end, InternString("1"));

		_block = end;
		return Reg();

	case OP_LOG_OR:
		end = NewBlock();
		b1 = NewBlock();
		failCase = NewBlock();
		passCase = NewBlock();

		EmitIf(EmitExpr(node->bin.left), passCase, b1);

		_block = b1;
		EmitIf(EmitExpr(node->bin.right), passCase, failCase);

		_block = failCase;
		EmitJmpImm(end, InternString("0"));

		_block = passCase;
		EmitJmpImm(end, InternString("1"));

		_block = end;
		return Reg();
	default:
		Fatal(MESSAGE, "Internal: EmitBinOp unknown AST kind");
	};
	target = Reg();
	Emit3(m, target, EmitExpr(node->bin.left), EmitExpr(node->bin.right));
	target->type = node->type;
	return target;
}

PRIVATE Value* EmitExpr(IN AstNode* node) {

	Value* r;
	AstNode*  arg;
	Vector*   param;

	switch (node->kind) {
	case AST_LITERAL: return EmitConstant(node->lit.val, node->type);
	case AST_TERNARY: return EmitTernOp(node);
	case AST_BINARY: return EmitBinOp(node);
	case AST_UNARY: return EmitUnOp(node);
	case AST_VARIABLE:
	case AST_STRUCT_REF:
		r = Reg();
		r->type = node->type;
		EmitLoad(r, EmitLval(node));
		return r;
	case AST_DEREF:
		r = Reg();
		r->type = node->type;
		EmitLoad(r, EmitExpr(node->deref.node));
		return r;
	case AST_ADDROF:
		return EmitLval(node->addrOf.node);
	case AST_FUNCTION_CALL:
		param = NewVector(sizeof(Value*));
		for (arg = VectorFirst(node->fc.args); arg; arg = VectorNext(node->fc.args, arg)) {
			r = EmitExpr(arg);
			VectorPush(param, &r);
		}
		r = EmitCall(EmitExpr(node->fc.expr), param);
		return r;
	case AST_INIT:
		return EmitExpr(node->init.initval);
	default:
		Fatal(MESSAGE, "Internal: EmitExpr unknown AST kind");
	}
}

typedef struct StmtContext {
	Block* cont;   // continue target
	Block* brk;    // break target
}StmtContext;

// add semantic checks :
// error: a continue may only be used in a loop
// error: a break may only be used in a loop or switch

Instr* _parentSwitch;

PRIVATE void EmitStmt(IN AstNode* node, IN StmtContext* ctx) {

	Block* end;
	Block* falseCase;
	Block* trueCase;
	Block* loopContinue;
	Block* loopBody;
	Block* lastCont; // restore continue/break
	Block* lastBrk;
	Value* r;
	Value* init;
	AstNode*  cur;
	AstNode** pcur;

	switch (node->kind) {
	case AST_COMPOUND_STMT:
		for (cur = VectorFirst(node->cmpnd.stmt); cur; cur = VectorNext(node->cmpnd.stmt, cur)) {
			if (cur->kind == AST_DECL) {
				init = NULL;
				if (cur->decl.init)
					init = EmitExpr(VectorFirst(cur->decl.init));
				r = Reg();
				r->name = cur->decl.declvar->var.symbol->name;
				r->type = PointerType(cur->decl.declvar->var.symbol->type);
				if (init)
					Emit2(I_STORE, r, init);
				continue;
			}
			EmitStmt(cur,ctx);
		}
		break;
	case AST_RETURN:
		// "return 0" by default:
		if (!node->ret.retval)
			r = EmitConstant(InternString("0"), IntType());
		else
			r = EmitExpr(node->ret.retval);
		EmitRet(r);
		break;
	case AST_IF:
		trueCase = NewBlock();
		falseCase = NewBlock();
		end = NewBlock();

		EmitIf(EmitExpr(node->ifStmt.cond), trueCase, falseCase);

		_block = trueCase;
		EmitStmt(node->ifStmt.then,ctx);
		EmitJmp(end);

		_block = falseCase;
		if (node->ifStmt.els)
			EmitStmt(node->ifStmt.els,ctx);
		EmitJmp(end);

		_block = end;
		break;
	case AST_WHILE:
		lastCont = ctx->cont;
		lastBrk = ctx->brk;

		loopBody = NewBlock();
		loopContinue = NewBlock();
		end = NewBlock();

		ctx->cont = loopContinue;
		ctx->brk = end;

		EmitJmp(loopContinue);

		_block = loopContinue;
		r = EmitExpr(node->loopwhile.expr);
		EmitIf(r, loopBody, end);

		_block = loopBody;
		EmitStmt(node->loopwhile.stat,ctx);
		EmitJmp(loopContinue);

		ctx->cont = lastCont;
		ctx->brk = lastBrk;
		_block = end;
		break;
	case AST_DO:
		lastCont = ctx->cont;
		lastBrk = ctx->brk;

		loopBody = NewBlock();
		loopContinue = NewBlock();
		end = NewBlock();

		ctx->cont = loopContinue;
		ctx->brk = end;

		EmitJmp(loopContinue);

		_block = loopContinue;
		EmitStmt(node->loopwhile.stat,ctx);
		r = EmitExpr(node->loopwhile.expr);
		EmitIf(r, loopContinue, end);

		ctx->cont = lastCont;
		ctx->brk = lastBrk;
		_block = end;
		break;
	case AST_FOR:
		lastCont = ctx->cont;
		lastBrk = ctx->brk;

		loopBody = NewBlock();
		loopContinue = NewBlock();
		end = NewBlock();

		ctx->cont = NewBlock(); // see below
		ctx->brk = end;

		if (node->loopfor.init)
			EmitStmt(node->loopfor.init,ctx);
		EmitJmp(loopContinue);

		_block = loopContinue;
		if (node->loopfor.cond) {
			r = EmitExpr(node->loopfor.cond);
			EmitIf(r, loopBody, end);
		}
		else
			EmitJmp(loopBody);

		_block = loopBody;
		EmitStmt(node->loopfor.stmt,ctx);

		// note: continue statement must still run "next"
		EmitJmp(ctx->cont);
		_block = ctx->cont;

		if (node->loopfor.next)
			EmitExpr(node->loopfor.next);
		EmitJmp(loopContinue);

		ctx->cont = lastCont;
		ctx->brk = lastBrk;
		_block = end;
		break;
	case AST_GOTO:
		// parser sets "to" to point to AST_LABEL.
		if (!node->got.to)
			Fatal(MESSAGE, "Internal: EmitStmt: node->got.to not set");
		if (!node->got.to->label.block)
			node->got.to->label.block = NewBlock();
		EmitJmp(node->got.to->label.block);
		_block = NewBlock();
		break;
	case AST_LABEL:
		end = node->label.block;
		if (!end) // if there was a block, its a forward reference
			end = NewBlock();
		end->label = node->label.label;
		node->label.block = end;
		EmitJmp(end);
		_block = end;
		break;
	case AST_CONTINUE:
		EmitJmp(ctx->cont);
		_block = NewBlock();
		break;
	case AST_BREAK:
		EmitJmp(ctx->brk);
		_block = NewBlock();
		break;
	case AST_SWITCH:
		lastBrk = ctx->brk;
		ctx->brk = NewBlock();

		if (node->switchStmt.defCase)
			end = NewBlock();
		else
			end = ctx->brk;

		_parentSwitch = InsSwitch(EmitExpr(node->switchStmt.expr), end);
		Addi(&_block->code, _parentSwitch);

		for (pcur = VectorFirst(&node->switchStmt.cases); pcur; pcur = VectorNext(&node->switchStmt.cases, pcur))
			EmitStmt(*pcur, ctx);

		_block = end;
		if (node->switchStmt.defCase) {
			EmitStmt(node->switchStmt.defCase, ctx);
		}
		EmitJmp(ctx->brk);

		_block = ctx->brk;
		ctx->brk = lastBrk;
		break;
	case AST_CASE:
		end = NewBlock();
		EmitJmp(end);
		_block = end;
		//		InsSwitchCase(_parentSwitch, 0, end);
		EmitStmt(node->caseStmt.stmt, ctx);
		break;
	case AST_DEFAULT:
		EmitStmt(node->caseStmt.stmt,ctx);
		break;
	default: // expression statement
		EmitExpr(node);
	}
}

PRIVATE BOOL Translate(IN File* file, OUT Function* fc) {

	AstNode*    decl;
	Block*      top;
	Value*   r;
	StmtContext ctx;

	fc->source = file;

	InitVector(&fc->blocks, sizeof(Block*));
	_bbList = &fc->blocks;

	// start of new function:
	_block = NewBlock();
	_regName = 0;
	ctx.brk = ctx.cont = NULL;

	while (TRUE) {

		top = _block;
		decl = GetSemanticAnalyzer()->analyze(file);
		if (!decl)
			break;
		if (decl->kind == AST_DECL) {
			if (decl->decl.declvar) {
				r = Reg();
//				decl->decl.declvar->var.symbol->reg = r;
				r->name = decl->decl.declvar->var.symbol->name;
				r->type = PointerType(decl->decl.declvar->var.symbol->type);
			}
			continue;
		}
		if (decl->kind != AST_FUNCTION) {
			__debugbreak();
			continue;
		}
		if (GetErrorCount() > 0)
			Fatal(MESSAGE,"Syntax errors detected, stopping.");

		EmitStmt(decl->function.body,&ctx);
		fc->entry = top;
		fc->name = decl->function.symbol->name->value;
		return TRUE;
	}
	fc->entry = NULL;
	fc->name = NULL;
	return FALSE;
}

PUBLIC BOOL InitFrontEnd(void) {

	RegisterMessages(_msgC);
	return TRUE;
}

PUBLIC BOOL Parse(IN char* fname, OUT Function* fc) {

	File* file;

	fc->source = NULL;
	file = GetFileManager()->open(fname);
	if (!file)
		return FALSE;
	Translate(file, fc);
	return TRUE;
}

PUBLIC BOOL NextParse(OUT Function* fc) {

	Translate(fc->source, fc);
	if (!fc->entry) {
		GetFileManager()->close(fc->source);
		fc->source = NULL;
		return FALSE;
	}
	return TRUE;
}

/*
	Translator _translate = { Init, Translate };

	Translator* GetTranslator(void) {
	return &_translate;
	}
*/

// https://gist.github.com/CMCDragonkai/2f4b5e078f690443d190

// https://baptiste-wicht.com/posts/2012/02/local-optimization-on-three-address-code.html

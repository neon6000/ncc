/************************************************************************
*
*	dot.c - DOT file generator
*
*   Copyright(c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

// dot -Tpng test.dot > test.png

/* This is a work in progress ------------------------- */

#include <stdio.h>
#include <malloc.h>
#include "ncc.h"
#include "icode.h"

PRIVATE FILE*   _outfile;

#if 0
PRIVATE char* MnemonicToStr(IN Mnemonic m) {

	char* mnemonic;

	return "invalid";
	/*
	switch (m) {
	case INVALID: mnemonic = "invalid"; break;
	case CALL: mnemonic = "call"; break;
	case PARAM: mnemonic = "param"; break;
	case RETURN: mnemonic = "return"; break;
	case GOTO: mnemonic = "goto"; break;
	case LABEL: mnemonic = "label"; break;
	case ASSIGN: mnemonic = "assign"; break;
	case CMPLT: mnemonic = "cmplt"; break;
	case CMPGT: mnemonic = "cmpgt"; break;
	case CMPLTE: mnemonic = "cmplte"; break;
	case CMPGTE: mnemonic = "cmpgte"; break;
	case CMPEQ: mnemonic = "iseq"; break;
	case CMPNEQ: mnemonic = "isnoteq"; break;
	case ADD: mnemonic = "add"; break;
	case SUB: mnemonic = "sub"; break;
	case MUL: mnemonic = "mul"; break;
	case DIV: mnemonic = "div"; break;
	case NOT: mnemonic = "not"; break;
	case COMPL: mnemonic = "compl"; break;
	case MODULO: mnemonic = "mod"; break;
	case SHL: mnemonic = "shl"; break;
	case SHR: mnemonic = "shr"; break;
	case INC: mnemonic = "inc"; break;
	case DEC: mnemonic = "dec"; break;
	case LOGAND: mnemonic = "logand"; break;
	case LOGOR: mnemonic = "logor"; break;
	case AND: mnemonic = "and"; break;
	case OR: mnemonic = "or"; break;
	case IF: mnemonic = "if"; break;
	case ADDROF: mnemonic = "addrof"; break;
	case DEREF: mnemonic = "deref"; break;
	default: mnemonic = NULL;  __debugbreak();
	};
	*/

	return mnemonic;
}

PRIVATE void OpToStr(IN Buffer* buf, IN Operand* op, IN int argidx) {

	Symbol* s;
	Value*  v;
	Block* b;

	/*
	if (!op)
		return;
	if (argidx == 0)
		BufferPrintf(buf, " ");
	else if (argidx > 0)
		BufferPrintf(buf, ", ");
	if (op->type == OPERAND_SYMBOL) {
		s = (Symbol*)op;
		if (s->sc == SYM_TEMPORARY)
			BufferPrintf(buf, "$t%i", s->tempName);
		else
			BufferPrintf(buf, "%s", s->name);
	}
	else if (op->type == OPERAND_BLOCK) {
		b = (Block*)op;
		BufferPrintf(buf, "$b%i", b->name);
		if (b->label)
			BufferPrintf(buf, "(%s)", b->label);
	}
	else if (op->type == OPERAND_VALUE) {
		v = (Value*)op;
		BufferPrintf(buf, "%i", v->ival);
	}
	else {
		v = (Value*)op;
		BufferPrintf(buf, "%s", v->sval);
	}
	*/
}

PRIVATE char* InsToStr(IN Instr* i) {

	Buffer b;

	InitBuffer(&b);
	BufferPrintf(&b, "%s", MnemonicToStr(i->mnemonic));

//	OpToStr(&b, (Operand*)i->op[0], 0);
//	OpToStr(&b, (Operand*)i->op[1], 1);
//	OpToStr(&b, (Operand*)i->op[2], 2);

	return GetBufferData(&b);
}
#endif


PRIVATE void Init(IN char* fname) {

	_outfile = fopen(fname, "w");
	fputs("digraph cfg{\n", _outfile);
	fputs("graph[fontsize = 10 fontname = \"Verdana\"];\n", _outfile);
	fputs("node[shape = record fontsize = 10 fontname = \"Verdana\"];\n", _outfile);
}

PRIVATE BOOL Visited(IN Vector* visited, IN Block* block) {

	Block** cur;
	for (cur = VectorFirst(visited); cur; cur = VectorNext(visited, cur)) {
		if (*cur == block)
			return TRUE;
	}
	return FALSE;
}

PRIVATE void WriteBlock(IN Block* cfg, IN Vector* visited) {
	
	int         idx;
	Instr*      ins;
	char*       s;
	Vector      children;
	Block** block;

	if (!cfg)
		return;

	//
	// if this block has already been visited, nothing to do:
	//

	if (Visited(visited, cfg))
		return;
	VectorPush(visited, &cfg);
	InitVector(&children, sizeof(Block*));

	//
	// prepare this block:
	//

	fprintf(_outfile, "%i [label=\"", cfg->name);
	fprintf(_outfile, ".L%i", cfg->name);
	if (cfg->label)
		fprintf(_outfile, " (%s)", cfg->label);
	fprintf(_outfile, "\\l");

	//
	// print instructions in this block:
	//
	for (ins = cfg->code.first; ins; ins = ins->next) {

		s = "instr";
//		s = InstrToStr(ins);
		fprintf(_outfile, "%s\\l", s);
		Free(s);

		//
		// save all output blocks:
		//
//		if (ins->out[0]) VectorPush(&children, &ins->out[0]);
//		if (ins->out[1]) VectorPush(&children, &ins->out[1]);
	}
	fprintf(_outfile, "\"];\n");

	//
	// print connecting blocks:
	//

	for (block = VectorFirst(&children); block; block = VectorNext(&children, block)) {

		WriteBlock(*block, visited);
		fprintf(_outfile, "%i->%i;\n", cfg->name, (*block)->name);
	}

	FreeVector(&children);
}

PRIVATE void Write(IN Block* cfg) {

	Vector* visited = NewVector(sizeof(Block*));
	WriteBlock(cfg, visited);
	FreeVector(visited);
}

PRIVATE void Flush(void) {

	fputs("}\n", _outfile);
	fclose(_outfile);
}

PRIVATE CodeGenerator _gen = { Init, Write, Flush };

PUBLIC CodeGenerator* GetCodeGeneratorDot(void) {
	return &_gen;
}

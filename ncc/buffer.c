/************************************************************************
*
*	buffer.c - Dynamic buffer.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "base.h"
#include <assert.h>
#include <malloc.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* This implements a dynamically resizing buffer. */

#define BUFFER_SIZE 8

PUBLIC Buffer* InitBuffer(IN Buffer* r) {

	r->data = AllocTag("BUFDATA", BUFFER_SIZE);
	r->allocCount = BUFFER_SIZE;
	r->elementCount = 0;
	return r;
}

PUBLIC Buffer* NewBuffer(void) {

	Buffer* r = AllocTag("BUF", sizeof(Buffer));
	r->data = AllocTag("BUFDATA", BUFFER_SIZE);
	r->allocCount = BUFFER_SIZE;
	r->elementCount = 0;
	return r;
}

PUBLIC void BufferFree(IN Buffer* r) {

	if (!r)
		return;
	Free(r->data);
	Free(r);
}

PUBLIC void* BufferFreePreserve(IN Buffer* r) {

	void* k;
	if (!r)
		return NULL;
	k = r->data;
	Free(r);
	return k;
}

PRIVATE void Resize(IN Buffer* b) {

	int newsize = b->allocCount * 2;
	uint8_t* data = Realloc(b->data, newsize);
	b->data = data;
	b->allocCount = newsize;
}

PUBLIC uint8_t* GetBufferData(IN Buffer* b) {

	return b->data;
}

PUBLIC size_t GetBufferLength(IN Buffer* b) {

	return b->elementCount;
}

PUBLIC void BufferWrite(IN Buffer* b, IN char c) {

	if (b->allocCount == (b->elementCount + 1))
		Resize(b);
	b->data[b->elementCount++] = c;
}

PUBLIC void BufferPush(IN Buffer* b, IN char c) {

	BufferWrite(b, c);
}

PUBLIC char BufferPop(IN Buffer* b) {

	assert(b->elementCount != 0);
	return b->data[--b->elementCount];
}

PUBLIC void BufferAppend(IN Buffer* b, IN char* s, IN int len) {

	for (int i = 0; i < len; i++)
		BufferWrite(b, s[i]);
}

PUBLIC void BufferPrintf(IN Buffer* b, IN char* fmt, ...) {

	va_list args = NULL;
	while (TRUE) {
		int avail = b->allocCount - b->elementCount;
		va_start(args, fmt);
		int written = vsnprintf(b->data + b->elementCount, avail, fmt, args);
		va_end(args);
		if (written == -1 || (avail <= written)) {
			Resize(b);
			continue;
		}
		b->elementCount += written;
		return;
	}
}

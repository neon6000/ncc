/************************************************************************
*
*	parse.c - Parser
*
*   Copyright(c) BrokenThorn Entertainment Co.All Rights Reserved.
*
************************************************************************/

/* This is a work in progress ----------------------------- */

/*
	This component implements the parser. It grabs tokens from the Preprocessor
	and returns an abstract syntax tree for global declarations. It performs
	syntax checking and adds symbols to the symbol table.

	Error recovery stragety:

		The parser pushes an error token TOK_ERROR to the token stream when expect()
		fails. The parser is returned to a state that processes TOK_ERROR and attempts
		to skip tokens until we are in a valid state. This is typically in declarations
		and statements. If T_EOF is reached while skipping tokens, issue a Fatal()
		with PREMATURE_EOF error. Fatal() flushes the output message buffer and terminates
		the program.

		"Parsing Techniques - A Practical Guide"
		https://github.com/awh/compsci-papersAstCase

		A. Ad-hoc
			1. Error productions
			2. Empty table slots
			3. Error tokens
		B. Regional
			1. Forward-Backward Move
		C. Local error
			1. Panic mode
			2. Foward Set
		D. Suffix methods
*/

#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdarg.h>
#include "base.h"
#include "ncc.h"
#include "icode.h"
#include "c.h"

// references --
// https://cs.wmich.edu/~gupta/teaching/cs4850/sumII06/The%20syntax%20of%20C%20in%20Backus-Naur%20form.htm
// http://www.quut.com/c/ANSI-C-grammar-y.html
// https://jhjourdan.mketjh.fr/pdf/jourdan2017simple.pdf
// https://cdecl.org/
// http://port70.net/~nsz/c/

PRIVATE Preprocessor* _scan;
PRIVATE SymbolTable*  _symbols;
PRIVATE Cache*        _astCache;
PRIVATE Vector        _switch;
PRIVATE Vector        _goto;
PRIVATE Vector        _label;

// Used by Preprocessor and Scanner:
PUBLIC Scope*        _scope;

PUBLIC AstNode*      constant_expression(void);

PRIVATE AstNode*    cast_expression(void);
PRIVATE AstNode*    conditional_expression(void);
PRIVATE AstNode*    logical_or_expression(void);
PRIVATE AstNode*    logical_and_expression(void);
PRIVATE AstNode*    inclusive_or_expression(void);
PRIVATE AstNode*    exclusive_or_expression(void);
PRIVATE AstNode*    and_expression(void);
PRIVATE AstNode*    equality_expression(void);
PRIVATE AstNode*    relational_expression(void);
PRIVATE AstNode*    shift_expression(void);
PRIVATE AstNode*    additive_expression(void);
PRIVATE AstNode*    multiplicative_expression(void);
PRIVATE AstNode*    unary_expression(void);
PRIVATE AstNode*    assignment_expression(void);
PRIVATE AstNode*    primary_expression(void);
PRIVATE AstNode*    expression(void);
PRIVATE AstNode*    postfix_expression(void);
PRIVATE AstNode*    statement(void);
PRIVATE AstNode*    compound_statement(void);
PRIVATE AstNode*    for_statement(void);
PRIVATE AstNode*    do_statement(void);
PRIVATE AstNode*    while_statement(void);
PRIVATE AstNode*    switch_statement(void);
PRIVATE AstNode*    if_statement(void);
PRIVATE AstNode*    goto_statement(void);
PRIVATE AstNode*    continue_statement(void);
PRIVATE AstNode*    break_statement(void);
PRIVATE AstNode*    return_statement(void);
PRIVATE AstNode*    default_statement(void);
PRIVATE AstNode*    case_statement(void);
PRIVATE AstNode*    labeled_statement(void);
PRIVATE AstNode*    expression_statement(void);
PRIVATE Vector*     argument_expression_list(void);
PRIVATE Type*       struct_or_union_specifier(void);
PRIVATE Type*       enum_specifier(void);
PRIVATE Type*       abstract_declarator(IN Type* basetype, OUT Vector* params);
PRIVATE Type*       declarator(OUT char** name, OUT Type* base, OUT Vector* params);
PRIVATE Type*       declaration_specifiers(void);
PRIVATE Type*       parameter_declaration(IN char** name);
PRIVATE Type*       pointer(IN Type* basetype);
PRIVATE Vector*     parameter_list(void);
PRIVATE Vector*     initializer(IN Type* type);
PRIVATE void        initializer_list(IN Type* type, OUT Vector* init);
PRIVATE void        declaration(IN Vector* vars);
PRIVATE void        parameter_type_list(OUT Vector* types, OUT Vector* vars, OUT BOOL* vararg);

Message _msgC[] = {

	{ PARSE_EXPECTED, "%z: Expected '%s' got '%s'" },
	{ PARSE_UNDECLARED, "%z: Undeclared identifier '%s'" },
	{ EXPECTED_STRUCT_OR_UNION, "%z: Structure or union expected" },
	{ NOT_A_MEMBER_OF, "%z: '%s' is not a member of '%s'" },
	{ POINTER_EXPECTED, "%z: Pointer expected" },
	{ CAST_NOT_ALLOWED, "%z: Cast from '%y' to '%y' not allowed" },
	{ ASSIGN_VOID_TYPE, "%z: Cannot assign void type" },
	{ EXPECTED_ARITH_OR_PTR_TYPE, "%z: Expected arithmitic or pointer type" },
	{ EXPECTED_ARITH_TYPE, "%z: Expected arithmitic type" },
	{ EXPECTED_INT_TYPE, "%z: Expected integer type" },
	{ EXPECTED_LVALUE, "%z: Expected lvalue" },
	{ EXPECTED_LVALUE_OR_FUNCTIONDESG, "%z: Expected lvalue or function designator" },
	{ EXPECTED_MODIFIABLE_LVALUE, "%z: Expected modifiable lvalue" },
	{ EXPECTED_COMPLETE_TYPE, "%z: Incomplete type '%y'" },
	{ EXPECTED_PTR_TYPE, "%z: Expected pointer" },
	{ PARSE_EMPTY_STRUCT_OR_UNION, "%z: C requires structs and unions to have at least one member" },
	{ SYM_REDEFINE, "%z: Redefined symbol '%s'" },
	{ INCOMPATIBLE_TYPE, "%z: Incompatible types - from '%y' to '%y'" },
	{ INCOMPATIBLE_ASSIGN_TYPE, "%z: Incompatible types when assigning to type '%y' from type '%y'" },
	{ PTR_INDIRECTION_MISMATCH, "%z: '%y' differs in levels of indirection from '%y'" },
	{ PREMATURE_EOF, "%z: Premature end of file reached (did you miss a '}'?" },
	{ PARSE_KR_FUNCTION, "%z: K&R C style function syntax not supported" },
	{ PARSE_UNTAGGED_TYPE_NO_SYMBOL, "%z: Untagged '%y' declared with no symbols" },
	{ PARSE_MISSING_TAG, "%z: Missing tag name for '%s'" },
	{ PARSE_SWITCH_LABEL, "%z: A %s label may only be used in a switch" },
	{ PARSE_SWITCH_DUPLICATE, "%z: Duplicate label '%s' in switch" },
	{ PARSE_LABEL_REDEFINE, "%z: Duplicate label '%s'" },
	{ 0, NULL }
};

PUBLIC int Evaluate(IN AstNode* node) {

	if (!node)
		return 0;

	switch (node->kind) {

	case AST_LITERAL:
		if (node->type && IsIntType(node->type))
			return strtol(node->lit.val->value, NULL, 10);
		else
			Fatal(PARSE_EXPR);

	case AST_UNARY:
		switch (node->unary.op) {
		case OP_NOT: return !Evaluate(node->unary.left);
		case OP_COMPL: return ~Evaluate(node->unary.left);
		}

	case AST_TERNARY:
		return Evaluate(node->ternary.one) ? Evaluate(node->ternary.two) : Evaluate(node->ternary.three);

#define LEFT Evaluate(node->bin.left)
#define RIGHT Evaluate(node->bin.right)

	case AST_BINARY:
		switch (node->bin.op) {
		case OP_PLUS: return LEFT + RIGHT;
		case OP_MINUS: return LEFT - RIGHT;
		case OP_DIV: return LEFT / RIGHT;
		case OP_MUL: return LEFT * RIGHT;
		case OP_MODULO: return LEFT % RIGHT;
		case OP_OR: return LEFT | RIGHT;
		case OP_AND: return LEFT & RIGHT;
		case OP_XOR: return LEFT ^ RIGHT;
		case OP_LESS: return LEFT < RIGHT;
		case OP_GREATER: return LEFT > RIGHT;
		case OP_LTE: return LEFT <= RIGHT;
		case OP_GTE: return LEFT >= RIGHT;
		case OP_LOG_OR: return LEFT || RIGHT;
		case OP_LOG_AND: return LEFT && RIGHT;
		case OP_ISEQ: return LEFT == RIGHT;
		case OP_ISNOTEQ: return LEFT != RIGHT;
		case OP_SHIFT_RIGHT: return LEFT >> RIGHT;
		case OP_SHIFT_LEFT: return LEFT << RIGHT;
		};
#undef LEFT
#undef RIGHT
	};
	__debugbreak();
	return 0;
}

PRIVATE void AdjustGotos(IN Scope* fnScope) {

	AstNode** got;
	AstNode** label;

	// match GOTO's with their LABEL's.
	for (got = VectorFirst(&_goto); got; got = VectorNext(&_goto, got)) {
		for (label = VectorFirst(&_label); label; label = VectorNext(&_label, label)) {
			if (!strcmp((*got)->got.label, (*label)->label.label))
				(*got)->got.to = *label;
		}
	}

	// if a GOTO has not been matched, its an error.
	for (got = VectorFirst(&_goto); got; got = VectorNext(&_goto, got)) {
		if ((*got)->got.to == NULL)
			Error(MESSAGE, "GOTO with no matching label");
	}
}

//
// Returns the current token:
//
PRIVATE Token gettok(void) {

	return _scan->reget();
}

//
// Allocate a new AST node object at the current
// scanner location:
//
PUBLIC AstNode* NewNode(IN AstNodeKind node) {

	AstNode* n = AllocObject(_astCache);
	n->kind = node;
	n->loc = gettok().loc;
	return n;
}

//
// Given a string, scan the integer suffix and
// return its Type.
//
//         6.4.4.1 Integer constants
//
PRIVATE Type* ScanIntegerSuffix(IN char* s) {

	if (!strcmp(s, "ul") || !strcmp(s, "lu"))
		return NewType(DT_LONG, Q_NONE, S_NONE, TRUE);
	if (!strcmp(s, "ull") || !strcmp(s, "llu"))
		return NewType(DT_LLONG, Q_NONE, S_NONE, TRUE);
	if (!strcmp(s, "u"))
		return NewType(DT_INT, Q_NONE, S_NONE, TRUE);
	if (!strcmp(s, "l"))
		return NewType(DT_LONG, Q_NONE, S_NONE, FALSE);
	if (!strcmp(s, "ll"))
		return NewType(DT_LLONG, Q_NONE, S_NONE, FALSE);
	return NewType(DT_INT, Q_NONE, S_NONE, FALSE);
}

//
// Given a string, scan its suffix and return its
// Type.
//
PRIVATE Type* ScanFloatSuffix(IN char* s) {

	if (!strcmp(s, "f") || !strcmp(s, "F"))
		return NewType(DT_FLOAT, Q_NONE, S_NONE, FALSE);
	if (!strcmp(s, "l") || !strcmp(s, "L"))
		return NewType(DT_FLOAT, Q_NONE, S_NONE, FALSE);
	return NewType(DT_DOUBLE, Q_NONE, S_NONE, FALSE);
}

//
// Scan the given string and return an AST_LITERAL of
// the integer constant represented by "s" in the correct
// Type. The Type is determined by the Suffix (if any)
// appended to the String.
//
//              6.4.4.1 Integer constants
//
PRIVATE AstNode* ScanIntegerConstant(IN char* s, IN BOOL ishex) {

	Type* type;
	char* suffix;
	AstNode* node;
	unsigned long ival;

	suffix = NULL;
	ival = strtoul(s, &suffix, ishex ? 16 : 10);
	type = ScanIntegerSuffix(suffix);
	node = NewNode(AST_LITERAL);
	node->lit.val = InternString(intToStr(ival));
	node->type = type;
	return node;
}

//
// Scan the given string and return as AST_LITERAL
// node with the correct Type. The Type is determined
// by the Suffix (if any) appended to the String.
//
//            6.4.4.2 Floating constants
//
PRIVATE AstNode* ScanFloatConstant(IN char* s) {

	AstNode* node;
	Type* type;
	char* suffix;
	double dval;

	suffix = NULL;
	dval = strtod(s, &suffix);
	type = ScanFloatSuffix(suffix);
	node = NewNode(AST_LITERAL);
	node->lit.val = InternString(s);
	node->type = type;
	return node;
}

//
// Scan the given string and return an AST_LITERAL
// with the correct Type.
//
PRIVATE AstNode* ScanNumericLiteral(IN char* s) {

	BOOL ishex = FALSE;

	if (strlen(s) > 3 && s[0] == '0' && tolower(s[1] == 'x'))
		ishex = TRUE;
	if (!ishex && strpbrk(s, ".eEpP"))
		return ScanFloatConstant(s);
	return ScanIntegerConstant(s,ishex);
}

//
// New AST_SIZEOF node.
//
PRIVATE AstNode* NewSizeofNode(IN AstNode* expr, IN Type* type) {

	AstNode* node;

	node = NewNode(AST_SIZEOF);
	node->size.expr = expr;
	node->size.type = type;
	return node;
}

//
// New AST_LITERAL node.
//
PRIVATE AstNode* NewNumber(IN Token tok) {

	return ScanNumericLiteral(tok.val.s);
}

//
// New AST_LITERAL node.
//
PRIVATE AstNode* NewCharacter(IN Token tok) {

	return ScanNumericLiteral(intToStr((int)tok.val.c));
}

//
// New AST_LITERAL node.
//
PRIVATE AstNode* NewString(IN Token tok) {

	AstNode* n = NewNode(AST_LITERAL);
	n->lit.val = InternString(tok.val.s);
	n->type = NULL;
	return n;
}

//
// New AST_UNARY node.
//
PRIVATE AstNode* NewUnaryOp(IN TokenOperator op, IN AstNode* left) {

	AstNode* n = NewNode(AST_UNARY);
	n->unary.left = left;
	n->unary.op = op;
	return n;
}

//
// New AST_UNARY node. This is primaily used
// for OP_CAST expressions to override the type.
//
PRIVATE AstNode* NewUnaryOpEx(IN TokenOperator op, IN Type* type, IN AstNode* left) {

	AstNode* n = NewUnaryOp(op, left);
	n->type = type;
	return n;
}

//
// New AST_BINARY node.
//
PRIVATE AstNode* NewBinaryOp(IN TokenOperator op, IN AstNode* left, IN AstNode* right) {

	AstNode* n = NewNode(AST_BINARY);
	n->bin.left = left;
	n->bin.right = right;
	n->bin.op = op;
	return n;
}

//
// New AST_TERNARY node. This is primarily used for ?: operator.
//
PRIVATE AstNode* NewTernaryOp(IN TokenOperator op, IN AstNode* one, IN AstNode* two, IN AstNode* three) {

	AstNode* n = NewNode(AST_TERNARY);
	n->ternary.one = one;
	n->ternary.two = two;
	n->ternary.three = three;
	return n;
}

//
// New AST_ADDROF node.
//
PRIVATE AstNode* NewAddressOf(IN AstNode* node) {

	AstNode* n = NewNode(AST_ADDROF);
	n->addrOf.node = node;
	return n;
}

//
// New AST_DEREF node.
//
PRIVATE AstNode* NewDeref(IN AstNode* node) {

	AstNode* n = NewNode(AST_DEREF);
	n->deref.node = node;
	return n;
}

//
// New AST_VARIABLE_DECL node. Used when variables are declared.
//
PRIVATE AstNode* NewVarDecl(IN Value* symbol) {

	AstNode* n;
	n = NewNode(AST_VARIABLE_DECL);
	n->var.symbol = symbol;
	n->type = symbol->type;
	return n;
}

//
// New AST_VARIABLE node. Used to represent identifiers that
// reference declared variables in the symbol table.
//
PRIVATE AstNode* NewVar(IN Value* symbol) {

	AstNode* n;
	n = NewNode(AST_VARIABLE);
	n->var.symbol = symbol;
	n->type = symbol->type;
	return n;
}

//
// New AST_FUNCTION node. Describes a function.
//
PRIVATE AstNode* NewFunction(IN Value* symbol, IN Type* rettype, IN Vector* params, IN AstNode* body) {

	AstNode* n = NewNode(AST_FUNCTION);
	n->function.symbol = symbol;
	n->function.params = NULL;
	n->function.rettype = rettype;
	n->function.body = body;
	n->function.varargs = FALSE;
	return n;
}

PRIVATE AstNode* NewFunctionCall(IN AstNode* expr, IN Vector* args) {

	AstNode* n = NewNode(AST_FUNCTION_CALL);
	if (expr->type->dt != DT_FUNCTION)
		__debugbreak();
	n->fc.expr = expr;
	n->fc.args = args;
	n->type = expr->type->function.rettype;
	return n;
}

PRIVATE AstNode* NewDeclaration(IN AstNode* var, IN Vector* init) {

	AstNode* n = NewNode(AST_DECL);
	n->decl.declvar = var;
	n->decl.init = init;
	return n;
}

PRIVATE AstNode* NewInitializer(IN AstNode* val, IN Type* to, IN int off) {

	AstNode* n = NewNode(AST_INIT);
	n->init.initval = val;
	return n;
}

PRIVATE AstNode* NewIf(IN AstNode* cond, IN AstNode* then, IN AstNode* els) {

	AstNode* n = NewNode(AST_IF);
	n->ifStmt.cond = cond;
	n->ifStmt.then = then;
	n->ifStmt.els = els;
	return n;
}

PRIVATE AstNode* NewReturn(IN AstNode* retval) {

	AstNode* n = NewNode(AST_RETURN);
	n->ret.retval = retval;
	return n;
}

PRIVATE AstNode* NewSwitch(IN AstNode* expr) {

	AstNode* n = NewNode(AST_SWITCH);
	n->switchStmt.expr = expr;
	n->switchStmt.stmt = NULL; // set later.
	return n;
}

PRIVATE AstNode* NewCase(IN String* value, IN AstNode* stmt) {

	AstNode* n = NewNode(AST_CASE);
	n->caseStmt.value = value;
	n->caseStmt.stmt = stmt;
	return n;
}

PRIVATE AstNode* NewDefault(IN AstNode* stmt) {

	AstNode* n = NewNode(AST_DEFAULT);
	n->defaultStmt.stmt = stmt;
	return n;
}

PRIVATE AstNode* NewCompoundStatement(IN Vector* stmts, IN Scope* scope) {

	AstNode* n = NewNode(AST_COMPOUND_STMT);
	n->cmpnd.stmt = stmts;
	n->cmpnd.scope = scope;
	return n;
}

PRIVATE AstNode* NewStructRef(IN AstNode* struc, IN char* name) {

	AstNode* n = NewNode(AST_STRUCT_REF);
	n->struc.struc = struc;
	n->struc.field = name;
	n->type = NULL;
	return n;
}

PRIVATE AstNode* NewGoto(IN char* label) {

	AstNode* n = NewNode(AST_GOTO);
	n->got.label = label;
	VectorPush(&_goto, &n);
	return n;
}

PRIVATE AstNode* NewContinue(void) {

	return NewNode(AST_CONTINUE);
}

PRIVATE AstNode* NewBreak(void) {

	return NewNode(AST_BREAK);
}

PRIVATE AstNode* NewLabel(IN char* label, IN Value* sym) {

	AstNode* n = NewNode(AST_LABEL);
	n->label.label = label;
	n->label.symbol = sym;
	VectorPush(&_label, &n);
	return n;
}

PRIVATE AstNode* NewFor(IN AstNode* init, IN AstNode* cond, IN AstNode* next, IN AstNode* stmt) {

	AstNode* n = NewNode(AST_FOR);
	n->loopfor.init = init;
	n->loopfor.cond = cond;
	n->loopfor.next = next;
	n->loopfor.stmt = stmt;
	return n;
}

PRIVATE AstNode* NewDoWhile(IN AstNode* cond, IN AstNode* stmt) {

	AstNode* n = NewNode(AST_DO);
	n->dowhile.stat = stmt;
	n->dowhile.expr = cond;
	return n;
}

PRIVATE AstNode* NewWhile(IN AstNode* cond, IN AstNode* stmt) {

	AstNode* n = NewNode(AST_WHILE);
	n->loopwhile.stat = stmt;
	n->loopwhile.expr = cond;
	return n;
}

PRIVATE Type* NewStrucType(IN DataType dt, IN OPTIONAL char* tag) {

	Type* t;

	assert(dt == DT_STRUCT || dt == DT_UNION);
	t = Alloc(sizeof(Type));
	memset(t, 0, sizeof(Type));
	t->align = 1;
	t->dt = dt;
	t->struc.tag = tag;
	return t;
}

PRIVATE Type* NewEnumType(void) {

	return NewType(DT_ENUM, Q_NONE, S_NONE, FALSE);
}

PRIVATE Type* CopyType(IN Type* type) {

	Type* t = Alloc(sizeof(Type));
	memcpy(t, type, sizeof(Type));
	return t;
}

PRIVATE Type* NewPointerType(IN Type* basetype) {

	Type* t = NewType(DT_PTR, basetype->tq, basetype->sc, FALSE);
	t->pointer.type = basetype;
	basetype->parent = t;
	return t;
}

PRIVATE Type* NewArrayType(IN Type* basetype, IN int len) {

	Type* t = NewType(DT_ARRAY, basetype->tq, basetype->sc, FALSE);
	if (len < 0)
		t->size = 0;
	else
		t->size = len * basetype->size;
	t->align = basetype->align;
	t->array.type = basetype;
	t->array.len = len;
	basetype->parent = t;
	return t;
}

PRIVATE Type* NewFunctionType(IN Type* rettype, IN Vector* parmtypes, IN BOOL hasVarArgs) {

	Type* t = NewType(DT_FUNCTION, Q_NONE, S_NONE, FALSE);
	t->function.rettype = rettype;
	t->function.params = parmtypes;
	t->function.hasva = hasVarArgs;
	t->sc = rettype->sc;
	t->tq = rettype->tq;
	rettype->parent = t;
	return t;
}


/* parser ------------------ */

//
// Consume the current token.
//
PRIVATE void next(void) {

	_scan->get();
}

//
// Peek "index" tokens ahead.
//
PRIVATE int peek(IN int index) {

	return _scan->peek(index).type;
}

//
// Peek "index" tokens ahead.
//
PRIVATE Token peekTok(IN int index) {

	return _scan->peek(index);
}

//
// Get token type of current token.
//
PRIVATE int get(void) {

	return _scan->reget().type;
}

//
// If current token type is accepted type,
// return TRUE, else FALSE.
//
PRIVATE BOOL accept(IN int type) {

	return get() == type;
}

//
// Construct a Token with the given type.
// Used by expect()
//
PRIVATE Token TokenFromType(IN int type) {

	Token tok;
	memset(&tok, 0, sizeof(Token));
	tok.type = type;
	return tok;
}

//
// Given a list of accepted tokens, return TRUE if
// current token matches an accepted token, FALSE if
// it does not match any.
//
PRIVATE BOOL acceptedList(IN int count, IN va_list args) {

	for (int i = 0; i < count; i++) {
		if (accept(va_arg(args, int)))
			return TRUE;
	}
	return FALSE;
}

//
// Given a list of expected tokens, if the current token
// matches a token in the Expected List, return TRUE. Else,
// raise an Error and push an error token on the token stream.
//
PRIVATE BOOL expect(IN int count, /* IN int token */ ...) {

	Token tok;
	Token err;
	va_list args;
	Buffer accepted;

	va_start(args, count);
	if (acceptedList(count, args))
		return TRUE;
	va_end(args);

	// acceptance list as string:
	va_start(args, count);
	InitBuffer(&accepted);
	for (int i = 0; i < count; i++) {
		Token t = TokenFromType(va_arg(args, int));
		BufferPrintf(&accepted, "%s", _scan->str(t));
		if (i + 1 < count)
			BufferPrintf(&accepted, ",");
	}
	va_end(args);

	tok = gettok();
	Error(PARSE_EXPECTED, tok.loc, GetBufferData(&accepted), _scan->str(tok));
	Free(GetBufferData(&accepted));

	tok = gettok();
	err = TokenFromType(TOK_ERROR);
	err.loc = tok.loc;

	// "tok" has already been read from input stream, so need to
	// put it back followed by error token:
	_scan->unget(tok);
	_scan->unget(err);
	return FALSE;
}

//
// Compute the size of a structure type
//
PRIVATE size_t GetStructSize(IN Type* type) {

	size_t    size;
	Type*     fieldType;
	List*     fields;
	ListNode* cur;

	size = 0;
	fields = MapGetKeys(type->struc.fields);
	for (cur = fields->first; cur; cur = cur->next) {
		fieldType = MapGet(type->struc.fields, cur->data);
		size += Align(fieldType->size, fieldType->align);
	}
	return size;
}

//
// Compute the size of a union type
//
PRIVATE size_t GetUnionSize(IN Type* type) {

	size_t    size;
	size_t    fieldSize;
	Type*     fieldType;
	List*     fields;
	ListNode* cur;

	size = 0;
	fields = MapGetKeys(type->struc.fields);
	for (cur = fields->first; cur; cur = cur->next) {
		fieldType = MapGet(type->struc.fields, cur->data);
		fieldSize = Align(fieldType->size, fieldType->align);
		if (fieldSize > size)
			size = fieldSize;
	}
	return size;
}

//
// Skip over parameter list. Called by is_function_definition()
//
PRIVATE void skip_parentheses(IN Vector* buf) {

	while (TRUE) {
		Token tok;
		next(); // consume '('
		tok = gettok();
		if (tok.type == TEOF)
			Fatal(PREMATURE_EOF, tok.loc);
		VectorPush(buf, &tok);
		if (tok.type == OP_RPAREN)
			return;
		if (tok.type == OP_LPAREN)
			skip_parentheses(buf); // skip inside parentheses
	}
}

/*
	attempt to detect if current token is the start of a function definition.
	Returns TRUE if it detects a function definition, FALSE otherwise.
*/
PRIVATE BOOL is_function_definition(void) {

	Vector* buf = NewVector(sizeof(Token));
	BOOL result = FALSE;
	BOOL paramListFound = FALSE;

	while (TRUE) {

		Token tok;

		// get next token and save it:
		tok = gettok();
		VectorPush(buf, &tok);

		if (tok.type == TEOF)
			Fatal(PREMATURE_EOF, tok.loc);

		// if this is a ';' this is not a function definition.
		// note this part isn't directly compatible with K&R syntax:
		if (tok.type == OP_SEMICOLON)
			break;

		// if this is a '=' this is not a function definition
		else if (tok.type == OP_EQ)
			break;

		// if next character is not '(' skip it:
		if (!paramListFound && tok.type != OP_LPAREN) {
			next();
			continue;
		}

		// skip over parameter list. this also consumes the last ')'.
		if (tok.type == OP_LPAREN) {
			skip_parentheses(buf);
			paramListFound = TRUE;
			next();
			continue;
		}

		// if this is on '{' we found a function definition:
		result = (tok.type == OP_LBRACE);
		break;
	}

	// put back tokens we consumed:
	while (VectorLength(buf) > 0) {
		Token tok;
		VectorPop(buf, &tok);
		_scan->unget(tok);
	}

	FreeVector(buf);
	next();
	return result;
}

PRIVATE BOOL is_type(IN Token tok) {

	switch (tok.type) {
	case K_AUTO: case K_REGISTER: case K_STATIC: case K_EXTERN: case K_TYPEDEF:
	case K_VOID: case K_CHAR: case K_SHORT: case K_INT: case K_LONG: case K_FLOAT:
	case K_DOUBLE: case K_SIGNED: case K_UNSIGNED: case K_BOOL:
	case K_STRUCT: case K_UNION: case K_ENUM: case K_CONST: case TYPENAME:
		return TRUE;
	};
	return FALSE;
}

BOOL assignment_operator(void) {

	switch (get()) {
	case OP_MUL_ASSIGN:   case OP_DIV_ASSIGN:  case OP_MOD_ASSIGN:
	case OP_ADD_ASSIGN:   case OP_SUB_ASSIGN:  case OP_LEFT_ASSIGN:
	case OP_RIGHT_ASSIGN: case OP_AND_ASSIGN:  case OP_XOR_ASSIGN:
	case OP_OR_ASSIGN:    case OP_EQ:
		return TRUE;
	};
	return FALSE;
}

BOOL unary_operator(void) {

	switch (get()) {
	case OP_AND:    case OP_MUL:   case OP_PLUS:
	case OP_MINUS:  case OP_COMPL: case OP_NOT:
		return TRUE;
	};
	return FALSE;
}

AstNode* constant_expression(void) {

	return conditional_expression();
}

AstNode* conditional_expression(void) {

	AstNode* cond = logical_or_expression();
	AstNode* then;
	AstNode* els;

	// <logical_or_expression> ? <expression> : <conditional_expression>
	if (accept(OP_QUESTION)) {
		next();
		then = expression();
		if (expect(1,OP_COLON))
			next();
		els = conditional_expression();
		return NewTernaryOp(OP_QUESTION, cond, then, els);
	}
	return cond;
}

AstNode* logical_or_expression(void) {

	AstNode* result = logical_and_expression();
	while (accept(OP_LOG_OR)) {
		next();
		result = NewBinaryOp(OP_LOG_OR, result, logical_and_expression());
	}
	return result;
}

AstNode* logical_and_expression(void) {

	AstNode* result = inclusive_or_expression();
	while (accept(OP_LOG_AND)) {
		next();
		result = NewBinaryOp(OP_LOG_AND, result, inclusive_or_expression());
	}
	return result;
}

AstNode* inclusive_or_expression(void) {

	AstNode* result = exclusive_or_expression();
	while (accept(OP_OR)) {
		next();
		result = NewBinaryOp(OP_OR, result, exclusive_or_expression());
	}
	return result;
}

AstNode* exclusive_or_expression(void) {

	AstNode* result = and_expression();
	while (accept(OP_XOR)) {
		next();
		result = NewBinaryOp(OP_XOR, result, and_expression());
	}
	return result;
}

AstNode* and_expression(void) {

	AstNode* result = equality_expression();
	while (accept(OP_AND)) {
		next();
		result = NewBinaryOp(OP_AND, result, equality_expression());
	}
	return result;
}

AstNode* equality_expression(void) {

	AstNode* result = relational_expression();
	while (accept(OP_ISEQ) || accept(OP_ISNOTEQ)) {
		TokenOperator op = get();
		next();
		result = NewBinaryOp(op, result, relational_expression());
	}
	return result;
}

AstNode* relational_expression(void) {

	AstNode* result = shift_expression();
	while (accept(OP_LESS) || accept(OP_GREATER) || accept(OP_LTE) || accept(OP_GTE)) {
		TokenOperator op = get();
		next();
		result = NewBinaryOp(op, result, shift_expression());
	}
	return result;
}

AstNode* shift_expression(void) {

	AstNode* result = additive_expression();
	while (accept(OP_SHIFT_LEFT) || accept(OP_SHIFT_RIGHT)) {
		TokenOperator op = get();
		next();
		result = NewBinaryOp(op, result, additive_expression());
	}
	return result;
}

AstNode* additive_expression(void) {

	AstNode* result = multiplicative_expression();
	while (accept(OP_PLUS) || accept(OP_MINUS)) {
		TokenOperator op = get();
		next();
		result = NewBinaryOp(op, result, multiplicative_expression());
	}
	return result;
}

AstNode* multiplicative_expression(void) {

	AstNode* result = cast_expression();
	while (accept(OP_MUL) || accept(OP_DIV) || accept(OP_MODULO)) {
		TokenOperator op = get();
		next();
		result = NewBinaryOp(op, result, cast_expression());
	}
	return result;
}

Type* type_specifier(void) {

	Type* type;
	Value* sym;
	DataType dt;
	StorageClass sc;
	TypeQualifier tq;
	BOOL isUnsigned;

	tq = Q_NONE;
	sc = S_NONE;
	dt = DT_INVALID;
	type = NULL;
	sym = NULL;
	isUnsigned = FALSE;

	while (TRUE) {
		switch (get()) {
		case K_AUTO:     SET(sc, S_AUTO);        next(); break;
		case K_REGISTER: SET(sc, S_REGISTER);    next(); break;
		case K_STATIC:   SET(sc, S_STATIC);      next(); break;
		case K_EXTERN:   SET(sc, S_EXTERN);      next(); break;
		case K_TYPEDEF:  SET(sc, S_TYPEDEF);     next(); break;
		case K_CONST:    SET(tq, Q_CONST); next(); break;
		case K_VOLATILE: SET(tq, Q_VOLATILE); next(); break;
		case K_VOID:     SET(dt, DT_VOID); next(); break;
		case K_CHAR:     SET(dt, DT_CHAR); next(); break;
		case K_INT:      SET(dt, DT_INT); next(); break;
		case K_LONG:     SET(dt, DT_LONG); next(); break;
		case K_FLOAT:    SET(dt, DT_FLOAT); next(); break;
		case K_DOUBLE:   SET(dt, DT_DOUBLE); next(); break;
		case K_SIGNED:   isUnsigned = FALSE; next(); break;
		case K_UNSIGNED: isUnsigned = TRUE; next(); break;
		case K_STRUCT:
		case K_UNION: type = struct_or_union_specifier(); break;
		case K_ENUM: type = enum_specifier(); break;
		case TYPENAME:
			sym = _symbols->lookup(_scope, gettok().val.s, NS_IDENTS);
			next();
			type = CopyType(sym->type);
			// this prevents "TYPENAME myObject" from being added as if it had TYPEDEF.
			CLEAR(type->sc, S_TYPEDEF);
			break;
		default:
			if (!type)
				return NewType(dt, tq, sc, isUnsigned);
			type->sc = sc;
			type->tq = tq;
			type->isUnsigned = isUnsigned;
			return type;
		};
	}
}

Type* declaration_specifiers(void) {

	return type_specifier();
}

Type* type_name(void) {

	return abstract_declarator(type_specifier(), NULL);
}

AstNode* cast_expression(void) {

	Type* type;

	if (accept(OP_LPAREN) && is_type(peekTok(1))) {
		next();
		type = type_name();
		if (expect(1,OP_RPAREN))
			next();
		return NewUnaryOpEx(OP_CAST, type, cast_expression());
	}
	else
		return unary_expression();
}

AstNode* unary_expression(void) {

	Type* type;
	AstNode* node;
	TokenOperator op;

	node = NULL;
	type = NULL;

	if (accept(OP_INC) || accept(OP_DEC)) {
		op = get();
		next();
		return NewUnaryOp(op, unary_expression());
	}
	else if (unary_operator()){
		op = get();
		next();

		// address-of operator:

		if (op == OP_AND)
			return NewAddressOf(cast_expression());

		// dereference operator:

		else if (op == OP_MUL)
			return NewDeref(cast_expression());

		// other unary operators:

		return NewUnaryOp(op, cast_expression());
	}
	else if (accept(K_SIZEOF)) {
		next();
		if (accept(OP_LPAREN)) {
			next();
			if (is_type(gettok()))
				type = type_name();
			else
				node = unary_expression();
			if(expect(1,OP_RPAREN))
				next();
		}
		else
			node = unary_expression();
		return NewSizeofNode(node,type);
	}
	else
		return postfix_expression();
}

AstNode* assignment_expression(void) {

	AstNode* cond = logical_or_expression();
	if (accept(OP_QUESTION)) {
		AstNode* then;
		AstNode* els;
		next();
		then = expression();
		if (expect(1,OP_COLON))
			next();
		els = conditional_expression();
		return NewTernaryOp(OP_QUESTION, cond, then, els);
	}
	else if (assignment_operator()) {
		TokenOperator op = get();
		next();
		return NewBinaryOp(op, cond, assignment_expression());
	}
	return cond;
}

AstNode* _postfix_expression_subscript(IN AstNode* primaryExpression) {

	AstNode* expr;

	next();
	expr = expression();
	if (expect(1,OP_RBRACKET))
		next();
	//
	// a[x] = *(a+x)
	//
	return NewDeref(NewBinaryOp(OP_PLUS, expr, primaryExpression));
}

Vector* argument_expression_list(void) {

	Vector* args;
	AstNode* expr;

	expr = assignment_expression();
	if (!expr)
		return NULL;

	args = NewVector(sizeof(AstNode));
	VectorPush(args, expr);
	while (accept(OP_COMMA)) {
		next();
		expr = assignment_expression();
		VectorPush(args, expr);
	}
	return args;
}

AstNode* _postfix_expression_function_call(IN AstNode* primaryExpression) {

	Vector* args;

	next();
	args = argument_expression_list();
	if (expect(1,OP_RPAREN))
		next();

	return NewFunctionCall(primaryExpression, args);
}

AstNode* _postfix_expression(IN AstNode* node) {

	while (TRUE) {
		if (accept(OP_LBRACKET)) {
			node = _postfix_expression_subscript(node);
		}
		else if (accept(OP_LPAREN)) {
			node = _postfix_expression_function_call(node);
		}
		else if (accept(OP_DOT)) {

			next();
			if (expect(1,TIDENTIFIER)) {
				char* s = gettok().val.s;
				next();
				node = NewStructRef(node, s);
			}
			else{
				__debugbreak();
			}
		}
		else if (accept(OP_PTR)) {
			//
			// "a->b" = "*(a).b"
			//
			next();
			if (expect(1,TIDENTIFIER)) {
				char* s = gettok().val.s;
				next();
				node = NewStructRef(NewDeref(node), s);
			}
			else{
				__debugbreak();
			}
		}
		else if (accept(OP_INC)) {
			next();
			node = NewUnaryOp(OP_POSTINC, node);
		}
		else if (accept(OP_DEC)) {
			next();
			node = NewUnaryOp(OP_POSTDEC, node);
		}
		else
			break;
	}
	return node;
}

AstNode* postfix_expression(void) {

	AstNode* t = primary_expression();
	if (!t)
		return NULL;
	return _postfix_expression(t);
}

AstNode* primary_expression(void) {

	AstNode* expr;
	Value*  symbol;

	expr = NULL;
	symbol = NULL;

	switch (get()) {
	case TCHAR:
		expr = NewCharacter(gettok());
		next();
		return expr;
	case TNUMBER:
		expr = NewNumber(gettok());
		next();
		return expr;
	case TSTRING:
		expr = NewString(gettok());
		next();
		return expr;
	case TIDENTIFIER:
		symbol = _symbols->lookup(_scope, gettok().val.s, NS_IDENTS);
		if (!symbol) {
			Error(PARSE_UNDECLARED, gettok().loc, gettok().val.s);
			return NULL;
		}
		else{
			expr = NewVar(symbol);
		}
		next();
		return expr;
	};

	// ( <expression> )
	if (accept(OP_LPAREN)) {
		next();
		expr = expression();
		if (expect(1,OP_RPAREN))
			next();
	}
	return expr;
}

AstNode* expression(void) {

	AstNode* expr = assignment_expression();
	while (accept(OP_COMMA)) {
		next();
		expr = NewBinaryOp(OP_COMMA, expr, assignment_expression());
	}
	return expr;
}

/*
=========================

	Declarators.

	For declarators, locate the IDENTIFIER which will become a leaf of the subtree,
	and work our way outward to construct the complete Type. Please see the following
	for an example:

	https://stackoverflow.com/questions/13808932/what-are-declarations-and-declarators-and-how-are-their-types-interpreted-by-the

	The basic rules are as follows:

	"An array of type T"
	"A pointer to type T"
	"A function of () returning type T"
	Working backward, T starts as the Base Type.

	direct_declarator:
		: IDENTIFER
		| '(' <declarator> ')'
		| <direct_declarator> '[' <constant_expression> ']'
		| <direct_declarator> '[' ']'
		| <direct_declarator> '(' <parameter_type_list> ')'
		| <direct_declarator> '(' <identifier_list> ')'
		| <direct_declarator> '(' ')'

	declarator:
		: <pointer> <direct_declarator>
		| <direct_declarator>

	pointer:
		: '*'
		| '*' <pointer>

=========================
*/

// "vars" or "parms" vector list:
typedef struct ParameterDetail {
	char* name;
	Type* type;
}ParameterDetail;
typedef Vector ParameterDetailArray;

Type* parameter_declaration(IN char** name) {

	Type* basetype;
	ParameterDetailArray* params = NULL;

	basetype = declaration_specifiers();
	return declarator(name, basetype, params);;
}

void parameter_type_list(OUT Vector* types, OUT ParameterDetailArray* vars, OUT BOOL* vararg) {

	ParameterDetail detail;

	// handle (void) parameter:
	if (accept(K_VOID) && peek(1) == OP_RPAREN) {
		next();
		detail.type = NewType(DT_VOID, Q_NONE, S_NONE, FALSE);
		if (types)
			VectorPush(types, &detail.type);
		return;
	}

	// handle () parameter:
	if (accept(OP_RPAREN))
		return;

	while (TRUE) {

		if (accept(OP_ELLIPSES)) {
			next();
			if (vararg)
				*vararg = TRUE;
			if (accept(OP_COMMA)) {
				//NccErrort(gettok(), "parameter_type_list: Operator '...' must be last parameter");
				__debugbreak();
			}
			break;
		}

		detail.name = NULL;
		detail.type = parameter_declaration(&detail.name);

		if (vars)
			VectorPush(vars, &detail);
		if (types)
			VectorPush(types, &detail.type);
		if (!accept(OP_COMMA))
			break;
		next();
	}
}

Type* direct_declarator_backend(IN Type* basetype, OUT ParameterDetailArray* params);

Type* direct_declarator_array(IN Type* basetype, OUT ParameterDetailArray* params) {

	AstNode* expr;
	int      len;

	len = 0;
	next();
	if (accept(OP_RBRACKET)) {
		next();
	}
	else{
		expr = constant_expression();
		len = Evaluate(expr);
		if(expect(1,OP_RBRACKET))
			next();
	}

	return NewArrayType(direct_declarator_backend(basetype, params), len);
}

Type* direct_declarator_function(IN Type* basetype, OUT ParameterDetailArray* params) {

	Type* type;
	Vector* types;
	BOOL vararg;

	type = NULL;
	types = NewVector(sizeof(Type*));
	vararg = FALSE;

	next();
	if (gettok().type == TIDENTIFIER)
		__debugbreak();
	else {
		parameter_type_list(types, params, &vararg);
		if(expect(1,OP_RPAREN))
			next();
	}

	return NewFunctionType(direct_declarator_backend(basetype, params), types, vararg);
}

Type* direct_declarator_backend(IN Type* basetype, OUT ParameterDetailArray* params) {

	if (accept(OP_LBRACKET))
		return direct_declarator_array(basetype, params);
	else if (accept(OP_LPAREN))
		return direct_declarator_function(basetype, params);
	return basetype;
}

// move type from "in" to "out" while conserving the parent/child
// relationship between subtypes:
PRIVATE void MoveSubtype(OUT Type* out, IN Type* in) {

	Type* parent;

	// copy over the new type:
	parent = out->parent;
	memcpy(out, in, sizeof(Type));
	out->parent = parent;

	// attach the immediate child if composite type
	// to point to the new parent:
	if (in->dt == DT_ARRAY)
		in->array.type->parent = out;
	else if (in->dt == DT_PTR)
		in->pointer.type->parent = out;
	else if (in->dt == DT_FUNCTION)
		in->function.rettype->parent = out;
	Free(in);
}

Type* direct_declarator(OUT char** name, IN Type* basetype, OUT ParameterDetailArray* params) {

	Type*  type;
	Type*  stub;
	Type* t;

	type = basetype;
	if (accept(TIDENTIFIER)) {
		*name = gettok().val.s;
		next();
	}
	else if (accept(OP_LPAREN)) {

		if (peekTok(1).type == OP_RPAREN || is_type(peekTok(1)))
			return direct_declarator_function(basetype, params);

		next();
		stub = NewType(DT_INVALID, basetype->sc, basetype->tq, FALSE);
		t = declarator(name, stub, params);
		if(expect(1,OP_RPAREN))
			next();
		MoveSubtype(stub, direct_declarator_backend(basetype, params));
		return t;
	}
	else if (accept(OP_MUL)) {
		next();
		return direct_declarator(name, NewPointerType(basetype), params);
	}

	return direct_declarator_backend(type, params);
}

Type* pointer(IN Type* basetype) {

	if (!accept(OP_MUL))
		return basetype;
	next();
	return NewPointerType(pointer(basetype));
}

Type* declarator(OUT char** name, IN Type* basetype, OUT ParameterDetailArray* params) {

	return direct_declarator(name, basetype, params);
}

Type* abstract_declarator(IN Type* basetype, OUT ParameterDetailArray* params) {

	return declarator(NULL, basetype, params);
}

//
// <function-definition> ::=  {<declaration-specifier>}* <declarator> {<declaration>}* <compound-statement>
//
// Notes -- This parses ANSI C function definitons and K&R C function definitions.
//
// https://stackoverflow.com/questions/24347927/function-definition-in-bnf-c-grammar
//
AstNode* function_definition(void) {

	char*         name;
	Type*         rettype = NULL;
	AstNode*      body = NULL;
	Type*         realtype;
	AstNode*      fn;
	ParameterDetailArray* params;
	ParameterDetail*      param;

	params = NewVector(sizeof(ParameterDetail));

	rettype = declaration_specifiers();
	realtype = declarator(&name, rettype, params);

	// this part is the old style k&r c syntax.
	if (get() != OP_LBRACE) {
		Fatal(PARSE_KR_FUNCTION, gettok().loc);
		declaration(params);
	}

	_symbols->insert(_scope, name, NS_IDENTS, realtype);

	_scope = _symbols->enterScope(_scope);

	for (param = VectorFirst(params); param; param = VectorNext(params, param))
		_symbols->insert(_scope, param->name, NS_IDENTS, param->type);

	InitVector(&_goto, sizeof(AstNode*));
	InitVector(&_label, sizeof(AstNode*));

	body = compound_statement();
	fn = NewFunction(_symbols->lookup(_scope, name, NS_IDENTS), rettype, params, body);
	if (VectorLength(params)) {
		fn->function.params = NewVector(sizeof(Value*));
		for (param = VectorFirst(params); param; param = VectorNext(params, param)) {
			Value* val = _symbols->lookup(_scope, param->name, NS_IDENTS);
			VectorPush(fn->function.params, &val);
		}
	}

	fn->function.scope = _scope;
	_scope = _symbols->exitScope(_scope);

	AdjustGotos(fn->function.scope);
	FreeVector(&_goto);
	FreeVector(&_label);
	FreeVector(params);

	return fn;
}

//
// <struct-or-union> ::= struct
//                      | union
//
BOOL struct_or_union(void) {

	if (accept(K_STRUCT))
		return TRUE;
	else if (accept(K_UNION))
		return TRUE;
	return FALSE;
}

//
// <external-declaration> ::= <function-definition>
//                          | <declaration>
//
Vector* external_declaration(void) {

	Token tok;
	Vector* ast;
	
	tok = gettok();

	ast = NewVector(sizeof(AstNode));
	while (TRUE) {
		if (is_function_definition())
			VectorPush(ast, function_definition());
		else
			declaration(ast);
		if (accept(TOK_ERROR)) {
			while (get() == TOK_ERROR
					|| (get() != OP_SEMICOLON
					&& !is_function_definition())) {
				if (get() == TEOF)
					Fatal(PREMATURE_EOF, tok.loc);
				next();
			}
			continue;
			__debugbreak();
		}
		break;
	}

	return ast;
}

/*
===========================

	Declaration specifiers

===========================
*/

int enumerator(IN int prevValue) {

	char*    ident;
	Type*    type;
	AstNode* expr;
	int      newValue;

	newValue = prevValue;
	if (accept(TIDENTIFIER)) {
		ident = gettok().val.s;
		next();
		if (accept(OP_EQ)) {
			next();
			expr = constant_expression();
			newValue = Evaluate(expr);
		}
		if (!_symbols->insert(_scope, ident, NS_IDENTS, IntType()))
			Error(SYM_REDEFINE, gettok().loc, ident);
	}
	return newValue;
}

BOOL enumerator_list(void) {

	int value;

	value = enumerator(0);
	while (accept(OP_COMMA)) {
		next();
		value = enumerator(value+1);
	}
	return TRUE;
}

Type* enum_specifier(void) {

	char* tag;
	Type* type;
	Value* sym;

	type = NULL;
	tag = NULL;
	if (!accept(K_ENUM))
		return NULL;
	next();
	if (accept(TIDENTIFIER)) {
		tag = gettok().val.s;
		next();
	}
	if (gettok().type != OP_LBRACE) {
		sym = _symbols->lookup(_scope, tag, NS_TAGS);
		if (!sym) {
			printf("\n\r*** enum %s: type not defined", tag);
			__debugbreak();
		}
		return sym->type;
	}
	next();
	enumerator_list();
	if (expect(1,OP_RBRACE)) {
		type = NewEnumType();
		if (tag) {
			if (!_symbols->insert(_scope, tag, NS_TAGS, type))
				Error(SYM_REDEFINE, gettok().loc, tag);
		}
		next();
	}
	return type;
}

void _struct_declarations(IN Map* map) {

	char* name;
	Type* basetype;
	Type* fieldtype;

	name = NULL;
	while (TRUE) {
		basetype = declaration_specifiers();
		while (TRUE) {
			fieldtype = declarator(&name, basetype, NULL);
			if (!name)
				Error(PARSE_EXPECTED, gettok().loc, "<identifier>", "<null>");
			if (fieldtype->dt == DT_INVALID){
				Error(PARSE_EXPECTED, gettok().loc, "<type>", gettok().val);
			}
			if (!IsCompleteType(fieldtype))
				Error(EXPECTED_COMPLETE_TYPE, gettok().loc, fieldtype);
			if (accept(OP_COLON)) {
				next();
				fieldtype->bitfield = Evaluate(constant_expression());
			}
			if (name)
				MapPut(map, name, fieldtype);
			if (!accept(OP_COMMA))
				break;
			next();
		}
		if (expect(1, OP_SEMICOLON))
			next();
		if (accept(TOK_ERROR)) {
			while (accept(TOK_ERROR)
				|| !accept(OP_SEMICOLON)
				&& !accept(OP_RBRACE)) {
					if (accept(TEOF))
						Fatal(PREMATURE_EOF, gettok().loc);
					next();
			}
		}
		if (accept(OP_SEMICOLON))
			next();
		if (accept(OP_RBRACE))
			break;
	}
}

Map* _struct_or_union_specifier_fields(void) {

	Map* map;

	if (accept(OP_RBRACE))
		return NULL;
	map = NewMap();
	_struct_declarations(map);
	return map;
}

Type* struct_or_union_specifier(void) {

	char* tag;
	Type* rectype;	
	DataType dt;
	Value* symbol;

	symbol = NULL;
	tag = NULL;
	rectype = NULL;

	if (gettok().type == K_STRUCT)
		dt = DT_STRUCT;
	else if (gettok().type == K_UNION)
		dt = DT_UNION;
	else
		return NULL;

	next();
	if (accept(TIDENTIFIER)) {
		tag = gettok().val.s;
		next();
	}

	if (accept(OP_LBRACE)) {

		rectype = NewStrucType(dt, tag);
		next();
		if (tag && !_symbols->insert(_scope, tag, NS_TAGS, rectype)) {

			Value* sym = _symbols->lookup(_scope, tag, NS_TAGS);
			if (IsCompleteType(sym->type))
				Error(SYM_REDEFINE, gettok().loc, tag);
		}
		rectype->struc.fields = _struct_or_union_specifier_fields();
		if (rectype->struc.fields == NULL) {
			Error(PARSE_EMPTY_STRUCT_OR_UNION, gettok().loc);
			rectype->size = 0;
		}
		else{
			if (rectype->dt == DT_STRUCT)
				rectype->size = GetStructSize(rectype);
			else
				rectype->size = GetUnionSize(rectype);
		}
		if(expect(1,OP_RBRACE))
			next();
	}
	else if (tag) {

		// attempt to lookup tag:
		symbol = _symbols->lookup(_scope, tag, NS_TAGS);

		// if tag does not exist, add it to scope with incomplete type (size=0):
		if (tag && !symbol) {
			rectype = NewStrucType(dt, tag);
			_symbols->insert(_scope, tag, NS_TAGS, rectype);
		}
		// tag already exists, return its type:
		else {
			rectype = symbol->type;
		}
	}
	else{
		Error(PARSE_MISSING_TAG, gettok().loc,
			dt == DT_STRUCT ? "struct" : "union");
	}

	return rectype;
}

void init_declarator_list (IN Type* basetype, OUT Vector* vars) {

	char* name;
	Type* type;
	Vector* params;
	AstNode* var;
	Vector* init;

	params = NULL;
	var = NULL;
	init = NULL;

	while (TRUE) {

		name = NULL;
		type = declarator(&name, basetype, params);

		if (IsStructOrUnionType(type)) {
			if (name==NULL && type->struc.tag==NULL)
				Warning(PARSE_UNTAGGED_TYPE_NO_SYMBOL, gettok().loc, type);
		}

		if (name) {

			//
			// <basetype> myFunction(arguments)
			//

			if (type->dt == DT_FUNCTION) {
				_symbols->insert(_scope, name, NS_IDENTS, type);
			}

			//
			// <basetype> myVariable
			//

			else{
				if (!_symbols->insert(_scope, name, NS_IDENTS, type))
					Error(SYM_REDEFINE, gettok().loc, name);
				var = NewVarDecl(_symbols->lookup(_scope, name, NS_IDENTS));
			}

			//
			// Initializer:
			//

			init = NULL;
			if (accept(OP_EQ)) {

				// <basetype> myFunction (args) = <initializer> is illegal:
				if (type->dt == DT_FUNCTION)
					Error(PARSE_INIT_FUNCTION);
				next();
				init = initializer(type);
			}
			var = NewDeclaration(var, init);
			VectorPush(vars, var);
		}
		if (!accept(OP_COMMA))
			break;
		next();
	}

	// for certain initializers, treat ';' as optional:
	if (init && (type->dt == DT_ARRAY || type->dt == DT_STRUCT
		|| type->dt == DT_UNION || type->dt == DT_ENUM)) {

		if (!accept(OP_SEMICOLON)) {
			_scan->unget(gettok());
			_scan->unget(TokenFromType(OP_SEMICOLON));
		}
	}
}

void declaration(IN Vector* vars) {
	init_declarator_list(declaration_specifiers(), vars);
	if (expect(1, OP_SEMICOLON)) {
		next();
	}
}

Vector* initializer(IN Type* type) {

	Vector* init;
	Token   tok;

	init = NewVector(sizeof(AstNode));
	// = { ... }
	if (accept(OP_LBRACE)) {
		next();
		initializer_list(type, init);

		// skip any trailing comma:
		if (accept(OP_COMMA))
			next();

		if (expect(1,OP_RBRACE))
			next();
	}
	else{
		VectorPush(init, NewInitializer(assignment_expression(),type,0));
	}

	return init;
}

void initializer_list(IN Type* type, OUT Vector* init) {

	while (TRUE) {
		VectorPush(init, initializer(type));
		if (!accept(OP_COMMA))
			break;
		next();
	}
}

void _declaration_or_statement(IN Vector* stmts) {

	AstNode* stmt;
	if (is_type(gettok()))
		declaration(stmts);
	else{
		stmt = statement();
		if (stmt)
			VectorPush(stmts, stmt);
	}
}

//
// <statement> ::= <labeled-statement>
//               | <expression-statement>
//               | <compound-statement>
//               | <selection-statement>
//               | <iteration-statement>
//               | <jump-statement>
//
AstNode* statement(void) {

	switch (get()) {
	case OP_LBRACE:   return compound_statement();
	case K_IF:        return if_statement();
	case K_WHILE:     return while_statement();
	case K_DO:        return do_statement();
	case K_FOR:       return for_statement();
	case K_GOTO:      return goto_statement();
	case K_RETURN:    return return_statement();
	case K_BREAK:     return break_statement();
	case K_CONTINUE:  return continue_statement();
	case K_SWITCH:    return switch_statement();
	case K_CASE:      return case_statement();
	case K_DEFAULT:   return default_statement();
	};
	if (accept(TIDENTIFIER) && peek(1) == OP_COLON)
		return labeled_statement();
	return expression_statement();
}

//
// compound_statement := '{' '}'
//                    |  '{' declaration_or_statement_list '}'
//                    |  '{' declaration_or_statement_list <TOK_ERROR> '}'
//
AstNode* compound_statement(void) {

	Vector* stmt;
	AstNode* cmpnd;

	_scope = _symbols->enterScope(_scope);
	next();
	stmt = NewVector(sizeof(AstNode));
	if (!accept(OP_RBRACE)) {
		while (TRUE) {
			if (accept(OP_RBRACE))
				break;
			if (accept(TEOF))
				Fatal(PREMATURE_EOF, gettok().loc);
			_declaration_or_statement(stmt);
			if (accept(TOK_ERROR)) {
				// consume tokens until we reach ';','}',EOF,or
				// a keyword:
				while (get() != OP_SEMICOLON
					&& get() != TEOF) {
					if (get() == OP_RBRACE)
						break;
					if (get() == K_WHILE)
						break;
					next();
				}
				if (gettok().type == TEOF)
					Fatal(PREMATURE_EOF, gettok().loc);
			}
		}
	}
	if (expect(1,OP_RBRACE))
		next();
	cmpnd = NewCompoundStatement(stmt, _scope);
	_scope = _symbols->exitScope(_scope);
	return cmpnd;
}

//
// selection_statement := IF '(' expression ')' statement
//                      | IF '(' expression ')' statement ELSE statement
//
AstNode* if_statement(void) {

	AstNode* cond, *then, *els;

	cond = then = els = NULL;
	next();
	if (expect(1,OP_LPAREN)) {
		next();
		cond = expression();
		if (expect(1,OP_RPAREN))
			next();
		then = statement();
		if (accept(K_ELSE)) {
			next();
			els = statement();
		}
	}
	return NewIf(cond,then,els);
}

AstNode* goto_statement(void) {

	char* s = NULL;
	next();
	if (expect(1,TIDENTIFIER)) {
		s = gettok().val.s;
		next();
	}
	else
		__debugbreak();
	if (expect(1,OP_SEMICOLON))
		next();
	return NewGoto(s);
}

AstNode* continue_statement(void) {

	next();
	if(expect(1,OP_SEMICOLON))
		next();
	return NewContinue();
}

AstNode* break_statement(void) {

	next();
	if(expect(1,OP_SEMICOLON))
		next();
	return NewBreak();
}

AstNode* return_statement(void) {

	AstNode* retval;

	retval = NULL;
	next();
	if (accept(OP_SEMICOLON)) {
		next();
		return NewReturn(NULL);
	}
	retval = expression();
	if (expect(1,OP_SEMICOLON))
		next();
	return NewReturn(retval);
}

AstNode* case_statement(void) {

	AstNode* expr;
	AstNode* stmt;
	AstNode* node;
	int      value;

	if (VectorLength(&_switch) == 0)
		Error(PARSE_SWITCH_LABEL, gettok().loc, "case");
	next();
	expr = constant_expression();
	if (expect(1,OP_COLON))
		next();
	stmt = statement();
	value = Evaluate(expr);
	node = NewCase(InternString(intToStr(value)), stmt);
	if (VectorLength(&_switch) > 0){
		VectorLastElement(&_switch, &node->caseStmt.parentSwitch);
		if (VectorLength(&node->caseStmt.parentSwitch->switchStmt.cases) == 0) {
			InitVector(&node->caseStmt.parentSwitch->switchStmt.cases, sizeof (AstNode*));
			if (node->caseStmt.parentSwitch->switchStmt.defCase == NULL)
				node->caseStmt.isFirst = TRUE; // first case in switch-case chain
		}
		VectorPush(&node->caseStmt.parentSwitch->switchStmt.cases, &node);
	}
	return node;
}

AstNode* default_statement(void) {

	AstNode* node;

	if (VectorLength(&_switch) == 0)
		Error(PARSE_SWITCH_LABEL, gettok().loc, "default");
	// continue reading (even though its not allowed)
	// to make parser happy:
	next();
	if (expect(1,OP_COLON))
		next();
	node = NewDefault(statement());
	if (VectorLength(&_switch) > 0) {
		VectorLastElement(&_switch, &node->defaultStmt.parentSwitch);
		if (node->caseStmt.parentSwitch->switchStmt.defCase)
			Error(PARSE_SWITCH_DUPLICATE, gettok().loc, "default");
		else if (VectorLength(&node->caseStmt.parentSwitch->switchStmt.cases) == 0)
			node->defaultStmt.isFirst = TRUE; // this is first case in switch-case chain
		node->caseStmt.parentSwitch->switchStmt.defCase = node;
	}
	return node;
}

AstNode* labeled_statement(void) {

	char* s;
	AstNode* label;
	AstNode* stmt;
	Vector*  stmts;
	Value*   symbol;

	s = gettok().val.s;
	next();
	if (expect(1,OP_COLON))
		next();

	symbol = _symbols->lookup(_scope, s, NS_LABELS);
	if (symbol)
		Error(PARSE_LABEL_REDEFINE, gettok().loc, s);
	else {
		_symbols->insert(_scope, s, NS_LABELS, NULL);
		symbol = _symbols->lookup(_scope, s, NS_LABELS);
	}

	label = NewLabel(s,symbol);
	stmt = statement();

	stmts = NewVector(sizeof(AstNode*));
	VectorPush(stmts, &label);
	if (stmt)
		VectorPush(stmts, &stmt);

	return NewCompoundStatement(stmts, _scope);
}

//
// <expression-statement> ::= {<expression>}? ;
//
AstNode* expression_statement(void) {

	AstNode* expr = expression();
	if (expect(1, OP_SEMICOLON))
		next();
	return expr;
}

//
// selection_statement := SWITCH '(' expression ')' statement
//
AstNode* switch_statement(void) {

	AstNode* expr;
	AstNode* stmt;
	AstNode* node;
	int i = 0;

	next();
	if (!expect(1,OP_LPAREN))
		return NULL;
	next();
	expr = expression();
	if (expect(1,OP_RPAREN))
		next();

	node = NewSwitch(expr);

	VectorPush(&_switch, &node);
	stmt = statement();
	VectorPop(&_switch, &node);

	node->switchStmt.stmt = stmt;
	return node;
}

//
// iteration_statement := WHILE '(' expression ')' statement
//
AstNode* while_statement(void) {

	AstNode* expr;
	AstNode* stmt;

	next();
	if(expect(1,OP_LPAREN))
		next();
	expr = expression();
	if (expect(1,OP_RPAREN))
		next();
	stmt = statement();
	return NewWhile(expr, stmt);
}

//
// iteration_statement := DO statement WHILE '(' expression ')' ';'
//
AstNode* do_statement(void) {

	AstNode* expr;
	AstNode* stmt;

	next();
	stmt = statement();
	expr = NULL;
	if (expect(1,K_WHILE)) {
		next();
		if (expect(1,OP_LPAREN)) {
			next();
			expr = expression();
			if (!expr)
				Error(MESSAGE, "Expected expression");
			if (expect(1,OP_RPAREN))
				next();
			if (expect(1,OP_SEMICOLON))
				next();
		}
	}
	return NewDoWhile(expr, stmt);
}

//
// iteration_statement := FOR '(' expression_statement expression_statement ')' statement
//                      | FOR '(' expression_statement expression_statement expression ')' statement
//
AstNode* for_statement(void) {

	AstNode* init;
	AstNode* cond;
	AstNode* eval;
	AstNode* stmt;

	init = cond = eval = stmt = NULL;
	next();
	if (expect(1,OP_LPAREN)) {
		next();
		init = expression();
		if (expect(1,OP_SEMICOLON))
			next();
		cond = expression();
		if (expect(1,OP_SEMICOLON))
			next();
		eval = expression();
		if (expect(1,OP_RPAREN))
			next();
		stmt = statement();
	}
	return NewFor(init,cond,eval,stmt);
}

//
// <translation-unit> ::= {<external-declaration>}*
//
Vector* translation_unit(void) {

	if (accept(TEOF))
		return NULL;
	return external_declaration();
}

PRIVATE void InitAstNode(IN AstNode* node) {

	memset(node, 0, sizeof(AstNode));
}

/* translation_unit returns a vector of declarations due to the
comma operator so we treat the vector as an AstNode pushback buffer. */

PRIVATE Vector*  _declBuffer;
PRIVATE AstNode* _currentDecl;

/* called by Parse. When we consume a new declaration list in _declBuffer,
we can free the old one as well as all of its AST's. */
PRIVATE void FreeAstVector(void) {

	FreeVector(_declBuffer);
}

/* test if _currentDecl is the last element. */
PRIVATE BOOL HasNextDeclaration(void) {

	return _declBuffer != NULL && VectorNext(_declBuffer, _currentDecl) != NULL;
}

/* consume the next declaration list. This calls the actual parser.
and sets _declBuffer and _currentDecl to start of buffer. Returns NULL
if translation_unit returns NULL (that is, out of tokens.) */
PRIVATE Vector* NextDeclarationList(void) {

	// bugfix: if elementCount==0 then VectorFirst returns NULL
	// which gets confused as EOF. so if it is 0, try again. It
	// may be 0 if user does forward references like "struct myStruct;"
	// which has no variable declaration.
	while (TRUE) {
		_declBuffer = translation_unit();
		if (!_declBuffer)
			return NULL;
		if (_declBuffer->elementCount == 0)
			continue;
		_currentDecl = VectorFirst(_declBuffer);
		return _declBuffer;
	}
}

/* returns the next declaration and updates _currentDecl */
PRIVATE AstNode* NextDeclaration(void) {

	_currentDecl = VectorNext(_declBuffer, _currentDecl);
	return _currentDecl;
}

/* if another declaration exists in the pushback buffer, return it.
Else, consume the next declaration list and return the first item from it.
If we are consuming a new declaration list, this will also free the old one
as well as the AST's in the list. */
PRIVATE AstNode* ParseInternal(void) {

	AstNode* decl;

	if (!_scan) {
		_scan = GetPreprocessor();
		_symbols = GetSymbolTable();
		_scope = GetSymbolTable()->enterScope(NULL);
		_astCache = NewCacheEx("AST", sizeof(AstNode), InitAstNode);
		next();
	}

	InitVector(&_switch, sizeof(AstNode*));
	if (!HasNextDeclaration()) {
		if (_declBuffer)
			FreeAstVector();
		if (!NextDeclarationList())
			return NULL;
		decl = _currentDecl;
	}
	else
		decl = NextDeclaration();
	return decl;
}

PRIVATE Parser _parser = {

	"C Parser",
	ParseInternal
};

PUBLIC Parser* GetParser(void) {

	return &_parser;
}

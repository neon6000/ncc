/************************************************************************
*
*	msg.h - NCC Message logging
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#ifndef MSG_H
#define MSG_H

/* scanner messages. */
#define MSG_SCANNER               1000
/* front-end messages (parser, semantic checks, preprocessor). */
#define MSG_PARSER                2000
/* symbol table related messages */
#define MSG_SYMBOL                3000
/* intermediate language */
#define MSG_IL                    4000
/* first optomization pass (on NCC IL) messages */
#define MSG_OPT1                  5000
/* code generator messages */
#define MSG_GEN                   6000
/* output file messages */
#define MSG_OUT                   7000
/* internal and system messages. */
#define MSG_SYSTEM                10000

/* system messages */
#define MESSAGE                   MSG_SYSTEM + 1
#define FILENOTFOUND              MSG_SYSTEM + 2
#define INTERNAL_MSG_FORMAT_LIMIT MSG_SYSTEM + 3
#define INTERNAL_MSG_UNKNOWN      MSG_SYSTEM + 4

/* message type. */
typedef struct Message {
	int code;
	char* msg;
}Message;

/* message format callback. */
typedef void(*msg_handler_t)(IN void* outstream, IN va_list* args, IN OPTIONAL void* descriptor);

/* PUBLIC definitions. */
extern void         Info                     (IN int code, ...);
extern void         Warning                  (IN int code, IN ...);
extern void         Error                    (IN int code, IN ...);
extern void         Fatal                    (IN int code, ...);
extern void         vInfo                    (IN int code, IN va_list args);
extern void         vError                   (IN int code, IN va_list args);
extern void         vWarning                 (IN int code, IN va_list args);
extern void         vFatal                   (IN int code, IN va_list args);
extern unsigned int GetErrorCount            (void);
extern unsigned int GetWarningCount          (void);
extern void         RegisterFormat           (IN char* fmt, IN msg_handler_t);
extern void         RegisterMessages         (IN Message* msgs);
extern void         RegisterInternalMessages (void);
extern void         FlushMessages            (void);

#endif

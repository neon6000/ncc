/************************************************************************
*
*	msg.c - NCC Message logging
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/*
	This component implements the message and logging protocol.
*/

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "ncc.h"
#include "msg.h"
#include "icode.h"

/* Private Definitions **********/

#define FATAL 1
#define ERROR 2
#define WARN  4
#define INFO  8

/* global message list max size. */
PRIVATE unsigned int _maxErrorCount = 100;

/* global error count. */
PRIVATE unsigned int _errors;

/* global warning count. */
PRIVATE unsigned int _warnings;

/* global info count. */
PRIVATE unsigned int _infoCount;

/* global message map. */
PRIVATE Map*         _messages;

/* global message descriptors. */
PRIVATE Vector       _descriptors;

/* message descriptor. */
typedef struct MessageDescriptor {
	Location loc;
	char*    msg;
}MessageDescriptor;

/* message format type. */
typedef struct MessageFormat {
	char* format;
	msg_handler_t callback;
}MessageFormat;

/*
	Internal and system-wide messages.
*/
PRIVATE Message _msg[] = {

	{ MESSAGE, "Message: %s" },
	{ FILENOTFOUND, "Unable to find file '%s'" },
	{ INTERNAL_MSG_FORMAT_LIMIT, "internal: attempt to register message format '%s' exceeds limit" },
	{ INTERNAL_MSG_UNKNOWN, "internal: unknown message with id '%i'" },
	{ 0, NULL }
};

PRIVATE void PrintLocation(IN Buffer* buf, IN Location loc, MessageDescriptor* desc) {

	if (loc.src) {
		BufferPrintf(buf, "%s(%i)", loc.src->fname, loc.line);
		desc->loc.src = loc.src;
		desc->loc.line = loc.line;
	}
	else
		BufferPrintf(buf, "%s", "<invalid location>");
}

PRIVATE void MsgDisplayLocation(IN Buffer* buf, IN va_list* args, OUT OPTIONAL MessageDescriptor* desc) {

	PrintLocation(buf, va_arg(*args, Location), desc);
}

PRIVATE void MsgDisplayType(IN Buffer* buf, IN va_list* args, OUT OPTIONAL MessageDescriptor* desc) {

	printf("CALL to msg.c DisplayType: update this");
	__debugbreak();
	PrintType(buf, va_arg(*args, Type*));
}

PRIVATE void MsgDisplayString(IN Buffer* buf, IN va_list* args, OUT OPTIONAL MessageDescriptor* desc) {

	BufferPrintf(buf, "%s", va_arg(*args, char*));
}

PRIVATE void MsgDisplayLong(IN Buffer* buf, IN va_list* args, OUT OPTIONAL MessageDescriptor* desc) {

	BufferPrintf(buf, "%i", va_arg(*args, long));
}

PRIVATE void MsgDisplayUnsignedLong(IN Buffer* buf, IN va_list* args, OUT OPTIONAL MessageDescriptor* desc) {

	BufferPrintf(buf, "%u", va_arg(*args, unsigned long));
}

PRIVATE void MsgDisplayHexLower(IN Buffer* buf, IN va_list* args, OUT OPTIONAL MessageDescriptor* desc) {

	BufferPrintf(buf, "%x", va_arg(*args, unsigned long));
}

PRIVATE void MsgDisplayHexUpper(IN Buffer* buf, IN va_list* args, OUT OPTIONAL MessageDescriptor* desc) {

	BufferPrintf(buf, "%X", va_arg(*args, unsigned long));
}

PRIVATE void MsgDisplayChar(IN Buffer* buf, IN va_list* args, OUT OPTIONAL MessageDescriptor* desc) {

	BufferPrintf(buf, "%c", va_arg(*args, char));
}

PRIVATE void MsgDisplayPercent(IN Buffer* buf, IN va_list* args, OUT OPTIONAL MessageDescriptor* desc) {

	BufferPrintf(buf, "%s", "%");
}

PRIVATE MessageFormat _formats[] = {

	{ "%%", MsgDisplayPercent },
	{ "%c", MsgDisplayChar },
	{ "%x", MsgDisplayHexLower },
	{ "%X", MsgDisplayHexUpper },
	{ "%u", MsgDisplayUnsignedLong },
	{ "%i", MsgDisplayLong },
	{ "%s", MsgDisplayString },
	{ "%y", MsgDisplayType },
	{ "%z", MsgDisplayLocation },
	{ NULL, NULL }, /* may be filled by RegisterFormat */
	{ NULL, NULL },
	{ NULL, NULL },
	{ NULL, NULL },
	{ NULL, NULL },
	{ NULL, NULL }
};

PRIVATE MessageFormat* GetFormat(IN char* s) {

	int max;
	int i;

	max = sizeof(_formats) / sizeof(MessageFormat);
	for (i = 0; _formats[i].format && i < max; i++) {
		if (strncmp(_formats[i].format, s, 2) == 0)
			return &_formats[i];
	}

	return NULL;
}

PRIVATE char* GetPriorityString(IN int priority) {

	switch (priority) {
	case ERROR: return "error";
	case WARN: return "warning";
	case FATAL: return "fatal";
	default: return "info";
	};
}

PRIVATE void DisplayMessageCode(IN Buffer* buf, IN char* type, IN int code) {

	BufferPrintf(buf, "\n\r%s C%04i: ", type, code);
}

PRIVATE char* GetMessage(IN int code) {

	return (char*)MapGet(_messages, (bucket_key_t)code);
}

PRIVATE void Print(IN int priority, IN int code, IN va_list args) {

	char*             msg;
	char*             s;
	char*             type;
	MessageFormat*    spec;
	Buffer            buf;
	MessageDescriptor desc;

	if (_errors >= _maxErrorCount) {
		FlushMessages();
		fprintf(stderr, "\n\n\r*** exceeded maximum error count, aborting.");
		__debugbreak();
		abort();
	}

	InitBuffer(&buf);
	type = GetPriorityString(priority);
	msg  = GetMessage(code);
	s    = msg;

	desc.msg = NULL;
	desc.loc.line = 0;
	desc.loc.src = NULL;

	if (priority == ERROR || priority == FATAL)
		_errors++;
	else if (priority == WARN)
		_warnings++;
	else if (priority == INFO)
		_infoCount++;

	if (!msg)
		Fatal(INTERNAL_MSG_UNKNOWN, code);

	DisplayMessageCode(&buf, type, code);

	while (*s) {
		if (*s == '%') {
			spec = GetFormat(s);
			if (spec && spec->callback) {
				spec->callback(&buf, &args, &desc);
				s += strlen(spec->format);
				continue;
			}
		}
		BufferPrintf(&buf, "%c", *s++);
	}
	BufferPrintf(&buf, "\0");

	if (_descriptors.elementSize == 0)
		InitVector(&_descriptors, sizeof(MessageDescriptor));

	desc.msg = buf.data;
	VectorPush(&_descriptors, &desc);
}

/* Public Definitions ********/

PUBLIC void vInfo(IN int code, IN va_list args) {

	Print(INFO, code, args);
}

PUBLIC void vError(IN int code, IN va_list args) {

	Print(ERROR, code, args);
}

PUBLIC void vWarning(IN int code, IN va_list args) {

	Print(WARN, code, args);
}

PUBLIC void vFatal(IN int code, IN va_list args) {

	Print(FATAL, code, args);
	FlushMessages();
	__debugbreak();
	exit(-1);
}

PUBLIC void Info(IN int code, ...) {

	va_list args;
	va_start(args, code);
	vInfo(code, args);
	va_end(args);
}

PUBLIC void Warning(IN int code, IN ...) {

	va_list args;
	va_start(args, code);
	vWarning(code, args);
	va_end(args);
}

PUBLIC void Error(IN int code, IN ...) {

	va_list args;
	va_start(args, code);
	vError(code, args);
	va_end(args);
}

PUBLIC void Fatal(IN int code, ...) {

	va_list args;
	va_start(args, code);
	vFatal(code, args);
	va_end(args);
}

PUBLIC unsigned int GetErrorCount(void) {

	return _errors;
}

PUBLIC unsigned int GetWarningCount(void) {

	return _warnings;
}

PUBLIC void RegisterFormat(IN char* fmt, IN msg_handler_t handler) {

	int i;
	int max;

	i = 0;
	max = sizeof(_formats) / sizeof(MessageFormat);
	while (_formats[i].format && i<max)
		i++;
	if (i == max)
		Fatal(INTERNAL_MSG_FORMAT_LIMIT, fmt);
	_formats[i].format = fmt;
	_formats[i].callback = handler;
}

PUBLIC void RegisterMessages(IN Message* messages) {

	Message* msg;

	if (!_messages)
		_messages = NewMapNumeric();

	for (msg = messages; msg->code; msg++)
		MapPut(_messages, (bucket_key_t) msg->code, msg->msg);
}

PUBLIC void RegisterInternalMessages(void) {

	RegisterMessages(_msg);
}

PRIVATE int CompareMessageDesc(IN const MessageDescriptor* a, IN const MessageDescriptor* b) {

	if (a->loc.line > b->loc.line)
		return 1;
	else if (a->loc.line < b->loc.line)
		return -1;
	return 0;
}

PUBLIC void FlushMessages(void) {

	MessageDescriptor* desc;

	// sort by line number:
	qsort(VectorData(&_descriptors),
		VectorLength(&_descriptors),
		sizeof (MessageDescriptor),
		CompareMessageDesc);

	// display all messages:
	for (desc = VectorFirst(&_descriptors); desc; desc = VectorNext(&_descriptors, desc))
		fprintf(stderr, desc->msg);

	// free vector:
	for (desc = VectorFirst(&_descriptors); desc; desc = VectorNext(&_descriptors, desc))
		Free(desc->msg);
	FreeVector(&_descriptors);
	InitVector(&_descriptors, sizeof(MessageDescriptor));
}


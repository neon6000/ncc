/************************************************************************
*
*	lex.c - Scanner
*
*   Copyright(c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This is a work in progress ------------------------- */

/*
	This component is responsible for implementing the token scanner. It returns
	Preprocessor Tokens which include all standard C tokens and additional
	tokens processed by the Preprocessor. It also implements the Lexer Hack
	used for implementing TYPEDEF.
*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include "ncc.h"
#include "icode.h"
#include "c.h"

PRIVATE FileManager* _fm;
PRIVATE SymbolTable* _symbols;
PRIVATE BOOL         _enableDigraphs = FALSE;
PRIVATE TokenVector* _pushback;
PRIVATE Vector*      _tokenStreams;
PRIVATE Location     _location;

extern  Scope*       _scope; // declared in parse.c. For lexer hack.

PRIVATE Keyword _operators[] = {

	{ OP_ELLIPSES, "..." },
	{ OP_LEFT_ASSIGN, "<<=" },
	{ OP_RIGHT_ASSIGN, ">>=" },
	{ OP_LOG_OR, "||" },
	{ OP_LOG_AND, "&&" },
	{ OP_PTR, "->" },
	{ OP_DEC, "--" },
	{ OP_INC, "++" },
	{ OP_SHIFT_LEFT, "<<" },
	{ OP_SHIFT_RIGHT, ">>" },
	{ OP_ISNOTEQ, "!=" },
	{ OP_ISEQ, "==" },
	{ OP_OR_ASSIGN, "|=" },
	{ OP_XOR_ASSIGN, "^=" },
	{ OP_AND_ASSIGN, "&=" },
	{ OP_MOD_ASSIGN, "%=" },
	{ OP_DIV_ASSIGN, "/=" },
	{ OP_MUL_ASSIGN, "*=" },
	{ OP_SUB_ASSIGN, "-=" },
	{ OP_ADD_ASSIGN, "+=" },
	{ OP_LTE, "<=" },
	{ OP_GTE, ">=" },
	{ OP_HASHHASH, "##" },
	{ OP_DOT, "." },
	{ OP_LPAREN, "(" },
	{ OP_RPAREN, ")" },
	{ OP_LBRACKET, "[" },
	{ OP_RBRACKET, "]" },
	{ OP_LBRACE, "{" },
	{ OP_RBRACE, "}" },
	{ OP_EQ, "=" },
	{ OP_SEMICOLON, ";" },
	{ OP_COLON, ":" },
	{ OP_PLUS, "+" },
	{ OP_MINUS, "-" },
	{ OP_DIV, "/" },
	{ OP_SLASH, "\\" },
	{ OP_MUL, "*" },
	{ OP_MODULO, "%" },
	{ OP_QUESTION, "?" },
	{ OP_NOT, "!" },
	{ OP_OR, "|" },
	{ OP_AND, "&" },
	{ OP_COMPL, "~" },
	{ OP_XOR, "^" },
	{ OP_LESS, "<" },
	{ OP_GREATER, ">" },
	{ OP_COMMA, "," },
	{ OP_HASH, "#" },
	{ 0, NULL }
};

PRIVATE Keyword _keywords[] = {
	{ K_ALIGNAS, "alignas" },
	{ K_ALIGNOF, "alignof" },
	{ K_ATOMIC, "atomic" },
	{ K_AUTO, "auto" },
	{ K_BOOL, "_Bool" },
	{ K_BREAK, "break" },
	{ K_CASE, "case" },
	{ K_CHAR, "char" },
	{ K_COMPLEX, "_Complex" },
	{ K_CONST, "const" },
	{ K_CONTINUE, "continue" },
	{ K_DEFAULT, "default" },
	{ K_DO, "do" },
	{ K_DOUBLE, "double" },
	{ K_ELSE, "else" },
	{ K_ENUM, "enum" },
	{ K_EXTERN, "extern" },
	{ K_FLOAT, "float" },
	{ K_FOR, "for" },
	{ K_GENERIC, "generic" },
	{ K_GOTO, "goto" },
	{ K_IF, "if" },
	{ K_IMAGINARY, "_Imaginary" },
	{ K_INLINE, "inline" },
	{ K_INT, "int" },
	{ K_LONG, "long" },
	{ K_NORETURN, "noreturn" },
	{ K_REGISTER, "register" },
	{ K_RESTRICT, "restrict" },
	{ K_RETURN, "return" },
	{ K_SHORT, "short" },
	{ K_SIGNED, "signed" },
	{ K_SIZEOF, "sizeof" },
	{ K_STATIC, "static" },
	{ K_STATIC_ASSERT, "static_assert" },
	{ K_STRUCT, "struct" },
	{ K_SWITCH, "switch" },
	{ K_THREAD_LOCAL, "thread_local" },
	{ K_TYPEDEF, "typedef" },
	{ K_UNION, "union" },
	{ K_UNSIGNED, "unsigned" },
	{ K_VOID, "void" },
	{ K_VOLATILE, "volatile" },
	{ K_WHILE, "while" },
	{ 0, 0 }
};

PRIVATE Token        InitToken(IN TokenType type);
PRIVATE Token        InitNumberToken(IN char* val);
PRIVATE Token        InitStringToken(IN char* val);
PRIVATE Token        InitIdentToken(IN char* val);
PRIVATE Token        InitCharToken(IN char c);
PRIVATE Token        InitKeywordToken(IN int id);
PRIVATE Token        InitEofToken(void);
PRIVATE Token        InitInvalidToken(void);
PRIVATE BOOL         SkipWhitespace(void);
PRIVATE Token        ScanCharacters(void);
PRIVATE char*        GetOperatorById(IN int id);
PRIVATE Token        ScanOperators(void);
PRIVATE Token        ScanNumber(void);
PRIVATE int          ScanHexEscapeSequence(void);
PRIVATE int          ScanUniversalCharEscapeSequence(int len);
PRIVATE int          ScanEscapeSequence(void);
PRIVATE Token        ScanStringLiteral(void);
PRIVATE void         ScanSingleLineComment(void);
PRIVATE void         ScanMultilineComment(void);
PRIVATE Token        ScanCharacterLiteral(void);
PRIVATE int          ScanDigraph(void);
PRIVATE void         PushTokenStream(IN TokenVector** tok);
PRIVATE TokenVector* PopTokenStream(void);
PRIVATE TokenVector* GetTokenStream(void);
PRIVATE Token        GetToken(void);
PRIVATE void         UngetToken(IN Token tok);
PRIVATE Token        PeekToken(IN int index);
PRIVATE void         ScannerInit(IN FileManager* fm);
PRIVATE char*        TokenToString(IN Token tok);
PUBLIC  Scanner*     GetScanner(void);

/**
*	InitToken
*
*	Description : Initializes a token.
*/
PRIVATE Token InitToken(IN TokenType type) {

	Token tok;
	File* file;
	memset(&tok, 0, sizeof(Token));
	tok.type = type;
	file = _fm->current();
	if (file) {
		tok.loc.line = file->line;
		tok.loc.src = file;
	}
	return tok;
}

/**
*	InitNumberToken
*
*	Description : Creates TNUMBER token types.
*/
PRIVATE Token InitNumberToken(IN char* val) {

	Token tok = InitToken(TNUMBER);
	tok.val.s = val;
	return tok;
}

/**
*	InitStringToken
*
*	Description : Creates TT_STRING token types.
*/
PRIVATE Token InitStringToken(IN char* val) {

	Token tok = InitToken(TSTRING);
	tok.val.s = val;
	return tok;
}

/**
*	InitIdentToken
*
*	Description : Creates TT_IDENT token types.
*/
PRIVATE Token InitIdentToken(IN char* val) {

	Token tok = InitToken(TIDENTIFIER);
	tok.val.s = val;
	return tok;
}

/**
*	InitTypenameToken
*
*	Description : Creates TYPENAME token. This is an identifier
*	that must be processed as a user defined type name.
*/
PRIVATE Token InitTypenameToken(IN char* val) {

	Token tok = InitToken(TYPENAME);
	tok.val.s = val;
	return tok;
}

/**
*	InitCharToken
*
*	Description : Creates TCHAR token types.
*/
PRIVATE Token InitCharToken(IN char c) {

	Token tok = InitToken(TCHAR);
	tok.val.c = c;
	return tok;
}

/**
*	InitKeywordToken
*
*	Description : Creates TT_KEYWORD token types.
*/
PRIVATE Token InitKeywordToken(IN int id) {

	return InitToken(id);
}

/**
*	InitEofToken
*
*	Description : Creates TT_EOF token types.
*/
PRIVATE Token InitEofToken(void) {

	return InitToken(TEOF);
}

PRIVATE Token InitEolToken(void) {

	return InitToken(TEOL);
}

PRIVATE Token InitSpaceToken(void) {

	return InitToken(TSPACE);
}

PRIVATE Token InitPpHeaderToken(IN char* s, IN BOOL std) {

	Token tok = InitToken(THEADER);
	tok.val.s = s;
	tok.val.b = std;
	return tok;
}

/**
*	InitInvalidToken
*
*	Description : Creates TT_INVALID token types.
*/
PRIVATE Token InitInvalidToken(void) {

	return InitToken(TINVALID);
}

/**
*	SkipWhitespace
*
*	Description : Skips over whitespace.
*/
PRIVATE BOOL SkipWhitespace(void) {

	BOOL ws = FALSE;
	int c = _fm->peek(1);
	if (isspace(c) && c != EOF)
		ws = TRUE;
	for (c = _fm->peek(1); isspace(c) && c != EOF && c != '\n'; c = _fm->peek(1))
		c = _fm->get();
	return ws;
}

/**
*	ScanCharacters
*
*	Description : Scans identifiers and keywords.
*
*	Output - TT_IDENT or TT_KEYWORD token type.
*/
PRIVATE Token ScanCharacters(void) {

	Buffer* ident;
	Token tok;
	Value* sym;
	int i = 0;
	int c;

	ident = NewBuffer();
	for (c = _fm->peek(1); (isalnum(c) || c == '_') && c != EOF && i < 127; c = _fm->peek(1))
		BufferPush(ident, _fm->get());
	BufferPush(ident, 0);

	sym = _symbols->lookup(_scope, GetBufferData(ident), NS_IDENTS);
	if (sym && ON(sym->type->sc, S_TYPEDEF))
		tok = InitTypenameToken(GetBufferData(ident));
	else
		tok = InitIdentToken(GetBufferData(ident));

	BufferFreePreserve(ident);
	return tok;
}

/**
*	GetOperatorById
*
*	Description : Given an operator token type, this returns its name.
*
*	Input - Ooperator token type id.
*
*	Output - Pointer to operator name string or NULL if id is invalid.
*/
PRIVATE char* GetOperatorById(IN int id) {

	Keyword* kw;
	for (kw = _operators; kw && kw->k; kw++) {
		if (kw->t == id)
			return kw->k;
	}
	return NULL;
}

PRIVATE char* GetKeywordById(IN int id) {

	Keyword* kw;
	for (kw = _keywords; kw && kw->k; kw++) {
		if (kw->t == id)
			return kw->k;
	}
	return NULL;
}

/**
*	ScanOperators
*
*	Description : Scans operator characters.
*
*	Output - The corrosponding operator token type.
*/
PRIVATE Token ScanOperators(void) {

	Keyword* key;
	char     op[4];
	Token    tok;
	int      i;
	int      c;

	/*
	Read at most 3 characters for the operator.
	*/
	for (i = 0, c = _fm->peek(1); ispunct(c) && c != EOF && i < 3; c = _fm->peek(1))
		op[i++] = _fm->get();
	op[i] = 0;

	/*
	Is this a valid operator?
	*/
	for (key = _operators; key->k; key++) {
		if (!strncmp(key->k, op, strlen(key->k)))
			break;
	}
	/*
	If this is an invalid operator, ignore it and return
	an invalid token.
	*/
	if (key->t == TINVALID) {
		while (--i)
			_fm->unget(op[i]);
		_fm->unget(op[0]);
		return InitToken(TINVALID);
	}
	tok = InitToken(key->t);
	/*
	If the operator matched has more characters then consumed,
	we need to put the characters not part of this operator back.
	*/
	i = strlen(key->k);
	c = strlen(op);
	if (c > i) {
		while (c != i)
			_fm->unget(op[--c]);
	}

	return tok;
}

/**
*	ScanNumber
*
*	Description : Scans numerical characters.
*
*	Output - TNUMBER token.
*/
PRIVATE Token ScanNumber(void) {

	char num[16];
	BOOL ishex;
	char c;
	int  i;

	i = 0;
	ishex = FALSE;
	if (_fm->peek(1) == '0' && tolower(_fm->peek(2)) == 'x')
		ishex = TRUE;
	for (c = _fm->peek(1); (isalpha(c) || isdigit(c) || c == '.') && c != EOF && i < 15; c = _fm->peek(1)) {
		num[i++] = _fm->get();
		if (!ishex && strchr("eEpP", c) && strchr("+-", _fm->peek(1)))
			num[i++] = _fm->get();
	}
	num[i] = 0;
	return InitNumberToken(_strdup(num));
}

/**
*	ScanHexEscapeSequence
*
*	Description : Scan and translates /x escape sequence.
*
*	Output - Translated character or 0 if escape sequence is invalid.
*/
PRIVATE int ScanHexEscapeSequence(void) {

	int c;
	int result;
	while (c = _fm->get()) {
		if (c >= '0' && c <= '9')
			result = (result << 4) | (c - '0');
		else if (c >= 'a' && c <= 'f')
			result = (result << 4) | (c - 'a' + 10);
		else if (c >= 'A' && c <= 'F')
			result = (result << 4) | (c - 'A' + 10);
		else{
			_fm->unget(c);
			return result;
		}
	}
	return 0;
}

/**
*	ScanUniversalCharEscapeSequence
*
*	Description : Scan and translates /u and /U escape sequences.
*
*	Output - Translated character or 0 if escape sequence is invalid.
*/
PRIVATE int ScanUniversalCharEscapeSequence(int len) {

	int c;
	int result;
	int i;
	for (i = 0; i < len; i++) {
		c = _fm->get();
		if (c >= '0' && c <= '9')
			result = (result << 4) | (c - '0');
		else if (c >= 'a' && c <= 'f')
			result = (result << 4) | (c - 'a' + 10);
		else if (c >= 'A' && c <= 'F')
			result = (result << 4) | (c - 'A' + 10);
		else{
			Error(LEX_UCS_INVALID, c);
			_fm->unget(c);
			return result;
		}
	}
	return 0;
}

/**
*	ScanEscapeSequence
*
*	Description : Scan and translates escape sequences.
*
*	Output - Translated character or 0 if escape sequence is invalid.
*/
PRIVATE int ScanEscapeSequence(void) {

	int c = _fm->get();
	switch (c) {
	case '\'': case '"': case '?': case '\\': return c;
	case 'a': return '\a';
	case 'b': return '\b';
	case 'f': return '\f';
	case 'n': return '\n';
	case 'r': return '\r';
	case 't': return '\t';
	case 'v': return '\v';
	case 'e': return '\033';
	case 'x': return ScanHexEscapeSequence();
	case 'u': return ScanUniversalCharEscapeSequence(4);
	case 'U': return ScanUniversalCharEscapeSequence(8);
	};
	Error(LEX_ESCAPE_INVALID, c);
	return 0;
}

/**
*	ScanStringLiteral
*
*	Description : Scans string literals. This procedure also translates
*	              escape characters.
*
*	Output - TT_STRING token.
*/
PRIVATE Token ScanStringLiteral(void) {

	Buffer* s;
	Token tok;
	char c;
	int i;

	s = NewBuffer();

	_fm->get(); // consume first '"'

	for (i = 0, c = _fm->peek(1); c != '"' && c != EOF && c != '\n'; c = _fm->peek(1), i++) {

		if (c == '\\') {
			_fm->get();
			char e = ScanEscapeSequence();
			if (e)
				BufferPush(s, e);
		}
		else{
			BufferPush(s, _fm->get());
		}
	}
	BufferPush(s, 0);

	if (c == '\n')      Error(MESSAGE, "ScanString: Trailing newline in string");
	else if (c == EOF)  Error(MESSAGE, "ScanString: EOF reached before end of string");
	else if (c != '"')  Error(MESSAGE, "ScanString: Trailing string");

	_fm->get(); // consume last '"'

	tok = InitStringToken(GetBufferData(s));
	BufferFreePreserve(s);
	return tok;
}

/*
	The source file is decomposed into preprocessing tokens) and sequences of
	white-space characters (including comments). A source file shall not end in a
	partial preprocessing token or in a partial comment. Each comment is replaced by
	one space character. New-line characters are retained. Whether each nonempty
	sequence of white-space characters other than new-line is retained or replaced by
	one space character is implementation-defined.
*/

/**
*	ScanSingleLineComment
*
*	Description : Scans and skips over single line comments.
*/
PRIVATE void ScanSingleLineComment(void) {

	char c = 0;
	while (c != '\n' && c != EOF)
		c = _fm->get();
}

/**
*	ScanMultilineComment
*
*	Description : Scans and skips over multiline comments.
*/
PRIVATE void ScanMultilineComment(void) {

	char c = 0;
	File* start = _fm->current();
	while (c != EOF) {
		c = _fm->get();
		if (c == '*' && _fm->peek(1) == '/')
			break;
	}

	if (c == EOF)
		Error(LEX_COMMENT_EOF);

	if (c == '*' && _fm->peek(1) == '/')
		_fm->get();
}

/**
*	ScanCharacterLiteral
*
*	Description : Scans character literal sequences.
*
*	Output - TT_CHAR token.
*/
PRIVATE Token ScanCharacterLiteral(void) {

	char c;
	int s[4];
	int i;
	Token tok;

	tok = InitToken(TCHAR);
	_fm->get(); // consume first '
	for (i = 0, c = _fm->peek(1); c != '\'' && c != EOF && c != '\n' && i < 1 /* for now, limit to 1 byte. */; c = _fm->peek(1), i++) {
		s[i] = c;
		_fm->get();
	}
	if (c == '\'')
		_fm->get();

	return InitCharToken(s[0]);
}

/**
*	ScanDigraph
*
*	Description : If digraph scanning is enabled, this function translates it.
*
*	Output - Translated character or 0 if invalid digraph sequence.
*/
PRIVATE int ScanDigraph(void) {

	int a;
	int b;

	if (!_enableDigraphs)
		return 0;

	a = _fm->peek(1);
	b = _fm->peek(2);
	if (a == '<' && b == ':') return '[';
	else if (a == ':' && b == '>') return ']';
	else if (a == '<' && b == '%') return '{';
	else if (a == '%' && b == '>') return '}';
	else if (a == '%' && b == ':') return '#';
	else return 0;
}

/**
*	ScanPpHeader
*
*	Description : Scans preprocessor #include header file string.
*
*	Output : THEADER token.
*	Header file string stored in token.val.s.
*	If parsed as <myFile.h> then token.val.b is TRUE.
*	If parsed as "myFile.h" then token.val.b is FALSE.
*/
PRIVATE Token ScanPpHeader(void) {

	Buffer* fname;
	File*   file;
	char    delim;
	BOOL    std;
	char*   s;
	char    c;

	file = _fm->current();
	c = _fm->get();

	if (c == '\"') {
		std = FALSE;
		delim = '\"';
	}
	else if (c == '<') {
		std = TRUE;
		delim = '>';
	}
	else
		return InitInvalidToken();

	fname = NewBuffer();
	while (TRUE) {

		c = _fm->get();
		if (c == '\n' || c == EOF) {
			Fatal(LEX_HEADER_EOF);
			BufferFree(fname);
			return InitInvalidToken();
		}
		else if (c == delim)
			break;
		BufferPush(fname, c);
	}

	BufferPush(fname, '\0');
	s = GetBufferData(fname);
	BufferFreePreserve(fname);
	
	return InitPpHeaderToken(s, std);
}

/**
*	PushTokenStream
*
*	Description : Push new token stream.
*
*	Input - tok - Pointer to vector of tokens.
*/
PRIVATE void PushTokenStream(IN TokenVector** tok) {

	if (!_tokenStreams)
		_tokenStreams = NewVector(sizeof(TokenVector**));
	VectorPush(_tokenStreams, tok);
}

/**
*	PopTokenStream
*
*	Description : Pop token stream from stack.
*
*	Output : Pointer to token stream or NULL if stack is empty.
*/
PRIVATE TokenVector* PopTokenStream(void) {

	TokenVector* tok = NULL;
	if (VectorLength(_tokenStreams) > 0)
		VectorPop(_tokenStreams, tok);
	return tok;
}

/**
*	GetTokenStream
*
*	Description : Returns a pointer to the current token stream.
*
*	Output : Pointer to current token stream or NULL if empty.
*/
PRIVATE TokenVector* GetTokenStream(void) {

	TokenVector* v = NULL;
	if (VectorLength(_tokenStreams) > 0)
		VectorLastElement(_tokenStreams, &v);
	return v;
}

PRIVATE Token PeekToken(IN int index);

/**
*	GetToken
*
*	Description : Gets the next token from one of the following sources:
*	              - Current token stream (if any.)
*	              - File manager.
*
*	Output - Token. Returns TT_EOF when no more tokens.
*/
PRIVATE Token GetToken(void) {

	char c;
	TokenVector* ts;

	if (!_fm) {
		_fm = GetFileManager();
		_pushback = NewVector(sizeof(Token));
		_symbols = GetSymbolTable();
		PushTokenStream(&_pushback);
	}

	/*
	get token from token stream, if any.
	*/
	ts = GetTokenStream();
	if (ts) {
		if (VectorLength(ts) > 0) {
			Token tok;
			VectorPop(ts, &tok);
//			printf("\n  lex.GetToken: return from token stream: %05x\t%s", tok.type, TokenToString(tok));
			return tok;
		}
		else if (ts != _pushback)
			return InitEofToken();
	}

	c = _fm->peek(1);
	if (c != EOF) {
		_location.src = _fm->current();
		_location.line = _location.src->line;
	}

	if (c == '\n') {
		_fm->get();
		return InitEolToken();
	}
	else if (SkipWhitespace())
		return InitSpaceToken();
	else if (c == EOF)
		return InitEofToken();
	else if (c == '/') {
		if (_fm->peek(2) == '/') {
			ScanSingleLineComment();
			return InitSpaceToken();
		}
		else if (_fm->peek(2) == '*') {
			ScanMultilineComment();
			return InitSpaceToken();
		}
	}

	if (isalpha(c) || c == '_')                               return ScanCharacters();
	else if (c == '\'')                                       return ScanCharacterLiteral();
	else if (c == '"')		                                  return ScanStringLiteral();
	else if (isdigit(c) || (c=='.' && isdigit(_fm->peek(2)))) return ScanNumber();
	else if (ispunct(c))                                      return ScanOperators();

	else return InitToken(TINVALID);
}

/**
*	TokenToString
*
*	Description : Given a token, returns the string of the token value.
*	Called from msg.c for displaying Tokens.
*
*	Input - tok - Token object to convert to string.
*
*	Output - Pointer to string representing the token value.
*/
PRIVATE char* TokenToString(IN Token tok) {

	// identifiers and typenames cant exceed 128 chars.
	// +14 for "identifier '<name>'":
	static char _s[128+14];
	char* res;

	memset(_s, 0, 128+14);
	res = NULL;

	switch (tok.type) {
	case TINVALID:
		return "<invalid>";
	case TEOF:
		return "<eof>";
	case TOK_ERROR:
		return "<error>";
	case TIDENTIFIER:
		strcpy(_s, "identifier");
		if (tok.val.s) {
			strcat(_s, " '");
			strcat(_s, tok.val.s);
			strcat(_s, "'");
		}
		return _s;
	case TYPENAME:
		strcpy(_s, "type");
		if (tok.val.s) {
			strcat(_s, " '");
			strcat(_s, tok.val.s);
			strcat(_s, "'");
		}
		return _s;
	case TSTRING:
		if (!tok.val.s)
			return "string";
		return tok.val.s;
	case TNUMBER:
		return tok.val.s;
	case TCHAR:
		_s[0] = tok.val.c;
		return _s;
	};
	res = GetOperatorById(tok.type);
	if (res) return res;
	return GetKeywordById(tok.type);
}

/**
*	UngetToken
*
*	Description : Puts a token back into the token stream.
*
*	Input - tok - Token to put back.
*/
PRIVATE void UngetToken(IN Token tok) {
	TokenVector* ts;
	ts = GetTokenStream();
	if (ts)
		VectorPush(ts, &tok);
	else
		__debugbreak();
}

/**
*	PeekToken
*
*	Description : Given an index, this returns the token at the
*	              specified index.
*
*	Input - index - Index of token. This value must be > 1 and < 10.
*
*	Output - Token at that index. If there are no tokens at that index,
*	         a TT_EOF token is returned.
*/
PRIVATE Token PeekToken(IN int index) {

	int i;
	Token tok[10];
	assert(index > 0);
	assert(index < 10);
	for (i = 0; i < index; i++)
		tok[i] = GetToken();
	while (i--)
		UngetToken(tok[i]);
	return tok[index - 1];
}

PRIVATE Keyword* GetKeyword(IN char* s) {

	Keyword* k;
	for (k = _keywords; k->t; k++) {
		if (!k->k) break;
		if (!strncmp(s, k->k, strlen(k->k)))
			return k;
	}
	return NULL;
}

PRIVATE Location GetLocation(void) {
	return _location;
}

PRIVATE Scanner _scanner = {
	GetToken, UngetToken, PeekToken,
	TokenToString, GetTokenStream, PopTokenStream,
	PushTokenStream, ScanPpHeader, GetKeyword,
	GetLocation
};

/**
*	GetScanner
*
*	Description : Returns pointer to global scanner object.
*/
PUBLIC Scanner* GetScanner(void) {

	return &_scanner;
}


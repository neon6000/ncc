/************************************************************************
*
*	c.h - C Front end private definitions
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#ifndef _FRONTEND_C_H
#define _FRONTEND_C_H

#include "icode.h"

#define LEX_COMMENT_EOF 1
#define LEX_ESCAPE_INVALID 2
#define LEX_HEADER_EOF 3
#define LEX_UCS_INVALID 4

#define PARSE_EXPECTED                MSG_PARSER+0
#define PARSE_EXPR                    MSG_PARSER+1
#define PARSE_INIT_FUNCTION           MSG_PARSER+2
#define PARSE_KR_FUNCTION             MSG_PARSER+3
#define PARSE_UNDECLARED              MSG_PARSER+4
#define PARSE_EMPTY_STRUCT_OR_UNION   MSG_PARSER+5

#define PPROC_HEADERNAME              MSG_PARSER+6

#define SYM_REDEFINE                  MSG_PARSER+7

#define EXPECTED_STRUCT_OR_UNION      MSG_PARSER+100
#define NOT_A_MEMBER_OF               MSG_PARSER+101
#define POINTER_EXPECTED              MSG_PARSER+102
#define CAST_NOT_ALLOWED              MSG_PARSER+103
#define ASSIGN_VOID_TYPE              MSG_PARSER+104
#define EXPECTED_ARITH_OR_PTR_TYPE    MSG_PARSER+105
#define EXPECTED_ARITH_TYPE           MSG_PARSER+106
#define EXPECTED_INT_TYPE             MSG_PARSER+107
#define EXPECTED_LVALUE               MSG_PARSER+108
#define EXPECTED_LVALUE_OR_FUNCTIONDESG MSG_PARSER+109
#define EXPECTED_MODIFIABLE_LVALUE    MSG_PARSER+110
#define EXPECTED_COMPLETE_TYPE        MSG_PARSER+111
#define EXPECTED_PTR_TYPE             MSG_PARSER+112
#define INCOMPATIBLE_TYPE             MSG_PARSER+113
#define INCOMPATIBLE_ASSIGN_TYPE      MSG_PARSER+114
#define PTR_INDIRECTION_MISMATCH      MSG_PARSER+115
#define PREMATURE_EOF                 MSG_PARSER+116
#define PARSE_UNTAGGED_TYPE_NO_SYMBOL MSG_PARSER+117
#define PARSE_MISSING_TAG             MSG_PARSER+118
#define PARSE_SWITCH_LABEL            MSG_PARSER+119
#define PARSE_SWITCH_DUPLICATE        MSG_PARSER+120
#define PARSE_LABEL_REDEFINE          MSG_PARSER+121

extern Message _msgC[];

/* symbol tables --------------- */

/*
typedef struct Symbol {
	String*          name;
	Type*            type;
	struct Value*    reg;
}Symbol;
*/

typedef enum Namespace {
	NS_TAGS = 0,
	NS_IDENTS = 1,
	NS_LABELS = 2,
	NS_MAX = 3
}Namespace;

#define NS_MAX 4
typedef struct Scope {
	struct AstNode* function;
	struct Scope* parent;
	uint32_t      level;
	Map*          sym[NS_MAX];
}Scope;

typedef Scope*    (*st_enter_scope_t)   (IN Scope*);
typedef Scope*    (*st_leave_scope_t)   (IN Scope*);
typedef BOOL      (*st_insert_t)        (IN Scope*, IN char*, IN Namespace, IN Type*);
typedef Value*    (*st_lookup_t)        (IN Scope*, IN char*, IN Namespace);
typedef Scope*    (*st_get_global_t)    (void);

typedef struct SymbolTable {
	st_lookup_t      lookup;
	st_insert_t      insert;
	st_enter_scope_t enterScope;
	st_leave_scope_t exitScope;
	st_get_global_t  global;
}SymbolTable;

extern SymbolTable* GetSymbolTable(void);

/* Scanner ---------------- */

/* ISO/IEC 9899:TC2 - 6.4 */
typedef enum TokenType {
	TINVALID,
	TPUNCTUATOR,
	TIDENTIFIER,
	TYPENAME,
	TPPNUMBER,
	TMACROARG,
	TKEYWORD,
	THEADER,
	TNUMBER,
	TSTRING,
	TCHAR,
	TSPACE,
	TEOL,
	TEOF,
	TOK_ERROR
}TokenType;

typedef enum TokenOperator {
	OP_HASH = '#',
	OP_LPAREN = '(',
	OP_RPAREN = ')',
	OP_LBRACKET = '[',
	OP_RBRACKET = ']',
	OP_LBRACE = '{',
	OP_RBRACE = '}',
	OP_EQ = '=',
	OP_SEMICOLON = ';',
	OP_COLON = ':',
	OP_DOT = '.',
	OP_PLUS = '+',
	OP_MINUS = '-',
	OP_DIV = '/',
	OP_SLASH = '\\',
	OP_MUL = '*',
	OP_MODULO = '%',
	OP_QUESTION = '?',
	OP_NOT = '!',
	OP_OR = '|',
	OP_AND = '&',
	OP_COMPL = '~',
	OP_XOR = '^',
	OP_LESS = '<',
	OP_GREATER = '>',
	OP_COMMA = ',',
	OP_ADD_ASSIGN = 256, // +=
	OP_SUB_ASSIGN,   // -=
	OP_MUL_ASSIGN,   // *=
	OP_DIV_ASSIGN,   // /=
	OP_MOD_ASSIGN,   // %=
	OP_AND_ASSIGN,   // &=
	OP_XOR_ASSIGN,   // ^=
	OP_OR_ASSIGN,    // |=
	OP_SHIFT_RIGHT,  // >>
	OP_SHIFT_LEFT,   // <<
	OP_LTE,          // <=
	OP_GTE,          // >=
	OP_INC,          // ++
	OP_DEC,          // --
	OP_PTR,          // ->
	OP_LOG_AND,      // &&
	OP_LOG_OR,       // ||
	OP_ISEQ,         // ==
	OP_ISNOTEQ,      // !=
	OP_HASHHASH,     // ##
	OP_RIGHT_ASSIGN, // >>=
	OP_LEFT_ASSIGN,  // <<=
	OP_ELLIPSES,     // ...
	OP_CAST,         // cast operator
	OP_POSTINC,      // <expr>++    
	OP_POSTDEC,      // <expr>--
	OP_SIZEOF,       // sizeof operator
	OP_ALIGNOF,      // alignof operator
	OP_MAX
}TokenOperator;

#define KEYWORD_C99 0x20000000
#define KEYWORD_C11 0x40000000

typedef enum TokenKeyword {
	K_AUTO = 1000,
	K_BREAK,
	K_CASE,
	K_CHAR,
	K_CONST,
	K_CONTINUE,
	K_DEFAULT,
	K_DO,
	K_DOUBLE,
	K_ELSE,
	K_ENUM,
	K_EXTERN,
	K_FLOAT,
	K_FOR,
	K_GOTO,
	K_IF,
	K_INT,
	K_LONG,
	K_REGISTER,
	K_RETURN,
	K_SHORT,
	K_SIGNED,
	K_SIZEOF,
	K_STATIC,
	K_STRUCT,
	K_SWITCH,
	K_TYPEDEF,
	K_UNION,
	K_UNSIGNED,
	K_VOID,
	K_VOLATILE,
	K_WHILE,
	/* c99 --------- */
	K_BOOL = KEYWORD_C99,
	K_RESTRICT,
	K_INLINE,
	K_COMPLEX,
	K_IMAGINARY,
	/* c11 --------- */
	K_ALIGNAS = KEYWORD_C11,
	K_ALIGNOF,
	K_ATOMIC,
	K_GENERIC,
	K_NORETURN,
	K_STATIC_ASSERT,
	K_THREAD_LOCAL
}TokenKeyword;

typedef enum EncodingType {
	ENC_NONE,
	ENC_CHAR8,
	ENC_CHAR16,
	ENC_CHAR32,
	ENC_UTF8,
	ENC_WCHAR
}EncodingType;

typedef struct Token {
	// TokenKeyword, TokenOperator, or TokenType:
	int type;
	// Used by preprocessor:
	int location;
	BOOL vararg;
	Set* hideset;
	// Used for messages:
	Location loc;
	// TRUE if this is an error token generated by parser:
	BOOL error;
	// Token detail:
	struct {
		EncodingType enc;
		char* s;
		char c;
		BOOL b;
	}val;
}Token;

typedef Vector TokenVector;

typedef struct Keyword {
	TokenType t;
	char*     k;
}Keyword;

typedef Token          (*lex_peek_t)      (IN int);
typedef void           (*lex_unget_t)     (IN Token);
typedef Token          (*lex_get_t)       (void);
typedef char*          (*lex_str_t)       (IN Token);
typedef TokenVector*   (*lex_get_ts_t)    (void);
typedef TokenVector*   (*lex_pop_ts_t)    (void);
typedef void           (*lex_push_ts_t)   (TokenVector*);
typedef Token          (*lex_ppheader_t)  (void);
typedef Keyword*       (*lex_keyword_t)   (IN char*);
typedef Location       (*lex_location_t)  (void);

typedef struct Scanner {
	lex_get_t      get;
	lex_unget_t    unget;
	lex_peek_t     peek;
	lex_str_t      str;
	lex_get_ts_t   getTokStream;
	lex_pop_ts_t   popTokStream;
	lex_push_ts_t  pushTokStream;
	lex_ppheader_t ppheader;
	lex_keyword_t  keyword;
	lex_location_t location;
}Scanner;

extern Scanner* GetScanner(void);

/* preprocessor --------------------------- */

typedef Token  (*pproc_get_t)    (void);
typedef Token  (*pproc_peek_t)   (IN int);
typedef void   (*pproc_unget_t)  (IN Token);
typedef Token  (*pproc_reget_t)  (void);
typedef char*  (*pproc_str_t)    (IN Token);

typedef struct Preprocessor {
	pproc_get_t   get;
	pproc_reget_t reget;
	pproc_peek_t  peek;
	pproc_unget_t unget;
	pproc_str_t   str;
}Preprocessor;

extern Preprocessor* GetPreprocessor(void);

/* parser ---------------- */

typedef enum AstNodeKind {
	AST_LITERAL,
	AST_DEREF,
	AST_ADDROF,
	AST_TERNARY,
	AST_BINARY,
	AST_UNARY,
	AST_VARIABLE_DECL,
	AST_VARIABLE,
	AST_FUNCTION_CALL,
	AST_FUNCTION,
	AST_DECL,
	AST_INIT,
	AST_IF,
	AST_DEFAULT,
	AST_RETURN,
	AST_COMPOUND_STMT,
	AST_STRUCT_REF,
	AST_GOTO,
	AST_LABEL,
	AST_ROOT,
	AST_WHILE,
	AST_FOR,
	AST_DO,
	AST_SWITCH,
	AST_CASE,
	AST_CONTINUE,
	AST_BREAK,
	AST_SIZEOF
}AstNodeKind;

typedef struct AstLiteral {
	String*  val;
}AstLiteral;

typedef struct AstVariable {
	Value* symbol;
}AstVariable;

typedef struct AstUnaryOp {
	TokenOperator op;
	struct AstNode* left;
}AstUnaryOp;

typedef struct AstBinaryOp {
	TokenOperator op;
	struct AstNode* left;
	struct AstNode* right;
}AstBinaryOp;

typedef struct AstTernaryOp {
	TokenOperator op;
	struct AstNode* one;
	struct AstNode* two;
	struct AstNode* three;
}AstTernaryOp;

typedef struct AstFunctionCall {
	struct AstNode* expr;
	Vector* args;
}AstFunctionCall;

typedef struct AstDeclaration {
	struct AstNode* declvar;
	Vector* init;
}AstDeclaration;

typedef struct AstInitializer {
	struct AstNode* initval;
}AstInitializer;

typedef struct AstIfStatement {
	struct AstNode* cond;
	struct AstNode* then;
	struct AstNode* els;
}AstIfStatement;

typedef struct AstLabel {
	char*  label;
	Value* symbol;
	Block* block; // for IL
}AstLabel;

typedef struct AstGoto {
	char*    label;
	struct AstNode* to;
}AstGoto;

typedef struct AstReturnStmt {
	struct AstNode* retval;
}AstReturnStmt;

typedef struct Scope Scope;

typedef struct AstCompoundStmt {
	Scope*  scope;
	Vector* stmt;
}AstCompoundStmt;

typedef struct AstStructRef {
	struct AstNode* struc;
	char* field;
}AstStructRef;

typedef struct AstWhile {
	struct AstNode* expr;
	struct AstNode* stat;
}AstWhile;

typedef struct AstDoWhile {
	struct AstNode* expr;
	struct AstNode* stat;
}AstDoWhile;

typedef struct AstFor {
	struct AstNode* init;
	struct AstNode* cond;
	struct AstNode* next;
	struct AstNode* stmt;
}AstFor;

typedef struct AstFunction {
	Value*  symbol;
	Type*   rettype;
	Vector* params;
	BOOL    varargs;
	Scope*  scope;
	uint32_t tempSymbolCount;
	struct AstNode* body;
}AstFunction;

typedef struct AstDeref {
	struct AstNode* node;
}AstDeref;

typedef struct AstAddressOf {
	struct AstNode* node;
	BOOL take;
}AstAddressOf;

typedef struct AstSwitch {
	struct AstNode* expr;
	struct AstNode* stmt;
	Vector          cases;
	struct AstNode* defCase;
}AstSwitch;

typedef struct AstCase {
	String*         value;
	struct AstNode* stmt;
	struct AstNode* parentSwitch;
	BOOL            isFirst; // used for IL
	Block*          block; // used for IL
}AstCase;

typedef struct AstCast {
	struct AstNode* to;
}AstCast;

typedef struct AstSizeof {
	struct Type*    type;
	struct AstNode* expr;
}AstSizeof;

typedef struct AstNode {
	AstNodeKind kind;
	Type* type;
	Location loc;
	union {
		AstLiteral      lit;
		AstVariable     var;
		AstBinaryOp     bin;
		AstDeref        deref;
		AstAddressOf    addrOf;
		AstUnaryOp      unary;
		AstTernaryOp    ternary;
		AstFunctionCall fc;
		AstFunction     function;
		AstDeclaration  decl;
		AstInitializer  init;
		AstIfStatement  ifStmt;
		AstLabel        label;
		AstGoto         got;
		AstReturnStmt   ret;
		AstCompoundStmt cmpnd;
		AstStructRef    struc;
		AstWhile        loopwhile;
		AstDoWhile      dowhile;
		AstFor          loopfor;
		AstSwitch       switchStmt;
		AstCase         caseStmt;
		AstCase         defaultStmt;
		AstCast         cast;
		AstSizeof       size;
	};
}AstNode;

typedef AstNode* (*parse_t)(void);

typedef struct Parser {
	char* name;
	parse_t parse;
}Parser;

extern Parser* GetParser(void);

/* syntax tree analyzers */

typedef AstNode* (*analyze_t)(void);

typedef struct Analyzer {
	analyze_t analyze;
}Analyzer;

extern Analyzer* GetSemanticAnalyzer(void);
extern Analyzer* GetLivenessAnalyzer(void);
extern Analyzer* GetConstantFolderAnalyzer(void);

/* expression evaluator ------- */

extern int Evaluate(IN AstNode* node);

#endif

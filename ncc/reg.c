/************************************************************************
*
*	reg.c - register allocator
*
*   Copyright(c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "ncc.h"
#include "icode.h"


#if 0
// Rewrite `A = B op C` to `A = B; A = A op C`.
static void three_to_two(BB *bb) {
	Vector *v = new_vec();

	for (int i = 0; i < bb->ir->len; i++) {
		IR *ir = bb->ir->data[i];

		if (!ir->r0 || !ir->r1) {
			vec_push(v, ir);
			continue;
		}

		assert(ir->r0 != ir->r1);

		IR *ir2 = calloc(1, sizeof(IR));
		ir2->op = IR_MOV;
		ir2->r0 = ir->r0;
		ir2->r2 = ir->r1;
		vec_push(v, ir2);

		ir->r1 = ir->r0;
		vec_push(v, ir);
	}
	bb->ir = v;
}



static void spill_store(Vector *v, IR *ir) {
	Reg *r = ir->r0;
	if (!r || !r->spill)
		return;

	IR *ir2 = calloc(1, sizeof(IR));
	ir2->op = IR_STORE_SPILL;
	ir2->r1 = r;
	ir2->var = r->var;
	vec_push(v, ir2);
}

static void spill_load(Vector *v, IR *ir, Reg *r) {
	if (!r || !r->spill)
		return;

	IR *ir2 = calloc(1, sizeof(IR));
	ir2->op = IR_LOAD_SPILL;
	ir2->r0 = r;
	ir2->var = r->var;
	vec_push(v, ir2);
}

static void emit_spill_code(BB *bb) {
	Vector *v = new_vec();

	for (int i = 0; i < bb->ir->len; i++) {
		IR *ir = bb->ir->data[i];

		spill_load(v, ir, ir->r1);
		spill_load(v, ir, ir->r2);
		spill_load(v, ir, ir->bbarg);
		vec_push(v, ir);
		spill_store(v, ir);
	}
	bb->ir = v;
}

void alloc_regs(Program *prog) {
	for (int i = 0; i < prog->funcs->len; i++) {
		Function *fn = prog->funcs->data[i];

		// Convert SSA to x86-ish two-address form.
		for (int i = 0; i < fn->bbs->len; i++) {
			BB *bb = fn->bbs->data[i];
			three_to_two(bb);
		}

		// Allocate registers and decide which registers to spill.
		Vector *regs = collect_regs(fn);
		scan(regs);

		// Reserve a stack area for spilled registers.
		for (int i = 0; i < regs->len; i++) {
			Reg *r = regs->data[i];
			if (!r->spill)
				continue;

			Var *var = calloc(1, sizeof(Var));
			var->ty = ptr_to(int_ty());
			var->is_local = true;
			var->name = "spill";

			r->var = var;
			vec_push(fn->lvars, var);
		}

		// Convert accesses to spilled registers to loads and stores.
		for (int i = 0; i < fn->bbs->len; i++) {
			BB *bb = fn->bbs->data[i];
			emit_spill_code(bb);
		}
	}
}

PRIVATE int ChooseToSpill(IN Variable **used) {
	int k = 0;
//	for (int i = 1; i < num_regs; i++)
//	if (used[k]->last_use < used[i]->last_use)
//		k = i;
	return k;
}

PRIVATE void Scan(IN Vector* regs, IN CodeGeneratorCaps c) {

	Variable** r;
	Variable** ireg;
	Variable** freg;
	Variable** used;
	uint32_t   num_regs;

	ireg = calloc(c.maxIntRegister, sizeof(Variable*));
	freg = calloc(c.maxFloatRegister, sizeof(Variable*));

	for (r = VectorFirst(regs); r; r = VectorNext(regs, r)){

		// Find an unused slot.
		BOOL found = FALSE;

		__debugbreak();


//		for (int i = 0; i < num_regs - 1; i++) {
//			if (used[i] && r->def < used[i]->last_use)
//				continue;
//			(*r)->real = i;
//			used[i] = r;
//			found = TRUE;
//			break;
//		}

		if (found)
			continue;

		printf("");
	}

	free(ireg);
	free(freg);
}
#endif

#if 0


// Allocate registers.
static void scan(Vector *regs) {
	Reg **used = calloc(num_regs, sizeof(Reg *));

	for (int i = 0; i < regs->len; i++) {
		Reg *r = regs->data[i];

		// Find an unused slot.
		bool found = false;
		for (int i = 0; i < num_regs - 1; i++) {
			if (used[i] && r->def < used[i]->last_use)
				continue;
			r->rn = i;
			used[i] = r;
			found = true;
			break;
		}

		if (found)
			continue;

		// Choose a register to spill and mark it as "spilled".
		used[num_regs - 1] = r;
		int k = choose_to_spill(used);

		r->rn = k;
		used[k]->rn = num_regs - 1;
		used[k]->spill = true;
		used[k] = r;
	}
}



PRIVATE void SetLastUse (IN Variable *r, IN uint32_t ic) {
	if (r && r->lastUse < ic)
		r->lastUse = ic;
}

PRIVATE void GetRegisters(IN Function* fc, OUT Vector* regs) {

	Block** b;
	Instr* i;
	Variable* r;
	uint32_t ic = 1;

	for (b = VectorFirst(&fc->blocks); b; b = VectorNext(&fc->blocks, b)) {

		for (i = (*b)->code.first; i; i = i->next, ic++) {

			if (i->target && !i->target->defined) {
				i->target->defined = ic;
				VectorPush(regs, &i->target);
			}
			SetLastUse(i->target,  ic);
			SetLastUse(i->regs[0], ic);
			SetLastUse(i->regs[1], ic);
			SetLastUse(i->regs[2], ic);
		}

		for (r = VectorFirst(&(*b)->outregs); r; r = VectorNext(&(*b)->outregs, r))
			SetLastUse(r, ic);
	}
}

#endif

PUBLIC void RegAlloc(IN Function* fc, IN CodeGeneratorCaps c) {

	Vector regs;

//	InitVector(&regs, sizeof(Variable*));
//	GetRegisters(fc, &regs);
//	Scan(&regs, c);



//	FreeVector(&regs);

	__debugbreak();
}

/************************************************************************
*
*	icode.c - Intermediate Code
*
*   Copyright(c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "base.h"
#include "icode.h"

/* IR Base type ----------------- */

PUBLIC Type* VoidType(void) {

	Type* type = Alloc(sizeof(Type));
	memset(type, 0, sizeof(Type));
	type->dt = DT_VOID;
	return type;
}

PUBLIC Type* FloatType(void) {

	Type* type = Alloc(sizeof(Type));
	memset(type, 0, sizeof(Type));
	type->dt = DT_FLOAT;
	type->size = 4;
	return type;
}

PUBLIC Type* DoubleType(void) {

	Type* type = Alloc(sizeof(Type));
	memset(type, 0, sizeof(Type));
	type->dt = DT_DOUBLE;
	type->size = 8;
	return type;
}

PUBLIC Type* LongDoubleType(void) {

	Type* type = Alloc(sizeof(Type));
	memset(type, 0, sizeof(Type));
	type->dt = DT_LDOUBLE;
	type->size = 8;
	return type;
}

PUBLIC Type* IntType(void) {

	Type* type = Alloc(sizeof(Type));
	memset(type, 0, sizeof(Type));
	type->dt = DT_INT;
	type->size = 4;
	return type;
}

PUBLIC Type* BoolType(void) {

	Type* type = Alloc(sizeof(Type));
	memset(type, 0, sizeof(Type));
	type->dt = DT_BOOL;
	type->size = 4;
	return type;
}

PUBLIC Type* BlockType(void) {

	static Type type = { DT_BLOCK };
	return &type;
}

/* Aggregate types ----------- */

PUBLIC Type* PointerType(IN Type* to) {

	Type* type = Alloc(sizeof(Type));
	memset(type, 0, sizeof(Type));
	type->dt = DT_PTR;
	type->pointer.type = to;
	type->size = 4;
	return type;
}

PUBLIC Type* ArrayType(IN Type* eltype, IN int count) {

	Type* type = Alloc(sizeof(Type));
	memset(type, 0, sizeof(Type));
	type->dt = DT_ARRAY;
	type->array.len = count;
	type->array.type = eltype;
	type->size = eltype->size * count;
	return type;
}

PUBLIC void PrintType(IN Type* type, IN FILE* stream) {

	Type** param;
	BOOL   next;

	param = NULL;
	next = FALSE;

	if (!type)
		__debugbreak();

	if (type->dt == DT_ARRAY) {
		PrintType(type->array.type, stream);
		fprintf(stream, "[%i]", type->array.len);
	}
	else if (type->dt == DT_PTR) {
		PrintType(type->pointer.type, stream);
		fprintf(stream, "*");
	}
	else if (type->dt == DT_FUNCTION) {
		PrintType(type->function.rettype, stream);
		fprintf(stream, "(");
		if (type->function.params) {
			for (param = VectorFirst(type->function.params); param; param = VectorNext(type->function.params, param)) {
				if (next)
					fprintf(stream, ", ");
				PrintType((*param), stream);
				next = TRUE;
			}
		}
		fprintf(stream, ")");
	}

	switch (type->dt) {
	case DT_CHAR: fprintf(stream, "char"); break;
	case DT_BOOL: fprintf(stream, "bool"); break;
	case DT_INT: fprintf(stream, "int"); break;
	case DT_SHORT: fprintf(stream, "short"); break;
	case DT_LONG: fprintf(stream, "long"); break;
	case DT_LLONG: fprintf(stream, "long long"); break;
	case DT_FLOAT: fprintf(stream, "float"); break;
	case DT_DOUBLE: fprintf(stream, "double"); break;
		//	case DTYPE_LDOUBLE: fprintf(stream, "double"); break;
	case DT_VOID: fprintf(stream, "void"); break;
	case DT_STRUCT:
		fprintf(stream, "struct ");
		fprintf(stream, "%s ", type->struc.tag ? type->struc.tag : "<anonymous>");
		break;
	case DT_UNION:
		fprintf(stream, "union ");
		fprintf(stream, "%s ", type->struc.tag ? type->struc.tag : "<anonymous>");
		break;
	case DT_ENUM:
		fprintf(stream, "enum ");
		fprintf(stream, "%s ", type->struc.tag ? type->struc.tag : "<anonymous>");
		break;
	}
}

/* Value -------------------------- */

PUBLIC Value* Constant(IN Type* type, IN String* literal) {

}

PUBLIC Value* AggregateConstant(IN Type* type, IN Vector elements) {

}

PUBLIC Value* NamedValue(IN Type* type, IN String* name) {

	Value* var;

	var = Alloc(sizeof(Value));
	memset(var, 0, sizeof(Value));
	var->kind = VALUE_COMMON;
	var->name = name;
	var->type = type;
	return var;
}

PUBLIC Value* UnnamedValue(IN Context* ctx, IN Type* type) {

	Value* var;

	if (ctx->curLocalName == (uint32_t)-1)
		__debugbreak();
	var = Alloc(sizeof(Value));
	memset(var, 0, sizeof(Value));
	var->kind = VALUE_COMMON;
	var->virt = ctx->curLocalName++;
	var->type = type;
	return var;
}

PUBLIC Value* TypeValue(IN Type* type) {

}

PUBLIC void PushValue(IN Value* value, IN Value* toPush) {

}

PUBLIC Value* PopValue(IN Value* value) {

}


/* Global value ------------------- */

PUBLIC Value* NamedGlobal (IN Type* type, IN String* name) {

	GlobalVariable* var;

	var = Alloc(sizeof(GlobalVariable));
	memset(var, 0, sizeof(GlobalVariable));
	var->linkage = LINKAGE_COMMON;
	var->val.name = name;
	var->val.kind = VALUE_GLOBAL;
	var->val.type = type;
	return (Value*) var;
}

PUBLIC Value* UnnamedGlobal(IN Context* ctx, IN Type* type) {

	GlobalVariable* var;

	if (ctx->curGlobalName == (uint32_t)-1)
		__debugbreak();
	var = Alloc(sizeof(GlobalVariable));
	memset(var, 0, sizeof(GlobalVariable));
	var->linkage = LINKAGE_COMMON;
	var->val.virt = ctx->curGlobalName++;
	var->val.kind = VALUE_GLOBAL;
	var->val.type = type;
	return (Value*)var;
}

PUBLIC Value* NamedGlobalBasic(IN Type* type, IN String* name, IN String* literal) {

	Value* var;

	var = NamedGlobal(type, name);
	var->literal = literal;
	return var;
}

PUBLIC Value* NamedGlobalAggregate(IN Type* type, IN String* name, IN Vector elements) {

	Value* var;

	var = NamedGlobal(type, name);
	var->elements = elements;
	return var;
}

PUBLIC Value* SetGlobalSection(IN GlobalVariable* var, IN String* section) {

	var->section = section;
	return var;
}

PUBLIC Value* SetGlobalLinkage(IN GlobalVariable* var, IN int linkage) {

	var->linkage = linkage;
	return var;
}

PUBLIC Value* SetGlobalStorageClassDLL(IN GlobalVariable* var, IN int dllStorageClass) {

	var->dllStorageClass = dllStorageClass;
	return var;
}

PUBLIC Value* UnnamedGlobalBasic(IN Context* ctx, IN Type* type, IN String* literal) {

	Value* v;

	v = UnnamedGlobal(ctx, type);
	v->literal = literal;
	return v;
}

PUBLIC Value* UnnamedGlobalAggregate(IN Context* ctx, IN Type* type, IN Vector elements) {

	Value* v;

	v = UnnamedGlobal(ctx, type);
	v->elements = elements;
	return v;
}

/* IR Instructions ------------------ */

PUBLIC Instr* NewIns(IN Mnemonic m) {

	Instr* i = Alloc(sizeof(Instr));
	memset(i, 0, sizeof(Instr));
	i->mnemonic = m;
	return i;
}

PRIVATE Instr* Ins1(IN Mnemonic m, IN Value* target, IN Value* a) {

	Instr* i = NewIns(m);
	i->target = target;
	i->ops[0] = a;
	return i;
}

PRIVATE Instr* Ins2(IN Mnemonic m, IN Value* target, IN Value* a, IN Value* b) {

	Instr* i = NewIns(m);
	i->target = a;
	i->ops[0] = a;
	i->ops[1] = b;
	return i;
}

PRIVATE Instr* Ins3(IN Mnemonic m, IN Value* target, IN Value* a, IN Value* b, IN Value* c) {

	Instr* i = NewIns(m);
	i->target = target;
	i->ops[0] = a;
	i->ops[1] = b;
	i->ops[3] = c;
	return i;
}

PUBLIC Instr* InsSwitch(IN Value* cond, IN Block* defCase) {

	return Ins3(I_SWITCH, NULL, cond, (Value*)defCase, NULL);
}

PUBLIC Instr* InsSwitchCase(IN Instr* insSwitch, IN Value* cond, IN Block* jmp) {

	// this is an array of <index,label> pairs:
	Value* v = insSwitch->ops[2];

	return insSwitch;
}

void blah() {

	Value expr;
	Value defCase;
	Value cases;
	Block block;

//	PushValue(&cases, Constant(IntType(), InternString("1")));
//	PushValue(&cases, &block));
//	cases.type = ArrayType(IntType(), cases.elements.elementCount);

//	InsSwitch(expr, defCase, cases);
}

PUBLIC Code*  Addi(IN Code* code, IN Instr* ins) {

	if (!code->first) {
		code->first = ins;
		code->last = ins;
		ins->next = NULL;
	}
	else{
		Instr* prev = code->last;
		code->last = ins;
		prev->next = ins;
		ins->next = NULL;
	}
	return code;
}

PUBLIC Type* NewType(IN DataType dt, IN TypeQualifier tq, IN StorageClass sc, IN BOOL isUnsigned) {

	Type* type;
	int size;
	int align;

	type = Alloc(sizeof(Type));
	memset(type, 0, sizeof(Type));
	size = 0;
	align = 1;

	if (dt == DT_VOID) size = align = 0;
	else if (dt == DT_BOOL) size = align = 1;
	else if (dt == DT_CHAR) size = align = 1;
	else if (dt == DT_SHORT) size = align = 2;
	else if (dt == DT_INT) size = align = 4;
	else if (dt == DT_LONG) size = align = 8;
	else if (dt == DT_LLONG) size = align = 8;
	else if (dt == DT_FLOAT) size = align = 4;
	else if (dt == DT_DOUBLE) size = align = 4;
	else if (dt == DT_LDOUBLE) size = align = 8;
	else if (dt == DT_PTR) size = align = 4;

	type->parent = NULL;
	type->dt = dt;
	type->tq = tq;
	type->sc = sc;
	type->size = size;
	type->align = align;
	type->isUnsigned = isUnsigned;
	return type;
}

/************************************************************************
*
*	out_il.c - Intermediate Code generator
*
*   Copyright(c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This is a work in progress ------------------------- */

#include <stdio.h>
#include <malloc.h>
#include <stdarg.h>
#include "ncc.h"
#include "icode.h"

PRIVATE FILE* _fileout;

PRIVATE void Text(IN char* fmt, IN ...) {

	va_list ap;

	va_start(ap, fmt);
	vfprintf(_fileout, fmt, ap);
	fprintf(_fileout, "\n");
	va_end(ap);
}

PRIVATE void Emit(IN char* fmt, IN ...) {

	va_list   ap;
	Block*    b;
	Type*     t;
	String*   s;
	Value*    r;
	int       i;

	va_start(ap, fmt);
	fprintf(_fileout, "\t");

	while (*fmt) {
		if (*fmt == '%') {
			if (fmt[1] == 'i') {
				fprintf(_fileout, "%i", va_arg(ap, int));
				fmt++;
			}
			else if (fmt[1] == 's') {
				fprintf(_fileout, "%s", va_arg(ap, char*));
				fmt++;
			}
			else if (fmt[1] == 'L') {
				b = va_arg(ap, Block*);
				fprintf(_fileout, ".L%i", b->name);
				fmt++;
			}
			else if (fmt[1] == 'T') {
				t = va_arg(ap, Type*);
				if (!t)
					fprintf(_fileout, "<type>");
				else
					PrintType(t, _fileout);
				fmt++;
			}
			else if (fmt[1] == 'S') {
				s = va_arg(ap, String*);
				fprintf(_fileout, "%s", s->value);
				fmt++;
			}
			else if (fmt[1] == 'R') {
				r = va_arg(ap, Value*);
				if (r->name)
					fprintf(_fileout, "@%i:%s", r->virt, r->name->value);
				else
					fprintf(_fileout, "@%i", r->virt);
				fmt++;
			}

			else if (fmt[1] == 'Y') {
				i = va_arg(ap, int);
				switch (i) {
				case ICMP_EQ: fprintf(_fileout, "eq"); break;
				case ICMP_NE: fprintf(_fileout, "ne"); break;
				case ICMP_UGT: fprintf(_fileout, "ugt"); break;
				case ICMP_UGE: fprintf(_fileout, "uge"); break;
				case ICMP_ULT: fprintf(_fileout, "ult"); break;
				case ICMP_ULE: fprintf(_fileout, "ule"); break;
				case ICMP_SGT: fprintf(_fileout, "sgt"); break;
				case ICMP_SGE: fprintf(_fileout, "sge"); break;
				case ICMP_SLT: fprintf(_fileout, "slt"); break;
				case ICMP_SLE: fprintf(_fileout, "sle"); break;
				}
				fmt++;
			}
			else if (fmt[1] == 'Z') {
				i = va_arg(ap, int);
				switch (i) {
				case FCMP_FALSE: fprintf(_fileout, "false"); break;
				case FCMP_OEQ: fprintf(_fileout, "oeq"); break;
				case FCMP_OGT: fprintf(_fileout, "ogt"); break;
				case FCMP_OGE: fprintf(_fileout, "oge"); break;
				case FCMP_OLT: fprintf(_fileout, "olt"); break;
				case FCMP_OLE: fprintf(_fileout, "ole"); break;
				case FCMP_ONE: fprintf(_fileout, "one"); break;
				case FCMP_ORD: fprintf(_fileout, "ord"); break;
				case FCMP_UEQ: fprintf(_fileout, "ueq"); break;
				case FCMP_UGT: fprintf(_fileout, "ugt"); break;
				case FCMP_UGE: fprintf(_fileout, "uge"); break;
				case FCMP_ULT: fprintf(_fileout, "ult"); break;
				case FCMP_ULE: fprintf(_fileout, "ule"); break;
				case FCMP_UNE: fprintf(_fileout, "une"); break;
				case FCMP_UNO: fprintf(_fileout, "uno"); break;
				case FCMP_TRUE: fprintf(_fileout, "true"); break;
				}
				fmt++;
			}

			else if (fmt[1] == '%') {
				fprintf(_fileout, "%");
				fmt++;
			}
		}
		else fprintf(_fileout, "%c", *fmt);
		fmt++;
	}
	fprintf(_fileout, "\n");
	va_end(ap);
}

PRIVATE void EmitIL(IN Instr* ins) {
	
	switch (ins->mnemonic) {
	case I_RET:
		Emit("ret %T %R", ins->ops[0]->type, ins->ops[0]);
		break;
	case I_JMP:
		Emit("jmp %L", (Block*)ins->ops[0]);
		break;
	case I_IF:
		Emit("if %T %R, %L, %L", ins->ops[0]->type, ins->ops[0],
			(Block*)ins->ops[1], (Block*)ins->ops[2]);
		break;
	case I_SWITCH:
		Emit("switch");
		break;
	case I_CALL:
		Emit("call");
		break;
	case I_UNREACHABLE:
		Emit("unreachable");
		break;
	case I_FNEG:
		Emit("fneg");
		break;
	case I_ADD:
		Emit("%R = add %T %R, %R", ins->target, ins->target->type, ins->ops[0], ins->ops[1]);
		break;
	case I_FADD:
		Emit("%R = fadd %T %R, %R", ins->target, ins->target->type, ins->ops[0], ins->ops[1]);
		break;
	case I_SUB:
		Emit("%R = sub %T %R, %R", ins->target, ins->target->type, ins->ops[0], ins->ops[1]);
		break;
	case I_FSUB:
		Emit("%R = fadd %T %R, %R", ins->target, ins->target->type, ins->ops[0], ins->ops[1]);
		break;
	case I_MUL:
		Emit("%R = mul %T %R, %R", ins->target, ins->target->type, ins->ops[0], ins->ops[1]);
		break;
	case I_FMUL:
		Emit("%R = fmul %T %R, %R", ins->target, ins->target->type, ins->ops[0], ins->ops[1]);
		break;
	case I_UDIV:
		Emit("%R = udiv %T %R, %R", ins->target, ins->target->type, ins->ops[0], ins->ops[1]);
		break;
	case I_UREM:
		Emit("%R = urem %T %R, %R", ins->target, ins->target->type, ins->ops[0], ins->ops[1]);
		break;
	case I_SDIV:
		Emit("%R = sdiv %T %R, %R", ins->target, ins->target->type, ins->ops[0], ins->ops[1]);
		break;
	case I_SREM:
		Emit("%R = srem %T %R, %R", ins->target, ins->target->type, ins->ops[0], ins->ops[1]);
		break;
	case I_FDIV:
		Emit("%R = fdiv %T %R, %R", ins->target, ins->target->type, ins->ops[0], ins->ops[1]);
		break;
	case I_FREM:
		Emit("%R = frem %T %R, %R", ins->target, ins->target->type, ins->ops[0], ins->ops[1]);
		break;
	case I_SHL:
		Emit("%R = shl %T %R, %R", ins->target, ins->target->type, ins->ops[0], ins->ops[1]);
		break;
	case I_LSHR:
		Emit("%R = lshr %T %R, %R", ins->target, ins->target->type, ins->ops[0], ins->ops[1]);
		break;
	case I_ASHR:
		Emit("%R = ashr %T %R, %R", ins->target, ins->target->type, ins->ops[0], ins->ops[1]);
		break;
	case I_AND:
		Emit("%R = and %T %R, %R", ins->target, ins->target->type, ins->ops[0], ins->ops[1]);
		break;
	case I_OR:
		Emit("%R = or %T %R, %R", ins->target, ins->target->type, ins->ops[0], ins->ops[1]);
		break;
	case I_XOR:
		Emit("%R = xor %T %R, %R", ins->target, ins->target->type, ins->ops[0], ins->ops[1]);
		break;
	case I_ALLOCA:
		Emit("alloca");
		break;
	case I_LOAD:
		Emit("load");
		break;
	case I_STORE:
		Emit("store");
		break;
	case I_TRUNC:
		Emit("%R = trunc %T %R to %T", ins->target, ins->ops[0]->type, ins->ops[0], ins->target->type);
		break;
	case I_ZEXT:
		Emit("%R = zext %T %R to %T", ins->target, ins->ops[0]->type, ins->ops[0], ins->target->type);
		break;
	case I_SEXT:
		Emit("%R = sext %T %R to %T", ins->target, ins->ops[0]->type, ins->ops[0], ins->target->type);
		break;
	case I_FPTRUNC:
		Emit("%R = fptrunc %T %R to %T", ins->target, ins->ops[0]->type, ins->ops[0], ins->target->type);
		break;
	case I_FPEXT:
		Emit("%R = fpext %T %R to %T", ins->target, ins->ops[0]->type, ins->ops[0], ins->target->type);
		break;
	case I_FPTOUI:
		Emit("%R = fptoui %T %R to %T", ins->target, ins->ops[0]->type, ins->ops[0], ins->target->type);
		break;
	case I_FPTOSI:
		Emit("%R = fptosi %T %R to %T", ins->target, ins->ops[0]->type, ins->ops[0], ins->target->type);
		break;
	case I_UITOFP:
		Emit("%R = uitofp %T %R to %T", ins->target, ins->ops[0]->type, ins->ops[0], ins->target->type);
		break;
	case I_SITOFP:
		Emit("%R = sitofp %T %R to %T", ins->target, ins->ops[0]->type, ins->ops[0], ins->target->type);
		break;
	case I_PTRTOINT:
		Emit("%R = ptrtoint %T %R to %T", ins->target, ins->ops[0]->type, ins->ops[0], ins->target->type);
		break;
	case I_INTTOPTR:
		Emit("%R = inttoptr %T %R to %T", ins->target, ins->ops[0]->type, ins->ops[0], ins->target->type);
		break;
	case I_ICMP:
		Emit("%R = icmp %Y <type> %R, %R", ins->target, ins->cmpOp, ins->ops[0], ins->ops[1]);
 		break;
	case I_FCMP:
		Emit("%R = fcmp %Z <type> %R, %R", ins->target, ins->cmpOp, ins->ops[0], ins->ops[1]);
		break;
	case I_VA_ARG:
		Emit("va_arg");
		break;

	default:
		Text(";;; *** unknown instruction");
		__debugbreak();
	}
}

PRIVATE void EmitData() {

}

PRIVATE void EmitBlock(IN Block* block) {

	Instr* inst;

	Text(".L%d:", block->name);
	for (inst = block->code.first; inst; inst = inst->next)
		EmitIL(inst);
}

PUBLIC void GenerateNCCIL(IN Function* fc) {

	Block** b;

	Text("define [cconv] <ResultType> @%s (<argument_list>)", fc->name);

	for (b = VectorFirst(&fc->blocks); b; b = VectorNext(&fc->blocks, b))
		EmitBlock(*b);
}

PUBLIC void NCCILInit(IN char* fname) {
	_fileout = fopen(fname, "w+");
	Text(";");
	Text("; Generated by NCC");
	Text(";");
}

/************************************************************************
*
*	main.c - Entry point
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* Test
	Neptune C Compiler

	Everything in the "C" directory implements
	the C front end parse stack. All front ends
	must implement Parse() and NextParse(). They
	return a GlobalDefinition with a list of Basic
	Blocks.
	+--------------------+
    | IL Translater <-- Parse()/NextParse() <--+
    | [AST Optimization] |                     |
	| Semantic checks    |                     |
    | Parser             |       for (GlobalDefinition* def=Parse("file.c");
    | Preprocessor       |                  def; def = NextParse()) {
    | Scanner            |
    | File stack         |       }
    +--------------------+
	The compiler treats the front end as a black
	box. Parse() and NextParse() are expected to
	return a valid list of Basic Blocks with correct
	Intermediate Code that faithfully represents the
	original program. If there are no front end errors
	reported, the compiler will pass the GlobalDefinition
	through a series of optimization passes, ending with
	a liveness analysis and register allocation before
	passing it onto the output.

	Out/dot.c - Writes a GlobalDefinition object to a DOT file
	(basically its control flow graph.) Useful for debugging.

	Out/x86.c - Translates a GlobalDefinition object to x86
	assembly code. Must be done after Liveness Analysis->Register
	allocation.

	This component implements the entry point and global exit function called
	by Fatal(). All command line switches are handled here as well.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <signal.h>
#include "ncc.h"
#include "icode.h"

void test() {

//	if (1)
//		static int MYSTATIC2=4;

	static Instr MYSTATIC[5];

	printf("\n\rhi %p", &MYSTATIC);
}

/* experimenting */
extern Block* RegisterAllocator(IN Block* cfg);

/* Front ends */

PRIVATE char* _dotfile;
PRIVATE char* _pngfile;

PRIVATE void _test(void);

PRIVATE void NccExit(void) {

	Buffer buf;

	// converts .dot to .png. Used to visualize control flow graph:
	InitBuffer(&buf);
	BufferPrintf(&buf, "dot -Tpng %s > %s", _dotfile, _pngfile);
	fprintf(stdout, GetBufferData(&buf));
	system(GetBufferData(&buf));
	Free(buf.data);

	__debugbreak();
}
 
char* GetBaseFileName(IN char const *path) {

	char *s;
	char* r;
	char* ext;
	s = strrchr(path, '/');
	if (!s)
		r = _strdup(path);
	else
		r = _strdup(s + 1);
	ext = strrchr(r, '.');
	if (ext)
		*ext = '\0'; // remove extension
	return r;
}

PRIVATE void PrepareTestOutput(IN char* infile) {

	char*          testdir;
	char*          basename;

	testdir = getenv("TEST_DIR");
	basename = GetBaseFileName(infile);

	_dotfile = malloc(strlen(testdir) + strlen(basename) + ".dot");
	_pngfile = malloc(strlen(testdir) + strlen(basename) + ".png");

	sprintf(_dotfile, "%s%s%s", testdir, basename, ".dot");
	sprintf(_pngfile, "%s%s%s", testdir, basename, ".png");
}

extern void RegAlloc(IN Function* fc, IN CodeGeneratorCaps c);
extern CodeGeneratorCaps X86GetCaps();

// https://github.com/dmlevering/c-garbage-collector

int MYGLOBAL;

PUBLIC int main(IN int argc, IN char** argv) {

	int MYLOCAL=3;

	switch (MYLOCAL) {

	default: printf("def"); break;
	case 1: printf("1"); break;
	case 2: printf("2"); break;
	}

	{
		int MYLOCAL;
		printf("\n\rhi %p", &MYLOCAL);
		{
			int MYLOCAL;
			printf("\n\rhi %p", &MYLOCAL);
		}
	}

	Function fc;

	printf("\n\rhi %p", &MYGLOBAL);
	printf("\n\rhi %p", &MYLOCAL);


	{

		// double
	//	00548928  movsd       xmm0, mmword ptr ds : [5518E0h]
	//	00548930  movsd       mmword ptr[ebp - 2Ch], xmm0

		// float
	//	00428928  movss       xmm0, dword ptr ds : [431868h]
	//	00428930  movss       dword ptr[ebp - 28h], xmm0

//		float k;
//		k = 1.2f;
//		__debugbreak();
	}

	MmInit();
	atexit(NccExit);
	RegisterInternalMessages();

//	??_C@_0CB@MEFGBCCJ@Execute?5using?5NCC?5path?1to?1file?4c@

	printf("Execute using NCC path/to/file.c");

	if (argc != 2)
		Fatal(MESSAGE, "Execute using NCC path/to/file.c");

	InitFrontEnd();
	X86Init("./test/test_asm.txt");
	NCCILInit("./test/test_il.txt");
	for (Parse(argv[1], &fc); fc.source; NextParse(&fc)) {

		FlushMessages();
		if (!fc.entry) continue;

//		Liveness(&fc);

		GenerateNCCIL(&fc);

//		RegAlloc(&fc, X86GetCaps());
//		GenerateX86(&fc);
	}

#if 0
	FileManager*   fm;
	File*          file;
	Translator*    translate;
	CodeGenerator* gen;

	typedef void(*signal_handler_t)(int);

	signal_handler_t previousHandler;
//	previousHandler = signal(SIGSEGV, SignalHandler);

	MmInit();
	atexit(NccExit);
	RegisterInternalMessages();

	if (argc != 2)
		Fatal(MESSAGE, "Execute using NCC path/to/file.c");

	PrepareTestOutput(argv[1]);

	fm = GetFileManager();
	gen = GetCodeGeneratorDot();

	file = fm->open(argv[1]);
	if (!file)
		Fatal(FILENOTFOUND, argv[1]);

	translate = GetTranslator();

	translate->init();
	gen->init("test/test.dot");

	{
		Block* b;
		while (TRUE) {
			b = translate->gen(file);
			if (!b)
				break;
			FlushMessages();
			b = RegisterAllocator(b);
			gen->write(b);
		}
	}

	gen->flush();

#endif
	FlushMessages();
	Info(MESSAGE, "Compilation end");
	return EXIT_SUCCESS;
}

/*
============================================================

	The following code is not part of the NCC core
	but is called by main() for explicate compiler tests.

============================================================
*/

void fooA(void) {

	int a[4][4];
	int cur = 0;
	int addr = &a;

	for (int z = 0; z < 4; z++)
	for (int zz = 0; zz < 4; zz++)
		a[z][zz] = cur++;

	// b = 64 (4 * 4 * sizeof (int))
	int b = sizeof a;
	// i = 4
	int i = sizeof (a + 1);
	// j = 16
	int j = sizeof *(a + 1);

	int c = &a + 1;

	int d = &(*(a + 1)); // &a[1]

	__debugbreak();
}

// C4047 warning

//  int s;
//  int* a = (int(*(*)(int, char))[3.45]) s;

PRIVATE void _test(void) {

	//	struct { int i; } object[2];

	int object[2];

	// a[x] = *(a+x)
	//	return NewDeref(NewBinaryOp(OP_PLUS, expr, primaryExpression));

	// expression must have an arithmitic or pointer type
	object - 2;

	//	a->b = (*a).b	

	int* i = 0x42;
	int k = &(*i);
	int j = i;

	// & * have same operator presidence
	// right to left associativity

	// 6.5.3.2 page 78
	//	If the operand is the result of a unary * operator,
	//		neither that operator nor the & operator is evaluated and the result is as if both were
	//		omitted, except that the constraints on the operators still apply and the result is not an
	//		lvalue.

	//	(*((struct { int i; }*)0)).i = 52;

	//	((struct { int i; }*)0)->i = 52;

	struct { int i; } o[2];

	// satisfies "expression must have arithmitic or pointer type"

	// mabye if we see an identifier that references an array type,
	// we should convert it to a pointer to array type?

	int v = k + 4;

	__debugbreak();
}


#if 0

Resources --

Array decay: http://c-faq.com/aryptr/pass2dary.html

https://bitbucket.org/neon6000/osds/branch/ps2key

https://stackoverflow.com/questions/41247102/git-branch-as-submodule-to-own-repository

https://github.com/awh/compsci-papers
file:///C:/Users/Michael/Downloads/Parsing%20Techniques%20-%20A%20Practical%20Guide.pdf
http://excel.fit.vutbr.cz/submissions/2015/065/65.pdf

https://www.isi.edu/~pedro/Teaching/CSCI565-Spring16/Lectures/ExtraNotes.pdf

https://www.inf.ed.ac.uk/teaching/courses/copt/
https://www.inf.ed.ac.uk/teaching/courses/copt/lecture-1.pdf
http://www.ccs.neu.edu/home/amal/course/4410-s13/lec/lec12-ssa-cps.pdf

https://www.cs.cmu.edu/~aplatzer/course/Compilers/waitegoos.pdf

https://courses.cs.washington.edu/courses/cse401/07wi/CSE401-07cogen.pdf
https://courses.cs.washington.edu/courses/cse401/07wi/CSE401-07ir.pdf

All lectures https://courses.cs.washington.edu/courses/cse401/07wi/

http://www.montefiore.ulg.ac.be/~geurts/Cours/compil/2012/05-intermediatecode-2012-2013.pdf
http://scheme2006.cs.uchicago.edu/11-ghuloum.pdf
https://www2.cs.arizona.edu/~collberg/Teaching/453/2009/Handouts/Handout-15.pdf

http://cs.lmu.edu/~ray/notes/ir/
https://stackoverflow.com/questions/10687899/static-single-assignment-not-all-possible-paths-define-a-variable-how-to-inse
https://cs.nyu.edu/~gottlieb/courses/2000s/2007-08-fall/compilers/lectures/lecture-10.html

http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1548.pdf
https://jhjourdan.mketjh.fr/pdf/jourdan2017simple.pdf
http://cs.lmu.edu/~ray/notes/ir/
http://www.cse.aucegypt.edu/~rafea/csce447/slides/IntermediateCode.pdf

http://cs.lmu.edu/~ray/notes/ir/
https://www.cs.cmu.edu/~fp/courses/15411-f08/lectures/10-irtrees.pdf
https://www.cs.cmu.edu/~fp/courses/15411-f08/lectures/
http://web.cs.iastate.edu/~weile/cs641/2.ProgramRepresentations.pdf

https://courses.cs.washington.edu/courses/cse501/04wi/slides/slides.slides.ps.pdf
https://www.cs.purdue.edu/homes/hosking/502/notes/15-ssa.pdf
http://compilers.cs.ucla.edu/fernando/projects/soc/reports/short_tech.pdf
https://www.cs.cmu.edu/~rjsimmon/15411-f15/lec/10-ssa.pdf

https://en.wikipedia.org/wiki/The_lexer_hack
ftp://ftp.cs.princeton.edu/reports/1991/303.pdf
http://www.es.ele.tue.nl/education/5JJ55-65/mmips/files/lcc/lcc4cgi.pdf

https://github.com/larmel/lacc
http://www.cs.cornell.edu/courses/cs412/2008sp/lectures/lec24.pdf
https://github.com/jjedele/C-to-3AddressCode-Compiler

http://www.diku.dk/hjemmesider/ansatte/torbenm/ICD/IntermCodeGen.pdf

http://www.cs.cornell.edu/courses/cs412/2008sp/lectures/lec24.pdf
https://www.cs.bu.edu/teaching/c/graph/linked/
http://homepages.dcc.ufmg.br/~fernando/classes/dcc888/ementa/slides/ControlFlowGraphs.pdf
	 
	 
	 void f() {
		 char*p;
		 **(struct a { int i; }**)p = 0;
		 (void)p(0);
		 return 0;
		 return;
	 }
	 
#endif

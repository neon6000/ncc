/************************************************************************
*
*	sym.c - Symbol table
*
*   Copyright(c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This is a work in progress ----------------- */

/*
	This component implements the symbol table.
*/

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "base.h"
#include "ncc.h"
#include "icode.h"
#include "c.h"

// http://www3.cs.stonybrook.edu/~cse304/Fall07/projects/sym/sym.html
// http://digital.cs.usu.edu/~allan/Compilers/Notes/SymbolTable.pdf
// http://www.cs.dartmouth.edu/~mckeeman/references/JCLT/ResolvingTypedefsInAMultipassCCompiler.pdf
// http://arantxa.ii.uam.es/~modonnel/Compilers/04_SymbolTablesII.pdf

PRIVATE Scope* _globalScope;

PRIVATE void Init(void) {

	_globalScope = Alloc(sizeof(Scope));
	memset(_globalScope, 0, sizeof(Scope));
	_globalScope->sym[NS_IDENTS] = NewMap();
	_globalScope->sym[NS_TAGS] = NewMap();
	_globalScope->sym[NS_LABELS] = NULL;
}

PRIVATE Scope* OpenScope(IN Scope* parent) {

	Scope* scope;
	if (!_globalScope)
		Init();
	if (!parent)
		return _globalScope;
	scope = Alloc(sizeof(Scope));
	scope->parent = parent;
	scope->sym[NS_LABELS] = parent->sym[NS_LABELS] ? parent->sym[NS_LABELS] : NewMap();
	scope->sym[NS_IDENTS] = NewMap();
	scope->sym[NS_TAGS] = NewMap();
	scope->level = parent->level + 1;
	scope->function = parent->function;
	return scope;
}

PRIVATE Scope* CloseScope(IN Scope* current) {

	if (current == _globalScope)
		Fatal(MESSAGE, "BUG: _CloseScope on global");
	return current->parent;
}

PRIVATE BOOL Insert(IN Scope* current, IN char* name, IN Namespace ns, IN Type* type) {

	Map*   map;
	BOOL   isLocal;
	Value* sym;

	if (!_globalScope)
		Init();
	map = current->sym[ns];
	if (MapGet(map, name))
		return FALSE;
	isLocal = current->parent != NULL;
	if (isLocal)
		sym = NamedValue(type, InternString(name));
	else
		sym = NamedGlobal(type, InternString(name));
	MapPut(map, name, sym);
	return TRUE;
}

PRIVATE Value* Lookup(IN Scope* current, IN char* name, IN Namespace ns) {

	Map*   map;
	Value* sym;

	map = NULL;
	do{
		map = current->sym[ns];
		sym = (Value*)MapGet(map, name);
		if (sym)
			return sym;
		current = current->parent;
	} while (current);
	return NULL;
}

PRIVATE Scope* GetGlobalScope(void) {

	if (!_globalScope)
		Init();
	return _globalScope;
}

PRIVATE SymbolTable _symbols = {
	Lookup, Insert, OpenScope, CloseScope, GetGlobalScope
};

PUBLIC SymbolTable* GetSymbolTable(void) {
	return &_symbols;
}

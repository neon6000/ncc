/************************************************************************
*
*	sem.c - Semantic Analyzer
*
*   Copyright(c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This is a work in progress ----------------- */

/*
	This component implements semantic checks and type checking. It analyzes
	the abstract syntax tree and may generate errors or modify the tree as needed
	to simplify later stages (the intermediate language generator.)
*/

#include "base.h"
#include "ncc.h"
#include "icode.h"
#include "c.h"

// https://github.com/tvcutsem/tinyc/blob/master/tinyc.pdf
// https://jameshfisher.com/2016/12/08/c-array-decaying/


PRIVATE uint32_t _parseErrors;

PRIVATE AstNode* Walk(IN AstNode* node);

extern PUBLIC AstNode* NewNode(IN AstNodeKind node);

PRIVATE AstNode* WalkDecay(IN AstNode* node);
PRIVATE AstNode* WalkNoDecay(IN AstNode* node);

PRIVATE AstNode* NewIntNode(IN int value) {

	AstNode* node;

	node = NewNode(AST_LITERAL);
	node->lit.val = InternString(intToStr(value));
	node->type = NewType(DT_INT, Q_NONE, S_NONE, TRUE);
	return node;
}

//
// 6.3.2.1 - Test if node is an lvalue expression
//
PRIVATE BOOL IsLValue(IN AstNode* node) {

	AstNodeKind kind = node->kind;
	if (kind != AST_VARIABLE && kind != AST_DEREF && kind != AST_STRUCT_REF)
		return FALSE;
	if (IsVoidType(node->type))
		return FALSE;
	return TRUE;
}

//
// 6.3.2.1 - Test if structure or union is modifiable
// If any member (including recursively) has CONST qualifier,
// then it is not modifiable.
//
PRIVATE BOOL IsStructModifiable(IN Type* type) {

	List*     members;
	ListNode* cur;
	Type*     mtype;

	/* if this is not a struct or union, thats a bug. */
	if (!IsStructOrUnionType(type))
		__debugbreak();

	/* for all members in this structure/union: */
	members = MapGetKeys(type->struc.fields);
	for (cur = members->first; cur; cur = cur->next) {

		/* if the member type has const qualifier, its not modifiable. */
		mtype = MapGet(type->struc.fields, cur->data);
		if (IsConstType(mtype))
			return FALSE;

		/* recursively descend if the member type is a struct or union. */
		if (IsStructOrUnionType(mtype)) {
			if (!IsStructModifiable(mtype))
				return FALSE;
		}
	}

	return TRUE;
}

//
// 6.3.2.1 - Check that the type of the speciifed node is a Complete type.
//
PRIVATE void CheckCompleteType(IN AstNode* node) {

	if (!IsCompleteType(node->type))
		Error(EXPECTED_COMPLETE_TYPE, node->loc, node->type);
}

//
// 6.3.2.1 - Check that the node is a modifiable lvalue.
//
PRIVATE void CheckModifiableLValue(IN AstNode* node) {

	if (!(IsLValue(node) && IsCompleteType(node->type) && !IsArrayType(node->type) && !IsConstType(node->type)))
		Error(EXPECTED_MODIFIABLE_LVALUE, node->loc);
	
	if (IsStructOrUnionType(node->type) && !IsStructModifiable(node->type))
		Error(EXPECTED_MODIFIABLE_LVALUE, node->loc);
}

//
// 6.3.2.1 - Check that the node is an lvalue.
//
PRIVATE void CheckLValue(IN AstNode* node) {

	if (IsLValue(node) || IsFunctionType(node->type) || IsArrayType(node->type))
		return;
	Error(EXPECTED_LVALUE_OR_FUNCTIONDESG, node->loc);
}

//
// Check that the node type is an integer (short,long,signed/unsiged) type.
//
PRIVATE void CheckInt(IN AstNode* node) {

	if (!IsIntType(node->type))
		Error(EXPECTED_INT_TYPE, node->loc);
}

//
// Check that the node type is a pointer
//
PRIVATE void CheckPointer(IN AstNode* node) {

	if (!IsPtrType(node->type))
		Error(EXPECTED_PTR_TYPE, node->loc);
}

//
// Check that the node type is an arithmitic type (Integer or Float.)
//
PRIVATE void CheckArithType(IN AstNode* node) {

	if (!IsIntOrFltType(node->type))
		Error(EXPECTED_ARITH_TYPE, node->loc);
}

//
// Check that the node type is an arithmitic or pointer type.
//
PRIVATE void CheckArithOrPtr(IN AstNode* node) {

	if (!(IsIntOrFltType(node->type) || IsPtrType(node->type)))
		Error(EXPECTED_ARITH_OR_PTR_TYPE, node->loc);
}

//
// Check that the node type is not void.
//
PRIVATE void CheckNotVoid(IN AstNode* node) {

	if (IsVoidType(node->type))
		Error(ASSIGN_VOID_TYPE, node->loc);
}

//
// Check that this is a valid cast.
//
PRIVATE void CheckCast(IN AstNode* node) {

	DataType to = node->type->dt;
	DataType from = node->unary.left->type->dt;

	if (to == DT_FUNCTION || to == DT_STRUCT || to == DT_UNION)
		Error(CAST_NOT_ALLOWED, node->loc, node->unary.left->type, node->type);
	if (from == DT_STRUCT || from == DT_UNION)
		Error(CAST_NOT_ALLOWED, node->loc, node->unary.left->type, node->type);
}

//
//	6.3.2.1 - Nodes of type "array of T" are decayed to
//  "pointer to element of type T" in all cases except:
//     1. If the node is an operand of SIZEOF operator.
//     2. If the node is an operand of the ADDRESS OF operator.
//
PRIVATE AstNode* MaybeDecay(IN AstNode* base, IN BOOL decay) {

	AstNode* node;
	int i = 0;

	if (!decay || !IsArrayType(base->type))
		return base;
	i = IsArrayType(base->type);
	node = NewNode(AST_ADDROF);
	node->type = PointerType(base->type->array.type);
	node->addrOf.node = base;
	return node;
}

PRIVATE AstNode* Cast(IN AstNode* left, IN Type* type) {

	AstNode* node;

	node = NewNode(AST_UNARY);
	node->type = type;
	node->unary.op = OP_CAST;
	node->unary.left = left;
	return node;
}

//
// 6.3.1.8 - Usual arithmitic conversions
//
PRIVATE BOOL MabeyConvert(IN AstNode* parent, IN DataType dt) {

	if (parent->bin.left->type->dt == dt) {
		parent->bin.right = Cast(parent->bin.right, parent->bin.left->type);
		parent->type = parent->bin.left->type;
		return TRUE;
	}
	if (parent->bin.right->type->dt == dt) {
		parent->bin.left = Cast(parent->bin.left, parent->bin.right->type);
		parent->type = parent->bin.right->type;
		return TRUE;
	}
	return FALSE;
}

PRIVATE void UsualArithConv(IN AstNode* parent) {

	if (parent->bin.left->type->dt == parent->bin.right->type->dt) {
		parent->type = parent->bin.left->type;
		return;
	}

	if (MabeyConvert(parent, DT_LDOUBLE))
		return;
	if (MabeyConvert(parent, DT_DOUBLE))
		return;
	if (MabeyConvert(parent, DT_FLOAT))
		return;

	// FIXME: extend to better support short/long,unsigned/signed

	parent->type = IntType();
	parent->type->isUnsigned = FALSE; //signed int type
}

//
// 6.5.3.3 - unary arithmetic operators
//
PRIVATE AstNode* CheckUnary(IN AstNode* node) {

	switch (node->unary.op) {
	case OP_POSTINC:
	case OP_POSTDEC:
	case OP_INC:
	case OP_DEC:
		node->unary.left = WalkDecay(node->unary.left);
		CheckArithOrPtr(node->unary.left);
		CheckModifiableLValue(node->unary.left);
		node->type = node->unary.left->type;
		node->type->tq = Q_NONE;
		break;
	case OP_NOT:
		node->unary.left = WalkDecay(node->unary.left);
		CheckArithOrPtr(node->unary.left); // scalar type
		node->type = IntType();
		break;
	case OP_COMPL:
		node->unary.left = WalkDecay(node->unary.left);
		CheckInt(node->unary.left);
		node->type = node->unary.left->type;
		break;
	case OP_CAST:
		node->unary.left = WalkDecay(node->unary.left);
		CheckCast(node);
		break;
	}
	return node;
}

//
// 6.2.7 - Compatible type and composite type
// additional rules are:
//   ..in.. 6.7.2 for type specifiers
//   ..in.. 6.7.3 for type qualifiers
//   ..in.. 6.7.5 for declarators
//
PRIVATE void CheckPtrIndirectionLevel(IN AstNode* node, IN Type* left, IN Type* right) {

	int leftLevel = 0;
	int rightLevel = 0;
	Type* cur;
	if (!(IsPtrType(left) && IsPtrType(right)))
		return;
	for (cur = left; IsPtrType(cur); cur = cur->pointer.type)
		leftLevel++;
	for (cur = right; IsPtrType(cur); cur = cur->pointer.type)
		rightLevel++;
	if (leftLevel!=rightLevel)
		Warning(PTR_INDIRECTION_MISMATCH, node->loc, left, right);
}

//
// binary operators
//
PRIVATE AstNode* CheckBinary(IN AstNode* node) {

#define LEFT node->bin.left
#define RIGHT node->bin.right

	switch (node->bin.op) {
	//
	// 6.5.16.1 - simple assignment
	//
	case OP_EQ:
		node->bin.left = WalkDecay(LEFT);
		node->bin.right = WalkDecay(RIGHT);
		CheckModifiableLValue(LEFT);
		CheckNotVoid(RIGHT);
		CheckPtrIndirectionLevel(node, LEFT->type, RIGHT->type);

		if (IsIntOrFltType(LEFT->type))
			CheckArithType(RIGHT);
		else if (IsStructOrUnionType(LEFT->type)) {
			if (!IsStructOrUnionType(RIGHT->type))
				Error(INCOMPATIBLE_ASSIGN_TYPE, node->loc, LEFT->type, RIGHT->type);
			if (LEFT->type->struc.tag == NULL || RIGHT->type->struc.tag == NULL)
				Error(INCOMPATIBLE_ASSIGN_TYPE, node->loc, LEFT->type, RIGHT->type);
			if (LEFT->type->struc.tag && LEFT->type->struc.tag != RIGHT->type->struc.tag)
				Error(INCOMPATIBLE_ASSIGN_TYPE, node->loc, LEFT->type, RIGHT->type);
		}
		else if (IsPtrType(LEFT->type) && IsPtrType(RIGHT->type)
			&& LEFT->type->tq == RIGHT->type->tq) {
			// if the data these are pointing to are not compatible, set
			// error incompatible types - from '' to ''
			;
		}
		else if (IsPtrType(LEFT->type) && IsPtrType(RIGHT->type)
			&& LEFT->type->tq == RIGHT->type->tq
			&& IsVoidType(RIGHT->type->pointer.type)) {
			;
		}
		else if (IsPtrType(LEFT->type) && IsIntType(RIGHT->type))
			; // this is good
		else
			Error(INCOMPATIBLE_ASSIGN_TYPE, node->loc, LEFT->type, RIGHT->type);

		node->type = LEFT->type;
		node->type->tq = Q_NONE;
		break;
	//
	// 6.5.16.2 - compound assignment
	//
	case OP_ADD_ASSIGN:
	case OP_SUB_ASSIGN:
		node->bin.left = WalkDecay(LEFT);
		node->bin.right = WalkDecay(RIGHT);
		CheckModifiableLValue(LEFT);
		if (IsPtrType(LEFT->type))
			CheckInt(RIGHT);
		else{
			CheckArithType(LEFT);
			CheckArithType(RIGHT);
		}
		node->type = LEFT->type;
		node->type->tq = Q_NONE;
		break;
	case OP_MUL_ASSIGN:
	case OP_DIV_ASSIGN:
	case OP_MOD_ASSIGN:
	case OP_AND_ASSIGN:
	case OP_XOR_ASSIGN:
	case OP_OR_ASSIGN:
	case OP_RIGHT_ASSIGN:
	case OP_LEFT_ASSIGN:
		node->bin.left = WalkDecay(node->bin.left);
		node->bin.right = WalkDecay(node->bin.right);
		CheckModifiableLValue(node->bin.left);
		CheckArithType(node->bin.left);
		CheckArithType(node->bin.right);
		node->type = node->bin.left->type;
		node->type->tq = Q_NONE;
		break;
	//
	// 6.5.17 - Comma operator
	//
	case OP_COMMA:
		node->bin.left = WalkDecay(node->bin.left);
		node->bin.right = WalkDecay(node->bin.right);
		node->type = node->bin.right->type;
		break;
	//
	// 6.5.5 - multiplicative operators
	//
	case OP_MUL:
	case OP_DIV:
		node->bin.left = WalkDecay(node->bin.left);
		node->bin.right = WalkDecay(node->bin.right);
		CheckArithType(node->bin.left);
		CheckArithType(node->bin.right);
		//
		// the usual arithmitic conversions are performed.
		// because of this, both operands will have same type
		// after implicate casts, in which case node->type would be that.
		// until this is done, this is marked FIXME.
		//
		UsualArithConv(node);
//		node->type = IntType();
		break;
	case OP_MODULO:
		node->bin.left = WalkDecay(node->bin.left);
		node->bin.right = WalkDecay(node->bin.right);
		CheckInt(node->bin.left);
		CheckInt(node->bin.right);
		node->type = IntType();
		break;
	//
	// 6.5.6 - additive operators
	//
	case OP_PLUS:
		node->bin.left = WalkDecay(node->bin.left);
		node->bin.right = WalkDecay(node->bin.right);
		if (node->bin.left->type->dt == DT_PTR) {
			CheckInt(node->bin.right);
			node->type = node->bin.left->type;
		}
		else if (node->bin.right->type->dt == DT_PTR) {
			CheckInt(node->bin.left);
			node->type = node->bin.right->type;
		}
		else{
			CheckArithType(node->bin.left);
			CheckArithType(node->bin.right);
			// FIXME: perform usual arithmitic conversions
			UsualArithConv(node);
//			node->type = node->bin.left->type;
		}
		break;
	case OP_MINUS:
		node->bin.left = WalkDecay(node->bin.left);
		node->bin.right = WalkDecay(node->bin.right);
		if (IsPtrType(node->bin.left->type) && IsPtrType(node->bin.right->type)) {
			// FIXME: actual type should be ptrdiff_t
			UsualArithConv(node);
//			node->type = IntType();
//			node->type->isUnsigned = FALSE; //signed int type
		}
		else if (IsPtrType(node->bin.left->type)) {
			CheckInt(node->bin.right);
			node->type = node->bin.left->type;
		}
		else{
			CheckArithType(node->bin.left);
			CheckArithType(node->bin.right);
			// FIXME: perform usual arithmitic conversions
			UsualArithConv(node);
//			node->type = node->bin.left->type;
		}
		break;
	//
	// 6.5.7 - bitwise shift operators
	//
	case OP_SHIFT_LEFT:
	case OP_SHIFT_RIGHT:
		node->bin.left = WalkDecay(node->bin.left);
		node->bin.right = WalkDecay(node->bin.right);
		CheckInt(node->bin.left);
		CheckInt(node->bin.right);
		node->type = node->bin.left->type;
		break;
	//
	// 6.5.8 - relational operators
	//
	case OP_LTE:
	case OP_GTE:
	case OP_LESS:
	case OP_GREATER:
		LEFT = WalkDecay(LEFT);
		RIGHT = WalkDecay(RIGHT);
		CheckArithOrPtr(LEFT);
		CheckArithOrPtr(RIGHT);
		CheckPtrIndirectionLevel(node, LEFT->type, RIGHT->type);
		node->type = IntType();
		break;
	//
	// 6.5.9 - equality operators
	//
	case OP_ISEQ:
	case OP_ISNOTEQ:
		node->bin.left = WalkDecay(node->bin.left);
		node->bin.right = WalkDecay(node->bin.right);
		CheckArithOrPtr(node->bin.left);
		CheckArithOrPtr(node->bin.right);
		CheckPtrIndirectionLevel(node, LEFT->type, RIGHT->type);
		node->type = IntType();
		break;
	//
	// 6.5.10 - bitwise AND operator
	// 6.5.11 - exclusive OR operator
	// 6.5.12 - inclusive OR operator
	//
	case OP_AND:
	case OP_XOR:
	case OP_OR:
		node->bin.left = WalkDecay(node->bin.left);
		node->bin.right = WalkDecay(node->bin.right);
		CheckInt(node->bin.left);
		CheckInt(node->bin.right);
		node->type = IntType();
		break;
	//
	// 6.5.13 - logical AND operator
	//
	case OP_LOG_OR:
	case OP_LOG_AND:
		node->bin.left = WalkDecay(node->bin.left);
		node->bin.right = WalkDecay(node->bin.right);
		CheckArithOrPtr(node->bin.left);
		CheckArithOrPtr(node->bin.right);
		node->type = IntType();
		break;
	default:
		__debugbreak();
	};

	return node;
}

//
// Descend children nodes and set types.
//
// node - Perform semantic analysis on this node and its children.
//        May modify child nodes and set the current node type as needed.
//
// decay - if TRUE, and the current node is of type "array",
//         decay it to a pointer. The only cases where current
//         node can be an array is AST_VARIABLE, AST_DEREF, and AST_STRUCT_REF
//         since no other node kind can ever return an array type.
//
PRIVATE AstNode* _Walk(IN AstNode* node, BOOL decay) {

	AstNode*  stmt;
	Type*     field;

	switch (node->kind) {
	case AST_LITERAL:
		return node;
	// FIXME: Decay does not apply if base is an array that has been initialized
	// to a string literal.
	case AST_VARIABLE:
		return MaybeDecay(node, decay);
	case AST_UNARY:
		return CheckUnary(node);
	case AST_BINARY:
		return CheckBinary(node);
	case AST_TERNARY:
		node->ternary.one = WalkDecay(node->ternary.one);
		node->ternary.two = WalkDecay(node->ternary.two);
		node->ternary.three = WalkDecay(node->ternary.three);
		node->type = node->ternary.two->type;
		return node;
	//
	// 6.5.3.2 - Address and indirection operators
	//
	case AST_DEREF:
		node->deref.node = WalkDecay(node->deref.node);
		if (node->deref.node->type->dt != DT_PTR)
			Error(POINTER_EXPECTED, node->loc);
		node->type = node->deref.node->type->pointer.type;
		return MaybeDecay(node, decay);
	//
	// 6.5.3.2 - Address and indirection operators
	//
	case AST_ADDROF:
		node->addrOf.node = WalkNoDecay(node->addrOf.node);
		node->type = PointerType(node->addrOf.node->type);
		CheckLValue(node->addrOf.node);
		node->addrOf.take = TRUE;
		if (node->addrOf.node->kind == AST_DEREF){
			node->addrOf.take = FALSE;
			return node->addrOf.node->deref.node;
		}
		return node;
	//
	// declaration list
	//
	case AST_DECL:
		node->type = node->decl.declvar->type;
		CheckCompleteType(node);
		for (stmt = VectorFirst(node->decl.init); stmt; stmt = VectorNext(node->decl.init, stmt)) {
			WalkDecay(stmt);
			CheckNotVoid(stmt);
//			CheckCompatible(node, node->type, stmt->type);
		}
		return node;

	case AST_INIT:	
		node->init.initval = WalkDecay(node->init.initval);
		node->type = node->init.initval->type;
		return node;

	case AST_RETURN:
		if (node->ret.retval) {
			node->ret.retval = WalkDecay(node->ret.retval);
			node->type = node->ret.retval->type;
		}
		else
			node->type = VoidType();
		return node;

	case AST_CASE:
		node->caseStmt.stmt = WalkDecay(node->caseStmt.stmt);
		return node;

	case AST_DEFAULT:
		node->defaultStmt.stmt = WalkDecay(node->defaultStmt.stmt);
		return node;

	case AST_STRUCT_REF:
		stmt = WalkDecay(node->struc.struc);
		if (!stmt->type)
			__debugbreak(); // bug if hit.
		if (stmt->type->dt != DT_STRUCT
			&& stmt->type->dt != DT_UNION) {
			Error(EXPECTED_STRUCT_OR_UNION, stmt->loc);
		}
		field = MapGet(stmt->type->struc.fields, node->struc.field);
		if (!field)
			Error(NOT_A_MEMBER_OF, stmt->loc, node->struc.field, stmt->type->struc.tag);
		node->type = field;
		return MaybeDecay(node, decay);

	case AST_FUNCTION_CALL:
		for (stmt = VectorFirst(node->fc.args);
			stmt;
			stmt = VectorNext(node->fc.args, stmt)) {

			VectorSetElement(node->cmpnd.stmt, stmt, WalkDecay(stmt));
		}
		node->fc.expr = WalkDecay(node->fc.expr);
		return node;

	case AST_IF:
		node->ifStmt.cond = WalkDecay(node->ifStmt.cond);
		node->ifStmt.then = WalkDecay(node->ifStmt.then);
		if (node->ifStmt.els)
			node->ifStmt.els = WalkDecay(node->ifStmt.els);
		return node;

	case AST_DO:
		node->dowhile.expr = WalkDecay(node->dowhile.expr);
		node->dowhile.stat = WalkDecay(node->dowhile.stat);
		return node;

	case AST_WHILE:
		node->loopwhile.expr = WalkDecay(node->loopwhile.expr);
		node->loopwhile.stat = WalkDecay(node->loopwhile.stat);
		return node;

	case AST_FOR:
		if (node->loopfor.cond)
			node->loopfor.cond = WalkDecay(node->loopfor.cond);
		if (node->loopfor.init)
			node->loopfor.init = WalkDecay(node->loopfor.init);
		if (node->loopfor.next)
			node->loopfor.next = WalkDecay(node->loopfor.next);
		node->loopfor.stmt = WalkDecay(node->loopfor.stmt);
		return node;

	case AST_SWITCH:
		node->switchStmt.expr = WalkDecay(node->switchStmt.expr);
		node->switchStmt.stmt = WalkDecay(node->switchStmt.stmt);
		return node;

	case AST_COMPOUND_STMT:
		for (stmt = VectorFirst(node->cmpnd.stmt);
			stmt;
			stmt = VectorNext(node->cmpnd.stmt, stmt)) {

			VectorSetElement(node->cmpnd.stmt, stmt, WalkDecay(stmt));
		}
		return node;
	case AST_GOTO:
		return node;
	case AST_CONTINUE:
	case AST_BREAK:
	case AST_LABEL:
		return node;
	case AST_SIZEOF:
		// sizeof can have type or expression operand:
		if (node->size.type)
			field = node->size.type;
		else
			field = WalkNoDecay(node->size.expr)->type;
		return NewIntNode(field->size);
	default:
		__debugbreak();
		return NULL;
	};
}

PRIVATE AstNode* WalkNoDecay(IN AstNode* node) {

	return _Walk(node, FALSE);
}

PRIVATE AstNode* WalkDecay(IN AstNode* node) {

	return _Walk(node, TRUE);
}

PRIVATE void Analyze(IN AstNode* node) {

	if (node->kind != AST_FUNCTION)
		return;
	WalkDecay(node->function.body);
}

PRIVATE AstNode* Process(void) {

	AstNode* decl;

	while (TRUE) {
		decl = GetParser()->parse();
		if (!decl) {
			return NULL;
		}
		if (GetErrorCount() > _parseErrors) {
			_parseErrors = GetErrorCount();
			fprintf(stderr, "Parse errors detected, skipping semantic checks...");
			continue;
		}
		Analyze(decl);
		return decl;
	}
}

PRIVATE Analyzer _analyzer = { Process };

PUBLIC Analyzer* GetSemanticAnalyzer(void) {
	return &_analyzer;
}


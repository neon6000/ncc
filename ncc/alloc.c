/************************************************************************
*
*	alloc.c - Allocator.
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/*
	alloc is a cache based memory allocator (i.e. slab allocator)
	this is for performance & to reduce time calling the OS.
	Freeing memory mearly stores it back in the cache allowing
	it to be resued later.

	PROGRAM calls Alloc or AllocObject to allocate as needed.
	Alloc aligns the size to a power of 2 and allocates from
	a general cache.

	Garbage collector calls Free (which determines the cache to
	free from based on pointer.) Note all this does is add it
	back to the cache.
*/


// garbage collector -- during scan, if we determine something is a pointer,
// get the Header and check magic & checksum. only support cache allocations.
// question -- does this work with pointer to pointers?

#include <malloc.h>
#include "base.h"

#define MAGIC 0xabcdabcd

typedef struct Header {
	uint32_t magic;
	BOOL     freed;
	char*    tag;
	Cache*   cache;
	size_t   size;
	struct   Header* flink;
	struct   Header* blink;
	// uimt32_t checksum; // extra security check, sum of values in above
}Header;

#define IsValidMagic(x) (x->magic == MAGIC)

// general allocator -------

// this allocator implements Alloc and Free for general (any size)
// allocations. Allocations are tracked to simplify debugging in
// a global allocation store.

PRIVATE Header  _top;
PRIVATE Header* _allocs;

PUBLIC void MmInit(void) {

	memset(&_top, 0, sizeof(Header));
	_top.tag = "MMTOP";
	_allocs = &_top;
}

PRIVATE void AllocListAdd(IN Header* object, IN Header** list) {

	if (*list)
		(*list)->flink = object;
	object->flink = NULL;
	object->blink = *list;
	*list = object;
}

PRIVATE Header* AllocListFree(IN Header* object, IN Header** list) {

	if (!object->blink) {
		__debugbreak(); // _top is always first item, so should not happen
		// this is the first item in this list
		*list = NULL;
	}
	else if (object == *list) {
		// this is the last item the list
		if (object->flink)
			__debugbreak(); // should not happen
		*list = (*list)->blink;
		(*list)->flink = NULL;
	}
	else {
		// this is a middle item
		object->blink->flink = object->flink;
		object->flink->blink = object->blink;
	}

	object->blink = NULL;
	object->flink = NULL;
	return object;
}

PUBLIC void* AllocTag(IN char* tag, IN size_t size) {

	Header* block = malloc(sizeof(Header)+size);
	block->magic = MAGIC;
	block->cache = NULL;
	block->freed = FALSE;
	block->flink = NULL;
	block->blink = NULL;
	block->size = size;
	block->tag = tag;

	AllocListAdd(block, &_allocs);
	return block + 1;
}

PUBLIC void* Alloc(IN size_t size) {

	return AllocTag("", size);
}

PUBLIC void* Realloc(IN void* memory, IN size_t size) {

	Header* block;
	char*   tag;
	Cache*  cache;
	Header* newBlock;

	if (!memory)
		return Alloc(size);

	block = (Header*)memory - 1;
	if (!IsValidMagic(block))
		__debugbreak();

	tag = block->tag;
	cache = block->cache;

	AllocListFree(block, &_allocs);

	newBlock = realloc(block, sizeof(Header)+size);
	newBlock->magic = MAGIC;
	newBlock->freed = FALSE;
	newBlock->flink = NULL;
	newBlock->blink = NULL;
	newBlock->size = size;
	newBlock->cache = cache;
	newBlock->tag = tag;

	AllocListAdd(newBlock, &_allocs);
	return newBlock + 1;
}

PUBLIC void Free(IN void* p) {

	Header* block = (Header*)p - 1;

	if (!IsValidMagic(block))
		__debugbreak();

	AllocListFree(block, &_allocs);
	free(block);
}

PUBLIC void VisitGlobalAlloc(IN alloc_callback_t callback) {

	Header* object;

	if (!callback) return;
	for (object = _allocs; object; object = object->blink) {
		if (callback(object + 1, object->size, object->tag, object->cache))
			return;
	}
}

// cache allocator --------

// implements AllocObject, FreeObject, NewCache, and FreeCache.
// caches free'd objects to improve performance and memory use.
// call FreeCache to release memory resources only when the cache
// is no longer needed.

PRIVATE void PushFreeObject(IN Header* object, IN Header** stack) {

	object->flink = NULL;
	object->blink = *stack;
	object->freed = TRUE;

	if (*stack)
		(*stack)->flink = object;

	*stack = object;
}

PRIVATE Header* PopFreeObject(IN Header** stack) {

	Header* object;

	// make sure there is something to pop
	if (*stack == NULL)
		__debugbreak();

	object = *stack;
	*stack = (*stack)->blink;

	if (*stack != NULL)
		(*stack)->flink = NULL;

	object->flink = NULL;
	object->blink = NULL;
	object->freed = FALSE;
	return object;
}

PUBLIC void* AllocObject(IN Cache* cache) {

	Header* object;

	if (!cache->freeStack)
		object = malloc(sizeof(Header)+cache->objSize);
	else
		object = PopFreeObject(&cache->freeStack);

	cache->totalSize += cache->objSize;
	cache->numAllocs++;

	object->magic = MAGIC;
	object->cache = cache;
	object->freed = FALSE;
	object->size = cache->objSize;

	if (cache->ctor)
		cache->ctor(object + 1);

	return object+1;
}

PUBLIC void FreeObject(IN void* p) {

	Header* block;
	Cache*  cache;

	block = (Header*)p - 1;

	// make sure memory is valid
	if (!IsValidMagic(block))
		__debugbreak();

	// watch for double frees
	if (block->freed)
		__debugbreak();

	cache = block->cache;

	PushFreeObject(block, &cache->freeStack);
	cache->totalSize -= cache->objSize;
	cache->numAllocs--;
}

PUBLIC Cache* NewCache(IN size_t size, IN OPTIONAL object_init_t ctor) {

	Cache* cache = malloc(sizeof(Cache));

	cache->name = NULL;
	cache->numAllocs = 0;
	cache->totalSize = 0;
	cache->objSize = size;
	cache->freeStack = NULL;
	cache->ctor = ctor;
	return cache;
}

PUBLIC Cache* NewCacheEx(IN char* name, IN size_t size, IN OPTIONAL object_init_t ctor) {

	Cache* cache;
	
	cache = NewCache(size, ctor);
	cache->name = name;
	return cache;
}

PUBLIC void FreeCache(IN Cache* cache) {

	Header* object;
	Header* temp;

	// attempt to free cache with objects in use
	if (cache->numAllocs > 0)
		__debugbreak();

	object = cache->freeStack;
	while (object) {
		temp = object;
		object = object->blink;
		free(temp);
	}
	cache->freeStack = NULL;
	free(cache);
}

PUBLIC void VisitCache(IN Cache* cache, IN alloc_cache_t callback) {

	Header* object;

	if (!callback) return;
	for (object = cache->freeStack; object; object = object->blink) {
		if (callback(cache, object + 1))
			return;
	}
}

PUBLIC void MmTest(IN Cache* cache) {

	Header* o1, *o2, *o3, *o4, *o5;

	o1 = AllocObject(cache);
	o2 = AllocObject(cache);

	FreeObject(o1);
	FreeObject(o2);

	o3 = AllocObject(cache);
	o4 = AllocObject(cache);
	o5 = AllocObject(cache);

	FreeObject(o3);
	FreeObject(o4);
	FreeObject(o5);

	FreeCache(cache);

	__debugbreak();
}

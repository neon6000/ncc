/************************************************************************
*
*	file.c - File Management
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* this is a work in progress ------------- */

// translation phase 1 (see 5.1.1.2)

// todo: do trigraph conversion here (if user flag set).
// digraphs are handled in scanner since they are token level.
// see https://en.wikipedia.org/wiki/Digraphs_and_trigraphs#C

#include <stdio.h>
#include <assert.h>
#include "base.h"
#include "ncc.h"

PRIVATE size_t FileDepth      (void);
PRIVATE BOOL   FilePush       (IN File* file);
PRIVATE File*  FilePop        (void);
PRIVATE File*  FileGetCurrent (void);
PRIVATE int    _FileGet       (void);
PRIVATE void   FileUnget      (IN int c);
PRIVATE int    FileGet        (void);
PRIVATE int    FilePeek       (IN int index);
PRIVATE File*  FileOpen       (IN char* filename);
PRIVATE void   FileClose      (IN File* file);

/*
	Global file stack.
*/
PRIVATE Vector* _FileStack;

/**
*	FileDepth
*
*	Description : This procedure returns the current file depth.
*
*	Output : Current file depth.
*/
PRIVATE size_t FileDepth(void) {

	return VectorLength(_FileStack);
}

/**
*	FilePush
*
*	Description : This procedure pushes a file on the file stack.
*
*	Input : Pointer to opened file object.
*
*	Output : TRUE if successful, FALSE otherwise.
*/
PRIVATE BOOL FilePush (IN File* file) {

	assert(file != NULL);
	if (!file->fstream)
		return FALSE;
	if (!_FileStack) {
		_FileStack = NewVector(sizeof(File*));
		if (!_FileStack)
			return FALSE;
	}
	VectorPush(_FileStack, &file);
	return TRUE;
}

/**
*	FilePop
*
*	Description : This procedure pops a file from the file stack.
*
*	Output : Pointer to file object or NULL if stack is empty.
*/
PRIVATE File* FilePop (void) {

	File* file = NULL;
	if (VectorLength(_FileStack) > 0)
		VectorPop(_FileStack, &file);
	return file;
}

/**
*	FileGetCurrent
*
*	Description : This procedure returns a pointer to the current file.
*
*	Output : Pointer to file object or NULL if stack is empty.
*/
PRIVATE File* FileGetCurrent(void) {

	File* file = NULL;
	if (VectorLength(_FileStack) > 0)
		VectorLastElement(_FileStack, &file);
	return file;
}

/**
*	_FileGet
*
*	Description : This gets the next character. It looks in this order:
*	               1. Pushback buffer.
*	               2. Character stream.
*	               3. (If NULL) Next file in file stack.
*
*	Output : Character.
*/
PRIVATE int _FileGet(void) {

	File* file;
	int c;

	file = FileGetCurrent();
try_again:
	
	if (!file)
		return EOF;
	/*
		if anything in this files pushback buffer,
		pop it and return it.
	*/
	if (GetBufferLength(file->pushback) > 0) {
		return BufferPop(file->pushback);
	}
	/*
		read character from this file stream.
	*/
	c = fgetc(file->fstream);
	/*
		if EOF, continue to pop files off the stack
		until we either get no EOF or reach no more files.
	*/
	if (c == EOF) {
		if (VectorLength(_FileStack) > 0) {
			VectorPop(_FileStack, &file);
			file = FileGetCurrent();
			if (!file)
				return EOF;
			file->line++; //#include directive was processed
			goto try_again;
		}
	}

	if (!VectorLength(_FileStack))
		c = EOF;
	return c;
}

/**
*	FileUnget
*
*	Description : This puts a character on the pushback buffer.
*
*	Input : c - Character to push.
*/
PRIVATE void FileUnget(IN int c) {

	File* file;

	file = FileGetCurrent();
	if (!file)
		return;

	if (c == '\n')
		file->line--;

	BufferPush(file->pushback, c);
}

/**
*	FileGet
*
*	Description : This file gets the next character from the file stack.
*	This procedure translates EOL and line-continuation.
*	If the current file is at its end, this gets next character from next file
*	in file stack, if it exists. If no more files, it returns EOF.
*
*	Output : Next character. It may also return EOL ('\n') or EOF if no more
*	files in the file stack.
*/
PRIVATE int FileGet(void) {

	File* file = FileGetCurrent();
	if (!file)
		return EOF;
	/*
		Get current character from file stream.
	*/
	int c = _FileGet();
	/*
		translate EOL markers. /r to /n and /r/n to /n:
	*/
	if (c == '\r' || c == '\n') {
		if (c == '\r' && FilePeek(1) == '\n')
			_FileGet(); // consume '\n'
		c = '\n';
		file->line++;
	}
	/*
		translate line continuation character
	*/
	if (c == '\\') {
		int n = FilePeek(1);
		BOOL lineContinue = FALSE;
		if (n == '\n' || n == '\r') {
			_FileGet(); // consume EOL character.
			lineContinue = TRUE;
		}
		if (n == '\r' && FilePeek(1) == '\n') {
			_FileGet(); // consume '\n'
			lineContinue = TRUE;
		}
		if (lineContinue) {
			c = _FileGet();
			file->line++;
		}
	}

	return c;
}

/**
*	FilePeek
*
*	Description : Peek ahead at the next couple of characters.
*
*	Input - index - Index of character to look ahead. Must be > 0 and < 32.
*
*	Output : Character at index.
*/
PRIVATE int FilePeek(IN int index) {

	int c;
	int lookahead[32];
	assert(index < 32);
	assert(index > 0);
	for (c = 0; c < index; c++)
		lookahead[c] = FileGet();
	while (c--)
		FileUnget(lookahead[c]);
	return lookahead[index-1];
}

/**
*	FileOpen
*
*	Description : Opens a file and pushes it on the file stack.
*
*	Input - filename - Pointer to file name string.
*
*	Output : Pointer to file object.
*/
PRIVATE File* FileOpen(IN char* filename) {

	FILE* stream;
	File* file;

	stream = fopen(filename, "r");
	if (!stream)
		return NULL;
	file = Alloc(sizeof(File));
	if (!file) {
		fclose(stream);
		return NULL;
	}
	file->line = 1;
	file->fname = filename;
	file->fstream = stream;
	file->eof = FALSE;
	file->pushback = NewBuffer();
	FilePush(file);
	return file;
}

/**
*	FileClose
*
*	Description : Closes the given file.
*
*	Input - file - Pointer to file object to close.
*/
PRIVATE void FileClose(IN File* file) {

	assert(file != NULL);
	if (file->fstream)
		fclose(file->fstream);
	Free(file);
}

/*
	File manager object.
*/
PRIVATE FileManager _fileManager = {
	FilePush,
	FilePop,
	FileDepth,
	FileGetCurrent,
	FilePeek,
	FileGet,
	FileUnget,
	FileClose,
	FileOpen
};

/**
*	GetFileManager
*
*	Description : Returns the global file manager object.
*
*	Output - Pointer to global file manager object.
*/
PUBLIC FileManager* GetFileManager(void) {

	return &_fileManager;
}

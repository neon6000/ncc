# Introduction #

This is the official repository for the Neptune C Compilier and Neptune C Runtime Library.

### Building ###

1. Create an empty folder. Inside of the folder
2. Run git init
3. Run git pull https://neon6000@bitbucket.org/neon6000/ncc.git
4. Go to Project Properties->C/C++->Preprocessor->Preprocessor Definitons and add _CRT_SECURE_NO_WARNINGS

### Command line ###

Currently, NCC does not have any command line switches. Run it using:

NCC path/to/test/file.c

In Visual C++, this can be set using Project Properties->Debugging->Command Arguments. Set it to path/to/test/file.c.

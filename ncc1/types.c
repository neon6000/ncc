/************************************************************************
*
*	types.c - Abstract data types and base functions
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <malloc.h>
#include <assert.h>
#include <string.h>
#include <stdarg.h>
#include "ncc.h"

PUBLIC void* Alloc (IN int size) {
	return malloc(size);
}

PUBLIC void* Allocz (IN int size) {
	char* s = Alloc(size);
	if (!s) __dump();
	memset(s, 0, size);
	return s;
}

PUBLIC void Free (IN void* p) {
	free(p);
}

PUBLIC PLIST_ENTRY RemoveHeadList(PLIST_ENTRY list, PLIST_ENTRY item) {
	PLIST_ENTRY f = list->flink;
	list->flink = list->flink->flink;
	list->flink->blink = list;
	return f;
}

PUBLIC PLIST_ENTRY RemoveTailList(PLIST_ENTRY list, PLIST_ENTRY item) {
	PLIST_ENTRY f = list->blink;
	list->blink = list->blink->blink;
	list->blink->flink = list;
	return f;
}

PUBLIC PSLIST_ENTRY PopEntryList(PSLIST_ENTRY slist, PSLIST_ENTRY item) {
	PSLIST_ENTRY f = slist->flink;
	slist->flink = slist->flink->flink;
	return f;
}

PUBLIC unsigned long Hash(IN char* key) {

	const unsigned char *str = (const unsigned char *)key;
	unsigned long hash = 5381;
	int c;

	/* djb2 algorithm */
	while ((c = *str++) != '\0')
		hash = hash * 33 + c;

	return hash;
}

PUBLIC unsigned long HashW(IN wchar_t* wkey) {

	const wchar_t *str = (const wchar_t *)wkey;
	unsigned long hash = 5381;
	int c;

	/* djb2 algorithm */
	while ((c = *str++) != '\0')
		hash = hash * 33 + c;

	return hash;
}

PMAP NewMap(IN bool_t allowDups, IN int numBuckets) {

	PMAP map = Alloc(sizeof(MAP));
	int i = 0;
	assert(numBuckets > 0);
	map->level = 0;
	map->parent = NULL;
	map->numBuckets = numBuckets;
	map->buckets = Allocz(sizeof(BUCKET)*numBuckets);
	for (i = 0; i < numBuckets;i++)
		InitListHead(&map->buckets[i].itemList);
	map->numElements = 0;
	map->allowDups = allowDups;
	return map;
}

PUBLIC void* MapGetA(IN PMAP map, IN char* key) {

	unsigned long idx;
	PBUCKET bucket;

	idx = Hash(key) % map->numBuckets;
	bucket = &map->buckets[idx];

	ForEach(&bucket->itemList, entry) {
		PBUCKET_ENTRY item = (PBUCKET_ENTRY)entry;
		if (!strcmp(item->key, key))
			return item->val;
	}
	return NULL;
}

PUBLIC bool_t MapPutA(IN PMAP map, IN char* key, IN void* value) {

	unsigned long idx;
	PBUCKET bucket;
	PBUCKET_ENTRY item;

	if (!map->allowDups && MapGetA(map, key))
		return FALSE;

	idx = Hash(key) % map->numBuckets;
	bucket = &map->buckets[idx];
	item = Allocz(sizeof(BUCKET_ENTRY));
	item->key = _strdup(key);
	item->wkey = NULL;
	item->val = value;
	InitListHead(&item->e);
	InsertTailList(&bucket->itemList, &item->e);
	return TRUE;
}

PUBLIC void* MapGetW(IN PMAP map, IN wchar_t* wkey) {

	unsigned long idx;
	PBUCKET bucket;

	idx = HashW(wkey) % map->numBuckets;
	bucket = &map->buckets[idx];

	ForEach(&bucket->itemList, entry) {
		PBUCKET_ENTRY item = (PBUCKET_ENTRY)entry;
		if (!wcscmp(item->wkey, wkey))
			return item->val;
	}
	return NULL;
}

PUBLIC bool_t MapPutW(IN PMAP map, IN wchar_t* wkey, IN void* value) {

	unsigned long idx;
	PBUCKET bucket;
	PBUCKET_ENTRY item;

	if (!map->allowDups && MapGetW(map, wkey))
		return FALSE;

	idx = HashW(wkey) % map->numBuckets;
	bucket = &map->buckets[idx];
	item = Allocz(sizeof(BUCKET_ENTRY));
	item->wkey = wkey;
	item->key = NULL;
	item->val = value;
	InitListHead(&item->e);
	InsertTailList(&bucket->itemList, &item->e);
	return TRUE;
}

PRIVATE PMAP _internTableA;
PRIVATE PMAP _internTableW;

PUBLIC PSTRING NewStringA (IN char* value) {

	PSTRING s;

	if (!_internTableA)
		_internTableA = NewMap(FALSE,50);
	s = MapGetA(_internTableA, value);
	if (!s) {
		s = Alloc(sizeof(STRING));
		s->ref = 0;
		// need to strdup this. Scanner has value
		// in an array which gets overwritten all the time.
		s->val = _strdup(value);
		s->encoding = ENC_ASCII;
		InitListHead(&s->e);
		MapPutA(_internTableA, value, s);
	}
	else s->ref++;
	return s;
}

PUBLIC PSTRING NewStringW(IN wchar_t* value) {

	PSTRING s;
	Fatal("NewStringW called");
	if (!_internTableW)
		_internTableW = NewMap(FALSE, 100);
	s = MapGetW(_internTableW, value);
	if (!s) {
		s = Alloc(sizeof(STRING));
		s->ref = 0;
		s->val = NULL;
//		s->val = value;
		s->encoding = ENC_ASCII;
		InitListHead(&s->e);
		MapPutW(_internTableW, value, s);
	}
	else s->ref++;
	return s;
}

PUBLIC bool_t StrCmp(IN PSTRING a, IN PSTRING b) {

	return strcmp(a->val, b->val) == 0;
}

PUBLIC size_t StrLen(IN PSTRING a) {

	return strlen(a->val);
}

PUBLIC PSTRING StrCat(IN PSTRING a, IN PSTRING b) {

	char* s;

	if (!a) return b;

	if (a->encoding != b->encoding) {
		Error("concatenating mismatched strings");
		return a;
	}

	s = Alloc(StrLen(a) + StrLen(b) + 1);
	s[0] = 0;

	strcat(s, a->val);
	strcat(s, b->val);

	return NewStringA(s);
}

PUBLIC char* StrToChar(IN PSTRING a) {
	char* s = Alloc(StrLen(a) + 1);
	strcpy(s, a->val);
	return s;
}

PUBLIC wchar_t* StrToWideChar(IN PSTRING a) {
	Fatal("StrToWideChar called");
	return NULL;
}

PUBLIC PSET SetInsert(IN PSET k, IN char* s) {

	PSET r;

	r = Alloc(sizeof(SET));
	if (!r) return NULL;
	r->next = k;
	r->s = s;
	return r;
}

PUBLIC bool_t SetFind(IN PSET k, IN char* s) {

	PSET v;

	if (!k) return FALSE;
	for (v = k; v; v = v->next) {
		if (!v->s)
			continue;
		if (!strcmp(v->s, s))
			return TRUE;
	}
	return FALSE;
}

PUBLIC PSET SetUnion(IN PSET a, IN PSET b) {

	PSET k = b;
	for (; a; a = a->next) {
		if (!SetFind(b, a->s))
			k = SetInsert(k, a->s);
	}
	return k;
}

PUBLIC PSET SetIntersect(IN PSET a, IN PSET b) {

	PSET k = NULL;
	for (; a; a = a->next) {
		if (SetFind(b, a->s))
			k = SetInsert(k, a->s);
	}
	return k;
}

#define MIN_SIZE 8
#define MAX(a,b) (a) > (b) ? (a) : (b)

PRIVATE int RoundUp(IN int n) {
	if (n == 0)
		return 0;
	int r = 1;
	while (n > r)
		r *= 2;
	return r;
}

PRIVATE void VectorExtend(IN PVECTOR v, IN size_t by) {
	size_t nalloc;
	if (v->count + by <= v->nalloc)
		return;
	nalloc = MAX(RoundUp(v->count + by), MIN_SIZE);
	v->data = realloc(v->data, nalloc * v->size);
	v->nalloc = nalloc;
}

PUBLIC void VectorSet(IN PVECTOR v, IN index_t idx, IN void* obj) {
	memcpy(&v->data[idx * v->size], obj, v->size);
}

PUBLIC void VectorPush(IN PVECTOR v, IN void* obj) {
	VectorExtend(v, 1);
	memcpy(&v->data[v->count * v->size], obj, v->size);
	v->count++;
}

PUBLIC void VectorPop(IN PVECTOR v, OUT void* obj) {
	v->count--;
	if (obj)
		memcpy(obj, &v->data[v->count * v->size], v->size);
}

PUBLIC PVECTOR VectorAppend(IN PVECTOR a, IN PVECTOR b) {

}

#define BUFFER_SIZE 8

PUBLIC PBUFFER NewBuffer(void) {

	PBUFFER r = malloc(sizeof(BUFFER));
	r->data = malloc(BUFFER_SIZE);
	r->allocCount = BUFFER_SIZE;
	r->elementCount = 0;
	return r;
}

PUBLIC void BufferFree(IN PBUFFER r) {

	if (!r)
		return;
	free(r->data);
	free(r);
}

PRIVATE void Resize(IN PBUFFER b) {

	int newsize = b->allocCount * 2;
	uint8_t* data = realloc(b->data, newsize);
	b->data = data;
	b->allocCount = newsize;
}

PUBLIC uint8_t* GetBufferData(IN PBUFFER b) {

	return b->data;
}

PUBLIC size_t GetBufferLength(IN PBUFFER b) {

	return b->elementCount;
}

PUBLIC void BufferWrite(IN PBUFFER b, IN char c) {

	if (b->allocCount == (b->elementCount + 1))
		Resize(b);
	b->data[b->elementCount++] = c;
}

PUBLIC void BufferPush(IN PBUFFER b, IN char c) {

	BufferWrite(b, c);
}

PUBLIC char BufferPop(IN PBUFFER b) {

	assert(b->elementCount != 0);
	return b->data[--b->elementCount];
}

PUBLIC void BufferAppend(IN PBUFFER b, IN char* s, IN int len) {

	for (int i = 0; i < len; i++)
		BufferWrite(b, s[i]);
}

PUBLIC void BufferPrintf(IN PBUFFER b, IN char* fmt, ...) {

	va_list args = NULL;
	if (strlen(fmt) == 1) {
		BufferPush(b, *fmt);
		BufferPush(b, '\0');
		return;
	}
	while (TRUE) {
		int avail = b->allocCount - b->elementCount;
		va_start(args, fmt);
		int written = vsnprintf(b->data + b->elementCount, avail, fmt, args);
		va_end(args);
		if (written == -1 || (avail <= written)) {
			Resize(b);
			continue;
		}
		b->elementCount += written;
		return;
	}
}


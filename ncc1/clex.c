/************************************************************************
*
*	clex.c - C scanner
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <ctype.h>
#include <stdio.h>
#include <malloc.h>
#include "ncc.h"

#if 0
Precedence
<unary,postfix> 14, 15
% * /  13
+ -    12
<< >>  11
< >    10
<= >=  10
== !=  9
&      8
^      7
|      6
&&     5
||     4
?:     3
<assignment> 2
,      1
#endif

// if we introduce PushFile, PopFile, can just update these
// to point to the new current file. This would allow us to possibly
// treat this File structure as PRIVATE to clex.

#define IDENT_SIZE     2048
#define PUSHBACK_EMPTY 0

PRIVATE char     _ident[IDENT_SIZE + 1];
PRIVATE FILE*    _src;

//
//	Pushback character for Unget and PeekChar
//
PRIVATE int      _pushbackChar;

//
//	Current FILE and LINE
//
//	This effects the location of items that will be displayed
//	in output messages as well as the predefined _FILE_ and
//	_LINE_ macros. This also gets carried into output listing files
//	and associated debug information.
//
PRIVATE COORD    _coord;

//
//	Used with #line directive and internally for
//	tracking current location
//
PUBLIC void SetLine(IN unsigned y, IN OPTIONAL PSTRING fname) {

	_coord.y = y;
	if (fname)
		_coord.fname = fname;
}

//
// Advance to next line in same file
//
#define NextLine() (_coord.y++)

//
// Go back a line in same file
//
#define LastLine() (_coord.y--)

//
// Get current file name
//
PUBLIC PSTRING GetFileName(void) {

	return _coord.fname;
}

//
// Get current line in current file
//
PUBLIC unsigned GetLine(void) {

	return _coord.y;
}

//
// Push new file to stack. Used with #include.
// This also sets current active line to the start
// of this file.
//
PUBLIC void PushFile(IN PSTRING fname) {

	FILE*    file;
	unsigned len;
	unsigned pad;

	file = fopen(fname->val, "rb");
	if (!file) Fatal("unable to open file");

	SetLine(1, fname);
	_src = file;
}

//
//	peek ahead from the input file. This
//	is only used by fgetc to test for the
//	Line Termination sequence '\n' and '\r'
//	and Line Continuation sequence
//
PRIVATE int fpeekc(IN FILE* in, IN int i) {

	int b[3];
	int c;
	int r;
	if (i == 0 || i>3) __debugbreak();
	for (c = 0; c < i; c++)
		b[c] = fgetc(in);
	r = b[--c];
	for (; c >= 0; --c)
		ungetc(b[c], in);
	return r;
}

//
//	test if valid new line char
//
PRIVATE bool_t IsNewLineChar(IN int c) {

	return c == '\n' || c == '\r';
}

//
//	consume new line characters
//
//	input "c" must have already been consumed (fgetc)
//	and is a valid starting new line character '\n' or '\r'
//
PRIVATE void GetCharLine(IN int c) {

	int c2;

	if (IsNewLineChar(c)) {
		c2 = fpeekc(_src, 1);
		if (c == '\n' && c2 == '\r')
			fgetc(_src);
		else if (c == '\r' && c2 == '\n')
			fgetc(_src);
	}
}

//
//	Return next character from file stream.
//	This currently reads from "_src". If we move
//	the PREPROC before LEX, this should call the
//	PPROC.
//
PRIVATE int GetChar(void) {

	int c;
	int c2;

	if (_pushbackChar != PUSHBACK_EMPTY) {
		c = _pushbackChar;
		_pushbackChar = PUSHBACK_EMPTY;
		if (c == '\n')
			NextLine();
		return c;
	}

	c = 0;
	while (c != EOF) {

		// get from current file
		c = fgetc(_src);

		//
		//	new lines
		//
		if (IsNewLineChar(c)) {
			GetCharLine(c);
			NextLine();
			return '\n';
		}

		//
		//	line continuation
		//
		if (c == '\\') {
			c2 = fpeekc(_src, 1);
			if (IsNewLineChar(c2)) {
				c2 = fgetc(_src);
				GetCharLine(c2);
				NextLine();
				continue;
			}
		}

		return c;
	}
}

//
//	Unget a character so it can be picked up again by GetChar.
//	Can only Unget one character.
//
PRIVATE void UngetChar(IN int c) {

	if (_pushbackChar != PUSHBACK_EMPTY)
		Fatal("Scanner: Unable to unget character");
	_pushbackChar = c;
	if (c == '\n')
		LastLine();
}

//
//	peek ahead
//
PRIVATE int PeekChar(void) {

	int c;

	c = GetChar();
	UngetChar(c);
	return c;
}




// ----------------------------


#define BUFSIZE 4096

PRIVATE char    cbuf[BUFSIZE+1];
PRIVATE bool_t  firstOnLine = TRUE;
PRIVATE VECTOR  tokstream;
PRIVATE TOKEN   lasttok;

PRIVATE TOKEN Token(IN int op) {

	TOKEN tok;
	memset(&tok, 0, sizeof(TOKEN));
	tok.firstOnLine = firstOnLine;
	tok.op = op;
	firstOnLine = FALSE;
	tok.coord = _coord;
	return tok;
}

PRIVATE TOKEN TokenOp(IN int op, IN int prec, IN int kind, IN LPNODE tree) {

	TOKEN t;
	
	t = Token(op);
	t.prec = prec;
	t.optype = kind;
	t.tree = tree;
	return t;
}

PRIVATE TOKEN TokenAttr(IN int op, IN int attr) {

	TOKEN t;
	
	t = Token(op);
	t.attr = attr;

	if (attr == ATTR_TYPENAME) {
		switch (op) {
		case K_CHAR: t.type = CharType; break;
		case K_INT: t.type = IntType; break;
		case K_FLOAT: t.type = FloatType; break;
		case K_DOUBLE: t.type = DoubleType; break;
		case K_SIGNED: t.type = IntType; break;
		case K_UNSIGNED: t.type = UnsignedIntType; break;
		case K_ENUM: t.type = EnumeratorType; break;
		case K_VOID: t.type = VoidType; break;
		case K_LONG: t.type = LongType; break;
		case K_SHORT: t.type = ShortType; break;
		};
	}
	return t;
}

PRIVATE TOKEN TokenTypeName(IN PSYMBOL sym) {

	TOKEN tok;
	
	tok = Token(TK_TYPENAME);
	tok.val.x = sym;
	tok.type = sym->type;
	tok.attr = ATTR_TYPENAME;
	return tok;
}

PRIVATE TOKEN TokenIdent(IN char* s) {

	TOKEN tok;
	
	tok = Token(TK_IDENT);
	tok.val.s = NewStringA(s);
	return tok;
}

PUBLIC TOKEN ErrorToken(void) {

	return Token(TK_ERROR);
}

PRIVATE TOKEN GetTokenStr(IN PTYPE type) {

	TOKEN    tok;
	char*    s;
	index_t  idx;
	int      c;

	tok = Token(TK_STRING);
	tok.type = type;
	tok.val.s = 0;

	s = NULL;
	for (idx = 0, c = GetChar(); c != '\"'; idx++) {
		s = realloc(s, idx+1);
		if (c == EOF || c == '\n') {
			Error("String token not terminated");
			UngetChar(c);
			s[idx] = 0;
			return tok;
		}
		s[idx] = c;
		c = GetChar();
	}
	if (!s) s = realloc(s, 1);
	s[idx] = 0;
	tok.val.s = NewStringA(s);
	return tok;
}

PRIVATE TOKEN GetTokenChar(IN PTYPE type) {

	TOKEN tok;
	
	tok = Token(TK_NUMBER);
	tok.type = type;
	tok.val.i = 0;

	__debugbreak();
}

PRIVATE int Escape(void) {

	int c = GetChar();

	switch (c) {
	case 'a': return '\a';
	case 'b': return '\b';
	case 'f': return '\f';
	case 'n': return '\n';
	case 'r': return '\r';
	case 't': return '\t';
	case 'v': return '\v';
	case '\'': return '\'';
	case '\"': return '\"';
	case '\\': return '\\';
	case 'x':
		Fatal("Scanner: /x escape sequence not yet supported");
		return 0;
	case '0': case '1': case '2': case '3':
	case '4': case '5': case '6': case '7':
		Fatal("Scanner: /0 ... /7 escape sequence not yet supported");
		return 0;
	default: Fatal("Scanner: Invalid escape sequence"); return 0;
	}
}

PRIVATE char* StrConst(void) {

	int     delim;
	index_t cidx;
	int     c;

	delim = GetChar();
	cidx = 0;

	if (delim != '\'' && delim != '\"') Fatal("Scanner: StrConst invalid delim");
	for (cidx = 0; cidx < BUFSIZE-2; cidx++) {
		c = GetChar();
		if (c == '\n' || c == EOF) Fatal("Missing closing quote");
		if (c == '\\') cbuf[cidx] = Escape();
		else if (c == delim) break;
		else cbuf[cidx] = c;
	}

	cbuf[cidx] = 0;

	if (cidx == BUFSIZE-2)
		Fatal("Scanner: String constant too large");

	UngetChar(c);
	return cbuf;
}

// return suffix + <number of chars read>
PRIVATE char* IntConst(IN char* suffix, IN VALUE_UINT i, IN bool_t overflow, IN int base, OUT PTOKEN tok) {

	// we can support 64 bit literals by just changing VALUE_TYPE. However need to make sure
	// compiler can produce 64 bit code on 32 bit targets. For now, holding on supporting.
	if (strlen(suffix) == 3 && ((tolower(suffix[0]) == 'u' && tolower(suffix[1]) == 'l' && tolower(suffix[2]) == 'l')
		|| (tolower(suffix[0]) == 'l' && tolower(suffix[1]) == 'l' && tolower(suffix[1]) == 'u'))) {

		Fatal("Scanner: ULL and LLU suffixes not yet supported");
	}
	else if ((tolower(suffix[0]) == 'l' && tolower(suffix[1]) == 'l')) {

		Fatal("Scanner: LL suffix not yet supported");
	}
	else if ((tolower(suffix[0]) == 'u' && tolower(suffix[1]) == 'l')
		|| (tolower(suffix[0]) == 'l' && tolower(suffix[1]) == 'u')) {

		suffix += 2;
		tok->type = UnsignedLongType;
	}
	else if (tolower(suffix[0]) == 'u') {
		suffix++;

		if (overflow || i <= UnsignedIntType->limits.max.ui)
			tok->type = UnsignedIntType;
		else
			tok->type = UnsignedLongType;

	} else if (tolower(suffix[0]) == 'l') {
		suffix++;

		if (overflow || i <= LongType->limits.max.i)
			tok->type = LongType;
		else
			tok->type = UnsignedLongType;
	}
	else if (i > LongType->limits.max.i) {

		tok->type = UnsignedLongType;
	}
	else if (i > IntType->limits.max.i) {

		tok->type = LongType;
	}
	else{

		tok->type = IntType;
	}

	if (tok->type->kind == TYPE_INT) {
		if (overflow || i > tok->type->limits.max.i) {
			Warn("Integer overflow");
			tok->val.i = tok->type->limits.max.i;
		}
		else tok->val.i = i;
	}
	else if (tok->type->kind == TYPE_UNSIGNED) {
		if (overflow || i > tok->type->limits.max.ui) {
			Warn("Unsigned integer overflow");
			tok->val.ui = tok->type->limits.max.ui;
		}
		else tok->val.ui = i;
	}
	else Fatal("Scanner: IntConst bug - non-int type");
	return suffix;
}

PRIVATE TOKEN FloatConst(IN char* tok) {

	__debugbreak();
}

PRIVATE TOKEN GetTokenNumber(void) {

	int        c;
	TOKEN      tok;
	bool_t     overflow;
	VALUE_UINT i;
	index_t    cidx;
	char*      s=cbuf;

	i = 0;
	overflow = FALSE;

	for (cidx = 0; cidx < BUFSIZE - 2; cidx++) {
		c = GetChar();
		if (!isalnum(c)) break;
		cbuf[cidx] = c;
	}
	cbuf[cidx] = 0;
	UngetChar(c);

	tok = Token(TK_NUMBER);
	if (s[0] == '0' && tolower(s[1]) == 'x') {
		s += 2;
		for (; *s; s++) {
			int digit;
			c = *s;
			if (isdigit(c))
				digit = c - '0';
			else if (c >= 'a' && c <= 'f')
				digit = c - 'a' + 10;
			else if (c >= 'A' && c <= 'F')
				digit = c - 'A' + 10;
			else
				break;
			if (VALUE_OVERFLOW(i,4))
				overflow = TRUE;
			else
				i = (i << 4) + digit;
		}
		s = IntConst(s, i, overflow, 16, &tok);
		if (*s != 0) Fatal("Scanner: Invalid hexadecimal constant");
		return tok;
	} else if (s[0] == '0' && tolower(s[1]) == 'b') {
		s += 2;
		for (; *s; s++) {
			int digit;
			c = *s;
			if (c == '0' || c == '1')
				digit = c - '0';
			else
				break;
			if (VALUE_OVERFLOW(i, 2))
				overflow = TRUE;
			else
				i = (i << 1) + digit;
		}
		s = IntConst(s, i, overflow, 2, &tok);
		if (*s != 0) Fatal("Scanner: Invalid binary constant");
		return tok;
	}
	else if (s[0] == '0') {
		s++;
		for (; *s; s++) {
			int digit;
			c = *s;
			if (c >= '0' || c <= '8')
				digit = c - '0';
			else
				break;
			if (VALUE_OVERFLOW(i, 3))
				overflow = TRUE;
			else
				i = (i << 3) + digit;
		}
		if (*s == '.' || *s == 'e' || *s == 'E')
			return FloatConst(cbuf);
		s = IntConst(s, i, overflow, 8, &tok);
		if (*s != 0) Fatal("Scanner: Invalid octal constant");
		return tok;
	} else {
		for (; *s; s++) {
			int digit;
			c = *s;
			if (isdigit(c))
				digit = c - '0';
			else
				break;
			if (i > (VALUE_MAX - digit) / 10)
				overflow = TRUE;
			else
				i = i * 10 + digit;
		}
		if (*s == '.' || *s == 'e' || *s == 'E')
			return FloatConst(cbuf);
		s = IntConst(s, i, overflow, 10, &tok);
		if (*s != 0) Fatal("Scanner: Invalid decimal constant");
		return tok;
	}

	__debugbreak();
}

PRIVATE TOKEN _Lex(void) {

	TOKEN    tok;
	PSYMBOL  sym;
	index_t  cidx;
	int      c;

	c = 0;
	if (VectorLen(&tokstream)) {
		VectorPop(&tokstream, &tok);
		return tok;
	}

	while (c != EOF) {
	next:
		c = GetChar();
		switch (c) {
		case EOF:return Token(TK_EOF);
		case '/':
			if (PeekChar() == '/') {
				// single line comment:
				while (c != '\n' && c != EOF)
					c = GetChar();
				if (c == '\n') goto next;
				Fatal("Unknown lexer state");
			}
			else if (PeekChar() == '*') {
				/* mult-line comment: */
				c = GetChar();
				while ((c != '*' || PeekChar() != '/') && c != EOF)
					c = GetChar();
				if (c == '*') {
					GetChar();
					goto next;
				}
				else if (c == EOF)
					Fatal("EOF reached while processing comment");
				Fatal("Unknown lexer state");
			}
			else {
				if (PeekChar() == '=') {
					GetChar();
					return TokenOp(TK_DIV_EQ, 10, OP_BINARY, Assgn);
				}
				return TokenOp('/', 13, OP_BINARY, TreeMul);
			}
		case '<':
			if (PeekChar() == '<') {
				GetChar();
				if (PeekChar() == '=') {
					GetChar();
					return TokenOp(TK_SHL_EQ, 10, OP_BINARY, Assgn);
				}
				return TokenOp(TK_LSHIFT, 11, OP_BINARY, TreeShift);
			}
			else if (PeekChar() == '=') {GetChar(); return TokenOp(TK_LEQ, 10, OP_BINARY, TreeCmp); }
			else return TokenOp('<', 10, OP_BINARY, TreeCmp);
		case '>':
			if (PeekChar() == '>') {
				GetChar();
				if (PeekChar() == '=') {
					GetChar();
					return TokenOp(TK_SHR_EQ, 10, OP_BINARY, Assgn);
				}
				return TokenOp(TK_RSHIFT, 11, OP_BINARY, TreeShift);
			}
			else if (PeekChar() == '=') { GetChar(); return TokenOp(TK_GEQ, 10, OP_BINARY, TreeCmp); }
			else return TokenOp('>', 10, OP_BINARY, TreeCmp);
		case '-':
			if (PeekChar() == '>') { GetChar(); return Token(TK_DEREF); }
			else if (PeekChar() == '-') { GetChar(); return Token(TK_DECR); }
			else if (PeekChar() == '=') {
				GetChar();
				return Token(TK_SUB_EQ);
			}
			else return TokenOp('-', 12, OP_BINARY, TreeSub);
		case '=':
			if (PeekChar() == '=') {
				GetChar();
				return TokenOp(TK_EQ, 9, OP_BINARY, TreeEq);
			}
			return Token('=');
		case '!':
			if (PeekChar() == '=') {
				GetChar();
				return TokenOp(TK_NEQ, 9, OP_BINARY, TreeEq);
			}
			return Token('!');
		case '|':
			if (PeekChar() == '|') { GetChar(); return TokenOp(TK_LOGOR, 4, OP_BINARY, LogAndOr); }
			else if (PeekChar() == '=') { GetChar(); return Token(TK_OR_EQ); }
			return TokenOp('|', 6, OP_BINARY, TreeBit);
		case '&':
			if (PeekChar() == '&') { GetChar(); return TokenOp(TK_LOGAND, 5, OP_BINARY, LogAndOr); }
			else if (PeekChar() == '=') { GetChar(); return Token(TK_AND_EQ); }
			return TokenOp('&', 8, OP_BINARY, TreeBit);
		case '+':
			if (PeekChar() == '+') { GetChar(); return Token(TK_INCR); }
			else if (PeekChar() == '=') { GetChar(); return Token(TK_ADD_EQ); }
			return TokenOp('+', 12, OP_BINARY, TreeAdd);
		case ';': case ',': case ':': case '~':
		case '?': case '[': case ']': case '{':
		case '}': case '(': case ')':
			return Token(c);
		case '*':
			if (PeekChar() == '=') { GetChar(); return Token(TK_MUL_EQ); }
			return TokenOp(c, 13, OP_BINARY, TreeMul);
		case '%':
			if (PeekChar() == '=') { GetChar(); return Token(TK_MOD_EQ); }
			return TokenOp(c, 13, OP_BINARY, TreeBit);
		case '^':
			if (PeekChar() == '=') { GetChar(); return Token(TK_XOR_EQ); }
			return TokenOp(c, 7, OP_BINARY, TreeBit);
		case '.':
			if (PeekChar() == '.') {
				GetChar();
				if (PeekChar() == '.')
					return Token(TK_ELLIPSIS);
				return Token(TK_ERROR);
			}
			return Token('.');
		case '#': // preprocessor
			if (PeekChar() == '#') { GetChar(); return Token(TK_2HASH); }
			return Token('#');
		case '\n':
			tok = Token('\n');
			firstOnLine = TRUE;
			return tok;
		case '\t':
		case ' ':{
			bool_t k = firstOnLine;
			while (c != EOF && isspace(c) && isspace(PeekChar()))
				c = GetChar();
			tok = Token(' ');
			firstOnLine = k;
			return tok;
		}
		case '0': case '1': case '2': case '3': case '4':
		case '5': case '6': case '7': case '8': case '9':
			UngetChar(c);
			return GetTokenNumber();
		case '\"': return GetTokenStr(ArrayOfCharType);
		case '\'': {

			UngetChar('\'');
			StrConst();

			cbuf;

		   __debugbreak();
			return GetTokenChar(CharType);
		}
		case 'L':
			if (PeekChar() == '\'') return GetTokenChar(WideCharType);
			else if (PeekChar() == '\"') return GetTokenStr(ArrayOfWideCharType);
		default:
			if (!(isalnum(c) || c == '_')) {
				// file encoding error
				Fatal("Scanner: Invalid character '%i'", c);
				return Token(TK_EOF);
			}
			cbuf[0] = c;
			for (cidx = 1; cidx < BUFSIZE - 2; cidx++) {
				c = GetChar();
				if (c == '_' || isalnum(c))
					cbuf[cidx] = c;
				else break;
			}
			UngetChar(c);
			if (cidx == BUFSIZE - 2)
				Fatal("Identifier cannot exceed %i characters", BUFSIZE - 2);
			cbuf[cidx] = 0;

			if (cbuf[0] == 'a') {
				if (cbuf[1]    == 'u'
					&& cbuf[2] == 't'
					&& cbuf[3] == 'o'
					&& cbuf[4] == 0) {

					return Token(K_AUTO);
				}
			}else if (cbuf[0] == 'b') {
				if (cbuf[1]    == 'r'
					&& cbuf[2] == 'e'
					&& cbuf[3] == 'a'
					&& cbuf[4] == 'k'
					&& cbuf[5] == 0) {

					return Token(K_BREAK);
				}
			}
			else if (cbuf[0] == 'c') {
				if (cbuf[1] == 'a'
					&& cbuf[2] == 's'
					&& cbuf[3] == 'e'
					&& cbuf[4] == 0) {

					return Token(K_CASE);
				}else if (cbuf[1] == 'h'
					&& cbuf[2] == 'a'
					&& cbuf[3] == 'r'
					&& cbuf[4] == 0) {

					return TokenAttr(K_CHAR, ATTR_TYPENAME);
				}else if (cbuf[1] == 'o'
					&& cbuf[2] == 'n'
					&& cbuf[3] == 's'
					&& cbuf[4] == 't'
					&& cbuf[5] == 0) {

					return Token(K_CONST);
				}else if (cbuf[1] == 'o'
					&& cbuf[2] == 'n'
					&& cbuf[3] == 't'
					&& cbuf[4] == 'i'
					&& cbuf[5] == 'n'
					&& cbuf[6] == 'u'
					&& cbuf[7] == 'e'
					&& cbuf[8] == 0) {

					return Token(K_CONTINUE);
				}
			}else if (cbuf[0] == 'd') {
				if (cbuf[1] == 'e'
					&& cbuf[2] == 'f'
					&& cbuf[3] == 'a'
					&& cbuf[4] == 'u'
					&& cbuf[5] == 'l'
					&& cbuf[6] == 't'
					&& cbuf[7] == 0) {

					return Token(K_DEFAULT);
				} else if (cbuf[1] == 'o'
					&& cbuf[2] == 'u'
					&& cbuf[3] == 'b'
					&& cbuf[4] == 'l'
					&& cbuf[5] == 'e'
					&& cbuf[6] == 0) {

					return TokenAttr(K_DOUBLE, ATTR_TYPENAME);
				}
				else if (cbuf[1] == 'o'
					&& cbuf[2] == 0) {

					return Token(K_DO);
				}
			}
			else if (cbuf[0] == 'e') {
				if (cbuf[1] == 'l'
					&& cbuf[2] == 's'
					&& cbuf[3] == 'e'
					&& cbuf[4] == 0) {

					return Token(K_ELSE);
				}
				else if (cbuf[1] == 'n'
					&& cbuf[2] == 'u'
					&& cbuf[3] == 'm'
					&& cbuf[4] == 0) {

					return TokenAttr(K_ENUM, ATTR_TYPENAME);
				}
				else if (cbuf[1] == 'x'
					&& cbuf[2] == 't'
					&& cbuf[3] == 'e'
					&& cbuf[4] == 'r'
					&& cbuf[5] == 'n'
					&& cbuf[6] == 0) {

					return Token(K_EXTERN);
				}
			}
			else if (cbuf[0] == 'f') {
				if (cbuf[1] == 'l'
					&& cbuf[2] == 'o'
					&& cbuf[3] == 'a'
					&& cbuf[4] == 't'
					&& cbuf[5] == 0) {

					return TokenAttr(K_FLOAT, ATTR_TYPENAME);
				}
				else if (cbuf[1] == 'o'
					&& cbuf[2] == 'r'
					&& cbuf[3] == 0) {

					return Token(K_FOR);
				}
			}else if (cbuf[0] == 'g') {
				if (cbuf[1] == 'o'
					&& cbuf[2] == 't'
					&& cbuf[3] == 'o'
					&& cbuf[4] == 0) {

					return Token(K_GOTO);
				}
			}
			else if (cbuf[0] == 'i') {
				if (cbuf[1] == 'f'
					&& cbuf[2] == 0) {

					return Token(K_IF);
				}
				else if (cbuf[1] == 'n'
					&& cbuf[2] == 'l'
					&& cbuf[3] == 'i'
					&& cbuf[4] == 'n'
					&& cbuf[5] == 'e'
					&& cbuf[6] == 0) {

					return Token(K_INLINE);
				}
				else if (cbuf[1] == 'n'
					&& cbuf[2] == 't'
					&& cbuf[3] == 0) {

					return TokenAttr(K_INT, ATTR_TYPENAME);
				}
			}
			else if (cbuf[0] == 'l') {
				if (cbuf[1] == 'o'
					&& cbuf[2] == 'n'
					&& cbuf[3] == 'g'
					&& cbuf[4] == 0) {

					return TokenAttr(K_LONG, ATTR_TYPENAME);
				}
			}
			else if (cbuf[0] == 'r') {
				if (cbuf[1] == 'e'
					&& cbuf[2] == 'g'
					&& cbuf[3] == 'i'
					&& cbuf[4] == 's'
					&& cbuf[5] == 't'
					&& cbuf[6] == 'e'
					&& cbuf[7] == 'r'
					&& cbuf[8] == 0) {

					return Token(K_REGISTER);
				}
				else if (cbuf[1] == 'e'
					&& cbuf[2] == 't'
					&& cbuf[3] == 'u'
					&& cbuf[4] == 'r'
					&& cbuf[5] == 'n'
					&& cbuf[6] == 0) {

					return Token(K_RETURN);
				}
			} else if (cbuf[0] == 's') {
				if (cbuf[1] == 'h'
					&& cbuf[2] == 'o'
					&& cbuf[3] == 'r'
					&& cbuf[4] == 't'
					&& cbuf[5] == 0) {

					return TokenAttr(K_SHORT, ATTR_TYPENAME);
				}
				else if (cbuf[1] == 'i'
					&& cbuf[2] == 'z'
					&& cbuf[3] == 'e'
					&& cbuf[4] == 'o'
					&& cbuf[5] == 'f'
					&& cbuf[6] == 0) {

					return Token(K_SIZEOF);
				}
				else if (cbuf[1] == 't'
					&& cbuf[2] == 'a'
					&& cbuf[3] == 't'
					&& cbuf[4] == 'i'
					&& cbuf[5] == 'c'
					&& cbuf[6] == 0) {

					return Token(K_STATIC);
				}
				else if (cbuf[1] == 't'
					&& cbuf[2] == 'r'
					&& cbuf[3] == 'u'
					&& cbuf[4] == 'c'
					&& cbuf[5] == 't'
					&& cbuf[6] == 0) {

					return TokenAttr(K_STRUCT, ATTR_TYPENAME);
				}
				else if (cbuf[1] == 'w'
					&& cbuf[2] == 'i'
					&& cbuf[3] == 't'
					&& cbuf[4] == 'c'
					&& cbuf[5] == 'h'
					&& cbuf[6] == 0) {

					return Token(K_SWITCH);
				}
				else if (cbuf[1] == 'i'
					&& cbuf[2] == 'g'
					&& cbuf[3] == 'n'
					&& cbuf[4] == 'e'
					&& cbuf[5] == 'd'
					&& cbuf[6] == 0) {

					return TokenAttr(K_SIGNED, ATTR_TYPENAME);
				}
			}else if (cbuf[0] == 't') {
				if (cbuf[1] == 'y'
					&& cbuf[2] == 'p'
					&& cbuf[3] == 'e'
					&& cbuf[4] == 'd'
					&& cbuf[5] == 'e'
					&& cbuf[6] == 'f'
					&& cbuf[7] == 0) {

					return Token(K_TYPEDEF);
				}
			}else if (cbuf[0] == 'u') {
				if (cbuf[1] == 'n'
					&& cbuf[2] == 'i'
					&& cbuf[3] == 'o'
					&& cbuf[4] == 'n'
					&& cbuf[5] == 0) {

					return TokenAttr(K_UNION, ATTR_TYPENAME);
				}
				else if (cbuf[1] == 'n'
					&& cbuf[2] == 's'
					&& cbuf[3] == 'i'
					&& cbuf[4] == 'g'
					&& cbuf[5] == 'n'
					&& cbuf[6] == 'e'
					&& cbuf[7] == 'd'
					&& cbuf[8] == 0) {

					return TokenAttr(K_UNSIGNED, ATTR_TYPENAME);
				}
			}else if (cbuf[0] == 'v') {
				if (cbuf[1] == 'o'
					&& cbuf[2] == 'i'
					&& cbuf[3] == 'd'
					&& cbuf[4] == 0) {

					return TokenAttr(K_VOID, ATTR_TYPENAME);
				}
				else if (cbuf[1] == 'o'
					&& cbuf[2] == 'l'
					&& cbuf[3] == 'a'
					&& cbuf[4] == 't'
					&& cbuf[5] == 'i'
					&& cbuf[6] == 'l'
					&& cbuf[7] == 'e'
					&& cbuf[8] == 0) {

					return TokenAttr(K_VOLATILE, ATTR_TYPENAME);
				}
			}else if (cbuf[0] == 'w') {
				if (cbuf[1] == 'h'
					&& cbuf[2] == 'i'
					&& cbuf[3] == 'l'
					&& cbuf[4] == 'e'
					&& cbuf[5] == 0) {

					return Token(K_WHILE);
				}
			}

			sym = LexIsTypeName(cbuf);
			if (sym)
				return TokenTypeName(sym);

			return TokenIdent(cbuf);
		}
	}

	return tok;
}

PUBLIC TOKEN Lex(void) {

	TOKEN tok = _Lex();
	lasttok = tok;
	return tok;
}

PUBLIC void Unlex(IN TOKEN tok) {

	if (tokstream.nalloc == 0)
		VectorInit(&tokstream, TOKEN);
	VectorPush(&tokstream, &tok);
}

PUBLIC TOKEN Look(IN index_t idx) {

	TOKEN tok[32];
	TOKEN ret;
	index_t c;

	if (idx == 0) return lasttok;
	if (idx > 31) Fatal("Scanner: lookahead exceeds limit");
	for (c = 0; c < idx; c++)
		tok[c] = Lex();
	ret = tok[c-1];
	for (c--; c>0; c--)
		Unlex(tok[c]);
	Unlex(tok[c]);
	return ret;
}

PUBLIC PTOKEN DupToken(IN TOKEN tok) {

	PTOKEN ptok = Alloc(sizeof(TOKEN));
	memcpy(ptok, &tok, sizeof(TOKEN));
	InitListHead(&ptok->e);
	return ptok;
}

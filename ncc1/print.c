/************************************************************************
*
*	print.c - Print formatted output
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "ncc.h"
#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

PUBLIC void VFNString(IN ADDCHAR addchar, IN char* s, IN void* context) {

	for (; *s; s++)
		addchar(context, *s);
}

PRIVATE void VFNPrintType(IN ADDCHAR addchar, IN PTYPE type, IN void* context)  {

}

PRIVATE void VFNPrintNode(IN ADDCHAR addchar, IN PNODE node, IN void* context) {

}

PRIVATE void VFNPrintSymbol(IN ADDCHAR addchar, IN PSYMBOL sym, IN void* context) {

}

PRIVATE void VFNPrints(IN ADDCHAR addchar, IN char* s, IN void* context) {

	for (; *s; s++)
		addchar(context, *s);
}

PRIVATE void VFNPrintString(IN ADDCHAR addchar, IN PSTRING str, IN void* context) {

	char* s = str->val;
	for (; *s; s++)
		addchar(context, *s);
}

PRIVATE void VFNPrintCoord(IN ADDCHAR addchar, IN COORD coord, IN void* context) {

	char  val[32];
	char* s;
	char* v;

	s = coord.fname->val;
	v = val;

	if (s) {
		for (; *s; s++)
			addchar(context, *s);
		addchar(context, '(');
	}

	sprintf(val, "%u", coord.y);
	for (; *v; v++)
		addchar(context, *v);
	if (s)
		addchar(context, ')');
}

PRIVATE void VFNPrintLineFromFile(IN ADDCHAR addchar, IN FILE* str, IN void* context) {

	int pos;
	int c;

	c = 0;
	pos = ftell(str);

	while (c != '\n' && c != '\r' && c != EOF) {
		c = fgetc(str);
		addchar(context, c);
	}
	fsetpos(str, pos);
}

PRIVATE void VFNPrintToken(IN ADDCHAR addchar, IN TOKEN tok, IN void* context) {

	char* s;
	char  val[32];
	addchar(context, '\'');
	switch (tok.op) {
	case K_AUTO: VFNPrints(addchar, "auto",context); break;
	case K_BREAK: VFNPrints(addchar, "break",context); break;
	case K_CASE: VFNPrints(addchar, "case",context); break;
	case K_CHAR: VFNPrints(addchar, "char",context); break;
	case K_CONST: VFNPrints(addchar, "const",context); break;
	case K_CONTINUE: VFNPrints(addchar, "continue",context); break;
	case K_DEFAULT: VFNPrints(addchar, "default",context); break;
	case K_DOUBLE: VFNPrints(addchar, "double",context); break;
	case K_DO: VFNPrints(addchar, "do",context); break;
	case K_ELSE: VFNPrints(addchar, "else",context); break;
	case K_ENUM: VFNPrints(addchar, "enum",context); break;
	case K_EXTERN: VFNPrints(addchar, "extern",context); break;
	case K_FLOAT: VFNPrints(addchar, "float",context); break;
	case K_FOR: VFNPrints(addchar, "for",context); break;
	case K_GOTO: VFNPrints(addchar, "goto",context); break;
	case K_LONG: VFNPrints(addchar, "long",context); break;
	case K_REGISTER: VFNPrints(addchar, "register",context); break;
	case K_RETURN: VFNPrints(addchar, "return",context); break;
	case K_SHORT: VFNPrints(addchar, "short",context); break;
	case K_SIZEOF: VFNPrints(addchar, "sizeof",context); break;
	case K_STATIC: VFNPrints(addchar, "static",context); break;
	case K_STRUCT: VFNPrints(addchar, "struct",context); break;
	case K_SWITCH: VFNPrints(addchar, "switch",context); break;
	case K_TYPEDEF: VFNPrints(addchar, "typedef",context); break;
	case K_UNION: VFNPrints(addchar, "union",context); break;
	case K_UNSIGNED: VFNPrints(addchar, "unsigned",context); break;
	case K_VOID: VFNPrints(addchar, "void",context); break;
	case K_VOLATILE: VFNPrints(addchar, "volatile",context); break;
	case K_WHILE: VFNPrints(addchar, "while",context); break;
	case K_IF: VFNPrints(addchar, "if",context); break;
	case K_INLINE: VFNPrints(addchar, "inline",context); break;
	case K_INT: VFNPrints(addchar, "int",context); break;
	case K_SIGNED: VFNPrints(addchar, "signed",context); break;
	case TK_IDENT: {
		s = StrToChar(tok.val.s);
		VFNPrints(addchar,s,context);
		Free(s); break;
	}
	case TK_STRING: {
		s = StrToChar(tok.val.s);
		VFNPrints(addchar,s,context);
		Free(s); break;
	}
	case TK_NUMBER: {
		if (tok.type->kind == TYPE_INT)
			sprintf(val, "%i", tok.val.i);
		else if (tok.type->kind == TYPE_UNSIGNED)
			sprintf(val, "%u", tok.val.ui);
		else if (tok.type->kind == TYPE_FLOAT) {
			if (tok.type == LongDoubleType)
				sprintf(val, "%ld", tok.val.lf);
			else
				sprintf(val, "%d", tok.val.f);
		}
		else sprintf(val, "<number>");
		VFNPrints(addchar, val, context);
		break;
	}
	case TK_EOF: VFNPrints(addchar, "<EOF>",context); break;
	case TK_ERROR: VFNPrints(addchar, "<error>",context); break;
	case TK_LEQ: VFNPrints(addchar, "<=",context); break;
	case TK_LSHIFT: VFNPrints(addchar, "<<",context); break;
	case TK_GEQ: VFNPrints(addchar, ">=",context); break;
	case TK_RSHIFT: VFNPrints(addchar, ">>",context); break;
	case TK_DEREF: VFNPrints(addchar, "->",context); break;
	case TK_DECR: VFNPrints(addchar, "--",context); break;
	case TK_EQ: VFNPrints(addchar, "==",context); break;
	case TK_NEQ: VFNPrints(addchar, "!=",context); break;
	case TK_LOGOR: VFNPrints(addchar, "||",context); break;
	case TK_LOGAND: VFNPrints(addchar, "&&",context); break;
	case TK_INCR: VFNPrints(addchar, "++",context); break;
	case TK_ELLIPSIS: VFNPrints(addchar, "...",context); break;
	case TK_2HASH: VFNPrints(addchar, "##",context); break;
	default:
		val[0] = tok.op;
		val[1] = 0;
		VFNPrints(addchar, val, context);
	}
	addchar(context, '\'');
}

PRIVATE void Reverse(char s[]) {
	int i, j;
	char c;

	for (i = 0, j = strlen(s) - 1; i<j; i++, j--) {
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
}

PRIVATE void Int2Str(int n, char s[]) {
	int i, sign;

	if ((sign = n) < 0)
		n = -n;
	i = 0;
	do {
		s[i++] = n % 10 + '0';
	} while ((n /= 10) > 0);
	if (sign < 0)
		s[i++] = '-';
	s[i] = '\0';
	Reverse(s);
}

PRIVATE void UInt2Str(unsigned int n, char s[]) {
	int i;

	i = 0;
	do {
		s[i++] = n % 10 + '0';
	} while ((n /= 10) > 0);
	s[i] = '\0';
	Reverse(s);
}

// octal
PRIVATE void VFNPrinto(IN ADDCHAR addchar,
	IN unsigned int val, IN void* context) {

}

PRIVATE int NumHexDigits(IN unsigned n) {

	if (!n) return 1;
	int ret = 0;
	for (; n; n >>= 4)
		++ret;
	return ret;
}

// hex
PRIVATE void VFNPrintx(IN ADDCHAR addchar,
	IN char op, IN unsigned int val, IN void* context) {

	char* hex_lookup;

	if (val == 0) {
		addchar(context, '0');
		return;
	}

	if (op == 'X') hex_lookup = "0123456789ABCDEF";
	else hex_lookup = "0123456789abcdef";

	int len = NumHexDigits(val);
	char* buf = malloc(len + 1), *s = buf;

	if (len & 1)
		*s++ = '0';
	s[len] = '\0';

	for (--len; len >= 0; val >>= 4, --len)
		s[len] = hex_lookup[val & 0xf];
	for (s = buf; *s; s++) addchar(context, *s);
	free(buf);
}

PRIVATE void VFNPrintu(IN ADDCHAR addchar, IN int base,
	IN unsigned int val, IN void* context) {

	char buf[32], *p = buf;
	UInt2Str(val, buf);
	for (; *p; p++) addchar(context, *p);
}

PRIVATE void VFNPrinti(IN ADDCHAR addchar, IN int val, IN void* context) {

	char buf[32], *p = buf;
	Int2Str(val, buf);
	for (; *p; p++) addchar(context, *p);
}

PRIVATE void VFNPrintp(IN ADDCHAR addchar, IN int base,
	IN void* val, IN void* context) {

	VFNPrintu(addchar, base, (intptr_t)val, context);
}

// FIXME: not a good solution:
PRIVATE void VFNPrintd(IN char op, IN ADDCHAR addchar,
	IN double val, IN void* context) {

	char c[_CVTBUFSIZE];
	char* p = c;
	char fmt[3] = { '%', op, 0 };
	_snprintf(c, _CVTBUFSIZE, fmt, val);
	for (; *p; p++) addchar(context, *p);
}

// %[flags][width][.precision][length]type
PRIVATE int VFNPrintf(IN ADDCHAR addchar, IN void *context,
	IN const char *format, IN va_list args) {

	char buf[32], *p = buf;
	unsigned count = 0;
	int flags = 0;
	unsigned int width = 0;
	unsigned int precision = 0;

	for (; *format; format++) {
		if (*format == '%' && format++) {
			p = buf;
			width = flags = precision = 0;

			// flags:
			for (; *format; format++) {
				if (*format == '-');
				else if (*format == '+');
				else if (*format == ' ');
				else if (*format == '0');
				else if (*format == '#');
				else break;
			}

			// width:
			if (*format == '*')
				width = va_arg(args, unsigned int);
			else if (isdigit(*format)) {
				for (; *format && isdigit(*format); format++)
					*p++ = *format;
				*p = '\0';
				width = strtol(buf, NULL, 10);
			}

			// precision:
			if (*format == '.' && format++) {
				if (*format == '*')
					precision = va_arg(args, unsigned int);
				else if (isdigit(*format)) {
					for (; *format && isdigit(*format); format++)
						*p++ = *format;
					*p = '\0';
					precision = strtol(buf, NULL, 10);
				}
			}

			// length:
			for (; *format; format++) {
				if (*format == '-');
				else if (*format == 'h');
				else if (*format == 'l');
				else if (*format == 'L');
				else if (*format == 'z');
				else if (*format == 'j');
				else if (*format == 't');
				else break;
			}

			// type:
			switch (*format) {
			case '%':
				addchar(context, '%');
				break;
			case 'd': case 'i':
				VFNPrinti(addchar, va_arg(args, int), context);
				break;
			case 'u':
				VFNPrintu(addchar, 10, va_arg(args, unsigned int), context);
				break;
			case 'f': case 'F':
			case 'g': case 'G':
			case 'e': case 'E':
				VFNPrintd(*format, addchar, va_arg(args, double), context);
				break;
			case 'x': case 'X':
				VFNPrintx(addchar, *format, va_arg(args, unsigned int), context);
				break;
			case 'o':
				VFNPrinto(addchar, va_arg(args, unsigned int), context);
				break;
			case 'c':
				addchar(context, va_arg(args, char));
				break;
			case 's':
				VFNPrints(addchar, va_arg(args, char*), context);
				break;
			case 'p':
				VFNPrintp(addchar, 16, va_arg(args, void*), context);
				break;
			case 'T':
				VFNPrintType(addchar, va_arg(args, PTYPE), context);
				break;
			case 'K':
				VFNPrintToken(addchar, va_arg(args, TOKEN), context);
				break;
			case 'N':
				VFNPrintNode(addchar, va_arg(args, PNODE), context);
				break;
			case 'S':
				VFNPrintSymbol(addchar, va_arg(args, PSYMBOL), context);
				break;
			case 'A':
				VFNPrintString(addchar, va_arg(args, PSTRING), context);
				break;
			case 'C':
				VFNPrintCoord(addchar, va_arg(args, COORD), context);
				break;
			case 'Z':
				VFNPrintLineFromFile(addchar, va_arg(args, FILE*), context);
				break;
			case 'I':
				XVFNPrintIns(addchar, va_arg(args, PINSTR), context);
				break;
			}
		}
		else addchar(context, *format);
	}
	return count;
}

PRIVATE int VPrintfAddChar(IN FILE* file, IN int c) {

	fputc(c, file);
	return 1; // # of chars written
}

PUBLIC void VPrintf(IN FILE* file, IN char* s, IN va_list args) {

	VFNPrintf(VPrintfAddChar, file, s, args);
}

PUBLIC void FPrintf(IN FILE* file, IN char* fmt, ...) {

	va_list ap;
	va_start(ap, fmt);
	VPrintf(file, fmt, ap);
	va_end(ap);
}

PUBLIC void Printf(IN char* fmt, ...) {

	va_list ap;
	va_start(ap, fmt);
	VPrintf(stdout, fmt, ap);
	va_end(ap);
}

PUBLIC void Puts(IN char* fmt, ...) {

	va_list ap;
	va_start(ap, fmt);
	VPrintf(stdout, fmt, ap);
	fprintf(stdout, "\n");
	va_end(ap);
}

typedef struct SPRINTFCTX {
	char* buf;
	size_t max;
	size_t curIdx;
}SPRINTFCTX;

PRIVATE int SprintfAddChar(IN SPRINTFCTX* ctx, IN int c) {

	if (ctx->max > 0 && ctx->curIdx >= ctx->max)
		return 0;
	ctx->buf[ctx->curIdx++] = c;
	return 1;
}

PRIVATE void Sprintfx(OUT char* buf, IN int len, IN char* fmt, IN va_list ap) {

	SPRINTFCTX ctx;

	ctx.buf = buf;
	ctx.max = len;
	ctx.curIdx = 0;
	VFNPrintf(SprintfAddChar, &ctx, fmt, ap);
	ctx.buf[ctx.curIdx] = 0;
}

PUBLIC void Sprintf(OUT char* buf, IN char* fmt, ...) {

	va_list ap;
	va_start(ap, fmt);
	Sprintfx(buf, 0, fmt, ap);
	va_end(ap);
}

PUBLIC void Sprintfn(OUT char* buf, IN size_t len, IN char* fmt, ...) {

	va_list ap;
	va_start(ap, fmt);
	Sprintfx(buf, len, fmt, ap);
	va_end(ap);
}

PUBLIC int _errorCount;
PUBLIC int _warnCount;

PUBLIC void Error(IN char* s, ...) {

	va_list ap;
	va_start(ap, s);
	VPrintf(stderr, "Error: ", ap);
	VPrintf(stderr, s, ap);
	VPrintf(stderr, "\n", ap);
	va_end(ap);
	_errorCount++;
}

PUBLIC void ParseError(IN char* s, ...) {

	va_list ap;
	va_start(ap, s);
	VPrintf(stderr, "Error: ", ap);
	VPrintf(stderr, s, ap);
	VPrintf(stderr, "\n", ap);
	va_end(ap);
	_errorCount++;
	UngetToken(ErrorToken());
}

PUBLIC void Warn(IN char* s, ...) {

	va_list ap;
	va_start(ap, s);
	VPrintf(stderr, "Warning: ", ap);
	VPrintf(stderr, s, ap);
	VPrintf(stderr, "\n", ap);
	va_end(ap);
	_warnCount++;
}

PUBLIC void Info(IN char* s, ...) {

	va_list ap;
	va_start(ap, s);
	VPrintf(stderr, s, ap);
	VPrintf(stderr, "\n", ap);
	va_end(ap);
}

PUBLIC void Fatal(IN char* s, ...) {

	va_list ap;
	va_start(ap, s);
	VPrintf(stderr, "Fatal: ", ap);
	VPrintf(stderr, s, ap);
	VPrintf(stderr, "\n", ap);
	__debugbreak();
}


/************************************************************************
*
*	quad.c
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This is a work in progress --------------- */

#include <malloc.h>
#include "ncc.h"

PRIVATE unsigned _quadcount;

PUBLIC PSYMBOL_LIST SymbolListFind(IN PSYMBOL_LIST list, PSYMBOL sym) {

	PSYMBOL_LIST item;

	for (item = list; item; item = item->next)
		if (item->me == sym) return item;
	return NULL;
}

PUBLIC PSYMBOL_LIST SymbolListAdd(IN PSYMBOL_LIST* list, PSYMBOL sym) {

	PSYMBOL_LIST item;

	item = SymbolListFind(*list, sym);
	if (item)
		return item;

	if (!*list) {
		*list = calloc(1, sizeof(SYMBOL_LIST));
		(*list)->me = sym;
		return *list;
	}
	for (item = *list; item->next; item = item->next)
		;
	item->next = calloc(1, sizeof(SYMBOL_LIST));
	item->next->prev = item;
	item->next->me = sym;
	return item->next;
}

PUBLIC PBLOCK_LIST BlockListFind(IN PBLOCK_LIST list, IN PBLOCK block) {

	PBLOCK_LIST item;

	for (item = list; item; item = item->next)
		if (item->me == block) return item;
	return NULL;
}

PUBLIC PBLOCK_LIST BlockListAdd(IN PBLOCK_LIST* list, PBLOCK block) {

	PBLOCK_LIST item;

	if (!*list) {
		*list = calloc(1, sizeof(BLOCK_LIST));
		(*list)->me = block;
		return *list;
	}
	for (item = *list; item->next; item = item->next)
		;
	item->next = calloc(1, sizeof(BLOCK_LIST));
	item->next->prev = item;
	item->next->me = block;
	return item->next;
}

PUBLIC PQUAD_LIST QuadListGetLast(IN PQUAD_LIST list) {

	PQUAD_LIST item;

	if (!list) return NULL;

	for (item = list; item->next; item = item->next)
		;
	return item;
}

PUBLIC PQUAD_LIST QuadListFind(IN PQUAD_LIST list, IN PQUAD quad) {

	PQUAD_LIST item;

	for (item = list; item; item = item->next)
		if (item->me == quad) return item;
	return NULL;
}

PUBLIC PQUAD_LIST QuadListAdd(IN PQUAD_LIST* list, PQUAD quad) {

	PQUAD_LIST item;

	if (!*list) {
		*list = calloc(1, sizeof(QUAD_LIST));
		(*list)->me = quad;
		return *list;
	}
	for (item = *list; item->next; item = item->next)
		;
	item->next = calloc(1, sizeof(QUAD_LIST));
	item->next->prev = item;
	item->next->me = quad;
	return item->next;
}

PRIVATE unsigned NextQuad() {

	return ++_quadcount;
}

PUBLIC PQUAD Quad(IN int op, IN PSYMBOL target, IN PRVAL src1, IN PRVAL src2) {

	PQUAD quad;

	quad = calloc(1, sizeof(QUAD));
	quad->name = NextQuad();
	quad->op = op;
	quad->target = target;
	quad->src1 = src1;
	quad->src2 = src2;
	return quad;
}

PUBLIC PQUAD QLabel(IN PSTRING label) {

	return Quad(QLABEL, NULL, RValStr(label), NULL);
}

PUBLIC PQUAD QJmp(IN PQUAD label) {

	PQUAD q;

	q = Quad(QJMP, NULL, NULL, NULL);
	q->label = label;
	return q;
}

PUBLIC PQUAD QJmpc(IN int condCode, PRVAL a, IN PRVAL b, PQUAD truLabel) {

	PQUAD q;

	q = Quad(QJMP, NULL, a, b);
	q->condCode = condCode; // <, >, <= and others.
	q->label = truLabel;
	return q;
}

PUBLIC PQUAD QMov(IN PSYMBOL target, IN PRVAL src1) {

	return Quad(QMOV, target, src1, NULL);
}

PUBLIC PQUAD QStore(IN PSYMBOL target, IN PRVAL src1, IN PRVAL index) {

	PQUAD q;

	q = Quad(QASGN, target, src1, NULL);
	q->index = index;
	return q;
}

PUBLIC PQUAD QLoad(IN PSYMBOL target, IN PSYMBOL src1, IN PRVAL index) {

	PQUAD q;

	q = Quad(QINDIR, target, RVar(src1), NULL);
	q->index = index;
	return q;
}

PUBLIC PQUAD QLBrace(void) {

	return Quad(QLBRACE, NULL, NULL, NULL);
}

PUBLIC PQUAD QRBrace(void) {

	return Quad(QRBRACE, NULL, NULL, NULL);
}

PUBLIC PQUAD QArg(IN PSYMBOL target) {

	return Quad(QARG, target, NULL, NULL);
}

PUBLIC PQUAD QCall(IN PSYMBOL target) {

	return Quad(QCALL, target, NULL, NULL);
}

PUBLIC PQUAD QRet(IN OPTIONAL PRVAL src1) {

	return Quad(QRET, NULL, src1, NULL);
}

PUBLIC PQUAD QAddrg(IN PSYMBOL target, IN PSYMBOL src1) {

	return Quad(QADDRG, target, RVar(src1), NULL);
}

PUBLIC PQUAD QAddrl(IN PSYMBOL target, IN PSYMBOL src1) {

	return Quad(QADDRL, target, RVar(src1), NULL);
}

PUBLIC PQUAD QAddrf(IN PSYMBOL target, IN PSYMBOL src1) {

	return Quad(QADDRF, target, RVar(src1), NULL);
}

PUBLIC PQUAD QUnaryOp(IN int op, IN PSYMBOL target, IN PRVAL src1) {

	return Quad(op, target, src1, NULL);
}

PUBLIC PQUAD QBinOp(IN int op, IN PSYMBOL target, IN PRVAL src1, IN PRVAL src2) {

	return Quad(op, target, src1, src2);
}

PUBLIC PQUAD QCoord(IN COORD coord) {

	PQUAD quad;

	quad = Quad(QCOORD, NULL, NULL, NULL);
	quad->coord = coord;
	return quad;
}

PUBLIC PRVAL RVar(IN PSYMBOL var) {

	PRVAL val;

	val = calloc(1, sizeof(RVAL));
	val->var = var;
	val->type = var->type;
	return val;
}

PUBLIC PRVAL RValInt(IN int i) {

	PRVAL val;

	val = calloc(1, sizeof(RVAL));
	val->type = IntType;
	val->intLit = i;
	return val;
}

PUBLIC PRVAL RValStr(IN PSTRING str) {

	PRVAL val;

	val = calloc(1, sizeof(RVAL));
	val->type = ArrayOfCharType;
	val->stringlit = str;
	return val;
}

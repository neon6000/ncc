/************************************************************************
*
*	tree.c - TREE IR
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <assert.h>
#include <stdlib.h>
#include "ncc.h"

PUBLIC PNODE Tree(IN unsigned op, IN PTYPE ty, IN PNODE l, IN PNODE r) {

	static unsigned ID = 0;
	PNODE t;

	t = Allocz(sizeof(NODE));
	t->op = op;
	t->type = ty;
	t->kids[0] = l;
	t->kids[1] = r;
	t->name = ++ID;
	return t;
}

PUBLIC PNODE Leaf(IN unsigned op, IN PTYPE ty, IN VALUE v) {

	PNODE t = Tree(MKOP(op, ty), ty, NULL, NULL);
	t->val = v;
	return t;
}

PUBLIC PNODE NodePreDec(IN PNODE lvalue) {

	return RValue(Tree(QPREDEC, lvalue->type, lvalue, NULL));
}

PUBLIC PNODE NodePostDec(IN PNODE lvalue) {

	return RValue(Tree(QPOSTDEC, lvalue->type, lvalue, NULL));
}

PUBLIC PNODE NodePreInc(IN PNODE lvalue) {

	return RValue(Tree(QPREINC, lvalue->type, lvalue, NULL));
}

PUBLIC PNODE NodePostInc(IN PNODE lvalue) {

	return RValue(Tree(QPOSTINC, lvalue->type, lvalue, NULL));
}

PUBLIC PNODE NodeBrace(void) {

	return Tree(QLBRACE, NULL, NULL, NULL);
}

PUBLIC PNODE NodeSubscript(IN PNODE lval, IN PNODE subscriptExp) {

	return Tree(QSUBSCRIPT, lval->type, lval, subscriptExp);
}

PUBLIC PNODE Value(IN PNODE in) {

	return in;
}

PUBLIC PNODE RValue(IN PNODE in) {

	PTYPE to;
	PNODE tree;

	to = Deref(in->type);
	tree = Tree(MKOP(QINDIR, to), to, in, NULL);
	tree->coord = in->coord;
	return tree;
}

PUBLIC PNODE LValue(IN PNODE in) {

	if (OPCODE(in->op) != QINDIR) {
		Error("lvalue required");
		return Value(in);
	}
	return in->kids[LEFT];
}

PUBLIC PNODE TreeAddr(IN PSYMBOL s) {

	PNODE    t;
	PTYPE    ty;
	unsigned op;

	t = NULL;
	ty = s->type ? s->type : VoidType;

	s->memory = TRUE;

	if (s->level == GLOBALS || s->sclass == SC_STATIC)
		op = QADDRG;
	else if (s->sclass == SC_EXTERN) {
		s = s->alias;
		op = QADDRG;
	}
	else if (s->level == PARMS)
		op = QADDRF;
	else
		op = QADDRL;

	if (IsArray(ty))
		t = Tree(MKOP(op,ty), ty, NULL, NULL);
	else if (IsFunction(ty))
		t = Tree(MKOP(op,ty), ty, NULL, NULL);
	else {
		ty = Pointer(ty);
		t = Tree(MKOP(op,ty), ty, NULL, NULL);
	}

	t->sym = s;
	t->val.x = s;

	if (IsPointer(t->type))
		t = RValue(t);

	return t;
}

PUBLIC PNODE Cast(IN PNODE t, IN PTYPE to) {

	unsigned op;

	if (t->type->kind == to->kind && t->type->size == to->size)
		return t;


	return t;

//	op = QCVTI;
//	if (IsUnsigned(to)) op = QCVTU;
//	else if (IsInt(to)) op = QCVTI;
//	else if (IsFloat(to)) op = QCVTF;
//	else if (IsPointer(to)) op = QCVTP;
//	else
//		Fatal("Cast: Invalid to type");

//	return Tree(op, to, t, NULL);
}

PUBLIC PNODE TreeConst(IN PTYPE ty, unsigned i) {

	PNODE t = Tree(MKOP(QCNST, ty), ty, NULL, NULL);
	t->val.i = i;
	return t;
}

PUBLIC PNODE Retype(IN PNODE in, IN PTYPE ty) {

	PNODE node;
	
	node = Tree(MKOP(in->op, ty), ty, in->kids[0], in->kids[1]);
	node->sym = in->sym;
	node->coord = in->coord;
	node->val = in->val;
	return node;
}

PUBLIC PNODE MabeyDecay(IN PNODE t) {

	if (IsArray(t->type))
		t = Retype (t, ArrayToPtr(t->type));
	else if (IsFunction(t->type))
		t = Retype (t, Pointer(t->type));
	return t;
}

PUBLIC bool_t Compatible(IN PTYPE lhs, IN PTYPE rhs) {

	return TRUE;
}

PUBLIC PNODE LogAndOr(IN int op, IN PNODE lhs, IN PNODE rhs) {

	if (IsScalar(lhs->type) && IsScalar(rhs->type)) {
		if (op == TK_LOGAND)
			return Tree(QAND, lhs->type, lhs, rhs);
		else if (op == TK_LOGOR)
			return Tree(QOR, lhs->type, lhs, rhs);
	}
	else
		Error("AND or OR type error");
}

//
// 6.3.1.8 ~ Usual Arithmetic conversions
//
PRIVATE PTYPE ArithConv(IN PTYPE lhs, IN PTYPE rhs) {

	assert(IsArith(lhs));
	assert(IsArith(rhs));
	if (lhs == rhs)
		return lhs;
	// the order is largest type to smallest:
	if (lhs == LongDoubleType || rhs == LongDoubleType)
		return LongDoubleType;
	if (lhs == DoubleType || rhs == DoubleType)
		return DoubleType;
	if (lhs == FloatType || rhs == FloatType)
		return FloatType;
	if (lhs == UnsignedLongLongType || rhs == UnsignedLongLongType)
		return UnsignedLongLongType;
	if (lhs == LongLongType || rhs == LongLongType)
		return LongLongType;
	if (lhs == UnsignedLongType || rhs == UnsignedLongType)
		return UnsignedLongType;
	if (lhs == LongType || rhs == LongType)
		return LongType;
	return IntType;
}

//
// 6.5.6 ~ Additive operators
//  x++ = ((x += 1) - 1)
//  ++x =  (x += 1)
//
PUBLIC PNODE TreeAdd(IN int op, IN PNODE lhs, IN PNODE rhs) {

	PTYPE ty;
	PNODE t;

	// LEX passes '+'
	op = QADD;

	ty = IntType;
	if (IsArith(lhs->type) && IsArith(rhs->type)) {
		ty = ArithConv(lhs->type, rhs->type);
		lhs = Cast(lhs, ty);
		rhs = Cast(rhs, ty);
	}
	else if (IsPointer(rhs->type) && IsInt(lhs->type)) {
		return TreeAdd(op, rhs, lhs);
	}
	else if (IsPointer(lhs->type) && !IsFunction(lhs->type->type) && IsInt(rhs->type)) {

		return Tree(MKOP(QADD, lhs->type),
			lhs->type, lhs,
			TreeMul('*',
				rhs,
				TreeConst(UnsignedIntType, lhs->type->type->size)));
	}
	else
		Error("+ Type error");

	t = Tree(MKOP(op, ty), ty, lhs, rhs);
	return t;
}

//
// 6.5.6 ~ Additive operators
//
PUBLIC PNODE TreeSub(IN int op, IN PNODE lhs, IN PNODE rhs) {

	PTYPE ty;
	PNODE t;

	// LEX passes '-'
	op = QSUB;
	if (IsArith(lhs->type) && IsArith(rhs->type)) {
		ty = ArithConv(lhs->type, rhs->type);
		lhs = Cast(lhs, ty);
		rhs = Cast(rhs, ty);
	}
	else if (IsPointer(lhs->type) && !IsFunction(lhs->type->type) && IsInt(rhs->type)) {

		ty = lhs->type;
	}
	else if (Compatible(lhs->type, rhs->type)) {

	}
	else
		Error("- Type error");

	ty = lhs->type;
	t = Tree(MKOP(op, ty), ty, lhs, rhs);
	return t;
}

PUBLIC PNODE TreeMul(IN int op, IN PNODE lhs, IN PNODE rhs) {

	PTYPE ty;
	PNODE t;

	// LEX passes '/' or '*'
	if (op == '/') op = QDIV;
	else if (op == '*') op = QMUL;
	else __debugbreak();

	if (IsArith(lhs->type) && IsArith(rhs->type)) {
		ty = ArithConv(lhs->type, rhs->type);
		lhs = Cast(lhs, ty);
		rhs = Cast(rhs, ty);
	}
	else
		Error("* Type error");

	ty = lhs->type;
	t = Tree(MKOP(op, ty), ty, lhs, rhs);
	return t;
}

PUBLIC PNODE TreeBit(IN int op, IN PNODE lhs, IN PNODE rhs) {

	PTYPE ty = lhs->type;
	PNODE t;

	if (IsArith(lhs->type) && IsArith(rhs->type)) {

	}
	else
		Error("Bit: Type error");

	ty = lhs->type;
	t = Tree(op, ty, lhs, rhs);
	return t;
}

PUBLIC PNODE TreeEq(IN int op, IN PNODE lhs, IN PNODE rhs) {

	PTYPE ty;
	PNODE t;

	t = TreeCmp(MKOP(op, lhs->type), lhs, rhs);
	return t;
}

PUBLIC PNODE Not(PNODE p) {

	return Tree(QNOT, p->type, p, NULL);
}

PUBLIC PNODE TreeCmp(IN int op, IN PNODE lhs, IN PNODE rhs) {

	PTYPE ty;
	PNODE t;
	int   ins;

	switch (op) {
	case TK_LEQ: ins = QLE; break;
	case TK_GEQ: ins = QGE; break;
	case TK_EQ: ins = QEQ; break;
	case TK_NEQ: ins = QNE; break;
	case '<': ins = QLT; break;
	case '>': ins = QGT; break;
	default: __debugbreak();
	};

	if (IsArith(lhs->type) && IsArith(rhs->type)) {

	}
	else if (Compatible(lhs->type, rhs->type)) {

	}
	else
		Error("CMP type error");

	ty = lhs->type;
	t = Tree(MKOP(ins, ty), ty, lhs, rhs);
	return t;
}

PUBLIC PNODE TreeShift(IN int op, IN PNODE lhs, IN PNODE rhs) {

	PTYPE ty;
	PNODE t;

	if (IsInt(lhs->type) && IsInt(rhs->type)) {

	}
	else
		Error("EQ type error");

	ty = lhs->type;
	t = Tree(MKOP(op,ty), ty, lhs, rhs);
	return t;
}

PUBLIC PNODE Assgn(IN int op, IN PNODE lvalue, IN PNODE rhs) {

	return Tree(MKOP(op, lvalue->type), lvalue->type, lvalue, rhs);
}

PUBLIC PNODE Addrof(IN PNODE p) {

	__debugbreak();
	return p;
}

PUBLIC PNODE Branch(IN PNODE left, IN PBLOCK truey, IN PBLOCK falsy) {

	PNODE t;

	t = Tree(QJMP, NULL, NULL, NULL);
	t->u.branch.cond = left;
	t->u.branch.target[LEFT] = truey;
	t->u.branch.target[RIGHT] = falsy;
	return t;
}

PUBLIC PNODE Return(IN PNODE expr) {

	if (!expr)
		return Tree(MKOP(QRET, IntType), IntType, NULL, NULL);
	else
		return Tree(MKOP(QRET, expr->type), expr->type, expr, NULL);
}

PUBLIC PNODE NodeSwitch(IN PNODE expr, IN PNODE body) {

	PNODE node;

	node = Tree(SWTCH, NULL, NULL, NULL);
	node->u.swtch.cond = expr;
	node->u.swtch.body = body;
	return node;
}

PUBLIC PNODE NodeCase(IN int cond, IN PNODE st) {

	PNODE node;

	node = Tree(CASE, NULL, NULL, NULL);
	node->u.caseStmt.cond = cond;
	node->u.caseStmt.stmt = st;
	return node;
}

PUBLIC PNODE NodeDefault(IN PNODE st) {

	PNODE node;

	node = Tree(DEFAULT, NULL, NULL, NULL);
	node->u.caseStmt.cond = NULL;
	node->u.caseStmt.stmt = st;
	return node;
}

PNODE NodeIf(IN PNODE cond, IN PNODE then, IN PNODE els) {

	PNODE node;

	node = Tree(IF, NULL, NULL, NULL);
	node->u.ifStmt.cond = cond;
	node->u.ifStmt.els = els;
	node->u.ifStmt.then = then;
	return node;
}

PNODE NodeWhile(IN PNODE cond, IN PNODE body) {

	PNODE node;

	node = Tree(WHILE, NULL, NULL, NULL);
	node->u.loopStmt.cond = cond;
	node->u.loopStmt.body = body;
	return node;
}

PNODE NodeDoWhile(IN PNODE cond, IN PNODE body) {

	PNODE node;

	node = Tree(DOWHILE, NULL, NULL, NULL);
	node->u.loopStmt.cond = cond;
	node->u.loopStmt.body = body;
	return node;
}

PNODE NodeFor(IN PNODE init, IN PNODE cond, IN PNODE next, IN PNODE body) {

	PNODE node;

	node = Tree(FOR, NULL, NULL, NULL);
	node->u.loopStmt.init = init;
	node->u.loopStmt.cond = cond;
	node->u.loopStmt.next = next;
	node->u.loopStmt.body = body;
	return node;
}

PUBLIC PNODE NodeGoto(IN PSTRING ident) {

	PNODE node;

	node = Tree(GOTO, NULL, NULL, NULL);
	node->ident = ident;
	return node;
}

PUBLIC PNODE NodeLabel(IN PSTRING ident) {

	PNODE node;

	node = Tree(LABEL, NULL, NULL, NULL);
	node->ident = ident;
	return node;
}

PUBLIC PNODE NodeMember(IN PNODE lhs, IN PFIELD field) {

	PNODE node;

	node = Tree(MEMBER, NULL, NULL, NULL);
	node->type = field->type;
	node->u.mem.lhs = lhs;
	node->u.mem.field = field;
	return node;
}

PUBLIC PNODE NodeListAlloc(IN PNODE me) {

	PNODE node;

	node = calloc(1, sizeof(NODE));
	node->op = NODELST;
	node->u.list.me = me;
	return node;
}

PUBLIC PNODE NodeListAppend(IN PNODE list, IN PNODE item) {

	PNODE newItem;

	newItem = NodeListAlloc(item);
	while (list->u.list.next)
		list = list->u.list.next;
	list->u.list.next = newItem;
	newItem->u.list.prev = list;
	return newItem;
}

/**
*	NodeListAppendUnique
*
*	Description : Appends item to list if it does not already exist.
*	If the list is NULL, this will allocate it.
*
*	Input : list - Pointer to PNODE list. If NULL, this will allocate it.
*	item - Item to add
*
*	Out : TRUE if item was added, FALSE if not
*/
PUBLIC bool_t NodeListAppendUnique(IN PNODE* list, IN PNODE item) {

	PNODE listItem;
	PNODE me;

	if (*list == NULL) {
		*list = NodeListAlloc(item);
		return TRUE;
	}

	for (listItem = *list; listItem; listItem = NodeListNext(listItem)) {

		me = listItem->u.list.me;
		if (me == item)
			return FALSE;
		if (me->sym && me->sym == item->sym)
			return FALSE;
		if (OPCODE(me->op) == QREGISTER &&
			OPCODE(item->op) == QREGISTER &&
			item->sym->tempname == me->sym->tempname)
			return FALSE;
	}
	NodeListAppend(*list, item);
	return TRUE;
}

PUBLIC PNODE NodeListDelink(IN PNODE listItem) {

	PNODE me;

	if (!listItem) return NULL;
	if (OPCODE(listItem->op) != NODELST)
		Fatal("tree.c: NodeListRemove: listItem is not NODELST");
	me = listItem->u.list.me;
	if (listItem->u.list.prev)
		listItem->u.list.prev->u.list.next = listItem->u.list.next;
	return me;
}

PUBLIC PNODE NodeListRemove(IN PNODE listItem) {

	PNODE me;

	me = NodeListDelink(listItem);
	free(listItem);
	return me;
}

PUBLIC PNODE NodeListNext(IN PNODE cur) {

	if (!cur) return NULL;
	return cur->u.list.next;
}

PUBLIC PNODE NodeListGet(IN PNODE listItem) {

	if (!listItem) return NULL;
	return listItem->u.list.me;
}

PUBLIC size_t NodeListSize(IN PNODE list) {

	size_t c;

	c = 0;
	while (list->u.list.next)
		c++;
	return c + 1;
}

/**
*	NodeBB
*
*	Description : Creates a BB node
*
*	Input : type - Type of temporary
*
*	Output : New node
*/
PNODE NodeBB(IN PBLOCK bb) {

	PNODE node;

	node = calloc(1, sizeof(NODE));
	node->op = QBLK;
	node->block = bb;
	return node;
}

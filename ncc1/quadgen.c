/************************************************************************
*
*	quadgen.c
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This is a work in progress --------------- */

#include <malloc.h>
#include "ncc.h"

typedef struct QCASE_LIST *PQCASE_LIST;
typedef struct QCASE_LIST {
	int          cond;
	PQUAD        label;
	bool_t       isDefault;
	PQCASE_LIST  next;
}QCASE_LIST, *PQCASE_LIST;

typedef struct CONTEXT {
	PNODE       swtch;
	PQUAD       brkLabel;
	PQUAD       contLabel;
	PQCASE_LIST swtchCase;
}CONTEXT, *PCONTEXT;

PRIVATE PSYMBOL      _function;
PRIVATE unsigned     _tempcount;
PRIVATE unsigned     _labcount;

PRIVATE void Stmt(IN PCONTEXT ctx, IN PNODE node);

PRIVATE PQCASE_LIST AddCase(IN PQCASE_LIST* list, IN PQUAD label, IN int cond, IN int isDefault) {

	PQCASE_LIST item;

	if (!*list) {
		*list = calloc(1, sizeof(QCASE_LIST));
		(*list)->label = label;
		(*list)->cond = cond;
		(*list)->isDefault = isDefault;
		return *list;
	}
	for (item = *list; item->next; item = item->next)
		;
	item->next = calloc(1, sizeof(QCASE_LIST));
	item->next->label = label;
	item->next->cond = cond;
	item->next->isDefault = isDefault;
	return item->next;
}

PRIVATE PSYMBOL Temp(IN PTYPE type) {

	PSYMBOL      sym;
	PSYMBOL_LIST item;

	sym = calloc(1, sizeof(SYMBOL));
	sym->tempname = ++_tempcount;
	sym->type = type;
	return sym;
}

PRIVATE unsigned NextLabel() {

	return ++_labcount;
}

PRIVATE PQUAD NewLabel() {

	char    buf[32];

	sprintf(buf, ".L%i", NextLabel());
	return QLabel(NewStringA(buf));
}

PRIVATE void Add(IN PQUAD quad) {

	QuadListAdd(&_function->quads, quad);
}

PRIVATE bool_t IsFallthrough(void) {

	PQUAD q;
	PQUAD_LIST item;

	item = QuadListGetLast(_function->quads);
	q = item->me;
	if (OPCODE(q->op) == QRET || OPCODE(q->op) == QJMP)
		return FALSE;
	return TRUE;
}

PRIVATE PSYMBOL Expr(IN PNODE node);

PRIVATE PSYMBOL Asgn(IN PNODE node) {

	PSYMBOL target;
	PRVAL   rhs;
	PRVAL   index;

	Add(QCoord(node->coord));
	rhs = RVar(Expr(node->kids[RIGHT]));
	index = NULL;

	target = Expr(node->kids[LEFT]);

	Add(QStore(target, rhs, index));
	return target;
}

//
// perform ++ or -- on "expr". op is QADD or QSUB
//
PRIVATE PSYMBOL IncOrDec(IN int op, IN PSYMBOL expr) {

	PSYMBOL target;
	PSYMBOL cons;

	// for now: XCODE doesn't support RValInt for XAdd.

	cons = Temp(IntType);
	Add(QMov(cons, RValInt(1)));

	//

	target = Temp(expr->type);
	Add(QLoad(target, expr, NULL));
	Add(QBinOp(op, target, RVar(target), RVar(cons) ));
	Add(QStore(expr, RVar(target), NULL));
}

PRIVATE PSYMBOL Expr(IN PNODE node) {

	PSYMBOL target;
	PSYMBOL lhs;
	PSYMBOL rhs;
	PQUAD   tru;
	PQUAD   fals;
	PQUAD   merge;
	PSYMBOL subidx;
	PSYMBOL val;

	if (!node)
		return NULL;

	switch (OPCODE(node->op)) {

	case QPREINC:

		lhs = Expr(node->kids[LEFT]);
		IncOrDec(QADD, lhs);
		target = Temp(lhs->type);
		Add(QLoad(target, lhs, NULL));
		return target;

	case QPOSTINC:

		lhs = Expr(node->kids[LEFT]);
		target = Temp(lhs->type);
		Add(QLoad(target, lhs, NULL));
		IncOrDec(QADD, lhs);	
		return target;

	case QPREDEC:

		lhs = Expr(node->kids[LEFT]);
		IncOrDec(QSUB, lhs);
		target = Temp(lhs->type);
		Add(QLoad(target, lhs, NULL));
		return target;

	case QPOSTDEC:

		lhs = Expr(node->kids[LEFT]);
		target = Temp(lhs->type);
		Add(QLoad(target, lhs, NULL));
		IncOrDec(QSUB, lhs);
		return target;

	case QCALL:

		// QCALL (arg, self)
		lhs = Expr(node->kids[LEFT]);


//		rhs = Expr(node->kids[RIGHT]);
//		Add(QCall(rhs));

		Add(QCall(node->kids[RIGHT]->sym));
	


		target = Temp(node->type);
		return target;

	case QARG:

		// QARG (nextarg, self)
		target = Expr(node->kids[RIGHT]);
		Add(QArg(target));
		Expr(node->kids[LEFT]);
		return target;

	case QADDRF:

		target = Temp(node->type);
		Add(QAddrf(target, node->sym));
		return target;

	case QADDRL:

//		target = Temp(node->type);
//		Add(QAddrl(target, node->sym));
//		return target;

		return node->sym;

	case QADDRG:

//		target = Temp(node->type);
//		Add(QAddrg(target, node->sym));
//		return target;

		return node->sym;

	case QCNST:

		target = Temp(node->type);
		Add(QMov(target, RValInt(node->val.i)));
		return target;

	case QADD:
	case QSUB:
	case QMUL:
	case QDIV:
	case QMOD:
	case QBSHL:
	case QBSHR:
	case QBAND:
	case QBCMPL:
	case QBOR:
	case QBXOR:

		lhs = Expr(node->kids[LEFT]);
		rhs = Expr(node->kids[RIGHT]);
		target = Temp(node->type);
		Add(QBinOp(node->op, target, RVar(lhs), RVar(rhs)));
		return target;

	case QNOT:
	case QNEG:

		lhs = Expr(node->kids[LEFT]);
		target = Temp(node->type);
		Add(QUnaryOp(node->op, target, RVar(lhs)));
		return target;

	case QINDIR:

		target = Temp(node->type);

		lhs = Expr(node->kids[LEFT]);
		subidx = node->kids[LEFT]->subscript;
		if (!subidx)
			Add(QLoad(target, lhs, NULL));
		else
			Add(QLoad(target, lhs, RVar(subidx)));
		return target;

	case QASGN:

		return Asgn(node);

	case QCVTI:
	case QCVTF:
	case QCVTU:
	case QCVTP:

		break;

	case QEQ:
	case QGE:
	case QGT:
	case QLE:
	case QLT:
	case QNE:

		target = Temp(node->type);
		tru = NewLabel();
		merge = NewLabel();

		lhs = Expr(node->kids[LEFT]);
		rhs = Expr(node->kids[RIGHT]);

		Add(QJmpc(OPCODE(node->op), RVar(lhs), RVar(rhs), tru));

		Add(QMov(target, RValInt(0)));
		Add(QJmp(merge));

		Add(tru);
		Add(QMov(target, RValInt(1)));
		Add(QJmp(merge));

		Add(merge);
		return target;

	case QAND:

		fals = NewLabel();
		merge = NewLabel();
		target = Temp(node->type);

		lhs = Expr(node->kids[LEFT]);
		Add(QJmpc(QEQ, RVar(lhs), RValInt(0), fals));

		rhs = Expr(node->kids[RIGHT]);
		Add(QJmpc(QEQ, RVar(rhs), RValInt(0), fals));

		Add(QMov(target, RValInt(1)));
		Add(QJmp(merge));

		Add(fals);
		Add(QMov(target, RValInt(0)));
		Add(QJmp(merge));

		Add(merge);
		return target;

	case QOR:

		tru = NewLabel();
		merge = NewLabel();
		target = Temp(node->type);

		lhs = Expr(node->kids[LEFT]);
		Add(QJmpc(QNE, RVar(lhs), RValInt(0), tru));

		rhs = Expr(node->kids[RIGHT]);
		Add(QJmpc(QNE, RVar(rhs), RValInt(0), tru));

		Add(QMov(target, RValInt(0)));
		Add(QJmp(merge));

		Add(tru);
		Add(QMov(target, RValInt(1)));
		Add(QJmp(merge));

		Add(merge);
		return target;

	case QSUBSCRIPT:

		target = Expr(node->kids[LEFT]);
		node->subscript = Expr(node->kids[RIGHT]);
		return target;

	case MEMBER:

		target = Expr(node->u.mem.lhs);

		val = Temp(IntType);
		Add(QMov(val, RValInt(node->u.mem.field->offset)));
		Add(QBinOp(QADD, target, RVar(target), RVar(val)));

		return target;
	};

	Fatal("Internal: Unknown QCODE '%i'", node->op >> 8);
	return NULL;
}

PRIVATE void IfStmt(IN PCONTEXT ctx, IN PNODE node) {

	PSYMBOL cond;
	PQUAD   merge;
	PQUAD   fals;

	merge = NewLabel();

	Add(QCoord(node->coord));

	cond = Expr(node->u.ifStmt.cond);

	if (node->u.ifStmt.els) {

		fals = NewLabel();

		Add(QJmpc(QEQ, RVar(cond), RValInt(0), fals));

		if (node->u.ifStmt.then)
			Stmt(ctx, node->u.ifStmt.then);
		Add(QJmp(merge));

		Add(fals);

		if (node->u.ifStmt.els)
			Stmt(ctx, node->u.ifStmt.els);
	}
	else{

		Add(QJmpc(QEQ, RVar(cond), RValInt(0), merge));

		if (node->u.ifStmt.then)
			Stmt(ctx, node->u.ifStmt.then);
	}

	if (IsFallthrough())
		Add(QJmp(merge));
	Add(merge);
}

PRIVATE void WhileStmt(IN PCONTEXT ctx, IN PNODE node) {

	PSYMBOL cond;
	PQUAD   merge;
	PQUAD   start;
	PQUAD   cont;
	PQUAD   brk;

	Add(QCoord(node->coord));

	cont = ctx->contLabel;
	brk = ctx->brkLabel;

	start = NewLabel();
	merge = NewLabel();

	ctx->contLabel = start;
	ctx->brkLabel = merge;

	Add(start);

	cond = Expr(node->u.loopStmt.cond);

	Add(QJmpc(QEQ, RVar(cond), RValInt(0), merge));

	Stmt(ctx, node->u.loopStmt.body);

	if (IsFallthrough())
		Add(QJmp(start));

	Add(merge);

	ctx->contLabel = cont;
	ctx->brkLabel = brk;
}

PRIVATE void DoStmt(IN PCONTEXT ctx, IN PNODE node) {

	PSYMBOL cond;
	PQUAD   merge;
	PQUAD   start;

	start = NewLabel();
	merge = NewLabel();

	Add(start);
	Stmt(ctx, node->u.loopStmt.body);

	Add(QCoord(node->coord));

	cond = Expr(node->u.loopStmt.cond);
	Add(QJmpc(QNE, RVar(cond), RValInt(0), start));
	Add(merge);
}

PRIVATE void ForStmt(IN PCONTEXT ctx, IN PNODE node) {

	PSYMBOL cond;
	PQUAD   merge;
	PQUAD   start;

	Add(QCoord(node->coord));

	start = NewLabel();
	merge = NewLabel();

	Stmt(ctx, node->u.loopStmt.init);

	Add(start);

	cond = Expr(node->u.loopStmt.cond);

	Add(QJmpc(QEQ, RVar(cond), RValInt(0), merge));

	Stmt(ctx, node->u.loopStmt.body);

	Expr(node->u.loopStmt.next);

	if (IsFallthrough())
		Add(QJmp(start));

	Add(merge);
}

PRIVATE void DefaultStmt(IN PCONTEXT ctx, IN PNODE node) {

	PQUAD label;

	label = NewLabel();

	AddCase(&ctx->swtchCase, label, 0, TRUE);

	Add(QCoord(node->coord));
	Add(label);
	Stmt(ctx, node->u.caseStmt.stmt);
}

PRIVATE void CaseStmt(IN PCONTEXT ctx, IN PNODE node) {

	PQUAD label;

	label = NewLabel();

	AddCase(&ctx->swtchCase, label, node->u.caseStmt.cond, FALSE);

	Add(QCoord(node->coord));
	Add(label);
	Stmt(ctx, node->u.caseStmt.stmt);
}

PRIVATE void SwtchStmt(IN PCONTEXT ctx, IN PNODE node) {

	PNODE       prevSwtch;
	PQCASE_LIST prevSwtchCase;
	PQUAD       prevBrkLabel;
	PQCASE_LIST swtchCase;
	PQUAD       brkLabel;
	PSYMBOL     cond;
	PQUAD       jmpTableLabel;
	PQUAD       defLabel;

	jmpTableLabel = NewLabel();
	brkLabel = NewLabel();
	defLabel = NULL;

	prevSwtch = ctx->swtch;
	prevBrkLabel = ctx->brkLabel;
	prevSwtchCase = ctx->swtchCase;

	ctx->swtch = node;
	ctx->swtchCase = NULL;
	ctx->brkLabel = brkLabel;

	Add(QCoord(node->coord));
	cond = Expr(node->u.swtch.cond);

	Add(QJmp(jmpTableLabel));

	Stmt(ctx, node->u.swtch.body);

	Add(QJmp(brkLabel));

	Add(jmpTableLabel);
	for (swtchCase = ctx->swtchCase; swtchCase; swtchCase = swtchCase->next) {
		if (swtchCase->isDefault)
			defLabel = swtchCase->label;
		if (!swtchCase->isDefault)
			Add(QJmpc(QEQ, RVar(cond), RValInt(swtchCase->cond), swtchCase->label));
	}
	if (defLabel)
		Add(QJmp(defLabel));

	Add(brkLabel);

	ctx->swtch = prevSwtch;
	ctx->brkLabel = prevBrkLabel;
	ctx->swtchCase = prevSwtchCase;
}

PRIVATE void GotoStmt(IN PCONTEXT ctx, IN PNODE node) {

	PQUAD label;

	label = QLabel(node->ident);
	Add(QCoord(node->coord));
	Add(QJmp(label));
}

PRIVATE void LabelStmt(IN PCONTEXT ctx, IN PNODE node) {

	Add(QCoord(node->coord));
	Add(QLabel(node->ident));
}

PRIVATE void Stmt(IN PCONTEXT ctx, IN PNODE node) {

	PSYMBOL target;
	PSYMBOL rhs;
	int     subscript;

	if (!node) return;
	subscript = 0;

	switch(OPCODE(node->op)) {

	case QLBRACE:

		Add(QLBrace());
		Stmt(ctx, node->kids[LEFT]); // NODELST
		Add(QRBrace());
		break;

	case NODELST:

		while (NodeListGet(node)) {
			Stmt(ctx, NodeListGet(node));
			node = NodeListNext(node);
		}
		break;

	case IF:
		
		IfStmt(ctx, node);
		break;

	case WHILE:
		
		WhileStmt(ctx, node);
		break;

	case DOWHILE:
		
		DoStmt(ctx, node);
		break;

	case FOR:
		
		ForStmt(ctx, node);
		break;

	case GOTO:

		GotoStmt(ctx, node);
		break;

	case LABEL:

		LabelStmt(ctx, node);
		break;

	case CONTINUE:

		Add(QCoord(node->coord));
		if (ctx->contLabel)
			Add(QJmp(ctx->contLabel));
		break;

	case BREAK:

		Add(QCoord(node->coord));
		if (ctx->brkLabel)
			Add(QJmp(ctx->brkLabel));
		break;

	case SWTCH:

		SwtchStmt(ctx, node);
		break;

	case CASE:

		CaseStmt(ctx, node);
		break;

	case DEFAULT:

		DefaultStmt(ctx, node);
		break;

	case QASGN:

		Asgn(node);
		break;

	case QRET:

		Add(QCoord(node->coord));
		rhs = Expr(node->kids[LEFT]);
		Add(QRet(RVar(rhs)));
		break;

	default:

		Add(QCoord(node->coord));
		Expr(node);
	};
}

extern PUBLIC void FlowPass(IN PSYMBOL funct);

PRIVATE void _QInitArray(IN PSYMBOL target, IN PNODE node, IN PTYPE type) {

}

PRIVATE void _QInit(IN PSYMBOL target, IN PNODE node) {

	PRVAL   rhs;
	PNODE   entry;

	entry = node;

	if (node->op == NODELST) {
		for (; entry; entry = entry->u.list.next) {
			_QInit(target, entry->u.list.me);
		}
		return;
	}

	// these have offsets relative to this symbol "target"

	rhs = RValInt(node->val.i);
	Add(QStore(target, rhs, NULL));
}


PRIVATE void QInit(IN PSYMBOL target) {

	PFIELD field;
	PTYPE  type;

	field = NULL;
	type = target->type;

	if (IsStruct(target->type)) {
		field = target->type->fields;
		type = field->type;
	}

	if (IsArray(target->type))
		type = target->type->type;



//	__debugbreak();

}



PUBLIC void QPass(IN PSYMBOL func) {

	CONTEXT ctx;
	PSYMBOL local;

	_function = func;
	_tempcount = 0;
	_labcount = 0;

	func->quads = calloc(1, sizeof(BLOCK));

	ctx.brkLabel = NULL;
	ctx.contLabel = NULL;
	ctx.swtch = NULL;
	ctx.swtchCase = NULL;

	for (local = func->localsListHead; local; local = local->nextLocal) {
		if (local->ast) {
			Add(QCoord(local->ast->coord));
			QInit(local);
		}
	}

	Add(NewLabel());
	Stmt(&ctx, func->ast);
}

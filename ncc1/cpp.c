/************************************************************************
*
*	cpp.c - C preprocessor
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "ncc.h"

#define MACRO_OBJECT   1
#define MACRO_FUNCTION 2

typedef struct MACRO {
	int     type;
	PVECTOR body;
	int     ops;
	bool_t  isVar;
}MACRO, *PMACRO;
 
PRIVATE MAP    macros;
PRIVATE VECTOR pushback;

PRIVATE void Directive(void) {

	TOKEN tok;

	while (TRUE) {
		tok = Lex();
		if (tok.op == '\n' || tok.op == TK_EOF)
			break;
	}
}

PUBLIC TOKEN GetToken(void) {

	TOKEN tok;

	while (TRUE) {

		tok = Lex();

		if (tok.op == '#') {
			if (!tok.firstOnLine)
				Fatal("Preprocessor command must start as first nonwhite space");
			Directive();
			continue;
		}
		else if (tok.op == ' ' || tok.op == '\n') {
			continue;
		}
		else if (tok.op == TK_IDENT) {
			// can be MACRO, or just an identifier...
			return tok;
		}
		else
			return tok;
	}
}

PUBLIC void UngetToken(IN TOKEN tok) {

	Unlex(tok);
}

PUBLIC TOKEN PeekToken(IN int idx) {

	TOKEN tok[32];
	TOKEN ret;
	index_t c;

	if (idx > 31) Fatal("Preprocessor: lookahead exceeds limit");
	for (c = 0; c < idx; c++)
		tok[c] = GetToken();
	ret = tok[c - 1];
	for (c--; c>0; c--)
		UngetToken(tok[c]);
	UngetToken(tok[c]);
	return ret;
}

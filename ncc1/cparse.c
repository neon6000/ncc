/************************************************************************
*
*	cparse.c - C parser & semantic analysis
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "ncc.h"
#include <string.h>
#include <assert.h>

extern PTARGET _target;

PRIVATE TOKEN tok;
#define token tok.op

#define SYM_BUCKETS 32

PRIVATE PMAP     constants;
PRIVATE PMAP     externals;
PRIVATE PMAP     tags;
PRIVATE PMAP     idents;
PRIVATE PMAP     labels;
PRIVATE unsigned level;
PRIVATE unsigned tempId;
PRIVATE PSYMBOL  _function;
PRIVATE PBLOCK   _lbrace;
PRIVATE PROGRAM  _program;

// scope / symbol table ---

//
//	enter scope
//
PRIVATE void EnterScope() {

	PMAP ntags;
	PMAP nidents;

	if (++level == LOCALS)
		tempId = 0;

	ntags = NewMap(FALSE, SYM_BUCKETS);
	nidents = NewMap(FALSE, SYM_BUCKETS);
	ntags->level = nidents->level = level;

	ntags->parent = tags;
	nidents->parent = idents;

	tags = ntags;
	idents = nidents;
}

//
//	leave scope
//
PRIVATE void LeaveScope() {

	if (tags->level == level)
		tags = tags->parent;
	if (idents->level == level)
		idents = idents->parent;
	assert(level >= GLOBALS);
	--level;
}

//
//	checks if any forward references (used with GOTO)
//
PRIVATE bool_t CheckForwardRefs(void) {

	PSYMBOL       sym;
	PBUCKET       bucket;
	int           index;
	PBUCKET_ENTRY item;

	bucket = labels->buckets;
	for (index = 0; index < labels->numBuckets; index++) {
		ForEach(&labels->buckets[index].itemList, entry) {
			item = (PBUCKET_ENTRY)entry;
			sym = (PSYMBOL) item->val;
			if (sym->fwref)
				return TRUE;
		}
	}
	return FALSE;
}

//
//	Lookup a symbol by name in map
//
PRIVATE PSYMBOL Lookup(IN PMAP m, IN PSTRING s) {

	PSYMBOL sym;

	for (; m; m = m->parent) {
		sym = MapGetA(m, s->val);
		if (sym) return sym;
	}
	return NULL;
}

//
//	"Lexer hack"
//	 called by scanner to identify type names
//
PUBLIC PSYMBOL LexIsTypeName(IN char* cbuf) {

	PSYMBOL sym;
	PMAP    m;

	for (m = idents; m; m = m->parent) {
		sym = MapGetA(m, cbuf);
		if (sym && sym->sclass & SC_TYPEDEF)
			return sym;
	}
	return NULL;
}

//
//	install symbol "s" to map
//
PRIVATE PSYMBOL _Install(IN PMAP* m, IN PSTRING s, IN unsigned level) {

	PSYMBOL sym;

	assert(level == 0 || level >= (*m)->level);
//	if (level > 0 && (*m)->level < level)
//		*m = NewMap(FALSE, SYM_BUCKETS);
	sym = Allocz(sizeof(SYMBOL));
	sym->name = s;
	sym->symbolic = s;
	sym->level = level;

	if (level >= PARMS)
		sym->symbolic = StrCat(s, NewStringA("$"));

	else if (level == GLOBALS)
		sym->symbolic = StrCat(NewStringA("_"), s);

	if (!MapPutA(*m, s->val, sym))
		return NULL;
	return sym;
}

PRIVATE void AddToBBList(IN PSYMBOL* listHead, IN PSYMBOL sym) {

	PSYMBOL s;

	if (*listHead == NULL) {
		*listHead = sym;
		return;
	}

	for (s = *listHead; s->bbnext; s = s->bbnext)
		;
	s->bbnext = sym;
}

PRIVATE void PrependToBBlock(IN PBLOCK block, IN PSYMBOL listHead) {

	PSYMBOL s;

	if (!listHead) return;

	for (s = listHead; s->bbnext; s = s->bbnext)
		;

//	s->bbnext = block->locals;
//	block->locals = listHead;
}

PRIVATE void AddToBlock(IN PBLOCK block, IN PSYMBOL sym) {

	if (!block) return;
//	AddToBBList(&block->locals, sym);
}

PRIVATE void AddLocal(IN PSYMBOL sym) {

	PSYMBOL s;

	AddToBlock(_lbrace, sym);
	if (!_function->localsListHead) {
		_function->localsListHead = sym;
		return;
	}

	for (s = _function->localsListHead; s->nextLocal; s = s->nextLocal)
		;
	s->nextLocal = sym;
}

// add local symbols:
PRIVATE PSYMBOL Install(IN PMAP* m, IN PSTRING s) {

	PSYMBOL sym;
	
	sym = _Install(m, s, level);

	if (m == &idents && level >= PARMS)
		AddLocal(sym);

	return sym;
}

PRIVATE PSYMBOL InstallUnique(IN PMAP* m, int level) {

	static unsigned i = 0;
	char* s;

	s = Alloc(32);
	sprintf(s, "??_C_@%i", i++);
	return _Install(m, NewStringA(s), level);
}

PRIVATE PSYMBOL InstallStatic(IN PMAP* m, IN PSTRING name) {

	char* s;
	PSYMBOL sym;

	s = Alloc(StrLen(name) + StrLen(_function->name) + 32);
	sprintf(s, "?%s@?%i??%s", name->val, _function->statics++, _function->name->val);
	sym = Install(m, name);
	sym->symbolic = NewStringA(s);
	sym->sclass = SC_STATIC;

	SymbolListAdd(&_program.gvars, sym);
	return sym;
}

PRIVATE PSYMBOL InstallString(IN PSTRING s, IN PTYPE type) {

	PSYMBOL sym;

	if (type != ArrayOfCharType && type != ArrayOfWideCharType)
		__debugbreak();

	sym = InstallUnique(&constants, GLOBALS);
	sym->type = type;
	sym->val.s = s;
	sym->sclass = SC_STATIC;
	sym->segment = RODATA;

	SymbolListAdd(&_program.cstr, sym);
	return sym;
}

PRIVATE PSYMBOL InstallGlobal(IN PMAP* m, IN PSTRING s) {

	PSYMBOL sym;

	sym = _Install(m, s, GLOBALS);
	SymbolListAdd(&_program.gvars, sym);
	return sym;
}

/*
==============================================

				PARSER

==============================================
*/

// consume/lookahead ---

// FIXME: currently called from Main.
PUBLIC void Next(void) {
	tok = GetToken();
}

PRIVATE unsigned Peek1(void) {
	TOKEN t = PeekToken(1);
	return t.op;
}

PRIVATE bool_t Accept(IN int op) {
	if (token != op) return FALSE;
	Next();
	return TRUE;
}

PRIVATE void Skip(IN int op) {
	while (TRUE) {
		if (Accept(TK_EOF)) Fatal("EOF reached");
		if (Accept(op)) break;
		Next();
	}
}

PRIVATE bool_t Expect(IN int op) {
	if (token != op) {
		Error("Expected type %i", op);
		return FALSE;
	}
	Next();
	return TRUE;
}

PRIVATE void Sync() {
	UngetToken(ErrorToken());
}

PRIVATE PTYPE Specifier(OUT OPTIONAL int* storage);
PRIVATE PTYPE Declarator(IN PTYPE base, OUT OPTIONAL PSTRING* name);
PRIVATE PTYPE AbstractDeclarator(IN PTYPE base);
PRIVATE PTYPE ParameterDecl(IN PTYPE func);

// specifiers ---

PRIVATE PNODE Constant();
PRIVATE PNODE ConstantInc(IN PNODE prev);
PRIVATE PNODE ParseConstant();

PRIVATE bool_t IsTypeName(IN TOKEN t) {

	return t.attr == ATTR_TYPENAME;
}

PRIVATE PTYPE EnumSpecifier() {

	PTYPE   ret;
	PSTRING tag;
	PSYMBOL sym;
	PSTRING fieldname;
	PTYPE   fieldtype;
	PNODE   value;
	PNODE   lastvalue;

	// fieldtype must be big enough to hold all possible values...

	value = NULL;
	lastvalue = NULL;
	sym = NULL;
	ret = NULL;

	Next();
	tag = NULL;
	if (token == TK_IDENT) {
		tag = tok.val.s;
		Next();
		sym = Lookup(tags, tag);
		if (sym)
			ret = sym->type;
		else {
			ret = Struc(TYPE_ENUM, tag);
			sym = Install(&tags, tag);
			sym->type = ret;
			sym->name = tag;
			ret->sym = sym;
		}
	}

	// enum <tag> ;
	if (!Accept('{')) {
		if (sym && sym->type->kind != TYPE_ENUM) {
			Error("first declaration was type 'enum'");
			return ret;
		}
		return ret;
	}

	// enum <tag> { ... }
	//ret->defined = TRUE;
	while (token != TK_ERROR) {

		if (token != TK_IDENT)
			Fatal("Enum: ident expected");

		fieldname = tok.val.s;
		Next();

		if (Accept('='))
			value = ParseConstant();
		else if (!lastvalue)
			value = Constant(0);
		else
			value = ConstantInc(lastvalue);

		lastvalue = value;

		sym = Install(&idents, fieldname);
		sym->type = EnumeratorType;
		sym->val.y = value;

		if (Accept(',')) continue;
		if (Accept('}')) break;
	}
	return ret;
}

PRIVATE PTYPE StructOrUnionSpecifier() {

	unsigned sclass;
	PSTRING  tag;
	PSTRING  fieldname;
	PTYPE    fieldtype;
	PTYPE    ret;
	PSYMBOL  sym;
	int      kind;

	kind = token == K_STRUCT ? TYPE_STRUCT : TYPE_UNION;
	Next();

	tag = NULL;
	ret = NULL;
	sym = NULL;
	fieldname = NULL;

	if (token == TK_IDENT) {
		tag = tok.val.s;
		Next();
		sym = Lookup(tags, tag);
		if (sym)
			ret = sym->type;
		else {
			ret = Struc(kind);
			sym = Install(&tags, tag);
			sym->type = ret;
			sym->name = tag;
			ret->sym = sym;
		}
	}

	// struct <tag> ;
	if (!Accept('{')) {
		if (sym && sym->type->kind != kind) {
			Error("redeclaration of struct/union is incompatible");
			return ret;
		}
		return ret;
	}

	if (!ret)
		ret = Struc(kind);

	// struct <tag> { ... }
	ret->defined = TRUE;
	while (token != TK_ERROR) {

		if (Accept('}')) break;

		fieldtype = Specifier(&sclass);
		while (token != TK_ERROR) {

			if (token != ':')
				fieldtype = Declarator(fieldtype, &fieldname);

			if (!fieldtype) {
				Fatal("struct/union member with unknown type name");
				return FALSE;
			}

			// FIXME: support bitfield -- <type> <ident> : <constant> ;
			if (token == ':') {
				Next();
				ParseConstant();
			}

			AddField(ret, fieldname, fieldtype, 0);

			if (token == ';') break;
			else if (token == ',') {
				Next();
				continue;
			}
		}
		if (!Expect(';')) Fatal("Error ';' parse struct");
	}

	return ret;
}

PRIVATE PTYPE Specifier(OUT int* storage) {

#define SHORT    1
#define LONG     2
#define LONGLONG 4

#define VOID     1
#define CHAR     2
#define INT      4
#define FLOAT    8
#define DOUBLE   16

#define SIGNED   1
#define UNSIGNED 2

#define CONST    1
#define VOLATILE 2

	bool_t done = FALSE;
	PTYPE t     = NULL;
	int sc      = 0;
	int size    = 0;
	int base    = 0;
	int sign    = 0;
	int qual    = 0;
	bool_t once = FALSE;

	while (TRUE) {
		switch (token) {
		//
		// storage class
		//
		case K_TYPEDEF:    Next(); sc |= SC_TYPEDEF; break;
		case K_EXTERN:     Next(); sc |= SC_EXTERN; break;
		case K_STATIC:     Next(); sc |= SC_STATIC; break;
		case K_AUTO:       Next(); sc |= SC_AUTO; break;
		case K_REGISTER:   Next(); sc |= SC_REGISTER; break;
		//
		// base types
		//
		case K_VOID:       Next(); base |= VOID; break;
		case K_CHAR:       Next(); base |= CHAR; break;
		case K_INT:        Next(); base |= INT; break;
		case K_FLOAT:      Next(); base |= FLOAT;  break;
		case K_DOUBLE :    Next(); base |= DOUBLE; break;
		//
		// signed/unsigned
		//
		case K_SIGNED:     Next(); sign |= SIGNED; break;
		case K_UNSIGNED:   Next(); sign |= UNSIGNED; break;
		//
		// size
		//
		case K_SHORT:      Next(); size |= SHORT; break;
		case K_LONG: Next();
			if ((size & LONG) == LONG) {
				size &= ~LONG;
				size |= LONGLONG;
			}
			else
				size |= LONG;
			break;
		//
		// Qualifiers
		//
		case K_CONST:     Next(); qual |= CONST; break;
		case K_VOLATILE:  Next(); qual |= VOLATILE; break;
		//
		// STRUCT/UNION,ENUM,TYPEDEF
		//
		case K_STRUCT:
		case K_UNION:
			t = StructOrUnionSpecifier();
			if (storage) *storage = sc;
			if (qual) return Qual(qual, t);
			return t;
		case K_ENUM:
			t = EnumSpecifier();
			if (storage) *storage = sc;
			if (qual) return Qual(qual, t);
			return t;
		case TK_TYPENAME:
			t = tok.val.x->type;
			Next();
			return t;
		default:
			// if we have not consumed a single token,
			// this isn't a specifier:
			if (!once) return NULL;
			done = TRUE;
			break;
		};
		if (done) break;
		once = TRUE;
	}

	if (qual && (qual != CONST && qual != VOLATILE))
		Error("Invalid combination of qualifiers");

	if (sc && (sc != SC_EXTERN && sc != SC_STATIC
		&& sc != SC_REGISTER && sc != SC_AUTO
		&& sc != SC_TYPEDEF)) {
		Error("Invalid combination of storage class specifiers");
	}

	if (!base) {
		base = INT;
		t = IntType;
	}

	if (base == CHAR && sign == SIGNED)
		t = SignedCharType;
	else if (base == CHAR && sign == UNSIGNED)
		t = UnsignedCharType;
	else if (size == SHORT && sign == SIGNED)
		t = ShortType;
	else if (size == SHORT && sign == UNSIGNED)
		t = UnsignedShortType;
	else if (size == LONG && base == DOUBLE)
		t = LongDoubleType;
	else if (size == LONGLONG && sign == UNSIGNED)
		t = UnsignedLongLongType;
	else if (size == LONGLONG && sign == SIGNED)
		t = LongLongType;
	else if (size == LONG && sign == UNSIGNED)
		t = UnsignedLongType;
	else if (size == LONG && sign == SIGNED)
		t = LongType;
	else if (base == INT && sign == UNSIGNED)
		t = UnsignedIntType;
	else if (base == INT)
		t = IntType;
	else if (base == CHAR)
		t = CharType;
	else if (base == VOID)
		t = VoidType;
	else {
		Error("Invalid combination of specifiers");
		__debugbreak();
	}

	if (storage) *storage = sc;
	if (qual) return Qual(qual, t);
	return t;
}

// expressions ---

PRIVATE PNODE Expr();


// op is QADD or QSUB


PRIVATE PNODE Primary(void) {

	PNODE   t;
	PSYMBOL s;
	PSYMBOL q;
	PSTRING st;

	t = NULL;
	switch (token) {

	case TK_NUMBER:

		t = Leaf(QCNST, tok.type, tok.val);
		break;

	case TK_IDENT:

		s = Lookup(idents, tok.val.s);
		if (!s) {
			if (Peek1() == '(') {
				q = Lookup(externals, tok.val.s);
				if (!q) {
					Warn("'%A' undefined, assuming external returning int", tok.val.s);
					q = InstallGlobal(&externals, tok.val.s);
					q->type = Function(IntType, NULL, FALSE);
					q->sclass = SC_EXTERN;
				}
				s = q;
			}
			else {
				Error("'%A' undefined", tok.val.s);
				s = Install(&idents, tok.val.s);
				s->type = IntType;
				s->sclass = SC_AUTO;
			}
		}
		t = TreeAddr(s);
		break;

	case TK_STRING:
		// consecutive strings are concatenated
		st = NULL;
		while (TRUE) {
			st = StrCat(st, tok.val.s);
			if (Peek1() == TK_STRING) {
				Next();
				continue;
			}
			break;
		}
		t = TreeAddr(InstallString(st, ArrayOfCharType));
		break;
	default:
		return NULL;
	}
	Next();
	return t;
}

PRIVATE PNODE StructRef(IN PNODE lhs, IN PSTRING ident) {

	PNODE  node;
	PFIELD field;
	PTYPE  type;

	if (IsPointer(lhs->type))
		type = lhs->type->type;
	else
		type = lhs->type;

	if (!IsStruct(type)) {
		Error("not a struct or union");
		__debugbreak();
	}

	field = GetField(type, ident);
	if (!field) {
		Error("field '%A' not a member of '<struct or union>'", field->name);
		__debugbreak();
	}

	return NodeMember(lhs, field);
}

PRIVATE PNODE Assignment(void);

PRIVATE PNODE Postfix(IN PNODE lhs) {

	PNODE     arg;
	PNODE     t;
	PTYPE     ty;
	PFIELD    field;
	unsigned  c;

	c = 0;
	arg = NULL;

	while (TRUE) {
		if (token == '[') {
			//
			// array subscript
			//
			if (!IsPointer(lhs->type))
				Error("Subscript requires pointer to object type");
			Next();

			t = TreeAdd('+', lhs, Expr());
			if (!t)
				Error("Expression expected");

 			lhs = RValue(t);

			Expect(']');
			continue;
		}

		if (token == '(') {
			//
			// function call
			//
			t = lhs;
			Next();
			// expect pointer-to-function type
			if (!(IsPointer(lhs->type) && IsFunction(lhs->type->type))) {
				Error("expected pointer-to-function type");
				break;
			}
			while (token != ')') {
				lhs = Assignment();
				arg = Tree(QARG, lhs->type, arg, lhs);
				if (token == ',') {
					Next();
					continue;
				}
				break;
			}
			Expect(')');
			// the Type of CALL is the return type of the function call
			lhs = Tree(QCALL, t->type->type->type, arg, t);
			continue;
		}

		if (token == '.') {
			//
			// . operator
			//
			if (IsPointer(lhs->type)) {
				Error("pointer not expected");
				__debugbreak();
			}
			Next();
			if (token != TK_IDENT) {
				Error("identifier expected");
				__debugbreak();
			}
			// C99: The value is that of the named member, and is an lvalue if the
			// first expression is an lvalue.
			lhs = StructRef(LValue(lhs), tok.val.s);
			lhs->type = Pointer(lhs->type);
			lhs = RValue(lhs);
			Next();
			continue;
		}

		/*
                    INDIR
					  |
                   MEMBER "pointer to lhs"
                    /   \
           lhs     /     FIELD
                INDIR
                  |
                ADDRL
		*/

		if (token == TK_DEREF) {
			//
			// -> operator: x->y = (*x).y
			//
			if (!IsPointer(lhs->type)) {
				Error("pointer expected");
				__debugbreak();
			}
			Next();
			if (token != TK_IDENT) {
				Error("identifier expected");
				__debugbreak();
			}
			lhs = StructRef(RValue(lhs), tok.val.s);
			lhs->type = Pointer(lhs->type);
			lhs = RValue(lhs);
			Next();
			continue;
		}

		if (token == TK_INCR) {
			Next();
			lhs = NodePostInc(LValue(lhs));
			continue;
		}

		if (token == TK_DECR) {
			Next();
			lhs = NodePostDec(LValue(lhs));
			continue;
		}

		return lhs;
	}
}

PRIVATE PNODE Unary(void) {

	PNODE   t;
	PTYPE   ty;
	PSYMBOL sym;
	int     sclass;

	t = NULL;
	sym = NULL;
	ty = NULL;

	switch (token) {
	case '*':
		Next(); t = Unary(); t = MabeyDecay(t);
		if (IsPointer(t->type) && (IsFunction(t->type) || IsArray(t->type)))
			t = Retype(t, t->type->type);
		else
			t = RValue(t);
		break;
	case '&':
		Next(); t = Unary();
		if (IsArray(t->type) || IsFunction(t->type))
			t = Retype(t, Pointer(t->type));
		else
			t = LValue(t);
		break;
	case '+':
		Next(); t = Unary(); t = MabeyDecay(t);
		if (!IsArith(t->type))
			Error("Must be arith type");
		else
			t = Cast(t, Promote(t->type));
		break;
	case '-':
		Next(); t = Unary(); t = MabeyDecay(t);
		if (!IsArith(t->type))
			Error("Must be arith type");
		else {
			if (t->type->kind == UNSIGNED)
				Warn("Unary - on unsigned operand");
			ty = Promote(t->type);
			t = Tree(QNEG, ty, Cast(t, ty), NULL);
		}
		break;
	case '~':
		Next(); t = Unary(); t = MabeyDecay(t);
		if (!IsInt(t->type))
			Error("Must be int type");
		else {
			ty = Promote(t->type);
			t = Tree(QBCMPL, ty, Cast(t, ty), NULL);
		}
		break;
	case '!':
		Next(); t = Unary(); t = MabeyDecay(t);
		if (!IsScalar(t->type))
			Error("Must be scalar type");
		else
			t = Not(t);
		break;
	case TK_INCR:
		Next(); t = Unary(); t = MabeyDecay(t);
		t = NodePreInc(LValue(t));
		break;
	case TK_DECR:
		Next(); t = Unary(); t = MabeyDecay(t);
		t = NodePreDec(LValue(t));
		break;
	case K_SIZEOF:
		Next();
		if (Accept('(')) {
			if (IsTypeName(tok)) {
				ty = Specifier(&sclass);
				ty = AbstractDeclarator(ty);
				t = Constant(ty->size);
			}
			else{
				Error("sizeof: expected type name");
				t = Constant(0);
			}
			Expect(')');
			return t;
		}
		else{
			t = Unary();
			if (t)
				t = Constant(t->type->size);
			else{
				Error("sizeof: expected expression");
				t = Constant(0);
			}
			return t;
		}
	case '(':
		Next();
		if (IsTypeName(tok)) {
			ty = Specifier(&sclass);
			ty = AbstractDeclarator(ty);
			if (!Expect(')'))
				return NULL;
			t = Unary();
			return Cast(t, ty);
		}
		else{
			t = Expr();
			if (!t) return NULL;
			if (!Expect(')')) return t;
			t = Postfix(MabeyDecay(t));
			return t;
		}
	default:
		t = Primary();
		if (!t) return NULL;
		t = Postfix(MabeyDecay(t));
		return t;
	}
	return t;
}

// Precedence climbling
PRIVATE PNODE Binary(IN PNODE lhs, IN int minPrec) {

	TOKEN op;
	PNODE rhs;

	rhs = NULL;
	while (tok.optype == OP_BINARY && tok.prec >= minPrec) {		
		op = tok;
		Next();
		rhs = Unary();
		while (tok.optype == OP_BINARY && tok.prec > op.prec)
			rhs = Binary(rhs, tok.prec);
		assert(op.tree);
		lhs = op.tree(op.op, lhs, rhs);
	}
	return lhs;
}

PRIVATE PNODE Ternary() {

	PNODE t;

	t = Binary(Unary(), 0);
	if (!t)
		return NULL;
	if (token == '?') {
		__debugbreak();
		Next();
		Expr();
		if (!Expect(':'))
			return FALSE;
		Ternary();
	}
	return t;
}

PRIVATE bool_t IsAssignment() {

	switch (token) {
	case '=':
	case TK_ADD_EQ:
	case TK_MUL_EQ:
	case TK_MOD_EQ:
	case TK_DIV_EQ:
	case TK_SUB_EQ:
	case TK_SHL_EQ:
	case TK_SHR_EQ:
	case TK_AND_EQ:
	case TK_OR_EQ:
	case TK_XOR_EQ:
		return TRUE;
	};
	return FALSE;
}


/*
PRIVATE PNODE _Assignment(IN PNODE lvalue, IN PNODE list) {

	PNODE  lhs;

	lhs = Ternary();
	if (!lhs) return NULL;

	if (IsAssignment()) {
//		Fatal("ASSIGN: this will trigger the a = b = c bug");
//		__debugbreak();
		Next();
//		rhs = Assignment();
		_Assignment(lvalue, list);
	}


	NodeListAppendUnique(&list, lvalue);

	return lhs;
}
*/

PRIVATE PNODE Assignment(void) {

	PNODE  lhs;
	PNODE  rhs;
	PNODE  ret;
	PNODE  lval;
	PNODE  list;
	int    op;

	lhs = Ternary();
	if (!lhs) return NULL;
	ret = lhs;

	if (IsAssignment()) {
		op = token;
		Next();

		lval = LValue(lhs);
		rhs = Assignment();

		ret = Assgn(QASGN, lval, rhs);
		ret->coord = tok.coord;
		ret = RValue(ret);
		ret->coord = tok.coord;
	}


#if 0
	while (IsAssignment()) {

		op = token;
		Next();

		lval = LValue(lhs);
		rhs = Assignment();

		// a = b = c

		// turn into:

		// b = c
		// a = b

		// as separate statement-expressions

//		The assignment operator ( = ) and the compound
//		assignment operators all group right-to-left.All
//		require a modifiable lvalue as their left operand
//		and return an lvalue referring to the left operand

		// a = b = c
		// b = c ->     rhs "c" return.. INDIR
		// a = b = c -> rhs "b = c" return.. ASGN

		switch (op) {
		case '=':

			ret = Assgn(QASGN, lval, rhs);

//			ret = lval; // this is return value <-- lvalue of first operand

			ret->coord = tok.coord;
			break;
		case TK_ADD_EQ:
			ret = Assgn(QASGN, lval, TreeAdd(QADD, lhs, rhs));
			break;
		case TK_MUL_EQ:
			ret = Assgn(QASGN, lval, TreeMul(QMUL, lhs, rhs));
			break;
		case TK_MOD_EQ:
//			ret = Assgn(QASGN, lval, TreeMod(QMOD, lhs, rhs));
			break;
		case TK_DIV_EQ:
			ret = Assgn(QASGN, lval, TreeMul(QDIV, lhs, rhs));
			break;
		case TK_SUB_EQ:
			ret = Assgn(QASGN, lval, TreeSub(QSUB, lhs, rhs));
			break;
		case TK_SHL_EQ:
		case TK_SHR_EQ:
		case TK_AND_EQ:
		case TK_OR_EQ:
		case TK_XOR_EQ:
			__debugbreak();
			return TRUE;
		default:
			__debugbreak();
			break;
		};
	}
#endif
	return ret;
}

PRIVATE PNODE Initializer(void) {

	PNODE list;
	PNODE item;

	list = NULL;

	if (Accept('{')) {
		while (TRUE) {
			item = Initializer();
			if (!list)
				list = NodeListAlloc(item);
			else
				NodeListAppend(list, item);
			if (Accept(','))
				continue;
			Expect('}');
			return list;
		}
	}
	return Assignment();
}

PRIVATE PNODE Expr() {

	PNODE  t;
	PNODE  lst;
	COORD  coord;

	lst = NULL;
	t = Assignment();

	if (!t)
		return NULL;

	coord = tok.coord;
	t->coord = coord;

	while (Accept(',')) {
		lst = NodeListAlloc(t);
		t = Assignment();
		if (!t)
			Fatal("Expr NODELST: comma without expression");
		t->coord = coord;
		NodeListAppend(lst, t);
	}
	if (lst) {
		lst->coord = coord;
		return lst;
	}
	return t;
}

PRIVATE PNODE Constant(IN unsigned int val) {

	PNODE node;

	node = Tree(QCNST, UnsignedIntType, NULL, NULL);
	node->val.ui = val;
	return node;
}

// adds prev + 1 used for enumerations
PRIVATE PNODE ConstantInc(IN PNODE prev) {

	return TreeAdd(QADD, prev, Constant(1));
}

PRIVATE PNODE ParseConstant() {

	return Ternary();
}

PRIVATE int Eval(IN PNODE cons) {

	int left;
	int right;

	if (OPCODE(cons->op) == QCNST)
		return cons->val.i;

	left = right = 0;

	if (cons->kids[LEFT])
		left = Eval(cons->kids[LEFT]);
	if (cons->kids[RIGHT])
		right = Eval(cons->kids[RIGHT]);

	switch (OPCODE(cons->op)) {
	case QADD: return left + right;
	case QSUB: return left - right;
	case QMUL: return left * right;
	case QDIV: return left / right;
	case QNEG: return -left;
	case QBSHL: return left << right;
	case QBSHR: return left >> right;
	case QBAND: return left & right;
	case QBCMPL: return ~left;
	case QBOR: return left | right;
	case QBXOR: return left ^ right;
	case QEQ: return left == right;
	case QGE: return left >= right;
	case QGT: return left > right;
	case QLE: return left <= right;
	case QLT: return left < right;
	case QNE: return left != right;
	case QNOT: return !left;
	case QAND: return left && right;
	case QOR: return left || right;
	default:
		Error("Constant expected");
		return 0;
	};
}


// statements ---

PRIVATE void Decl();
PRIVATE PNODE Stmt(void);

PRIVATE bool_t IsStorageClassSpecifier(IN TOKEN tok) {

	return tok.op == K_AUTO || tok.op == K_EXTERN
		|| tok.op == K_REGISTER || tok.op == K_STATIC
		|| tok.op == K_TYPEDEF;
}

/*
PRIVATE PCASE NewCase(IN PNODE expr, IN PBLOCK code) {

	PCASE ccase;

	ccase = Alloc(sizeof(CASE));
	ccase->code = code;
	ccase->expr = expr;
	ccase->next = NULL;
	return ccase;
}

PRIVATE void AddCase(IN PNODE swtch, IN PNODE expr, IN PBLOCK code) {

	PCASE ccase;

	ccase = NewCase(expr, code);
	if (!swtch->u.swtch.last)
		swtch->u.swtch.cases = ccase;
	else
		swtch->u.swtch.last->next = ccase;
	swtch->u.swtch.last = ccase;
}

PRIVATE bool_t HasDefaultCase(IN PNODE swtch) {

	PCASE ccase;

	for (ccase = swtch->u.swtch.cases; ccase; ccase = ccase->next) {
		if (!ccase->expr)
			return TRUE;
	}
	return FALSE;
}
*/

PRIVATE PNODE IfStmt(void) {

	PNODE  expr;
	PNODE  tru;
	PNODE  els;
	PNODE  ifnode;
	COORD  coord;

	if (!Expect('(')) return NULL;
	expr = Expr();
	if (!expr) return NULL;

	coord = tok.coord;

	if (!Expect(')')) return NULL;

	tru = Stmt();
	els = NULL;
	if (Accept(K_ELSE))
		els = Stmt();

	ifnode = NodeIf(expr, tru, els);
	ifnode->coord = coord;
	return ifnode;
}

PRIVATE PNODE WhileStmt() {

	PNODE   expr;
	PNODE   body;
	PNODE   whilenode;
	COORD   coord;

	if (!Expect('(')) return FALSE;
	expr = Expr();
	if (!expr) return FALSE;

	coord = tok.coord;

	if (!Expect(')')) return FALSE;

	body = Stmt();
	whilenode = NodeWhile(expr,body);
	whilenode->coord = coord;

	return whilenode;
}

PRIVATE PNODE DoStmt() {

	PNODE  expr;
	PNODE  body;
	PNODE  donode;
	COORD  coord;

	body = Stmt();
	if (!Expect(K_WHILE)) return FALSE;
	if (!Expect('(')) return FALSE;
	expr = Expr();
	if (!expr) return FALSE;

	if (!Expect(')')) return FALSE;

	coord = tok.coord;

	if (!Expect(';')) return FALSE;
	donode = NodeDoWhile(expr,body);
	donode->coord = coord;

	return donode;
}

PRIVATE PNODE ForStmt() {

	PNODE  init;
	PNODE  next;
	PNODE  cmp;
	PNODE  body;
	PNODE  fornode;
	COORD  coord;

	if (!Expect('(')) return FALSE;
	init = Expr();
	if (!Expect(';')) return FALSE;
	cmp = Expr();
	if (!Expect(';')) return FALSE;
	next = Expr();
	if (!Expect(')')) return FALSE;

	coord = tok.coord;

	body = Stmt();
	fornode = NodeFor(init, cmp, next, body);
	fornode->coord = coord;

	return fornode;
}

PRIVATE PNODE SwtchStmt() {

	PNODE  cmp;
	PNODE  body;
	PNODE  swtchnode;
	COORD  coord;

	if (!Expect('(')) return FALSE;
	cmp = Expr();

	if (!Expect(')')) return FALSE;

	coord = tok.coord;

	body = Stmt();

	swtchnode = NodeSwitch(cmp, body);
	swtchnode->coord = coord;

	return swtchnode;
}

PRIVATE PNODE CompoundStmt() {

	PNODE lbrace;
	PNODE list;
	PNODE stmt;

	lbrace = NodeBrace();

	list = NULL;
	if (!Accept('{'))
		return FALSE;

	EnterScope();

	while (token != EOF && token != '}') {
		if (IsTypeName(tok) || IsStorageClassSpecifier(tok))
			Decl();
		else {
			stmt = Stmt();
			if (!list)
				list = NodeListAlloc(stmt);
			else
				NodeListAppend(list, stmt);
		}
	}

	if (token == EOF) Fatal("missing } in compound statement");

	LeaveScope();

	lbrace->kids[LEFT] = list;

	if (!Expect('}'))
		return FALSE;
	return lbrace;
}

PRIVATE PNODE Stmt() {

	PNODE   st;
	PNODE   exp;
	PNODE   stprev;
	PBLOCK  bb;
	PBLOCK  brk;
	PSYMBOL sym;
	PSTRING label;
	COORD   coord;

	switch (token) {

	case K_IF:

		Next();
		return IfStmt();

	case K_WHILE:

		Next();
		return WhileStmt();

	case K_DO:

		Next();
		return DoStmt();

	case K_FOR:

		Next();
		return ForStmt();

	case K_SWITCH:

		Next();
		return SwtchStmt();

	case '{':

		return CompoundStmt();

	case K_RETURN:

		Next();
		exp = Expr();
		if (!Expect(';'))
			return FALSE;

		coord = tok.coord;

		st = Return(exp);
		st->coord = coord;
		return st;

	case K_CONTINUE:

		Next();
		if (!Expect(';'))
			return FALSE;

		coord = tok.coord;

		st = Tree(CONTINUE, NULL, NULL, NULL);
		st->coord = coord;
		return st;

	case K_BREAK:

		Next();
		if (!Expect(';'))
			return FALSE;

		coord = tok.coord;

		st = Tree(BREAK, NULL, NULL, NULL);
		st->coord = coord;
		return st;

	case K_DEFAULT:

		Next();
		if (!Expect(':'))
			return FALSE;
		coord = tok.coord;
		st = Stmt();
		st = NodeDefault(st);
		st->coord = coord;
		return st;

	case K_CASE:

		Next();
		exp = Expr(); // FIXME: should be const_expr
		if (!Expect(':'))
			return FALSE;
		coord = tok.coord;
		st = Stmt();
		st = NodeCase(Eval(exp), st);
		st->coord = coord;
		return st;

	case K_GOTO:

		Next();
		if (tok.op != TK_IDENT)
			return FALSE;
		st = NodeGoto(tok.val.s);
		st->coord = tok.coord;
		Next();
		if (!Expect(';'))
			return FALSE;
		return st;

	case TK_IDENT:

		if (Peek1() == ':') {
			st = NodeLabel(tok.val.s);
			st->coord = tok.coord;
			Next();
			Next();
			return st;
		}

	default:

		//
		// ';'
		//

		if (Accept(';'))
			return NULL;

		//
		// expression ';'
		//

		st = Expr();
		if (!st) return NULL;
		st->coord = tok.coord;

		if (!Expect(';'))
			Fatal("Stmt: Expr-stmt no ;");

		return st;
	}
	return NULL;
}

// declarators ---

PRIVATE PTYPE DeclPointer(IN PTYPE base) {

	if (token != '*')
		return base;
	Next();
	return DeclPointer(Pointer(base));
}

PRIVATE PTYPE DeclaratorBack(IN PTYPE base) {

	PTYPE func;
	PTYPE arr;
	int   arrsize;

	while (token != TK_ERROR) {
		if (token != '[' && token != '(')
			break;
		//
		// array type
		//
		if (token == '[') {
			Next();
			if (Accept(']'))
				continue;
			arrsize = Eval(ParseConstant());
			if (arrsize < 0)
				Fatal("Array size must be > 0");
			if (!Expect(']'))
				Fatal("DirectDeclaratorBack: Syntax");

			arr = DeclaratorBack(base);
			base = Array(arr, arrsize * base->size, 0);
			base->arrsize = arrsize;
		}
		//
		// function type
		//
		else if (token == '(') {
			Next();
			func = ParameterDecl(Function(base, NULL, FALSE));
			if (!Expect(')')) Fatal("DirectDeclaratorBack: Syntax");
			base = func;
		}
	}
	return base;
}

PRIVATE PTYPE DirectDeclarator(IN PTYPE base, OUT PSTRING* name) {

	TYPE  stub;
	PTYPE type;
	PTYPE decl;

	if (token == TK_IDENT) {
		if (name)
			*name = tok.val.s;
		Next();
	}
	else if (token == '(') {
		Next();
		type = Declarator(&stub, name);
		if (!Expect(')')) Fatal("DirectDeclarator: Syntax error");
		decl = DeclaratorBack(base);
		type->type = decl;
		return type;
	}
	return DeclaratorBack(base);
}

PRIVATE PTYPE Declarator(IN PTYPE base, OUT PSTRING* name) {

	return DirectDeclarator(DeclPointer(base), name);
}

PRIVATE PTYPE AbstractDeclarator(IN PTYPE base) {

	return DirectDeclarator(DeclPointer(base), NULL);
}

PRIVATE PTYPE ParameterDecl (IN PTYPE func)  {

	PTYPE   base;
	PTYPE   type;
	PSTRING name;
	int     sclass;

	if (token == ')') return func;

	name = NULL;
	while (TRUE) {
		if (token == '{' || token == EOF) break;

		base = Specifier(&sclass);
		type = Declarator(base, &name);

		AddParam(func, name, type);

		if (Accept(',')) continue;
		break;
	}

	return func;
}

PRIVATE void Decl() {

	int      sclass;
	PTYPE    base;
	PTYPE    type;
	PSTRING  name;
	PSYMBOL  sym;
	PSYMBOL  argsym;
	PSYMBOL  argListHead;
	PPARAM   arg;
	PNODE    node;

	type = NULL;
	sym = NULL;

	base = Specifier(&sclass);
	if (base) {
		if (token == ';') {
			Next();
			return;
		}
		while (token != TK_ERROR) {

			type = Declarator(base, &name);
			sym = Lookup(idents, name);

			if (sym == NULL || sym->level != level) {

				if (_function && sclass == SC_STATIC) {
					sym = InstallStatic(&idents, name);
				}
				else {
					sym = Install(&idents, name);
					if (!IsFunction(type) && level == GLOBALS)
						SymbolListAdd(&_program.gvars, sym);
				}
				sym->type = type;
				sym->sclass = sclass;
			}

			// if we didn't create this just now
			// and this symbol is at same scope level, throw error

			else if (sym->level == level)
				Error("'%A' redefined", name);

			if (level == GLOBALS && IsFunction(type))
				break;

			//
			// this is a local or global variable. Watch
			// for initializer and emit it if its global
			//

			if (token == '=') {
				if (sclass == SC_TYPEDEF)
					Error("= not allowed here");
				Next();

				sym->ast = Initializer();
				sym->ast->coord = tok.coord;
				sym->segment = DATA;
			}
			else
				sym->segment = BSS;

			if (level == GLOBALS) {
				if (!sym->sclass) sym->sclass = SC_EXTERN;
			}

			if (Accept(';')) break;
			if (!Expect(',')) break;
		}
	}

	//
	// if no base type, we assume Int type
	//
	else {
		base = IntType;
	}

	if (type && IsFunction(type) && level == GLOBALS && token == ';') {

		// this is a function declaration not a definition

		__debugbreak();

	}
	else if (type && IsFunction(type) && level == GLOBALS && token == '{') {

		//
		// this is a function definition. Enter into PARAMS scope and
		// process the argument list first.
		//

		_function = sym;
		EnterScope();

		argListHead = NULL;
		if (type->args && type->args == type->lastarg && type->args->type == VoidType)
			; // empty (void) list
		else {
			for (arg = type->args; arg; arg = arg->next) {
				if (!arg->name) {
					Error("No-named argument not allowed in function definition");
					continue;
				}
				if (Lookup(idents, arg->name))
					Error("Argument '%A' redeclared", arg->name);
				argsym = Install(&idents, arg->name);
				argsym->type = arg->type;
			}
		}

		sym->coord = tok.coord;

		node = CompoundStmt();

		// leave PARAMS scope. This puts us into GLOBAL scope
		LeaveScope();

		// fix up any forward references involving GOTO
		if (CheckForwardRefs()) {
			Error("forward references not resolved");
			return;
		}

		sym->segment = TEXT;
		sym->ast = node;

		SymbolListAdd(&_program.functs, sym);
	}

	// "int a() int b, int c; {}
	else if (type && IsFunction(type) && level == GLOBALS) {
		Fatal("K&R C style function definitions not yet supported");
	}
}

PRIVATE void Start() {
	while (token != TK_EOF)
		Decl();
}

PRIVATE void Init() {

	if (!externals) {
		externals = NewMap(FALSE, SYM_BUCKETS);
		constants = NewMap(FALSE, SYM_BUCKETS); // C strings
	}

	labels = NewMap(FALSE, SYM_BUCKETS);

	EnterScope(); // CONSTANTS
	EnterScope(); // LABELS
	EnterScope(); // GLOBALS
}

PUBLIC PPROGRAM Parse(void) {

	Init();
	Next();
	Start();
	return &_program;
}

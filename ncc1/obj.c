/************************************************************************
*
*	obj.c - PE/COFF object file
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

// NCC...

/*
	This implements the Portable Executable COFF target which is responsible for
	back-end section and symbol management, performing relocations, and writing
	the section data to a flat binary file.

	This is designed to be compatible with other NSDK tools and MSVC.
*/

#include <time.h>
#include <assert.h>
#include <string.h>
#include <malloc.h>
#include "ncc.h"

typedef struct COFF_FILE_HDR {
	uint16_t machine;
	uint16_t numberOfSections;
	uint32_t timeDateStamp;
	uint32_t pointerToSymbolTable;
	uint32_t numberOfSymbols;
	uint16_t sizeOfOptionalHeader;
	uint16_t characteristics;
}COFF_FILE_HDR, *PCOFF_FILE_HDR;

/* machine types. */
#define IMAGE_FILE_MACHINE_UNKNOWN 0
#define IMAGE_FILE_MACHINE_I386  0x14c
#define IMAGE_FILE_MACHINE_IA64 0x200
/* we don't support other machine types at this time. */

/* characteristics. */
#define IMAGE_FILE_RELOCS_STRIPPED 1
#define IMAGE_FILE_EXECUTABLE_IMAGE 2
#define IMAGE_FILE_LINE_NUMS_STRIPPED 4
#define IMAGE_FILE_LOCAL_SYMS_STRIPPED 8
#define IMAGE_FILE_AGGRESSIVE_WS_TRIM 0x10
#define IMAGE_FILE_LARGE_ADDRESS_AWARE 0x20
#define IMAGE_FILE_16BIT_MACHINE 0x40
#define IMAGE_FILE_BYTES_REVERSED_LO 0x80
#define IMAGE_FILE_32BIT_MACHINE 0x100
#define IMAGE_FILE_DEBUG_STRIPPED 0x200
#define IMAGE_FILE_REMOVABLE_RUN_FROM_SWAP 0x400
#define IMAGE_FILE_SYSTEM 0x1000
#define IMAGE_FILE_DLL 0x2000
#define IMAGE_FILE_UP_SYSTEM_ONLY 0x4000
#define IMAGE_FILE_BYTES_REVERSED_HI 0x8000

typedef struct COFF_SECTION_HDR {
	uint8_t name[8];
	uint32_t virtualSize;
	uint32_t virtualAddress;
	uint32_t sizeOfRawData;
	uint32_t pointerToRawData;
	uint32_t pointerToRelocations;
	uint32_t pointerToLineNumbers;
	uint16_t numberOfRelocations;
	uint16_t numberOfLineNumbers;
	uint32_t characteristics;
}COFF_SECTION_HDR, *PCOFF_SECTION_HDR;

/* characteristics. */
#define IMAGE_SCN_TYPE_NO_PAD 8
#define IMAGE_SCN_CNT_CODE 0x20
#define IMAGE_SCN_CNT_INITIALIZED_DATA 0x40
#define IMAGE_SCN_CNT_UNINITIALIZED_DATA 0x80
#define IMAGE_SCN_LNK_INFO 0x200
#define IMAGE_SCN_LNK_REMOVE 0x800
#define IMAGE_SCN_LNK_COMDAT 0x1000
#define IMAGE_SCN_ALIGN_1BYTES 0x100000
#define IMAGE_SCN_ALIGN_2BYTES 0x200000
#define IMAGE_SCN_ALIGN_4BYTES 0x300000
#define IMAGE_SCN_ALIGN_8BYTES 0x400000
#define IMAGE_SCN_ALIGN_16BYTES 0x500000
#define IMAGE_SCN_ALIGN_32BYTES 0x600000
#define IMAGE_SCN_ALIGN_64BYTES 0x700000
#define IMAGE_SCN_ALIGN_128BYTES 0x800000
#define IMAGE_SCN_ALIGN_256BYTES 0x900000
#define IMAGE_SCN_ALIGN_512BYTES 0xa00000
#define IMAGE_SCN_ALIGN_1024BYTES 0xb00000
#define IMAGE_SCN_ALIGN_2048BYTES 0xc00000
#define IMAGE_SCN_ALIGN_4096BYTES 0xd00000
#define IMAGE_SCN_ALIGN_8192BYTES 0xe00000
#define IMAGE_SCN_LNK_NRELOC_OVFL 0x1000000
#define IMAGE_SCN_MEM_DISCARDABLE 0x2000000
#define IMAGE_SCN_MEM_NOT_CACHED 0x4000000
#define IMAGE_SCN_MEM_NOT_PAGED 0x8000000
#define IMAGE_SCN_MEM_SHARED 0x10000000
#define IMAGE_SCN_MEM_EXECUTE 0x20000000
#define IMAGE_SCN_MEM_READ 0x40000000
#define IMAGE_SCN_MEM_WRITE 0x80000000

/* relocation types. */

/* absolute */
#define IMAGE_REL_I386_ABSOLUTE 0

/* 32 bit target VA */
#define IMAGE_REL_I386_DIR32 6

/* 32 bit target RVA */
#define IMAGE_REL_I386_DIR32NB 7

/* 16 bit section index of the section that contains the target. Used
for debugging information */
#define IMAGE_REL_I386_SECTION 0xa

/* 32 bit offset of the target from the beginning of its section.
Used for debugging information and static thread local storage */
#define IMAGE_REL_I386_SECREL 0xb

/* 32 bit relative displacement to the target. This supports
x86 relative branch and call instructions */
#define IMAGE_REL_I386_REL32 0x14

/* symbol number values. */
#define IMAGE_SYM_UNDEFINED 0
#define IMAGE_SYM_ABSOLUTE -1
#define IMAGE_SYM_DEBUG -2

/* complex type. */
#define IMAGE_SYM_DTYPE_NULL 0
#define IMAGE_SYM_DTYPE_POINTER 1
#define IMAGE_SYM_DTYPE_FUNCTION 2
#define IMAGE_SYM_DTYPE_ARRAY 3

/* storage class values. */
#define IMAGE_SYM_CLASS_END_OF_FUNCTION -1
#define IMAGE_SYM_CLASS_NULL 0
#define IMAGE_SYM_CLASS_AUTOMATIC 1
#define IMAGE_SYM_CLASS_EXTERNAL 2
#define IMAGE_SYM_CLASS_STATIC 3
#define IMAGE_SYM_CLASS_REGISTER 4
#define IMAGE_SYM_CLASS_EXTERNAL_DEF 5
#define IMAGE_SYM_CLASS_LABEL 6
#define IMAGE_SYM_CLASS_UNDEFINED_LABEL 7
#define IMAGE_SYM_CLASS_MEMBER_OF_STRUCT 8
#define IMAGE_SYM_CLASS_ARGUMENT 9
#define IMAGE_SYM_CLASS_STRUCT_TAG 10
#define IMAGE_SYM_CLASS_MEMBER_OF_UNION 11
#define IMAGE_SYM_CLASS_UNION_TAG 12
#define IMAGE_SYM_CLASS_TYPE_DEFINITION 13
#define IMAGE_SYM_CLASS_UNDEFINED_STATIC 14
#define IMAGE_SYM_CLASS_ENUM_TAG 15
#define IMAGE_SYM_CLASS_MEMBER_OF_ENUM 16
#define IMAGE_SYM_CLASS_REGISTER_PARAM 17
#define IMAGE_SYM_CLASS_BIT_FIELD 18
#define IMAGE_SYM_CLASS_BLOCK 100
#define IMAGE_SYM_CLASS_FUNCTION 101
#define IMAGE_SYM_CLASS_END_OF_STRUCT 102
#define IMAGE_SYM_CLASS_FILE 103
#define IMAGE_SYM_CLASS_SECTION 104
#define IMAGE_SYM_CLASS_WEAK_EXTERNAL 105

typedef struct COFF_RELOC {
	uint32_t virtualAddress;
	uint32_t symbolTableIndex;
	uint16_t type;
}COFF_RELOC, *PCOFF_RELOC;

typedef struct COFF_RELOC_LIST* PCOFF_RELOC_LIST;
typedef struct COFF_RELOC_LIST {
	PCOFF_RELOC       sym;
	PCOFF_RELOC_LIST  next;
}COFF_RELOC_LIST, *PCOFF_RELOC_LIST;

typedef struct COFF_SYMBOL_OFFSET {
	unsigned long zero;
	unsigned long offset;
}COFF_SYMBOL_OFFSET;

typedef struct COFF_SYMBOL* PCOFF_SYMBOL;
typedef struct COFF_SYMBOL {
	union {
		char shortname[8];
		COFF_SYMBOL_OFFSET e;
	}u;
	int32_t  value;
	int16_t  section;
	int16_t  type;
	int8_t   storageClass;
	int8_t   numAuxSymbols;
	//
	// management:
	//
	index_t  index;
	// unique segment ID - NextSegmentIndex()
	unsigned int seg;
}COFF_SYMBOL, *PCOFF_SYMBOL;

typedef struct COFF_SYMBOL_AUX_FUNCTION {
	uint32_t tagIndex;
	uint32_t totalSize;
	uint32_t pointerToLineNumber;
	uint32_t pointerToNextFunction;
	uint16_t unused;
}COFF_SYMBOL_AUX_FUNCTION, *PCOFF_SYMBOL_AUX_FUNCTION;

typedef struct COFF_SYMBOL_AUX_BF_EF {
	uint32_t  unused;
	uint16_t  lineNumber;
	uint8_t   unused2[6];
	uint32_t  pointerToNextFunction; // SymbolAuxBF only.
	uint16_t  unused3;
}COFF_SYMBOL_AUX_BF, COFF_SYMBOL_AUX_EF;

typedef struct COFF_SYMBOL_AUX_WEAK_EXTERNAL {
	uint32_t  tagIndex;
	uint32_t  characteristics;
	uint8_t   unused[10];
}COFF_SYMBOL_AUX_WEAK_EXTERNAL, *PCOFF_SYMBOL_AUX_WEAK_EXTERNAL;

typedef struct COFF_SYMBOL_AUX_FILE {
	char      fileName[18];
}COFF_SYMBOL_AUX_FILE, *PCOFF_SYMBOL_AUX_FILE;

typedef struct COFF_SYMBOL_AUX_SECTION_DEF {
	uint32_t  length;
	uint16_t  numberOfRelocations;
	uint16_t  numberOfLineNumbers;
	uint32_t  checksum;
	uint16_t  number;  // one based index into section table. Used when COMDAT selection = 5.
	uint8_t   selection; // COMDAT selection number.
	uint8_t   unused[3];
}COFF_SYMBOL_AUX_SECTION_DEF, *PCOFF_SYMBOL_AUX_SECTION_DEF;

typedef struct COFF_SYMBOL_LIST* PCOFF_SYMBOL_LIST;
typedef struct COFF_SYMBOL_LIST {
	PCOFF_SYMBOL me; // can be any SYMBOL type
	PCOFF_SYMBOL_LIST next;
}COFF_SYMBOL_LIST, *PCOFF_SYMBOL_LIST;

#define COFF_SECTION_NAMELEN 8

typedef struct COFF_SECTION {
	char             magic[8];
	char             name[COFF_SECTION_NAMELEN + 1];
	int              index;
	PBUFFER          data;
	PCOFF_RELOC_LIST relocs;
	unsigned         numRelocs;
	PSYMBOL_LIST     symbolEntry;
	int32_t          flags;
	int32_t          pos;
	int32_t          relpos;
	int              symindex;
	unsigned long    seg;
}COFF_SECTION, *PCOFF_SECTION;

typedef struct COFF_SECTION_LIST* PCOFF_SECTION_LIST;
typedef struct COFF_SECTION_LIST {
	PCOFF_SECTION      sym;
	PCOFF_SECTION_LIST next;
}COFF_SECTION_LIST, *PCOFF_SECTION_LIST;

typedef struct COFF_STRINGTAB_HDR {
	uint32_t totalSize;
}COFF_STRINGTAB_HDR;

/* default section flags. */
#define TEXT_FLAGS  (IMAGE_SCN_CNT_CODE|IMAGE_SCN_ALIGN_16BYTES|IMAGE_SCN_MEM_EXECUTE|IMAGE_SCN_MEM_READ)
#define DATA_FLAGS  (IMAGE_SCN_CNT_INITIALIZED_DATA|IMAGE_SCN_ALIGN_4BYTES|IMAGE_SCN_MEM_READ|IMAGE_SCN_MEM_WRITE)
#define BSS_FLAGS   (IMAGE_SCN_CNT_UNINITIALIZED_DATA|IMAGE_SCN_ALIGN_4BYTES|IMAGE_SCN_MEM_READ|IMAGE_SCN_MEM_WRITE)
#define RDATA_FLAGS (IMAGE_SCN_CNT_INITIALIZED_DATA|IMAGE_SCN_ALIGN_8BYTES|IMAGE_SCN_MEM_READ)
#define PDATA_FLAGS (IMAGE_SCN_CNT_INITIALIZED_DATA|IMAGE_SCN_ALIGN_4BYTES|IMAGE_SCN_MEM_READ)
#define XDATA_FLAGS (IMAGE_SCN_CNT_INITIALIZED_DATA|IMAGE_SCN_ALIGN_8BYTES|IMAGE_SCN_MEM_READ)
#define INFO_FLAGS  (IMAGE_SCN_ALIGN_1BYTES|IMAGE_SCN_LNK_INFO|IMAGE_SCN_LNK_REMOVE)

//
//
//

PCOFF_SECTION_LIST _sections;
PCOFF_SYMBOL_LIST  _symbols;
unsigned           _sectionCount;
unsigned           _symbolCount;
PCOFF_SYMBOL_LIST  _lastSymbolEntry;
PCOFF_SECTION      _currentSection;
PBUFFER            _stringTable;

PRIVATE offset_t AddString(IN char* s) {

	offset_t r;

	if (!_stringTable)
		_stringTable = NewBuffer();
	
	r = GetBufferLength(_stringTable);
	BufferAppend(_stringTable, s, strlen(s) + 1);

	// the string table starts with CoffStringTableHeader,
	// so we adjust for it here.
	return r + sizeof(COFF_STRINGTAB_HDR);
}

PRIVATE unsigned NextSymbolIndex(void) {

	static _symIndex = 0;
	return _symIndex++;
}

PRIVATE PCOFF_SYMBOL NewSymbol(void) {

	PCOFF_SYMBOL      sym;
	PCOFF_SYMBOL_LIST entry;

	sym = calloc(1, sizeof(COFF_SYMBOL));
	entry = calloc(1, sizeof(COFF_SYMBOL_LIST));
	sym->index = NextSymbolIndex();

	if (!_symbols) {
		_symbols = entry;
		_lastSymbolEntry = entry;
	}
	else{
		_lastSymbolEntry->next = entry;
		_lastSymbolEntry = entry;
	}
	entry->me = sym;
	_symbolCount++;
	return sym;
}

PRIVATE PCOFF_SYMBOL_LIST GetSymbolByName(IN char* name) {

	PCOFF_SYMBOL_LIST symbolEntry;
	PCOFF_SYMBOL  symbol;
	char*         realname;
	int           numAuxSymbols;
	int           maxlen;

	for (symbolEntry = _symbols; symbolEntry; symbolEntry = symbolEntry->next) {

		symbol = symbolEntry->me;

		if (symbol->u.e.zero == 0) {
			if (!_stringTable)
				Fatal("*** Bugcheck: obj.c: GetSymbolByName: Long symbol name with no string table");
			realname = &_stringTable->data[symbol->u.e.offset - sizeof(COFF_STRINGTAB_HDR)];
			maxlen = strlen(realname);
		}
		else {
			realname = symbol->u.shortname; /// NOT 0 terminated!!
			maxlen = 8;
		}

		if (strnlen(realname, maxlen) == strnlen(name, maxlen) && strncmp(realname, name, maxlen) == 0)
			return symbolEntry;

		numAuxSymbols = symbol->numAuxSymbols;
		while (numAuxSymbols-- > 0)
			symbolEntry = symbolEntry->next;
	}
	return NULL;
}

PRIVATE PCOFF_SYMBOL NewFileSymbol(IN PSTRING filename) {

	PCOFF_SYMBOL_AUX_FILE file;
	PCOFF_SYMBOL          symbol;
	char                  filepad[18];

	memcpy(filepad, filename->val, 18);
	filepad[17] = '\0';

	symbol = NewSymbol();
	file = (PCOFF_SYMBOL_AUX_FILE) NewSymbol();

	strcpy(symbol->u.shortname, ".file");
	symbol->storageClass = IMAGE_SYM_CLASS_FILE;
	symbol->numAuxSymbols = 1;

	strcpy(file->fileName, filepad);
	return symbol;
}

PRIVATE PCOFF_SYMBOL NewSectionSymbol(IN PCOFF_SECTION section){

	PCOFF_SYMBOL                 symbol;
	PCOFF_SYMBOL_AUX_SECTION_DEF sectionDef;
	char                 sectionPadded[18];

	memcpy(sectionPadded, section->name, 18);
	sectionPadded[17] = '\0';

	symbol = NewSymbol();
	sectionDef = (PCOFF_SYMBOL_AUX_SECTION_DEF) NewSymbol();

	strcpy(symbol->u.shortname, sectionPadded);
	symbol->section = section->index;
	symbol->storageClass = IMAGE_SYM_CLASS_STATIC;
	symbol->numAuxSymbols = 1;

	sectionDef->length = GetBufferLength(section->data);
	sectionDef->numberOfRelocations = section->numRelocs;
	section->symindex = symbol->index;
	return symbol;
}

PRIVATE PCOFF_SYMBOL NewAbsolutSymbol(void) {

	PCOFF_SYMBOL s;
	
	s = NewSymbol();
	strcpy(s->u.shortname, ".absolut");
	s->section = IMAGE_SYM_ABSOLUTE;
	s->seg = s->section;
	s->storageClass = IMAGE_SYM_CLASS_STATIC;
	return s;
}

PRIVATE int NextSectionIndex(void) {

	static int index = 1;
	return index++;
}

PRIVATE void AppendSection(IN PCOFF_SECTION section) {

	PCOFF_SECTION_LIST entry;
	PCOFF_SECTION_LIST newEntry;

	newEntry = calloc(1, sizeof(COFF_SECTION_LIST));
	newEntry->sym = section;
	if (!_sections) {
		_sections = newEntry;
		return;
	}
	for (entry = _sections; entry->next; entry = entry->next)
		;
	entry->next = newEntry;
}

PRIVATE PCOFF_SECTION GetSectionByName(IN char* name) {

	PCOFF_SECTION section;
	PCOFF_SECTION_LIST entry;

	if (strlen(name) > COFF_SECTION_NAMELEN)
		Fatal("*** bugcheck: obj.c: GetSectionByName: name length > 8!");
	for (entry = _sections; entry; entry = entry->next)
		if (!strcmp(entry->sym->name, name))
			return entry->sym;
	return NULL;
}

PRIVATE PCOFF_SECTION NewSection(IN char* name, IN uint32_t flags) {

	PCOFF_SECTION section;

	section = calloc(1, sizeof(COFF_SECTION));
	if (flags != BSS_FLAGS)
		section->data = NewBuffer();
	section->index = NextSectionIndex();
	strncpy(section->name, name, 8);
	section->name[8] = '\0';
	section->flags = flags;
	AppendSection(section);
	_sectionCount++;
	return section;
}

PRIVATE void SectionWrite(IN PCOFF_SECTION s, IN uint8_t* data, IN uint32_t length) {

	BufferAppend(s->data, data, length);
}

PRIVATE PCOFF_SECTION GetSectionBySeg(IN int segSymIndex) {

	PCOFF_SECTION_LIST entry;

	for (entry = _sections; entry; entry = entry->next) {
		if (entry->sym->symindex == segSymIndex)
			return entry->sym;
	}
	return NULL;
}

PRIVATE unsigned GetRelocationCount(IN PCOFF_SECTION section) {

	PCOFF_RELOC_LIST entry;
	unsigned         count;

	count = 0;
	for (entry = section->relocs; entry; entry = entry->next)
		count++;
	return count;
}

PRIVATE void AppendRelocation(IN PCOFF_SECTION section, IN PCOFF_RELOC reloc) {

	PCOFF_RELOC_LIST entry;
	PCOFF_RELOC_LIST newEntry;

	newEntry = calloc(1, sizeof(COFF_RELOC_LIST));
	newEntry->sym = reloc;
	section->numRelocs++;
	if (!section->relocs) {
		section->relocs = newEntry;
		return;
	}
	for (entry = section->relocs; entry->next; entry = entry->next)
		;
	entry->next = newEntry;
}

PRIVATE PCOFF_RELOC NewRelocation(IN PCOFF_SECTION section) {

	PCOFF_RELOC reloc;

	reloc = calloc(1, sizeof(COFF_RELOC));
	AppendRelocation(section, reloc);
	return reloc;
}

PRIVATE void AddRelocation(IN PCOFF_SECTION section, IN long symbolTableIndex, IN int16_t type) {

	PCOFF_RELOC reloc;

	reloc = NewRelocation(section);
	reloc->type = type;
	reloc->virtualAddress = GetBufferLength(section->data);
	reloc->symbolTableIndex = symbolTableIndex;
}

/* writes to file ------------------- */

PRIVATE void WriteStringTable(IN FILE* file) {

	COFF_STRINGTAB_HDR sth;

	/*
	PE/COFF Spec 5.6 -- At the beginning of the COFF string table are 4
	bytes containing the total size(in bytes) of the rest of the string
	table. This size includes the size field itself, so that the value
	in this location would be 4 if no strings were present.
	*/
	sth.totalSize = sizeof(COFF_STRINGTAB_HDR);
	if (_stringTable)
		sth.totalSize += GetBufferLength(_stringTable);

	fwrite(&sth, sizeof(COFF_STRINGTAB_HDR), 1, file);
	if (_stringTable)
		fwrite(GetBufferData(_stringTable), GetBufferLength(_stringTable), 1, file);
}

PRIVATE void WriteSymbol(IN FILE* file, IN PCOFF_SYMBOL symbol) {

	fwrite(symbol->u.shortname, 8, 1, file);
	fwrite(&symbol->value, sizeof(int32_t), 1, file);
	fwrite(&symbol->section, sizeof(int16_t), 1, file);
	fwrite(&symbol->type, sizeof(int16_t), 1, file);
	fwrite(&symbol->storageClass, 1, 1, file);
	fwrite(&symbol->numAuxSymbols, 1, 1, file);
}

PRIVATE void WriteSymbolTable(IN FILE* file) {

	PCOFF_SYMBOL_LIST entry;

	for (entry = _symbols; entry; entry = entry->next)
		WriteSymbol(file, entry->me);
}

PRIVATE uint32_t WriteSectionHeader(IN FILE* file, IN PCOFF_SECTION section) {

	COFF_SECTION_HDR sh;

	memset(&sh, 0, sizeof(COFF_SECTION_HDR));
	memcpy(sh.name, section->name, 8);
	sh.pointerToRawData = section->pos;
	sh.pointerToRelocations = section->relpos;
	sh.sizeOfRawData = GetBufferLength(section->data);
	sh.numberOfRelocations = GetRelocationCount(section);
	sh.characteristics = section->flags;

	fwrite(&sh, sizeof(COFF_SECTION_HDR), 1, file);

	return sh.sizeOfRawData;
}

PRIVATE void WriteSectionHeaders(IN FILE* file) {

	PCOFF_SECTION_LIST entry;

	for (entry = _sections; entry; entry = entry->next)
		WriteSectionHeader(file, entry->sym);
}

PRIVATE void WriteFileHeader(FILE* file, IN uint32_t symbolTablePointer) {

	COFF_FILE_HDR fh;

	memset(&fh, 0, sizeof(COFF_FILE_HDR));
	fh.machine = IMAGE_FILE_MACHINE_I386;
	fh.numberOfSections = _sectionCount;
	fh.timeDateStamp = (uint32_t)time(NULL);
	fh.pointerToSymbolTable = symbolTablePointer;
	fh.numberOfSymbols = _symbolCount;
	fh.sizeOfOptionalHeader = 0;
	fh.characteristics = 0x104;

	fwrite(&fh, sizeof(COFF_FILE_HDR), 1, file);
}

PRIVATE void WriteRelocs(IN FILE* file, IN PCOFF_SECTION section) {

	PCOFF_RELOC_LIST entry;

	for (entry = section->relocs; entry; entry = entry->next)
		fwrite(entry->sym, sizeof(COFF_RELOC), 1, file);
}

PRIVATE void WriteSections(IN FILE* file) {

	PCOFF_SECTION_LIST entry;
	PCOFF_SECTION      section;

	for (entry = _sections; entry; entry = entry->next) {
		section = entry->sym;
		fwrite(GetBufferData(section->data), GetBufferLength(section->data), 1, file);
		WriteRelocs(file, section);
	}
}

PRIVATE offset_t GetSymbolTableOffset(void) {

	offset_t           offset;
	PCOFF_SECTION_LIST sectionEntry;
	PCOFF_SECTION      section;

	/*
	the symbol table is right after the section data. So
	file header + (section header * numberOfSections) + (length_of_each_section)
	*/
	offset = sizeof(COFF_FILE_HDR) + (sizeof(COFF_SECTION_HDR) * _sectionCount);
	for (sectionEntry = _sections; sectionEntry; sectionEntry = sectionEntry->next) {
		section = sectionEntry->sym;
		if (section->data)
			offset += GetBufferLength(section->data);
		offset += section->numRelocs * sizeof(COFF_RELOC);
	}
	return offset;
}

PRIVATE void UpdateSectionOffsets(void) {

	offset_t           offset;
	PCOFF_SECTION_LIST sectionEntry;
	PCOFF_SECTION      section;

	// the sections starts here.
	offset = sizeof(COFF_FILE_HDR) + (sizeof(COFF_SECTION_HDR) * _sectionCount);

	// go through each section and update the section record
	// to point to the correct file offset.

	for (sectionEntry = _sections; sectionEntry; sectionEntry = sectionEntry->next) {

		section = sectionEntry->sym;

		section->pos = 0;
		section->relpos = 0;

		if (section->data) {

			// adjust the segment file position.
			section->pos = offset;
			offset += GetBufferLength(section->data);

			// if there is more then one relocation, then
			// adjust the relpos to point to the relocation table
			// right after the section data:
			if (section->relocs)
				section->relpos = offset;
		}
		offset += section->numRelocs * sizeof(COFF_RELOC);
	}
}

PRIVATE void UpdateSectionSymbol(IN PCOFF_SECTION section) {

	PCOFF_SYMBOL_LIST symbolEntry;
	PCOFF_SYMBOL_AUX_SECTION_DEF sectionDef;
	PCOFF_SYMBOL symbol;

	symbolEntry = GetSymbolByName(section->name);
	symbol = symbolEntry->me;
	sectionDef = (PCOFF_SYMBOL_AUX_SECTION_DEF)symbolEntry->next->me;

	sectionDef->length = GetBufferLength(section->data);
	sectionDef->numberOfRelocations = section->numRelocs;
	symbol->section = section->index;
}

PRIVATE void UpdateSectionSymbols(void) {

	PCOFF_SECTION_LIST sectionEntry;
	PCOFF_SECTION      section;

	for (sectionEntry = _sections; sectionEntry; sectionEntry = sectionEntry->next) {
		section = sectionEntry->sym;
		if (section) UpdateSectionSymbol(section);
	}
}

/**
*	OutCOFF
*
*	Description : Outputs COFF file
*/
PUBLIC void OutCOFF(IN PSTRING outfile) {

	offset_t symtable;
	FILE*    out;

	out = fopen(outfile->val, "wb");

	UpdateSectionOffsets();
	UpdateSectionSymbols();

	symtable = GetSymbolTableOffset();

	WriteFileHeader(out, symtable);
	WriteSectionHeaders(out);
	WriteSections(out);
	WriteSymbolTable(out);
	WriteStringTable(out);

	fclose(out);
}

/**
*	InitCOFF
*
*	Description : Initializes COFF/OBJ writer
*/
PUBLIC void InitCOFF(IN PSTRING src) {

	_currentSection = NewSection(".text", TEXT_FLAGS);

	NewAbsolutSymbol();
	NewFileSymbol(src);
	NewSectionSymbol(_currentSection);
}

/**
*	WriteCOFF
*
*	Description : Write data to the sepecified section. When type is ADDRESS or
*		REL, a relocation will be created in "segSymIndex" referencing "symIndex".
*/
PUBLIC void WriteCOFF(
	IN int segSymIndex,
	IN uint8_t* data,
	IN OUTPUT type,
	IN size_t size,
	IN int symIndex) {

	PCOFF_SECTION section;
	uint8_t       zero[4];

	if (segSymIndex == ABS_SEG) return;
	memset(zero, 0, 4);

	section = GetSectionBySeg(segSymIndex);

	switch (type) {
	case WRITE_RAWDATA:
		SectionWrite(section, data, size);
		break;
	case WRITE_ADDRESS:
		if (size != 4)
			Error("PE/COFF does not support non-32 bit relocations");
		else
			AddRelocation(section, symIndex, IMAGE_REL_I386_DIR32);
		SectionWrite(section, zero, 4);
		break;
	case WRITE_REL1ADR:
		Error("PE/COFF does not support non-32 bit relative relocations");
		SectionWrite(section, data, 1); /* write something */
		break;
	case WRITE_REL2ADR:
		Error("PE/COFF does not support non-32 bit relative relocations");
		SectionWrite(section, data, 2); /* write something */
		break;
	case WRITE_REL4ADR:
		AddRelocation(section, symIndex, IMAGE_REL_I386_REL32);
		SectionWrite(section, zero, 4);
	};
}

PRIVATE void AddSymbol(IN PSTRING name, IN int segment, IN offset_t offset, IN int flags) {

	PCOFF_SYMBOL      sym;
	PCOFF_SYMBOL_LIST entry;
	offset_t         off;

	off = (offset_t)-1;

	entry = GetSymbolByName(name->val);
	if (!entry) {

		sym = NewSymbol();
		if (strlen(name->val) > COFF_SECTION_NAMELEN) {
			off = AddString(name->val);
			sym->u.e.zero = 0;
			sym->u.e.offset = off;
		}
		else
			strcpy(sym->u.shortname, name->val);
	}

	sym->value = off;
	sym->type = 0;

	if (flags & SYM_EXTERN || flags & SYM_GLOBAL)
		sym->storageClass = IMAGE_SYM_CLASS_EXTERNAL;
	else
		sym->storageClass = IMAGE_SYM_CLASS_STATIC;
	if (segment == ABS_SEG)
		sym->section = 0xffff;
	else if (flags & SYM_EXTERN)
		sym->section = 0;
	else
		sym->section = 1; // Current Section Index...

	sym->seg = sym->section;
}

/************************************************************************
*
*	X86obj.c - Object code generator
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "ncc.h"

/*
	The Code Generator has the following functions:

		* Pass 0       -- Define Symbols to the Target.
		* Pass CODEGEN -- Write code to the target.

	The Code Generator translates instructions and directives during both passes
	to insure correct offsets are generated for Symbols for Pass 1. This involves
	two scans of the AST.

	The IA32 ISA defines instructions that have the following format.

	+-----------------------------------------------------------------------------------------------+
	| prefix    | REX prefix |     OPCode     | Mod R/M |   SIB  | Displacement   | Immediate       |
	| 1-4 bytes |   1 byte   | 1,2,or 3 bytes |  1 byte | 1 byte | 1,2, or 4 byte | 1,2, or 4 bytes |
	+-----------------------------------------------------------------------------------------------+
	optional     64bit only      required               32 bit only

	2 and 3 byte OPCodes
	---------------------------------

	1. Two byte opcodes always began with 0F.
		+----+--------+
		| 0F | opcode |
`		+----+--------+

	2. Three byte opcodes include a "fixed extraordinary prefix" byte.
	This is a prefix byte that is considered part of the opcode. They
	have the format:
	    +--------+----+--------+
		| prefix | 0F | opcode |
		+--------+----+--------+
	The prefix must be present, although it may be accompanied by other
	prefix bytes.

	OpCode rcode
	-----------------------------
	Instructions with the "rcode" modifier store a register ID
	into the low 3 bits of the opcode byte.

	Mod R/M
	7                           0
	+---+---+---+---+---+---+---+---+
	|  mod  |    reg    |     rm    |
	+---+---+---+---+---+---+---+---+
	2 bits   3 bits       3 bits

	OpCode Extension
	--------------------------------

	1. ModRM.reg contains an opcode extension for all instructions
	that support it.

	SIB
	7                           0
	+---+---+---+---+---+---+---+---+
	| scale |   index   |    base   |
	+---+---+---+---+---+---+---+---+
	2 bits   3 bits       3 bits

	The code generator has two parts:

	1. Translator

		This part translates PARSEDINSTR and PARSEDINSTROP
		into INSTR.

	2. Assembler

		Writes INSTR to the output target. The output target is expected to
		only write the code during the CODEGEN pass. This is also where the
		Listing output gets called if needed.
*/

PRIVATE BITS _mode = BITS32;
PRIVATE int  _pass = PASS0;

PRIVATE unsigned int _offset;
PRIVATE unsigned int _segment;

/**
*	ModR/M mode values. This describes address formats.
*	MemoryDisp32 and MemoryDisp16 have same value; its
*	meaning depends on operand size.
*/
typedef enum MODRM_MODE {
	ModRM_Memory = 0,                        /* [rm] */
	ModRM_MemoryDisp8,                       /* [rm + disp8] */
	ModRM_MemoryDisp16,                      /* [rm + disp16] */
	ModRM_MemoryDisp32 = ModRM_MemoryDisp16, /* [rm + disp32] */
	ModRM_Register                           /* reg */
}MODRM_MODE;

/**
*	All addressing modes.
*/
typedef enum ADDR_MODE_TYPE {
	Addr_Imm, /* [immediate] */
	/* 16 bit addressing modes. */
	Addr_Bx_Si, /* [bx+si] */
	Addr_Bx_Di,
	Addr_Bp_Si,
	Addr_Bp_Di,
	Addr_Si, /* [si] */
	Addr_Di,
	Addr_Bp,
	Addr_Bx,
	/* 32/64 bit addressing modes. */
	Addr_Eax,
	Addr_Ecx,
	Addr_Edx,
	Addr_Ebx,
	Addr_Ebp,
	Addr_Esp,
	Addr_Esi,
	Addr_Edi,
	Addr_Sp,
	Addr_SIB,/* [sib] */
	/* 64 bit only */
	Addr_Rax,
	Addr_Rcx,
	Addr_Rdx,
	Addr_Rbx,
	Addr_Rbp,
	Addr_Rsp,
	Addr_Rsi,
	Addr_Rdi,
	Addr_R8,
	Addr_R9,
	Addr_R10,
	Addr_R11,
	Addr_R12,
	Addr_R13,
	Addr_R14,
	Addr_R15,
	Addr_R8d,
	Addr_R9d,
	Addr_R10d,
	Addr_R11d,
	Addr_R12d,
	Addr_R13d,
	Addr_R14d,
	Addr_R15d,
	Addr_Invalid
}ADDR_MODE_TYPE;

/**
*	This describes ModRM 16 bit addressing modes.
*/
typedef enum MODRM_MODE16 {
	ModRM16_Bx_Si = 0, /* [bx+si] */
	ModRM16_Bx_Di,
	ModRM16_Bp_Si,
	ModRM16_Bp_Di,
	ModRM16_Si,
	ModRM16_Di,
	ModRM16_Bp,    /* mode 0: use imm, mode 1,2 uses bp. */
	ModRM16_Imm = ModRM16_Bp,
	ModRM16_Bx
}MODRM_MODE16;

/**
*	This describes ModRM 32 bit addressing modes.
*/
typedef enum MODRM_MODE32 {
	ModRM32_Eax = 0, /* [eax] */
	ModRM32_Ecx, /* [ecx] */
	ModRM32_Edx,
	ModRM32_Ebx,
	ModRM32_SIB, /* sib byte follows. */
	ModRM32_Ebp,
	ModRM32_Imm = ModRM32_Ebp,
	ModRM32_Esi,
	ModRM32_Edi
}MODRM_MODE32;

PRIVATE OPTYPE GetImmOp(IN PINSTR instr) {

	if (instr->src1 && (instr->op1) == OperandClassImm)
		return instr->op1;
	else if (instr->src2 && (instr->op2) == OperandClassImm)
		return instr->op2;
	return OperandInvalid;
}

PRIVATE OPTYPE GetMemOp(IN PINSTR instr) {

	if (instr->src1 && (instr->op1) == OperandClassMem)
		return instr->op1;
	else if (instr->src2 && (instr->op2) == OperandClassMem)
		return instr->op2;
	return OperandInvalid;
}

/**
*	GetAddressMode
*
*	Description : Given a memory operand, this procedure attempts to
*	              get the address mode that best matches it.
*
*	Input : mem - Pointer to a memory operand.
*
*	Output : Address mode type for the memory operand or Addr_Invalid
*	if the operand is not a memory type.
*/
PRIVATE ADDR_MODE_TYPE GetAddressMode(IN PINSTR mem) {

	OPTYPE memtype;
	
	memtype = GetMemOp(mem);

	if (GetOperandClass(memtype) != OperandClassMem)
		return Addr_Invalid;

	if (mem->addr.base && mem->addr.index) {
		if (mem->addr.base->type == RegBx && mem->addr.index->type == RegSi) return Addr_Bx_Si;
		else if (mem->addr.base->type == RegBx && mem->addr.index->type == RegDi) return Addr_Bx_Di;
		else if (mem->addr.base->type == RegBp && mem->addr.index->type == RegSi) return Addr_Bp_Si;
		else if (mem->addr.base->type == RegBp && mem->addr.index->type == RegDi) return Addr_Bp_Di;
		else return Addr_SIB;
	}
	else if (mem->addr.base) {
		if (mem->addr.base->type == RegSi) return Addr_Si;

		else if (mem->addr.base->type == RegDi) return Addr_Di;
		else if (mem->addr.base->type == RegBp) return Addr_Bp;
		else if (mem->addr.base->type == RegBx) return Addr_Bx;

		else if (mem->addr.base->type == RegEax) return Addr_Eax;
		else if (mem->addr.base->type == RegRax) return Addr_Rax;
		else if (mem->addr.base->type == RegR8d) return Addr_R8d;
		else if (mem->addr.base->type == RegR8) return Addr_R8;

		else if (mem->addr.base->type == RegEcx) return Addr_Ecx;
		else if (mem->addr.base->type == RegRcx) return Addr_Rcx;
		else if (mem->addr.base->type == RegR9d) return Addr_R9d;
		else if (mem->addr.base->type == RegR9) return Addr_R9;

		else if (mem->addr.base->type == RegEdx) return Addr_Edx;
		else if (mem->addr.base->type == RegRdx) return Addr_Rdx;
		else if (mem->addr.base->type == RegR10d) return Addr_R10d;
		else if (mem->addr.base->type == RegR10) return Addr_R10;

		else if (mem->addr.base->type == RegEbx) return Addr_Ebx;
		else if (mem->addr.base->type == RegRbx) return Addr_Rbx;
		else if (mem->addr.base->type == RegR11d) return Addr_R11d;
		else if (mem->addr.base->type == RegR11) return Addr_R11;

		else if (mem->addr.base->type == RegEbp) return Addr_Ebp;
		else if (mem->addr.base->type == RegRbp) return Addr_Rbp;
		else if (mem->addr.base->type == RegR13d) return Addr_R13d;
		else if (mem->addr.base->type == RegR13) return Addr_R13;

		else if (mem->addr.base->type == RegEsp) return Addr_Esp;
		else if (mem->addr.base->type == RegRsp) return Addr_Rsp;
		else if (mem->addr.base->type == RegR12d) return Addr_R12d;
		else if (mem->addr.base->type == RegR12) return Addr_R12;

		else if (mem->addr.base->type == RegEsi) return Addr_Esi;
		else if (mem->addr.base->type == RegRsi) return Addr_Rsi;
		else if (mem->addr.base->type == RegR14d) return Addr_R14d;
		else if (mem->addr.base->type == RegR14) return Addr_R14;

		else if (mem->addr.base->type == RegEdi) return Addr_Edi;
		else if (mem->addr.base->type == RegRdi) return Addr_Rdi;
		else if (mem->addr.base->type == RegR15d) return Addr_R15d;
		else if (mem->addr.base->type == RegR15) return Addr_R15;

		else {
			Fatal("GetAddressMode: Unknown mode type");
		}
	}
	return Addr_Imm;
}

/**
*	GetAddrOperationMode
*
*	Description : This procedure gets the Address Size for the specified
*	address mode.
*
*	Input : mode - Address mode type.
*
*	Output : Bits16, Bits32, Bits64 depending on the address mode type.
*/
PRIVATE BITS GetAddrOperationMode(IN ADDR_MODE_TYPE mode) {

	BITS m;

	switch (mode) {
	case Addr_Bx_Si:
	case Addr_Bx_Di:
	case Addr_Bp_Si:
	case Addr_Bp_Di:
	case Addr_Si:
	case Addr_Di:
	case Addr_Bp:
	case Addr_Bx:
		m = BITS16; /* 16 bit addressing mode. */
		break;
	case Addr_Imm:
		return _mode; /* [imm] */
	case Addr_Invalid:
		return _mode; /* not a memory operand. */
	case Addr_Eax:
	case Addr_Ecx:
	case Addr_Edx:
	case Addr_Ebx:
	case Addr_Ebp:
	case Addr_Esp:
	case Addr_Esi:
	case Addr_Edi:
	case Addr_Sp:
	case Addr_SIB: // FIXME: size is dependent on base reg
		m = BITS32;
		break;
	case Addr_R8d: /* extended 32 bit registers */
	case Addr_R9d:
	case Addr_R10d:
	case Addr_R11d:
	case Addr_R12d:
	case Addr_R13d:
	case Addr_R14d:
	case Addr_R15d:
		m = BITS32;
		break;
	default:
		m = BITS64;
	};
	return m;
}

PRIVATE uint16_t SwapBytes2(IN uint16_t in) {

	return ((in & 0xff00) >> 8) | ((in & 0xff) << 8);
}

PRIVATE void Write(
	IN offset_t offset,
	IN int32_t segSymIndex,
	IN uint8_t* data,
	IN OUTPUT type,
	IN size_t size,
	IN int32_t symIndex) {

//	WriteListing(data, type, size)

	WriteCOFF(segSymIndex, data, type, size, symIndex, PASS0);
}

PRIVATE size_t WritePrefix(IN PINSTR instr) {

	int c;

	for (c = 0; c < instr->prefixCount; c++)
		Write(_offset, 0, &instr->prefix[c], WRITE_RAWDATA, 1, 0);
	return c;
}

PRIVATE size_t WriteOpcode(IN PINSTR instr) {

	size_t   size;
	uint32_t data;
	int      opcode;

	size = GetOpcodeSize(instr->templ->opcode);
	opcode = instr->templ->opcode;

	if (size == 1) data = instr->templ->opcode;
	else if (size == 2) data = SwapBytes2(instr->templ->opcode);
	else if (size == 3) data = instr->templ->opcode;

	Write(_offset, 0, &data, WRITE_RAWDATA, size, 0);
	return size;
}

PRIVATE size_t WriteRex(IN PINSTR instr) {

	//
	// -  bits 7-4: 0100
	// W: bit 3: 0: Operand size determined by CS.d, 1 = 64 bit
	// R: bit 2: Extension of ModRM.reg
	// X: bit 1: Extension of SIB.index
	// B: bit 0: Extension of ModRM.rm, SIB.base, or Opcode.reg
	//
#define REX_RESBIT 0x40
#define REX_64BIT 8

	uint8_t     value;

	// set REX.W
	value = 0;
	if (instr->templ->numParms == 0 && (instr->modifier & ModSizeQword))
		value = REX_64BIT;
	if (instr->templ->numParms == 0 && GetOperandSize(instr->op2) == OperandSize64)
		value = REX_64BIT;
	if (instr->templ->numParms == 1 && GetOperandSize(instr->op2) == OperandSize64)
		value = REX_64BIT;

	// set REX.R and REX.X
	value |= (instr->modrm.reg >> 3) << 2;
	value |= (instr->sib.index >> 3) << 1;

	// set REX.B
	if (instr->modifier & ModSIB)
		value |= instr->sib.base >> 3;
	else if (instr->modifier & ModMODRM)
		value |= instr->modrm.regMem >> 3;
	else if (instr->modifier & ModEXT)
		value |= instr->templ->opcodeExt >> 3;
	if (value > 0) {
		value |= REX_RESBIT;
		Write(_offset, 0, &value, WRITE_RAWDATA, 1, 0);
		return 1;
	}
	return 0;
}

PRIVATE size_t WriteModRM(IN PINSTR instr) {

	PMODRM  modrm;
	uint8_t value;

	modrm = &instr->modrm;
	if (instr->modifier & ModMODRM) {
		value = ((modrm->mode & 0x3) << 6) | ((modrm->reg & 0x7) << 3) | (modrm->regMem & 0x7);
		Write(_offset, 0, &value, WRITE_RAWDATA, 1, 0);
		return 1;
	}
	return 0;
}

PRIVATE size_t WriteSIB(IN PINSTR instr) {

	PSIB    sib;
	uint8_t value;

	sib = &instr->sib;
	if (instr->modifier & ModSIB) {
		value = ((sib->scale & 0x3) << 6) | ((sib->index & 0x7) << 3) | (sib->base & 0x7);
		Write(_offset, 0, &value, WRITE_RAWDATA, 1, 0);
		return 1;
	}
	return 0;
}

PRIVATE size_t WriteDisp(IN PINSTR instr) {

	OPTYPE          mem;
	BITS            m;
	ADDR_MODE_TYPE  mode;
	OUTPUT          out;

	out = WRITE_RAWDATA;

	mem = GetMemOp(instr);
	if (!mem)
		return 0;

	/* if there is no displacement, nothing to do. */
	if (instr->addr.type == DispNone)
		return 0;

	mode = GetAddressMode(instr);
	m = GetAddrOperationMode(mode);

	/* if this displacement references a symbol, we need
	to generate a relocation. */
	if (instr->segto != ABS_SEG && instr->segto != NO_SEG)
		out = WRITE_ADDRESS;

	switch (instr->modrm.mode) {
	case ModRM_Memory:
		if (_mode == BITS16) {
			Write(_offset, instr->segto, &instr->addr.disp, out, 2, _segment);
			return 2;
		}
		else if (_mode == BITS32) {
			Write(_offset, instr->segto, &instr->addr.disp, out, 4, _segment);
			return 4;
		}
	case ModRM_MemoryDisp8:
		Write(_offset, instr->segto, &instr->addr.disp, out, 1, _segment);
		return 1;
		/*	case ModRM_MemoryDisp16: */
	case ModRM_MemoryDisp32:
		if (m == BITS16) {
			Write(_offset, instr->segto, &instr->addr.disp, out, 2, _segment);
			return 2;
		}
		else if (m == BITS32) {
			Write(_offset, instr->segto, &instr->addr.disp, out, 4, _segment);
			return 4;
		}
	}
	return 0;
}

PRIVATE size_t WriteImmPtr(IN PINSTR instr, IN OPTYPE imm) {

	OPSIZE s;
	OUTPUT out;

	s = GetOperandSize(imm);
	out = WRITE_RAWDATA;

	// if this references a symbol, we need to create a relocation.

	if (instr->segto != NO_SEG && instr->segto != ABS_SEG)
		out = WRITE_ADDRESS;

	switch (s) {
	case OperandSize16:
		Write(_offset, instr->segto, &instr->value, out, 2, _segment);
		Write(_offset, instr->segto, &instr->farPtrSeg, WRITE_RAWDATA, 2, _segment);
		return 4;
	case OperandSize32:
		Write(_offset, instr->segto, &instr->value, out, 4, _segment);
		Write(_offset, instr->segto, &instr->farPtrSeg, WRITE_RAWDATA, 2, _segment);
		return 6;
	};

	return 0;
}

PRIVATE size_t WriteImmRel(IN PINSTR instr, IN OPTYPE imm) {

	OPSIZE s;
	int size;
	int realaddr;

	// this is the relative value to write

	s = GetOperandSize(imm);
	if (s == OperandSize8) size = 1;
	else if (s == OperandSize16) size = 2;
	else size = 4;
	realaddr = instr->value - (_offset + size + 1);

	switch (s) {
	case OperandSize8:
		if (_segment == instr->segto || instr->segto == NO_SEG)
			Write(_offset, instr->segto, &realaddr, WRITE_RAWDATA, 1, _segment);
		else
			Write(_offset, instr->segto, &instr->value, WRITE_REL1ADR, 1, _segment);
		return 1;
	case OperandSize16:
		if (_segment == instr->segto || instr->segto == NO_SEG)
			Write(_offset, instr->segto, &realaddr, WRITE_RAWDATA, 2, _segment);
		else
			Write(_offset, instr->segto, &instr->value, WRITE_REL2ADR, 2, _segment);
		return 2;
	case OperandSize32:
		if (_segment == instr->segto || instr->segto == NO_SEG)
			Write(_offset, instr->segto, &realaddr, WRITE_RAWDATA, 4, _segment);
		else
			Write(_offset, instr->segto, &instr->value, WRITE_REL4ADR, 4, _segment);
		return 4;
	};

	return 0;
}

PRIVATE size_t WriteImm(IN PINSTR instr) {

	OPTYPE imm;
	size_t s;

	imm = GetImmOp(instr);

	if (GetOperandClass(imm) == OperandClassImm) {

		if ((GetOperandSubclass(imm) & OperandImmRel) == OperandImmRel)
			return WriteImmRel(instr, imm);
		else if ((GetOperandSubclass(imm) & OperandImmPtr) == OperandImmPtr)
			return WriteImmPtr(instr, imm);

		switch (GetOperandSize(imm)) {
		case OperandSize8: s = 1; break;
		case OperandSize16: s = 2; break;
		case OperandSize32: s = 4; break;
		case OperandSize64: s = 8; break;
		default:
			Fatal("*** BUGCHECK: WriteImm invalid operand size");
		};
	}

	// FIXME: for cases like mov [disp], imm that can have 2 relocs,
	// this segto may have to be per operand like the way NASM treats it.
	// not an issue at the moment as NCC does not generate this type of code

	if (instr->segto != NO_SEG && instr->segto != ABS_SEG)
		Write(_offset, instr->segto, &instr->value, WRITE_ADDRESS, 4, _segment);
	else
		Write(_offset, instr->segto, &instr->value, WRITE_RAWDATA, s, _segment);

	return s;
}

PRIVATE size_t Assemble(IN PINSTR instr) {

	size_t c = 0;

	c += WritePrefix(instr);
	if (_mode == BITS64)
		c += WriteRex(instr);
	c += WriteOpcode(instr);

	if ((instr->modifier & ModEmitOpcodeOnly) == 0) {
		c += WriteModRM(instr);
		c += WriteSIB(instr);
		c += WriteDisp(instr);
		c += WriteImm(instr);
	}
	return c;
}

//PUBLIC void GenerateCode(IN PXBLOCK code) {


//}

PUBLIC void GenerateData(IN PSYMBOL symboL) {

	// strings go into a .rdata section (new section for each string)

	// if its initialized to 0, it goes into .bss

	// if its not initialized at all, create the symbol but with
	// NO section number

	// if it is initialized, it of course goes into .data

	// write symbol info to .stabstr and .stab

}






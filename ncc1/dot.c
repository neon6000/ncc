/************************************************************************
*
*	dot.c - Target for DOT graphs
*
*   Copyright(c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <stdarg.h>
#include "ncc.h"

/*** PRIVATE Definitions ******************************************/

PRIVATE FILE* _outfile;

PRIVATE void Write(IN char* fmt, ...) {

	va_list args;

	if (!_outfile)
		return;

	va_start(args, fmt);
	vfprintf(_outfile, fmt, args);
}

PRIVATE char* Col(IN PNODE node) {

	switch (OPCODE(node->op)) {
	case QASGN:
	case QINDIR:
	case QJMP:
		return "white";
	default: return "lightblue";
	}
}

PRIVATE char* InsSize(IN PNODE node) {

	switch (OPSIZE(node->op)) {
	case 0: return "";
	case 1: return "1";
	case 2: return "2";
	case 4: return "4";
	case 8: return "8";
	};
	return "?";
}

PRIVATE char* Ins(IN PQUAD node) {

	switch (OPCODE(node->op)) {
	case QADD: return "+";
	case QSUB: return "-";
	case QMUL: return "*";
	case QDIV: return "/";
	case QMOD: return "%";
	case QNEG: return "-";
	case QBSHL: return "<<";
	case QBSHR: return ">>";
	case QBAND: return "&";
	case QBCMPL: return "~";
	case QBOR: return "|";
	case QBXOR: return "^";
	case QNOT: return "!";
	case QCALL: return "CALL";
	case QARG: return "ARG";
	case QRET: return "RET";
	case QADDRG:
	case QADDRL:
	case QADDRF: return "&";
	case QCVTI: return "CVTI";
	case QCVTF: return "CVTF";
	case QCVTU: return "CVTU";
	case QCVTP: return "CVTP";
	case ASM: return "ASM";
	case QINDIR: return "*";
	case SWTCH: return "SWTCH";
	case QCMP: return "CMP";
	case QLABEL: return "LABEL";
	case QLBRACE: return "{";
	case QRBRACE: return "}";
	case QCOORD: return "COORD";
	default: return "UNK";
	}
}

PRIVATE void WriteNode(IN PNODE node) {

	if (!node) return;

	switch (OPCODE(node->op)) {
	case QBLK:
		Write(" .L%i", node->block->name);
		break;
	case QCNST:
		if (IsInt(node->type))
			Write(" %i", node->val.i);
		else
			Write(" <CNST>");
		break;
	case QREGISTER:
		Write(" T%i", node->sym->tempname);
		break;
	default:
		if (node->sym)
			Write(" %s", node->sym->symbolic->val);
		else
			Write(" ?");
	};
}

PRIVATE void WriteList(IN PSYMBOL_LIST list) {

	PSYMBOL_LIST item;
	PSYMBOL sym;

	Write(" {");
	for (item = list; item; item = item->next) {
		sym = item->me;
		if (sym->tempname > 0)
			Write("T%i", sym->tempname);
		else if (sym)
			Write("%s", sym->symbolic->val);
		else
			Write("?");
		if (item->next)
			Write(", ");
	}
	Write("}");
}

PRIVATE void WriteSuccList(IN PQUAD quad) {

	PQUAD succ;
	PQUAD_LIST item;

	if (!quad->succlist) return;
	Write(" (");
	for (item = quad->succlist; item; item = item->next) {
		succ = item->me;
		Write("%i", succ->name);
		if (item->next)
			Write(",");
	}
	Write(")");
}

PRIVATE void WritePredList(IN PQUAD quad) {

	PQUAD pred;
	PQUAD_LIST item;

	if (!quad->predlist) return;
	Write(" (");
	for (item = quad->predlist; item; item = item->next) {
		pred = item->me;
		Write("%i", pred->name);
		if (pred)
			Write(",");
	}
	Write(")");
}

PRIVATE void WriteSym(IN PSYMBOL sym) {

	if (sym->name)
		Write("%s", sym->name->val);
	else
		Write("T%i", sym->tempname);
}

PRIVATE void VisitQuadBin(IN PQUAD quad) {

	if (quad->target) {
		WriteSym(quad->target);
		Write("=");
	}
	if (quad->src1) {
		if (quad->src1->var)
			WriteSym(quad->src1->var);
		else
			Write("%i", quad->src1->intLit);
	}
	Write(Ins(quad));
	if (quad->src2) {
		if (quad->src2->var)
			WriteSym(quad->src2->var);
		else
			Write("%i", quad->src2->intLit);
	}
}

PRIVATE void VisitQuadUnary(IN PQUAD quad) {

	if (quad->target) {
		if (OPCODE(quad->op) == QASGN)
			Write("[");
		WriteSym(quad->target);

		if (OPCODE(quad->op) == QASGN) {

			if (quad->index && quad->index->var)
				Write("+T%i", quad->index->var->tempname);

			Write("]");
		}
		Write("=");
	}

	// '=' is implied for MOV
	if (OPCODE(quad->op) != QMOV && OPCODE(quad->op) != QASGN)
		Write(Ins(quad));

	if (quad->src1) {
		if (quad->src1->var)
			WriteSym(quad->src1->var);
		else
			Write("%i", quad->src1->intLit);
	}
}

PRIVATE void VisitQuad(IN PQUAD quad) {

	if (OPCODE(quad->op) == QCOORD) {
		Write("coord %i\\l", quad->coord.y);
		return;
	}

	if (OPCODE(quad->op) == QLABEL) {
		Write("%s:\\l", quad->src1->stringlit->val);
		return;
	}
	if (OPCODE(quad->op) == QJMP) {
		if (quad->condCode == 0) {
			if (!quad->label->src1->stringlit)
				Write("goto <symbol> ??");
			else
				Write("goto %s", quad->label->src1->stringlit->val);
		}
		else {
			Write("if ");
			WriteSym(quad->src1->var);
			switch (quad->condCode) {
			case QEQ: Write(" == "); break;
			case QGT: Write(" > "); break;
			case QGE: Write(" >= "); break;
			case QLE: Write(" <= "); break;
			case QLT: Write(" < "); break;
			case QNE: Write(" != "); break;
			default: Write(" <?> "); break;
			};
			if (quad->src2->var)
				WriteSym(quad->src2->var);
			else
				Write("%i", quad->src2->intLit);
			Write(" goto %s", quad->label->src1->stringlit->val);
		}

		WriteList(quad->deflist);
		WriteList(quad->uselist);

		Write("\\l");
		return;
	}

	if (quad->src2)
		VisitQuadBin(quad);
	else
		VisitQuadUnary(quad);

//	WriteList(quad->deflist);
	WriteList(quad->uselist);

//	WriteList(quad->inlist);
	WriteList(quad->outlist);

	Write("\\l");
}

PRIVATE void VisitQuads(IN PQUAD_LIST quads) {

	PQUAD_LIST q;
	for (q = quads; q; q = q->next)
		VisitQuad(q->me);
}

PRIVATE void Visit(IN PBLOCK block, IN PSYMBOL func) {

	PBLOCK_LIST succ;

	if (!block) return;

	// basic blocks are grouped together
	Write("\n%s_block_%i [", func->name->val, block->name);

	Write("fontname = \"Consolas\"");
	Write(" color = \"gray\"");
	Write(" style = \"filled\"");
	Write(" shape = \"plaintext\"");
	Write(" label = \"");

	if (block->label)
		Write("%s\n", block->label->src1->stringlit->val);

	VisitQuads(block->quads);

	Write("\"];");

	// "next" edge between blocks

	if (block->next && block->next->name)
		Write("%s_block_%i -> %s_block_%i [weight=1, penwidth=3, color=\"black\"];",
			func->name->val,
			block->name,
			func->name->val,
			block->next->name);

	// successor edges

	for (succ = block->succlist; succ && succ->me; succ = succ->next)
		Write("%s_block_%i -> %s_block_%i [style=\"dashed\" weight=1, penwidth=3, color=\"red\"];",
			func->name->val,
			block->name,
			func->name->val,
			succ->me->name);
}

/*** PUBLIC Definitions ******************************************/

PUBLIC void EmitDOT(IN PSYMBOL sym) {

	PBLOCK bb;

	if (!IsFunction(sym->type))
		return;

	Write("\nsubgraph cluster_%s {", sym->symbolic->val);
	Write("color = \"blue\"");
	Write(" style = \"filled\"");
	Write(" label = \"%s\"", sym->symbolic->val);

	for (bb = sym->code; bb; bb = bb->next)
		Visit(bb, sym);

	Write("\n}");
}

PUBLIC void InitDOT(IN FILE* out) {

	_outfile = out;

	Write("# IR file generated by Neptune C Compiler\n");
	Write("digraph cfg{");
}

PUBLIC void FlushDOT(void) {

	Write("\n}");
}

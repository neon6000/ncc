/************************************************************************
*
*	stab.c - STABS debug support
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <stdio.h>
#include <stdarg.h>
#include "ncc.h"

/*** PRIVATE Defintiions ***********************************/

#define Out3(...) EmitAsm2(__LINE__, __VA_ARGS__)
#define Out2(...) EmitAsm2(__LINE__, "\t" __VA_ARGS__)
#define Out1(...) EmitAsm(__LINE__, __VA_ARGS__)
#define Out(...) EmitAsm(__LINE__, "\t" __VA_ARGS__)

PRIVATE void EmitAsm(unsigned line, char* fmt, ...) {

	va_list args;

	va_start(args, fmt);
	vfprintf(stdout, fmt, args);
	fprintf(stdout, "\n");
}

PRIVATE void EmitAsm2(unsigned line, char* fmt, ...) {

	va_list args;

	va_start(args, fmt);
	vfprintf(stdout, fmt, args);
}

// Current number of Types used to give each Type a unique ID.
PRIVATE unsigned ntypes;

PRIVATE void AsgnCode(IN PTYPE ty) {

	PFIELD field;

	if (ty->marked || ty->typeno)
		return;

	ty->marked = TRUE;
	switch (ty->kind) {
		// introduce new type "CONST" and "VOLATILE".
		// its child type is the actual type (like "const int");
		// case TYPE_CONST: case TYPE_VOLATILE: case TYPE_CONST+TYPE_VOLATILE:
		//		AsgnCode(ty->type);
		//      ty->typeno = ty->type->typeno;
		//		break;
	case TYPE_POINTER: case TYPE_FUNCTION: case TYPE_ARRAY:
		AsgnCode(ty->type);
		// fall through
	case TYPE_VOID: case TYPE_INT: case TYPE_UNSIGNED: case TYPE_FLOAT:
		break;
	case TYPE_STRUCT: case TYPE_UNION:
		for (field = ty->fields; field; field = field->next)
			AsgnCode(field->type);
		// fall through
	case TYPE_ENUM:
		if (ty->typeno == 0)
			ty->typeno = ++ntypes;
		//		if (lev > 0 && (*ty->u.sym->name < '0' || *ty->u.sym->name > '9'))
		//			dbxout(ty);
		break;
	default:
		Fatal("internal: x86: unknown type");
	};
}

PRIVATE void EmitType(IN PTYPE ty) {

	PFIELD   field;
	PNODE    expr;
	unsigned tn;

	tn = ty->typeno;
	if (IsConst(ty) || IsVolatile(ty)) {
		//		EmitType(ty->type);
		ty->written = TRUE;
		__debugbreak();
	}

	if (!tn) ty->typeno = tn = ++ntypes;
	Out3("%d", tn);
	if (ty->written) return;

	ty->written = TRUE;
	switch (ty->kind) {

	case TYPE_VOID:

		// void type references itself
		Out3("=%d", tn);
		break;

	case TYPE_INT:

		// char type is a subrange of itself
		// other signed int types are subranges of int type
		if (ty == CharType)
			Out3("=r%d;%d;%d;", tn, ty->limits.min.i, ty->limits.max.i);
		else
			Out3("=r1;%d;%d;", ty->limits.min.i, ty->limits.max.i);
		break;

	case TYPE_UNSIGNED:

		// char type is a subrange of itself
		// other unsigned int types are subranges of int type
		if (ty == CharType)
			Out3("=r%d;0;%u;", tn, ty->limits.max.i);
		else
			Out3("=r1;0;%u;", ty->limits.max.i);
		break;

	case TYPE_FLOAT:

		// float types are encoded a little different: in order
		// to differ from other types, the max of the range is 0.
		Out3("=r1;%d;0;", ty->size);
		break;

	case TYPE_POINTER:

		Out3("=*");
		EmitType(ty->type);
		break;

	case TYPE_FUNCTION:

		Out3("=f");
		EmitType(ty->type);
		break;

	case TYPE_ARRAY:

		// arrays are encoded with number of elements in int range
		if (ty->size && ty->type->size)
			Out3("=ar1;0;%d", ty->size / ty->type->size - 1);
		else
			Out3("=ar1;0;-1");
		EmitType(ty->type);
		break;

	case TYPE_STRUCT: case TYPE_UNION:

		if (!ty->defined)
			Out3("=x%c%s:", ty->kind == TYPE_STRUCT ? 's' : 'u', ty->sym->name->val);

		Out3("=%c%d", ty->kind == TYPE_STRUCT ? 's' : 'u', ty->size);
		for (field = ty->fields; field; field = field->next) {
			if (field->name)
				Out3("%s:", field->name->val);
			else
				Out3(":");
			EmitType(field->type);
			Out3(",%d,%d;", field->offset, field->type->size);
		}
		Out3(";");
		break;

	case TYPE_ENUM:

		Out3("=e");
		for (field = ty->fields; field; field = field->next) {
			// evaluate "expr" that goes here:
			expr = ty->sym->val.y;
			Out3("%s:%d", ty->name->val, 0);
		}

		Out3(";");
		break;

	default:
		Fatal("internal: x86: unknown type");
	};
}

PRIVATE void _OutType(IN PTYPE ty) {

	if (ty->written) return;
	Out3("stabs \"");
	if (ty->name && !(IsFunction(ty) || IsArray(ty) || IsPointer(ty)))
		Out3("%s", ty->name->val);
	Out3(":%c", IsStruct(ty) || IsEnum(ty) ? 'T' : 't');
	EmitType(ty);
	Out3("\", N_LSYM,0,0,0\n");
}

/*** PUBLIC Defintiions ***********************************/

PUBLIC unsigned OutTypeSTABS(IN PTYPE ty) {

	AsgnCode(ty);
	_OutType(ty);
	return ty->typeno;
}

PUBLIC void OutSymbolSTABS(IN PSYMBOL sym) {

	unsigned tn;
	char* code = "<sym?>";

	if (IsFunction(sym->type)) {
		Out3("stabs \"%s:%c%d\",N_FUNC,0,0,%s\n", sym->name->val,
			sym->sclass == SC_STATIC ? 'f' : 'F', OutTypeSTABS(sym->type),
			sym->symbolic->val);
		return;
	}

	tn = OutTypeSTABS(sym->type);

	if (sym->sclass == SC_STATIC) {
		Out3("stabs \"%s:%c%d\", %s, 0, 0, %s\n",
			sym->name->val, sym->level == GLOBALS ? 'S' : 'V',
			tn, sym->segment == BSS ? "N_LCSYM" : "N_STSYM",
			sym->symbolic->val);
		return;
	}
	else if (sym->sclass == SC_AUTO && sym->level == GLOBALS || sym->sclass == SC_EXTERN) {
		Out3("stabs \"%sG", sym->name->val);
		code = "N_GSYM";
	}
	else if (sym->sclass == SC_REGISTER) {
		return;
	}
	else if (sym->level == PARMS) {
		Out3("stabs \"%s:p", sym->name->val);
		code = "N_PSYM";
	}
	else if (sym->level >= LOCALS) {
		Out3("stabs \"%s:", sym->name->val);
		code = "N_LSYM";
	}
	else Fatal("x86: unknown symbol type");

	Out3("%d\", %s, 0, 0, %i\n", tn, code,
		sym->level >= PARMS && sym->sclass != SC_EXTERN ?
		sym->offset : 0);
}

/************************************************************************
*
*	dwarf.c - Debug format
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "ncc.h"

// points to IR. Use IR->nextFn
PLIST_ENTRY functs;

// http://www.dwarfstd.org/doc/DWARF4.pdf

// #define FUNC_BEGIN_LABEL  "LFB"
// #define FUNC_END_LABEL    "LFE"
// #define BLOCK_BEGIN_LABEL "LBB"
// #define BLOCK_END_LABEL   "LBE"
// ASM_GENERATE_INTERNAL_LABEL(loclabel, "LVL", loclabel_num);
/*
	Compilation Unit @ offset 0x0:
	Length:        0x9a (32-bit) <===
	Version:       4
	Abbrev Offset: 0x0
	Pointer Size:  4

	When a function is completed, stip all code
	from the dag instead of debug info. Then call
	fend(). We will keep all function debug info
	here so when exit() is called, we'll dump it out.

	Or...we can just define the end label & take the
	difference now. I.e. emit (end - start) even though
	end has not yet been emitted.

  https://sourceware.org/binutils/docs-2.18/as/LNS-directives.html#LNS-directives
*/

//
// .debug_line section
//
// TODO: Add .loc directive in NASM.
//
//  .loc <file_number> <line> <column>
//
// TODO: Add .file directive to NASM.
//
//  .file <optional_file_number> <file_name>
//
typedef struct DWARF_LINEHDR {
	uint32_t length;
	uint16_t version;
	uint32_t hdrLength;
	uint8_t  minInstrLen;
	uint8_t  defIsStmt;
	int8_t   lineBase;
	uint8_t  lineRange;
	uint8_t  opcodeBase;
	uint8_t  stdOpcodeLengths[12];
}DWARF_LINEHDR, *PDWARF_LINEHDR;

// children indicators:
#define DW_CHILDREN_no  0
#define DW_CHILDREN_yes 1

// data types:
#define DW_TAG_base_type          0x24
#define DW_TAG_unspecified_type   0x3b // VOID type
#define DW_TAG_const_type         0x26
#define DW_TAG_member             0x0d
#define DW_TAG_subroutine_type    0x15
#define DW_TAG_ptr_to_member_type 0x1f
#define DW_TAG_pointer_type       0x0f
#define DW_TAG_array_type         0x01
#define DW_TAG_enumeration_type   0x04
#define DW_TAG_structure_type     0x13
#define DW_TAG_variant            0x19
#define DW_TAG_union_type         0x17
#define DW_TAG_packed_type        0x2d
#define DW_TAG_volatile_type      0x35
#define DW_TAG_typedef            0x16

// data objects (4.1)
#define DW_TAG_constant               0x27
#define DW_TAG_variable               0x34
#define DW_TAG_formal_parameter       0x05

#define DW_TAG_enumerator             0x28
#define DW_TAG_label                  0x0a
#define DW_TAG_lexical_block          0x0b
#define DW_TAG_module                 0x1e
#define DW_TAG_subprogram             0x2e
#define DW_TAG_inlined_subroutine     0x1d
#define DW_TAG_imported_declaration   0x08
#define DW_TAG_unspecified_parameters 0x18
#define DW_TAG_compile_unit           0x11
#define DW_TAG_dwarf_procedure        0x36
#define DW_TAG_lo_user                0x4080
#define DW_TAG_hi_user                0xffff

// attribute encodings:
#define DW_AT_sibling       0x01
#define DW_AT_location      0x02
#define DW_AT_name          0x03
#define DW_AT_byte_size     0x0b
#define DW_AT_stmt_list     0x10
#define DW_AT_low_pc        0x11
#define DW_AT_high_pc       0x12
#define DW_AT_language      0x13
#define DW_AT_comp_dir      0x1b
#define DW_AT_producer      0x25
#define DW_AT_prototyped    0x27
#define DW_AT_decl_column   0x39
#define DW_AT_decl_file     0x3a
#define DW_AT_decl_line     0x3b
#define DW_AT_encoding      0x3e
#define DW_AT_external      0x3f
#define DW_AT_frame_base    0x40
#define DW_AT_type          0x49

// attribute from encodings:
#define DW_FORM_addr     0x01
#define DW_FORM_block2   0x03
#define DW_FORM_block4   0x04
#define DW_FORM_data2    0x05
#define DW_FORM_data4    0x06
#define DW_FORM_data8    0x07
#define DW_FORM_string   0x08
#define DW_FORM_block    0x09
#define DW_FORM_block1   0x0a
#define DW_FORM_data1    0x0b
#define DW_FORM_flag     0x0c
#define DW_FORM_sdata    0x0d
#define DW_FORM_STRP     0x0e
#define DW_FORM_udata    0x0f
#define DW_FORM_ref_addr 0x10
#define DW_FORM_ref1     0x11
#define DW_FORM_ref2     0x12
#define DW_FORM_ref4     0x13
#define DW_FORM_ref8     0x14
#define DM_FORM_ref_udata 0x15
#define DM_FORM_indirect 0x16

// interface ---------

typedef struct DBG {

	// can we store debug info somehow
	// in SYMBOL and TYPE's themselves?
	// i.e. type->d back-end debug data.
	//   --> inc. basic types.

	// emits line & file directives
	void(*line)(IN PCOORD);
	void(*file)(IN PCOORD);

	// may annotate the IR with debug nodes.
	// fend will scan the IR and emit
	// the debug tree in debug_abbrev & debug_info.
//	void(*fstart)(IN PIR ir);
//	void(*fend)(IN PIR ir);
//	void(*bstart)(IN PIR ir);
//	void(*bend)(IN PIR ir);

	// may annotate symbol & emit debug
	// info to debug_abbrev & debug_info
	// struct/union/enum decls
	void(*decl)(IN PSYMBOL);

	void(*symbol)(IN PSYMBOL);
	void(*type)(IN PTYPE);
	void(*init)(void);
	void(*exit)(void);
}DBG, *PDBG;

// call on file or line change:
void DbgFile(IN PCOORD loc) {
//	Puts("file %i %s", loc->fnum, loc->fname->val);
}
void DbgLine(IN PCOORD loc) {
//	Puts("line %i %i %i", loc->fnum, loc->x, loc->y);
}



void DbgType(IN PTYPE ty) {

}

void DbgSymbol(IN PSYMBOL sy) {

}

void DbgFunctionStart(IN PCOORD loc) {

}

void DbgFunctionEnd(IN PCOORD loc) {

}

void DbgBlockBegin(IN PCOORD loc) {

}

void DbgBlockEnd(IN PCOORD loc) {

}

void DbgInit(IN void) {

}

void DbgExit(void) {

//	Puts("section .debug_info");
//	Puts("Ldebug_info0:");

//	Puts("section .debug_abbrev");
//	Puts("Ldebug_abbrv0:");debug_abbrev

}

/************************************************************************
* 
*	ncc.h - Neptune C Compiler
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*	
************************************************************************/

#include <stdio.h>

#ifndef NCC_H
#define NCC_H

#define IN
#define OUT
#define OPTIONAL
#define PRIVATE static
#define PUBLIC
#define TRUE 1
#define FALSE 0
#ifndef NULL
#ifdef __cplusplus
#  define NULL 0
#else
#  define NULL ((void*)0)
#endif
#endif
#ifndef NELEMS
#  define NELEMS(a) ((int)(sizeof (a)/sizeof ((a)[0])))
#endif
#ifndef ROUNDUP
#  define ROUNDUP(x,n) (((x)+((n)-1))&(~((n)-1)))
#endif

#define ON(v,n)    ((v&n)==(n))
#define OFF(A,B)   (!ON(A,B)) 
#define CLEAR(v,n) (v &= ~(n))
#define SET(v,n)   (v |= (n))
#define FLIP(v,n)  (ON(v,(n)) ? CLEAR(v,(n)) : SET(v,(n)))

typedef char           int8_t;
typedef short          int16_t;
typedef int            int32_t;
typedef unsigned char  uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int   uint32_t;
typedef signed   char  sint8_t;
typedef signed   short sint16_t;
typedef signed   int   sint32_t;
typedef unsigned int   bool_t;
typedef unsigned int   handle_t;
typedef unsigned int   index_t;
typedef unsigned int   offset_t;
#ifndef _MSC_VER
typedef unsigned int   size_t;
#endif
typedef void*          addr_t;
typedef uint16_t       wchar_t;
typedef unsigned long long uint64_t;
typedef long long      int64_t;

// used by print.c extensions:
typedef int(*ADDCHAR)(void *context, int c);

// helper method: calls addchar for each char in "s"
extern PUBLIC void VFNString(IN ADDCHAR addchar, IN char* s, IN void* context);

// ---

typedef struct SYMBOL SYMBOL, *PSYMBOL;

#define EC_SUCCESS        0
#define EC_ERROR_OVERFLOW 1

extern void Error      (IN char* template, ...);
extern void ParseError (IN char* template, ...);
extern void Warn       (IN char* template, ...);
extern void Info       (IN char* template, ...);
extern void Fatal      (IN char* template, ...);
extern void Puts       (IN char* fmt, ...);
extern void Printf     (IN char* fmt, ...);
extern void FPrintf    (IN FILE*, IN char* fmt, ...);
extern void Sprintf    (OUT char* buf, IN char* fmt, ...);
extern void Sprintfn   (OUT char* buf, IN size_t len, IN char* fmt, ...);

extern int _errorCount;
extern int _warnCount;

extern void* Alloc   (IN int);
extern void* Allocz  (IN int);
extern void  Free    (IN void*);
extern unsigned long Hash(IN char* key);

// linked list ---

typedef struct LIST_ENTRY* PLIST_ENTRY;
typedef struct LIST_ENTRY {
	PLIST_ENTRY flink;
	PLIST_ENTRY blink;
}LIST_ENTRY, *PLIST_ENTRY;

#define ForEach(list,item) \
	for (PLIST_ENTRY item = (list)->flink; item != (list); item = item->flink)

#define InitListHead(list) \
	(list)->flink = (list)->blink = (list)

#define IsListEmpty(list) \
	(((list)->flink == (list)->blink) && ((list)->flink==(list)))

#define InsertHeadList(list,item)  \
{                                  \
	PLIST_ENTRY f = (list)->flink; \
	(list)->flink = (item);        \
	(item)->flink = f;             \
	(item)->blink=(list);          \
	f->blink=item;                 \
}

#define InsertTailList(list,item)  \
{                                  \
	PLIST_ENTRY f = (list)->blink; \
	(list)->blink = (item);        \
	(item)->blink = f;             \
	(item)->flink = (list);        \
	f->flink = item;               \
}

#define RemoveListEntry(list, item) { \
	(item)->blink->flink = (item)->flink; \
	(item)->flink->blink = (item)->blink; \
}

extern PLIST_ENTRY RemoveHeadList(PLIST_ENTRY list, PLIST_ENTRY item);
extern PLIST_ENTRY RemoveTailList(PLIST_ENTRY list, PLIST_ENTRY item);

// single linked list ---

typedef struct SLIST_ENTRY* PSLIST_ENTRY;
typedef struct SLIST_ENTRY {
	PSLIST_ENTRY flink;
}SLIST_ENTRY, *PSLIST_ENTRY;

#define PushEntryList(slist,item)  \
{                                  \
	PSLIST_ITEM f = (slist)->flink; \
	(slist)->flink = (item);        \
	(item)->flink = f;              \
}

extern PSLIST_ENTRY PopEntryList(PSLIST_ENTRY slist, PLIST_ENTRY item);

// string table ---

#define ENC_ASCII 1
#define ENC_UTF8  1
#define ENC_UTF16 2
#define ENC_UTF32 4

typedef struct STRING {
	LIST_ENTRY e;
	int        encoding;
	char*      val;
	index_t    ref;
}STRING, *PSTRING;

extern PSTRING  NewStringA(IN char* value);
extern PSTRING  NewStringW(IN wchar_t* value);
extern bool_t   StrCmp(IN PSTRING a, IN PSTRING b);
extern size_t   StrLen(IN PSTRING a);
extern PSTRING  StrCat(IN PSTRING a, IN PSTRING b);
extern char*    StrToChar(IN PSTRING a);
extern wchar_t* StrToWideChar(IN PSTRING a);

// vector ---

typedef struct VECTOR {
	size_t   size;
	size_t   count;
	size_t   nalloc;
	uint8_t* data;
}VECTOR, *PVECTOR;

#define VectorInit(v,t) do{ \
	(v)->count = 0;         \
	(v)->nalloc = 0;        \
	(v)->data = NULL;       \
	(v)->size = sizeof(t);  \
	}while (0);
#define VectorSize(v)   (v)->size
#define VectorLen(v)    (v)->count
#define VectorOk(v)     ((v)->nalloc > 0)
#define VectorHead(v)   (v)->data
#define VectorTail(v)   (&(v)->data[(v)->count*(v)->size])
#define VectorAt(v,i)   ((i)>=(v)->count?NULL:&(v)->data[(i)*(v)->size])
extern void    VectorSet      (IN PVECTOR v, IN index_t idx, IN void* obj);
extern void    VectorPush     (IN PVECTOR v, IN void* obj);
extern PVECTOR VectorAppend   (IN PVECTOR a, IN PVECTOR b);
extern void    VectorPop      (IN PVECTOR v, OUT void* obj);

// set ---

typedef struct SET* PSET;
typedef struct SET {
	char* s;
	PSET  next;
}SET, *PSET;

extern PSET   SetInsert    (IN PSET k, IN char* s);
extern bool_t SetFind      (IN PSET k, IN char* s);
extern PSET   SetUnion     (IN PSET a, IN PSET b);
extern PSET   SetIntersect (IN PSET a, IN PSET b);

// map ---

typedef struct BUCKET_ENTRY {
	LIST_ENTRY e;
	char*      key;
	wchar_t*   wkey;
	void*      val;
}BUCKET_ENTRY, *PBUCKET_ENTRY;

typedef struct BUCKET {
	LIST_ENTRY itemList;
}BUCKET,*PBUCKET;

typedef struct MAP* PMAP;
typedef struct MAP {
	PMAP       parent;
	int        level;
	int        numBuckets;
	int        numElements;
	bool_t     allowDups;
	PBUCKET    buckets;
}MAP, *PMAP;

extern PMAP NewMap(IN bool_t allowDups, IN int numBuckets);
extern bool_t MapPutA(IN PMAP, IN char* key, IN void* value);
extern void* MapGetA(IN PMAP, IN char* key);
extern bool_t MapPutW(IN PMAP, IN wchar_t* key, IN void* value);
extern void* MapGetW(IN PMAP, IN wchar_t* key);

// buffer --

typedef struct BUFFER {
	uint8_t* data;
	uint32_t allocCount;
	uint32_t elementCount;
}BUFFER, *PBUFFER;

extern PUBLIC PBUFFER NewBuffer(void);
extern PUBLIC void BufferFree(IN PBUFFER r);
extern PUBLIC uint8_t* GetBufferData(IN PBUFFER b);
extern PUBLIC size_t GetBufferLength(IN PBUFFER b);
extern PUBLIC void BufferWrite(IN PBUFFER b, IN char c);
extern PUBLIC void BufferPush(IN PBUFFER b, IN char c);
extern PUBLIC char BufferPop(IN PBUFFER b);
extern PUBLIC void BufferAppend(IN PBUFFER b, IN char* s, IN int len);
extern PUBLIC void BufferPrintf(IN PBUFFER b, IN char* fmt, ...);

// code generation mode --

typedef enum BITS {
	BITS16, BITS32, BITS64
}BITS;

// file/location ---

typedef struct COORD {
	PSTRING  fname;
	unsigned y;
}COORD, *PCOORD;

// value ---

typedef long                VALUE_INT;
typedef unsigned long       VALUE_UINT;
typedef double              VALUE_FLOAT;
typedef long double         VALUE_LFLOAT;
#define VALUE_OVERFLOW(i,e) ((i)&~(~0UL >> (e)))
#define VALUE_MAX           0xffffffff
#define VALUE_MASK(n)       ((n)>=8*sizeof (VALUE_UINT) ? ~0UL : ~((~0UL)<<(n)))

typedef struct NODE *PNODE;
typedef struct BLOCK *PBLOCK;

//typedef struct XBLOCK* PXBLOCK;

typedef union VALUE {
	VALUE_INT     i;
	VALUE_UINT    ui;
	VALUE_FLOAT   f;
	VALUE_LFLOAT  lf;
	void*         p;
	PSTRING       s;
	PSYMBOL       x;
	PNODE         y;
	PBLOCK        z;
}VALUE, *PVALUE;

// scanner ---

#define TK_ANY      0
#define TK_IDENT    0x1
#define TK_STRING   0x2
#define TK_NUMBER   0x3
#define TK_EOF      0x4
#define TK_ERROR    0x5
#define TK_TYPENAME 0x6

#define TK_LEQ      0x80
#define TK_LSHIFT   0x81
#define TK_GEQ      0x82
#define TK_RSHIFT   0x83
#define TK_DEREF    0x84
#define TK_DECR     0x85
#define TK_EQ       0x86
#define TK_NEQ      0x87
#define TK_LOGOR    0x88
#define TK_LOGAND   0x89
#define TK_INCR     0x8A
#define TK_ELLIPSIS 0x8B
#define TK_2HASH    0x8C

#define TK_ADD_EQ   0x90
#define TK_SUB_EQ   0x91
#define TK_MUL_EQ   0x92
#define TK_DIV_EQ   0x93
#define TK_MOD_EQ   0x94
#define TK_AND_EQ   0x95
#define TK_XOR_EQ   0x96
#define TK_OR_EQ    0x97
#define TK_SHL_EQ   0x98
#define TK_SHR_EQ   0x99

#define KW           0xA0
#define K_AUTO       KW+1
#define K_BREAK      KW+2
#define K_CASE       KW+3
#define K_CHAR       KW+4
#define K_CONST      KW+5
#define K_CONTINUE   KW+6
#define K_DEFAULT    KW+7
#define K_DOUBLE     KW+8
#define K_DO         KW+9
#define K_ELSE       KW+10
#define K_ENUM       KW+11
#define K_EXTERN     KW+12
#define K_FLOAT      KW+13
#define K_FOR        KW+14
#define K_GOTO       KW+15
#define K_LONG       KW+16
#define K_REGISTER   KW+17
#define K_RETURN     KW+18
#define K_SHORT      KW+19
#define K_SIZEOF     KW+20
#define K_STATIC     KW+21
#define K_STRUCT     KW+22
#define K_SWITCH     KW+23
#define K_TYPEDEF    KW+24
#define K_UNION      KW+25
#define K_UNSIGNED   KW+26
#define K_VOID       KW+27
#define K_VOLATILE   KW+28
#define K_WHILE      KW+29
#define K_IF         KW+30
#define K_INLINE     KW+31
#define K_INT        KW+32
#define K_SIGNED     KW+33

#define OP_UNARY     1
#define OP_BINARY    2

#define ATTR_TYPENAME 1

typedef struct NODE *PNODE;
typedef PNODE (*LPNODE)(IN int op, IN PNODE lhs, IN PNODE rhs);

typedef struct TYPE* PTYPE;
typedef struct TOKEN {
	LIST_ENTRY e;
	int        prec;
	int        optype;
	int        op;
	int        attr;
	PTYPE      type;
	VALUE      val;
	bool_t     firstOnLine;
	PSET       hideset;
	unsigned   loc;
	bool_t     varg;
	LPNODE     tree;
	COORD      coord;
}TOKEN, *PTOKEN;

extern void   PrintToken (IN FILE* out, IN TOKEN tok);
extern TOKEN  Lex        (void);
extern void   Unlex      (IN TOKEN tok);
extern TOKEN  Look       (IN index_t);
extern TOKEN  ErrorToken (void);

// preprocessor ---

extern TOKEN GetToken   ();
extern void  UngetToken (IN TOKEN);
extern TOKEN PeekToken  (IN int);

// parser ---

typedef struct SYMBOL  *PSYMBOL;
typedef struct PROGRAM *PPROGRAM;

extern PSYMBOL  LexIsTypeName (IN char* cbuf);
extern PPROGRAM Parse         (void);

/*
   IR

   To facilitate code generation, these encode the Operation Code, Size,
   and Type. For instructions without these fields, they can be assumed to
   be 0. For example, the ADDI2 instruction is represented as

   ADD+I+2  -> ADD Integer 2 byte

   OPCODE(ADD+I+2) -> ADD
   OPTYPE(ADD+I+2) -> I
   OPSIZE(ADD+I+2) -> 2

   The only valid sizes are 1, 2, 4, and 8. These are derived from the Basic Types
   as defined in ctypes.c.

   +------------+------+------+
   | mnemonic   | type | size |
   +------------+------+------+
   32           8      4      0
*/

#define OPCODE(i)  (i & 0xffffff00)
// #define OPTYPE(i)  (i & 0x000000f0)
#define OPSIZE(i)  (i & 0x0000000f)

#define INSTR(i)   (i << 8)
#define TYPEOP(i)  (i << 4)
#define SIZEOP(i)  (i)

#define MKOP(op,ty) (OPCODE(op) + TypeToOp(ty))

extern int   TypeToOp  (IN PTYPE type);
extern PTYPE OpToType  (IN int op, IN int size);

#define TYPE_VOID       TYPEOP(1)
#define TYPE_INT        TYPEOP(2)
#define TYPE_UNSIGNED   TYPEOP(3)
#define TYPE_FLOAT      TYPEOP(4)
#define TYPE_ARRAY      TYPEOP(5)
#define TYPE_POINTER    TYPEOP(6)
#define TYPE_FUNCTION   TYPEOP(7)
#define TYPE_STRUCT     TYPEOP(8)
#define TYPE_UNION      TYPEOP(9)
#define TYPE_ENUM       TYPEOP(10)
#define TYPE_ENUMERATOR TYPEOP(11)
#define TYPE_CONST      TYPEOP(12)
#define TYPE_VOLATILE   TYPEOP(13)

#define F TYPE_FLOAT
#define I TYPE_INT
#define U TYPE_UNSIGNED
#define P TYPE_POINTER
#define V TYPE_VOID
#define B TYPE_STRUCT

//
// storage classes
//
#define SC_TYPEDEF   1
#define SC_EXTERN    2
#define SC_STATIC    4
#define SC_AUTO      8
#define SC_REGISTER  16

#define IsArray(t)    (t->kind==TYPE_ARRAY)
#define IsFunction(t) (t->kind==TYPE_FUNCTION)
#define IsStruct(t)   (t->kind==TYPE_STRUCT||t->kind==TYPE_UNION)
#define IsPointer(t)  (t->kind==TYPE_POINTER)
#define IsEnum(t)     (t->kind==TYPE_ENUM)
#define IsVoid(t)     (t->kind==TYPE_VOID)
#define IsFloat(t)    (t->kind==TYPE_FLOAT)
#define IsInt(t)      (t->kind==TYPE_INT||t->kind==TYPE_UNSIGNED)
#define IsIntOrPtr(t) (t->kind==TYPE_INT||t->kind==TYPE_UNSIGNED||t->kind==TYPE_POINTER)
#define IsUnsigned(t) (t->kind==TYPE_UNSIGNED)
#define IsArith(t)    (t->kind==TYPE_INT||t->kind==TYPE_UNSIGNED||t->kind==TYPE_FLOAT)
#define IsScalar(t)   (t->kind==TYPE_INT||t->kind==TYPE_UNSIGNED||t->kind==TYPE_ENUM)
#define IsConst(t)    (t->kind==TYPE_CONST)
#define IsVolatile(t) (t->kind==TYPE_VOLATILE)
#define IsQual(t)     (t->kind>=TYPE_CONST)
#define Unqual(t)     (t->kind>=TYPE_CONST?t->type:t)
#define IsVoidPtr(t)  (t->kind==TYPE_POINTER&&t->type->kind==TYPE_VOID)

typedef struct LIMITS {
	VALUE min;
	VALUE max;
}LIMITS, *PLIMITS;

typedef struct TYPE_INFO {
	size_t   size;
	uint32_t align;
	uint32_t prop; // integer or float?
}TYPE_INFO, *PTYPE_INFO;

typedef struct tagFIELD *PFIELD;
typedef struct tagFIELD {
	PSTRING    name;
	PTYPE      type;
	offset_t   offset;
	short      bits;
	PFIELD     next;
} *PFIELD;

typedef struct PARAM* PPARAM;
typedef struct PARAM {
	PSTRING name;
	PTYPE   type;
	PPARAM  next;
}PARAM, *PPARAM;

typedef struct TYPE {
	PSTRING      name;
	unsigned     typeno;
	bool_t       written;
	bool_t       marked;
	bool_t       defined;
	PSYMBOL      sym;
	int          kind;
	TYPE_INFO    info;
	LIMITS       limits;
	uint32_t     size;
	uint32_t     align;
	PTYPE        type; // pointer to "type", array of "type", function of "type"
	size_t       arrsize;
	PFIELD       fields;
	PFIELD       lastfield;
	PPARAM       args;
	PPARAM       lastarg;
	bool_t       oldProtoStyle;
}TYPE, *PTYPE;

extern PTYPE CharType;                // char
extern PTYPE WideCharType;            // wchar_t
extern PTYPE SignedCharType;          // signed char
extern PTYPE IntType;                 // signed int
extern PTYPE LongType;                // long
extern PTYPE LongLongType;            // long long
extern PTYPE ShortType;               // signed short int
extern PTYPE FloatType;               // float
extern PTYPE DoubleType;              // double
extern PTYPE LongDoubleType;          // long double
extern PTYPE UnsignedCharType;        // unsigned char
extern PTYPE UnsignedIntType;         // unsigned int
extern PTYPE UnsignedLongType;        // unsigned long
extern PTYPE UnsignedLongLongType;    // unsigned long long
extern PTYPE UnsignedShortType;       // unsigned short
extern PTYPE VoidType;                // void
extern PTYPE ArrayOfCharType;         // ""
extern PTYPE ArrayOfWideCharType;     // L""
extern PTYPE EnumeratorType;          // member of enum
extern PTYPE PointerType;             // T*

extern PTYPE  Pointer      (IN PTYPE to);
extern PTYPE  Array        (IN PTYPE type, IN int size, IN int align);
extern PTYPE  Function     (IN PTYPE ret, IN PTYPE* proto, IN bool_t oldStyle);
extern PTYPE  Struc        (IN int kind);
extern PFIELD AddField     (IN PTYPE struc, IN PSTRING ident, IN PTYPE ty, IN short bits);
extern PFIELD GetField     (IN PTYPE struc, IN PSTRING ident);
extern PPARAM AddParam     (IN PTYPE func, IN PSTRING ident, IN PTYPE type);
extern PPARAM GetParam     (IN PTYPE func, IN index_t idx);
extern PTYPE  Deref        (IN PTYPE of);
extern PTYPE  ArrayToPtr   (IN PTYPE ar);
extern PTYPE  FunctionRet  (IN PTYPE fn);
extern PTYPE  Promote      (IN PTYPE t);
extern PTYPE  Qual         (IN int op, IN PTYPE t);

// symbol ---

// scoping goes GLOBALS->PARMS->LOCALS+k
#define CONSTANTS  1
#define LABELS     2
#define GLOBALS    3
#define PARMS      4
#define LOCALS     5

typedef struct BLOCK* PBLOCK;
typedef struct QUAD*  PQUAD;
typedef struct QUAD_LIST* PQUAD_LIST;
typedef struct SYMBOL_LIST* PSYMBOL_LIST;
typedef struct INSTR* PINSTR;

typedef struct SYMBOL {
	PSTRING      name;
	PSTRING      symbolic;
	COORD        coord;
	unsigned     tempname;
	unsigned     opflags;
	unsigned     opflags2;
	unsigned     baseReg;
	bool_t       memory;
	PTYPE        type;
	unsigned     level;
	unsigned     sclass;
	VALUE        val;
	bool_t       addrTaken;
	bool_t       fwref;
	LIST_ENTRY   uses;
	int          offset; // stack offset if local
	int          segment;
	PSYMBOL      alias;
	PSYMBOL_LIST interf; // Register Interference Graph edges
	//
	// function types and global symbols:
	//
	unsigned     statics;  // static counter
	size_t       stacksize;
	PQUAD_LIST   quads;
	PBLOCK       code;
	PBLOCK       codePreorder;
	PBLOCK       codePostorder;
	PNODE        ast;
	PSYMBOL      localsListHead; // PARAMS and LOCALS
	PSYMBOL      nextLocal;
	PSYMBOL      bbnext;         // BLOCK has bblocalsListHead
//	PXBLOCK      xcode;
	PINSTR       xcode;
}SYMBOL, *PSYMBOL;

typedef struct SYMBOL_LIST {
	PSYMBOL      me;
	PQUAD        lastUse;
	PSYMBOL_LIST next;
	PSYMBOL_LIST prev;
}SYMBOL_LIST, *PSYMBOL_LIST;

typedef struct PROGRAM {
	PSYMBOL_LIST cstr;
	PSYMBOL_LIST gvars;
	PSYMBOL_LIST functs;
}PROGRAM, *PPROGRAM;

/*
	The IR consists of a control flow graph. Within each
	basic block is a list of instruction nodes.
*/

#define TEXT   0
#define DATA   1
#define BSS    2
#define RODATA 3

#define QADD      INSTR(1) /* expression */
#define QSUB      INSTR(2)
#define QMUL      INSTR(3)
#define QDIV      INSTR(4)
#define QMOD      INSTR(5)
#define QNEG      INSTR(6)
#define QBSHL     INSTR(7)
#define QBSHR     INSTR(8)
#define QBAND     INSTR(9)
#define QBCMPL    INSTR(10)
#define QBOR      INSTR(11)
#define QBXOR     INSTR(12)
#define QEQ       INSTR(13) /* comparision */
#define QGE       INSTR(14)
#define QGT       INSTR(15)
#define QLE       INSTR(16)
#define QLT       INSTR(17)
#define QNE       INSTR(18)
#define QNOT      INSTR(19)
#define QAND      INSTR(20)
#define QOR       INSTR(21)
#define QADDRG    INSTR(22) /* global */
#define QADDRL    INSTR(23) /* local */
#define QADDRF    INSTR(24) /* parameter */
#define QCVTI     INSTR(25)
#define QCVTF     INSTR(26)
#define QCVTU     INSTR(27)
#define QCVTP     INSTR(28)
#define QCALL     INSTR(29)
#define QCNST     INSTR(30)
#define QARG      INSTR(31)
#define QASGN     INSTR(32)
#define QINDIR    INSTR(33)
#define QRET      INSTR(34) /* statement */
#define QJMP      INSTR(35)
#define QREGISTER INSTR(36)
#define QINDREG   INSTR(37)
#define QBLK      INSTR(38) /* node with basic block */
#define QLBRACE   INSTR(39)
#define QRBRACE   INSTR(40)
#define QMOV      INSTR(41)
#define QCMP      INSTR(42)
#define QLABEL    INSTR(43)
#define QCOORD    INSTR(44)
#define QSUBSCRIPT INSTR(45)
#define QPOSTINC  INSTR(46)
#define QPREINC   INSTR(47)
#define QPOSTDEC  INSTR(48)
#define QPREDEC   INSTR(49)

/*
	AST-Specific nodes
*/
#define NODELST  INSTR(60)
#define ASM      INSTR(61)
#define SWTCH    INSTR(62)
#define IF       INSTR(63)
#define WHILE    INSTR(64)
#define CONTINUE INSTR(65)
#define BREAK    INSTR(66)
#define FOR      INSTR(67)
#define DOWHILE  INSTR(68)
#define DEFAULT  INSTR(69)
#define CASE     INSTR(70)
#define GOTO     INSTR(71)
#define LABEL    INSTR(72)
#define MEMBER   INSTR(73)


extern PUBLIC PNODE NodeListAlloc(IN PNODE me);
extern PUBLIC PNODE NodeListAppend(IN PNODE list, IN PNODE item);
extern PUBLIC PNODE NodeListNext(IN PNODE cur);
extern PUBLIC PNODE NodeListGet(IN PNODE listItem);
extern PUBLIC size_t NodeListSize(IN PNODE list);

#define LEFT  0
#define RIGHT 1

typedef struct NODE *PNODE;
typedef struct BLOCK* PBLOCK;
typedef struct QUAD *PQUAD;

#define IsTemp(n) (n->temp > 0)

typedef struct QUAD* PQUAD;

typedef struct NODE {

	unsigned   op;
	unsigned   name;
	COORD      coord;
	PTYPE      type;

	// QSUBSCRIPT index
	PSYMBOL    subscript;

	// Goto or Label
	PSTRING    ident;

	// Symbols
	PSYMBOL    sym;

	// Constants (If Sym is NULL)
	VALUE      val;

	// NODE of type BLOCK
	PBLOCK     block;

	// Expressions
	PNODE      kids[2];

	union {
		struct {
			PNODE  cond;
			PBLOCK target[2];
		}branch;
		struct {
			unsigned count;
			PNODE* params;
		}funct;
		struct {
			PNODE cond;
			PNODE body;
		}swtch;
		struct {
			int   cond;
			PNODE stmt;
			PQUAD label;
		}caseStmt;
		struct {
			PNODE cond;
			PNODE then;
			PNODE els;
		}ifStmt;
		struct {
			PNODE init;
			PNODE next;
			PNODE cond;
			PNODE body;
		}loopStmt;
		struct {
			PNODE me;
			PNODE next;
			PNODE prev;
		}list;
		struct {
			PNODE    value;
			PTYPE    totype;
			unsigned offset;
		}init;
		struct {
			PNODE  lhs;
			PFIELD field;
		}mem;
	}u;

}NODE, *PNODE;

typedef struct QUAD_LIST* PQUAD_LIST;
typedef struct QUAD_LIST {
	PQUAD      me;
	PQUAD_LIST next;
	PQUAD_LIST prev;
}QUAD_LIST, *PQUAD_LIST;

typedef struct BLOCK_LIST* PBLOCK_LIST;
typedef struct BLOCK_LIST {
	PBLOCK      me;
	PBLOCK_LIST next;
	PBLOCK_LIST prev;
}BLOCK_LIST, *PBLOCK_LIST;

typedef struct RVAL {
	PTYPE    type;
	PSYMBOL  var;
	PSTRING  stringlit;
	int      intLit;	 
}RVAL, *PRVAL;

typedef struct QUAD {

	// name (debugging)
	int    name;

	// mnemonic
	int    op;

	// array subscript (if any) for QSTORE and QLOAD
	PRVAL index;

	// QCOORD instr only
	COORD  coord;

	// conditional
	int    condCode;
	PQUAD  label;

	// operands and target
	PSYMBOL target;
	PRVAL   src1, src2;

	// parent block
	PBLOCK block;

	// liveness

	PQUAD_LIST   succlist;
	PQUAD_LIST   predlist;

	PSYMBOL_LIST uselist;
	PSYMBOL_LIST deflist;
	PSYMBOL_LIST inlist;
	PSYMBOL_LIST outlist;

}QUAD, *PQUAD;

typedef struct INSTR_LIST* PINSTR_LIST;

typedef struct BLOCK {

	// unique ID (for DOT file)
	unsigned name;

	// parent function
	PSYMBOL  fn;

	// this block has been written out?
	bool_t   written;

	// visited during liveness analysis
	bool_t   liveVisited;

	// intermediate code
	PQUAD_LIST quads;

	// target instructions
	PINSTR_LIST instrs;

	// label for this block
	PQUAD     label;

	// next block in linear Sequence
	PBLOCK    next;

	// next block in pre-order Sequence
	PBLOCK   preordnext;
	PBLOCK   preorderprev;
	bool_t   preordvisit;

	// next block in post-order Sequence
	PBLOCK   postordernext;
	PBLOCK   postorderprev;
	bool_t   postordervisit;

	// successor and predicate list
	PBLOCK_LIST  succlist;
	PBLOCK_LIST  predlist;

	// successor blocks:
	PBLOCK       outDirect;
	PBLOCK       outCond;

	PSYMBOL_LIST inlist;
	PSYMBOL_LIST outlist;

	// map from "symbol" to last QUAD used
	PSYMBOL_LIST lastUseList;

	// Local variables
	PSYMBOL  locals;

}BLOCK, *PBLOCK;

extern unsigned NextLabel ();

extern PNODE RValue          (IN PNODE in);
extern PNODE LValue          (IN PNODE in);
extern PNODE NodePreDec      (IN PNODE lvalue);
extern PNODE NodePostDec     (IN PNODE lvalue);
extern PNODE NodePreInc      (IN PNODE lvalue);
extern PNODE NodePostInc     (IN PNODE lvalue);
extern PNODE LogAndOr        (IN int op, IN PNODE lhs, IN PNODE rhs);
extern PNODE Tree            (IN unsigned op, IN PTYPE ty, IN PNODE l, IN PNODE r);
extern PNODE Leaf            (IN unsigned op, IN PTYPE ty, IN VALUE v);
extern PNODE NodeBrace       (void);
extern PNODE TreeAddr        (IN PSYMBOL s);
extern PNODE Cast            (IN PNODE t, IN PTYPE to);
extern PNODE TreeConst       (IN PTYPE ty, unsigned i);
extern PNODE MabeyDecay      (IN PNODE t);
extern PNODE Retype          (IN PNODE in, IN PTYPE ty);
extern PNODE TreeAdd         (IN int op, IN PNODE lhs, IN PNODE rhs);
extern PNODE TreeSub         (IN int op, IN PNODE lhs, IN PNODE rhs);
extern PNODE TreeMul         (IN int op, IN PNODE lhs, IN PNODE rhs);
extern PNODE TreeBit         (IN int op, IN PNODE lhs, IN PNODE rhs);
extern PNODE TreeEq          (IN int op, IN PNODE lhs, IN PNODE rhs);
extern PNODE TreeCmp         (IN int op, IN PNODE lhs, IN PNODE rhs);
extern PNODE TreeShift       (IN int op, IN PNODE lhs, IN PNODE rhs);
extern PNODE Assgn           (IN int op, IN PNODE lvalue, IN PNODE rhs);
extern PNODE Addrof          (IN PNODE p);
extern PNODE Not             (PNODE p);
extern PNODE Merge           (void);
extern PNODE Branch          (IN PNODE left, IN PBLOCK truey, IN PBLOCK falsy);
extern PNODE Trace           (IN PCOORD coord);
extern PNODE Return          (IN PNODE expr);
extern PNODE NodeSwitch      (IN PNODE expr, IN PNODE body);
extern PNODE NodeIf          (IN PNODE cond, IN PNODE then, IN PNODE els);
extern PNODE NodeWhile       (IN PNODE cond, IN PNODE body);
extern PNODE NodeDoWhile     (IN PNODE cond, IN PNODE body);
extern PNODE NodeFor         (IN PNODE init, IN PNODE cond, IN PNODE next, IN PNODE body);
extern PNODE NodeBB          (IN PBLOCK bb);
extern PNODE NodeSubscript   (IN PNODE lval, IN PNODE subscript);
extern PNODE NodeCase        (IN int cond, IN PNODE st);
extern PNODE NodeDefault     (IN PNODE st);
extern PNODE NodeGoto        (IN PSTRING ident);
extern PNODE NodeLabel       (IN PSTRING ident);
extern PNODE NodeMember      (IN PNODE lhs, IN PFIELD field);

extern PUBLIC PNODE   NodeListAlloc  (IN PNODE me);
extern PUBLIC PNODE   NodeListAppend (IN PNODE list, IN PNODE item);
extern PUBLIC bool_t  NodeListAppendUnique (IN PNODE* list, IN PNODE item);
extern PUBLIC PNODE   NodeListNext   (IN PNODE cur);
extern PUBLIC PNODE   NodeListGet    (IN PNODE listItem);
extern PUBLIC size_t  NodeListSize   (IN PNODE list);
extern PUBLIC PNODE   NodeListRemove (IN PNODE listItem);
extern PUBLIC PNODE   NodeListDelink (IN PNODE listItem);

// target ---

#define TARG_X86 1
#define TARG_X64 2
#define TARG_DOT 3

typedef struct TARGET {
//	LIST_ENTRY  e;
//	int         targ;
	TYPE_INFO   charinfo;
	TYPE_INFO   shortinfo;
	TYPE_INFO   intinfo;
	TYPE_INFO   longinfo;
	TYPE_INFO   longlonginfo;
	TYPE_INFO   floatinfo;
	TYPE_INFO   doubleinfo;
	TYPE_INFO   longdoubleinfo;
	TYPE_INFO   ptrinfo;
	TYPE_INFO   structinfo;
	void(*Init)(void);
	void(*Close)(void);
	void(*Emit)(IN PSYMBOL);
	void(*Str)(IN PSYMBOL);
	void(*Global)(IN PSYMBOL);
	void(*Function)(IN PSYMBOL);
	void(*Local)(IN PSYMBOL);
	void(*Import)(IN PSYMBOL);
	void(*Export)(IN PSYMBOL);
	//void(*DefString)(IN int n, IN PSTRING s);
}TARGET, *PTARGET;

extern PTARGET _target;

// quad.c

extern PUBLIC PSYMBOL_LIST SymbolListFind(IN PSYMBOL_LIST list, PSYMBOL sym);
extern PUBLIC PSYMBOL_LIST SymbolListAdd(IN PSYMBOL_LIST* list, PSYMBOL sym);
extern PUBLIC PBLOCK_LIST BlockListFind(IN PBLOCK_LIST list, PBLOCK block);
extern PUBLIC PBLOCK_LIST BlockListAdd(IN PBLOCK_LIST* list, PBLOCK block);
extern PUBLIC PQUAD_LIST QuadListFind(IN PQUAD_LIST list, PQUAD quad);
extern PUBLIC PQUAD_LIST QuadListAdd(IN PQUAD_LIST* list, PQUAD quad);
extern PUBLIC PQUAD_LIST QuadListGetLast(IN PQUAD_LIST list);
extern PUBLIC PQUAD QLabel(IN PSTRING str);
extern PUBLIC PQUAD QJmp(IN PQUAD label);
extern PUBLIC PQUAD QJmpc(IN int condCode, PRVAL a, IN PRVAL b, PQUAD truLabel);
extern PUBLIC PQUAD QMov(IN PSYMBOL target, IN PRVAL src1);
extern PUBLIC PQUAD QStore(IN PSYMBOL target, IN PRVAL src1, IN PRVAL index);
extern PUBLIC PQUAD QLoad(IN PSYMBOL target, IN PSYMBOL src1, IN PRVAL index);
extern PUBLIC PQUAD QLBrace(void);
extern PUBLIC PQUAD QRBrace(void);
extern PUBLIC PQUAD QArg(IN PSYMBOL target);
extern PUBLIC PQUAD QCall(IN PSYMBOL target);
extern PUBLIC PQUAD QRet(IN OPTIONAL PRVAL src1);
extern PUBLIC PQUAD QAddrg(IN PSYMBOL target, IN PSYMBOL src1);
extern PUBLIC PQUAD QAddrl(IN PSYMBOL target, IN PSYMBOL src1);
extern PUBLIC PQUAD QAddrf(IN PSYMBOL target, IN PSYMBOL src1);
extern PUBLIC PQUAD QUnaryOp(IN int op, IN PSYMBOL target, IN PRVAL src1);
extern PUBLIC PQUAD QBinOp(IN int op, IN PSYMBOL target, IN PRVAL src1, IN PRVAL src2);
extern PUBLIC PQUAD QCoord(IN COORD coord);
extern PUBLIC PRVAL RValStr(IN PSTRING str);
extern PUBLIC PRVAL RVar(IN PSYMBOL var);
extern PUBLIC PRVAL RValInt(IN int i);

// quadGen.c

extern PUBLIC void QPass(IN PSYMBOL root);

// flowPass.c

extern PUBLIC void AppendQuadSucc(IN PQUAD quad, IN PQUAD item);
extern PUBLIC void AppendQuadPred(IN PQUAD quad, IN PQUAD item);

// livePass.c

extern PUBLIC void         LivenessPass(IN PSYMBOL func);
extern PUBLIC PSYMBOL_LIST GetLiveIn(IN PBLOCK block);
extern PUBLIC PSYMBOL_LIST GetLiveOut(IN PBLOCK block);
extern PUBLIC PSYMBOL_LIST GetLastUseList(IN PBLOCK block);

// colorPass.c

extern PUBLIC void ColorPass(IN PSYMBOL func);
extern PUBLIC void DumpInterfGraph(IN FILE* out, IN PSYMBOL func);

// stab.c

extern PUBLIC unsigned OutTypeSTABS(IN PTYPE ty);
extern PUBLIC void OutSymbolSTABS(IN PSYMBOL sym);

// x86.c / x86gen.c

typedef enum SIZE {
	SizeByte,
	SizeWord,
	SizeDword,
	SizeQword,
	SizeTbyte,
	SizeInvalid
}SIZE;

typedef enum PREFIX {
	PrefixLock = 0,
	PrefixRep,
	PrefixRepe,
	PrefixRepz,
	PrefixRepne,
	PrefixRepnz,
	PrefixXacquire,
	PrefixXrelease,
	PrefixBnd,
	PrefixNobnd,
	PrefixA16,
	PrefixA32,
	PrefixO16,
	PrefixO32,
	PrefixDS,
	PrefixES,
	PrefixSS,
	PrefixCS,
	PrefixFS,
	PrefixGS,
	PrefixInvalid
}PREFIX;

#define PREFIX_MAX 4

typedef enum REGTYPE {
	RegInvalid,
	RegSs, RegCs, RegDs, RegEs, RegFs, RegGs,
	RegAh, RegAl, RegBh, RegBl, RegCh, RegCl, RegDh, RegDl,
	RegSi, RegDi, RegBp, RegSp, RegAx, RegBx, RegCx, RegDx,
	RegEax, RegEbx, RegEcx, RegEdx,
	RegEsi, RegEdi, RegEbp, RegEsp,
	RegRax, RegRbx, RegRcx, RegRdx,
	RegRsi, RegRdi, RegRbp, RegRsp,
	RegCr0, RegCr1, RegCr2, RegCr3, RegCr4, RegCr5, RegCr6, RegCr7,
	RegCr8, RegCr9, RegCr10, RegCr11, RegCr12, RegCr13, RegCr14, RegCr15,
	RegDr0, RegDr1, RegDr2, RegDr3, RegDr4, RegDr5, RegDr6, RegDr7,
	RegDr8, RegDr9, RegDr10, RegDr11, RegDr12, RegDr13, RegDr14, RegDr15,
	RegR8l, RegR9l, RegR10l, RegR11l, RegR12l, RegR13l, RegR14l, RegR15l,
	RegR8w, RegR9w, RegR10w, RegR11w, RegR12w, RegR13w, RegR14w, RegR15w,
	RegR8d, RegR9d, RegR10d, RegR11d, RegR12d, RegR13d, RegR14d, RegR15d,
	RegR8, RegR9, RegR10, RegR11, RegR12, RegR13, RegR14, RegR15,
	RegMmx0, RegMmx1, RegMmx2, RegMmx3, RegMmx4, RegMmx5, RegMmx6, RegMmx7,
	RegXmm0, RegXmm1, RegXmm2, RegXmm3, RegXmm4, RegXmm5, RegXmm6, RegXmm7,
	RegXmm8, RegXmm9, RegXmm10, RegXmm11, RegXmm12, RegXmm13, RegXmm14, RegXmm15,
	RegYmm0, RegYmm1, RegYmm2, RegYmm3, RegYmm4, RegYmm5, RegYmm6, RegYmm7,
	RegYmm8, RegYmm9, RegYmm10, RegYmm11, RegYmm12, RegYmm13, RegYmm14, RegYmm15,
	RegSt0, RegSt1, RegSt2, RegSt3, RegSt4, RegSt5, RegSt6, RegSt7
} REGTYPE;

typedef enum DISPTYPE {
	DispNone,
	Disp8,
	Disp16,
	Disp32
}DISPTYPE;

/*
	OperandType bit field:

	Reference Neptune Assembler NASM/NASM.H for details.

	31                                                        0
	+-------+-------+----------+---------------+---------------+
	|   0   |  000  | 000 000  | 0 0000 0000 0 | 000 0000 0000 |
	+-------+-------+----------+---------------+---------------+
	 Ignore | Class | Subclass | Operand Size  | Substructure
*/

#define GetOperandIgnore(x)    (x & 0x80000000)
#define GetOperandClass(x)     (x & 0x38000000)
#define GetOperandSubclass(x)  (x & 0x7e00000)
#define GetOperandSubstruct(x) (x & 0x7ff)
#define GetOperandSize(x)      (x & 0x1ff800)
#define GetOperandSizeMask()   (0x1ff800)

// ignore bit
#define OperandIgnore 0x80000000

#define OperandInvalid 0
#define OpcodeExtNone (unsigned int)-1

/* operand classes */
typedef enum OperandClass {
	OperandClassMem = 1 << 27,
	OperandClassImm = 2 << 27,
	OperandClassReg = 3 << 27,
	OperandClassPointer = 4 << 27
}OperandClass;

typedef enum OperandImmSubClass{
	OperandImmRel = 1 << 21, // rel8, rel16, and rel32 types.
	OperandImmPtr = 1 << 22  // ptr16:16, ptr16:32 types.
}OperandImmSubClass;

typedef enum OperandRegSubClass {
	OperandRegCDT = 1 << 21, // control, data, or test register.
	OperandRegGPR = 2 << 21, // general purpose register.
	OperandRegSeg = 3 << 21,
	OperandRegFpu = 4 << 21,
	OperandRegMmx = 5 << 21,
	OperandRegXmm = 6 << 21,
	OperandRegYmm = 7 << 21,
}OperandRegSubClass;

typedef enum OPSIZE {
	OperandSize8 = 1 << 11,
	OperandSize16 = 1 << 12,
	OperandSize32 = 1 << 13,
	OperandSize64 = 1 << 14,
	OperandSize80 = 1 << 15,
	OperandSize128 = 1 << 16,
	OperandSize256 = 1 << 17,
	OperandSizeFar = 1 << 18,
	OperandSizeNear = 1 << 19,
	OperandSizeShort = 1 << 20
}OPSIZE;

/* operand types. */
typedef enum OPTYPE {

	OperandPointer = OperandClassPointer,

	OperandImm8 = OperandClassImm | OperandSize8,
	OperandImm16 = OperandClassImm | OperandSize16,
	OperandImm32 = OperandClassImm | OperandSize32,
	OperandImm64 = OperandClassImm | OperandSize64,

	OperandSeg = OperandClassReg | OperandRegSeg | OperandSize16,

	OperandReg8 = OperandClassReg | OperandRegGPR | OperandSize8,
	OperandReg16 = OperandClassReg | OperandRegGPR | OperandSize16,
	OperandReg32 = OperandClassReg | OperandRegGPR | OperandSize32,
	OperandReg64 = OperandClassReg | OperandRegGPR | OperandSize64,

	OperandRm8 = OperandClassMem | OperandRegGPR | OperandSize8,
	OperandRm16 = OperandClassMem | OperandRegGPR | OperandSize16,
	OperandRm32 = OperandClassMem | OperandRegGPR | OperandSize32,
	OperandRm64 = OperandClassMem | OperandRegGPR | OperandSize64,

	OperandMem8 = OperandClassMem | OperandSize8,
	OperandMem16 = OperandClassMem | OperandSize16,
	OperandMem32 = OperandClassMem | OperandSize32,
	OperandMem64 = OperandClassMem | OperandSize64,
	OperandMem80 = OperandClassMem | OperandSize80,
	OperandMem128 = OperandClassMem | OperandSize128,

	OperandRel8 = OperandClassImm | OperandImmRel | OperandSize8,
	OperandRel16 = OperandClassImm | OperandImmRel | OperandSize16,
	OperandRel32 = OperandClassImm | OperandImmRel | OperandSize32,
	OperandRel64 = OperandClassImm | OperandImmRel | OperandSize64,

	OperandPtr16_16 = OperandClassImm | OperandImmPtr | OperandSize16,
	OperandPtr16_32 = OperandClassImm | OperandImmPtr | OperandSize32,

	OperandCs = OperandClassReg | OperandRegSeg | RegCs | OperandSize16,
	OperandDs = OperandClassReg | OperandRegSeg | RegDs | OperandSize16,
	OperandEs = OperandClassReg | OperandRegSeg | RegEs | OperandSize16,
	OperandSs = OperandClassReg | OperandRegSeg | RegSs | OperandSize16,
	OperandFs = OperandClassReg | OperandRegSeg | RegFs | OperandSize16,
	OperandGs = OperandClassReg | OperandRegSeg | RegGs | OperandSize16,
	OperandAl = OperandClassReg | OperandRegGPR | RegAl | OperandSize8,
	OperandAh = OperandClassReg | OperandRegGPR | RegAh | OperandSize8,
	OperandAx = OperandClassReg | OperandRegGPR | RegAx | OperandSize16,
	OperandEax = OperandClassReg | OperandRegGPR | RegEax | OperandSize32,
	OperandRax = OperandClassReg | OperandRegGPR | RegRax | OperandSize64,
	OperandBl = OperandClassReg | OperandRegGPR | RegBl | OperandSize8,
	OperandBh = OperandClassReg | OperandRegGPR | RegBh | OperandSize8,
	OperandBx = OperandClassReg | OperandRegGPR | RegBx | OperandSize16,
	OperandEbx = OperandClassReg | OperandRegGPR | RegEbx | OperandSize32,
	OperandRbx = OperandClassReg | OperandRegGPR | RegRbx | OperandSize64,
	OperandDl = OperandClassReg | OperandRegGPR | RegDl | OperandSize8,
	OperandDh = OperandClassReg | OperandRegGPR | RegDh | OperandSize8,
	OperandDx = OperandClassReg | OperandRegGPR | RegDx | OperandSize16,
	OperandEdx = OperandClassReg | OperandRegGPR | RegEdx | OperandSize32,
	OperandRdx = OperandClassReg | OperandRegGPR | RegRdx | OperandSize64,
	OperandCl = OperandClassReg | OperandRegGPR | RegCl | OperandSize8,
	OperandCh = OperandClassReg | OperandRegGPR | RegCh | OperandSize8,
	OperandCx = OperandClassReg | OperandRegGPR | RegCx | OperandSize16,
	OperandEcx = OperandClassReg | OperandRegGPR | RegEcx | OperandSize32,
	OperandRcx = OperandClassReg | OperandRegGPR | RegRcx | OperandSize64,
	OperandSp = OperandClassReg | OperandRegGPR | RegSp | OperandReg16,
	OperandEsp = OperandClassReg | OperandRegGPR | RegEsp | OperandReg32,
	OperandRsp = OperandClassReg | OperandRegGPR | RegRsp | OperandSize64,
	OperandBp = OperandClassReg | OperandRegGPR | RegBp | OperandReg16,
	OperandEbp = OperandClassReg | OperandRegGPR | RegEbp | OperandReg32,
	OperandRbp = OperandClassReg | OperandRegGPR | RegRbp | OperandSize64,
	OperandSi = OperandClassReg | OperandRegGPR | RegSi | OperandReg16,
	OperandEsi = OperandClassReg | OperandRegGPR | RegEsi | OperandReg32,
	OperandRsi = OperandClassReg | OperandRegGPR | RegRsi | OperandSize64,
	OperandDi = OperandClassReg | OperandRegGPR | RegDi | OperandReg16,
	OperandEdi = OperandClassReg | OperandRegGPR | RegEdi | OperandReg32,
	OperandRdi = OperandClassReg | OperandRegGPR | RegRdi | OperandSize64,
	OperandCr0 = OperandClassReg | OperandRegCDT | RegCr0,
	OperandCr1 = OperandClassReg | OperandRegCDT | RegCr1,
	OperandCr2 = OperandClassReg | OperandRegCDT | RegCr2,
	OperandCr3 = OperandClassReg | OperandRegCDT | RegCr3,
	OperandCr4 = OperandClassReg | OperandRegCDT | RegCr4,
	OperandCr5 = OperandClassReg | OperandRegCDT | RegCr5,
	OperandCr6 = OperandClassReg | OperandRegCDT | RegCr6,
	OperandCr7 = OperandClassReg | OperandRegCDT | RegCr7,
	OperandCr8 = OperandClassReg | OperandRegCDT | RegCr8,
	OperandCr9 = OperandClassReg | OperandRegCDT | RegCr9,
	OperandCr10 = OperandClassReg | OperandRegCDT | RegCr10,
	OperandCr11 = OperandClassReg | OperandRegCDT | RegCr11,
	OperandCr12 = OperandClassReg | OperandRegCDT | RegCr12,
	OperandCr13 = OperandClassReg | OperandRegCDT | RegCr13,
	OperandCr14 = OperandClassReg | OperandRegCDT | RegCr14,
	OperandCr15 = OperandClassReg | OperandRegCDT | RegCr15,
	OperandR8l = OperandClassReg | OperandRegGPR | RegR8l | OperandSize8,
	OperandR9l = OperandClassReg | OperandRegGPR | RegR9l | OperandSize8,
	OperandR10l = OperandClassReg | OperandRegGPR | RegR10l | OperandSize8,
	OperandR11l = OperandClassReg | OperandRegGPR | RegR11l | OperandSize8,
	OperandR12l = OperandClassReg | OperandRegGPR | RegR12l | OperandSize8,
	OperandR13l = OperandClassReg | OperandRegGPR | RegR13l | OperandSize8,
	OperandR14l = OperandClassReg | OperandRegGPR | RegR14l | OperandSize8,
	OperandR15l = OperandClassReg | OperandRegGPR | RegR15l | OperandSize8,
	OperandR8w = OperandClassReg | OperandRegGPR | RegR8w | OperandSize16,
	OperandR9w = OperandClassReg | OperandRegGPR | RegR9w | OperandSize16,
	OperandR10w = OperandClassReg | OperandRegGPR | RegR10w | OperandSize16,
	OperandR11w = OperandClassReg | OperandRegGPR | RegR11w | OperandSize16,
	OperandR12w = OperandClassReg | OperandRegGPR | RegR12w | OperandSize16,
	OperandR13w = OperandClassReg | OperandRegGPR | RegR13w | OperandSize16,
	OperandR14w = OperandClassReg | OperandRegGPR | RegR14w | OperandSize16,
	OperandR15w = OperandClassReg | OperandRegGPR | RegR15w | OperandSize16,
	OperandR8d = OperandClassReg | OperandRegGPR | RegR8d | OperandSize32,
	OperandR9d = OperandClassReg | OperandRegGPR | RegR9d | OperandSize32,
	OperandR10d = OperandClassReg | OperandRegGPR | RegR10d | OperandSize32,
	OperandR11d = OperandClassReg | OperandRegGPR | RegR11d | OperandSize32,
	OperandR12d = OperandClassReg | OperandRegGPR | RegR12d | OperandSize32,
	OperandR13d = OperandClassReg | OperandRegGPR | RegR13d | OperandSize32,
	OperandR14d = OperandClassReg | OperandRegGPR | RegR14d | OperandSize32,
	OperandR15d = OperandClassReg | OperandRegGPR | RegR15d | OperandSize32,
	OperandR8 = OperandClassReg | OperandRegGPR | RegR8 | OperandSize64,
	OperandR9 = OperandClassReg | OperandRegGPR | RegR9 | OperandSize64,
	OperandR10 = OperandClassReg | OperandRegGPR | RegR10 | OperandSize64,
	OperandR11 = OperandClassReg | OperandRegGPR | RegR11 | OperandSize64,
	OperandR12 = OperandClassReg | OperandRegGPR | RegR12 | OperandSize64,
	OperandR13 = OperandClassReg | OperandRegGPR | RegR13 | OperandSize64,
	OperandR14 = OperandClassReg | OperandRegGPR | RegR14 | OperandSize64,
	OperandR15 = OperandClassReg | OperandRegGPR | RegR15 | OperandSize64,

	OperandST0 = OperandClassReg | OperandRegFpu | RegSt0,
	OperandST1 = OperandClassReg | OperandRegFpu | RegSt1,
	OperandST2 = OperandClassReg | OperandRegFpu | RegSt2,
	OperandST3 = OperandClassReg | OperandRegFpu | RegSt3,
	OperandST4 = OperandClassReg | OperandRegFpu | RegSt4,
	OperandST5 = OperandClassReg | OperandRegFpu | RegSt5,
	OperandST6 = OperandClassReg | OperandRegFpu | RegSt6,
	OperandST7 = OperandClassReg | OperandRegFpu | RegSt7,

	OperandMmx0 = OperandClassReg | OperandRegMmx | RegMmx0,
	OperandMmx1 = OperandClassReg | OperandRegMmx | RegMmx1,
	OperandMmx2 = OperandClassReg | OperandRegMmx | RegMmx2,
	OperandMmx3 = OperandClassReg | OperandRegMmx | RegMmx3,
	OperandMmx4 = OperandClassReg | OperandRegMmx | RegMmx4,
	OperandMmx5 = OperandClassReg | OperandRegMmx | RegMmx5,
	OperandMmx6 = OperandClassReg | OperandRegMmx | RegMmx6,
	OperandMmx7 = OperandClassReg | OperandRegMmx | RegMmx7,

	OperandXmm0 = OperandClassReg | OperandRegXmm | RegXmm0,
	OperandXmm1 = OperandClassReg | OperandRegXmm | RegXmm1,
	OperandXmm2 = OperandClassReg | OperandRegXmm | RegXmm2,
	OperandXmm3 = OperandClassReg | OperandRegXmm | RegXmm3,
	OperandXmm4 = OperandClassReg | OperandRegXmm | RegXmm4,
	OperandXmm5 = OperandClassReg | OperandRegXmm | RegXmm5,
	OperandXmm6 = OperandClassReg | OperandRegXmm | RegXmm6,
	OperandXmm7 = OperandClassReg | OperandRegXmm | RegXmm7,
	OperandXmm8 = OperandClassReg | OperandRegXmm | RegXmm8,
	OperandXmm9 = OperandClassReg | OperandRegXmm | RegXmm9,
	OperandXmm10 = OperandClassReg | OperandRegXmm | RegXmm10,
	OperandXmm11 = OperandClassReg | OperandRegXmm | RegXmm11,
	OperandXmm12 = OperandClassReg | OperandRegXmm | RegXmm12,
	OperandXmm13 = OperandClassReg | OperandRegXmm | RegXmm13,
	OperandXmm14 = OperandClassReg | OperandRegXmm | RegXmm14,
	OperandXmm15 = OperandClassReg | OperandRegXmm | RegXmm15,

}OPTYPE;

typedef enum InstrModifier {
	ModRegOpcode = 1,
	ModTTTN = 2,
	ModSegReg2Bit = 4,
	ModSegReg3Bit = 8,
	ModMemFormat = 0x10,
	ModMODRM = 0x20,
	ModSIB = 0x40,
	ModJmp8 = 0x80,
	ModEXT = 0x100,
	ModJmp = 0x200,
	ModSizeWord = 0x400,
	ModSizeDword = 0x800,
	ModSizeQword = 0x1000,
	ModEmitOpcodeOnly = 0x2000,
	ModPsuedo = 0x4000,
	ModAddrSizeWord = 0x8000,
	ModAddrSizeDword = 0x10000
}InstrModifier;

typedef enum InternalPrefixType {
	PrefixClass1_Lock = 0xf0,
	PrefixClass1_Repne = 0xf2,
	PrefixClass1_Rep = 0xf3,
	PrefixClass2_Cs = 0x2e,
	PrefixClass2_Ss = 0x36,
	PrefixClass2_Ds = 0x3e,
	PrefixClass2_Es = 0x26,
	PrefixClass2_Fs = 0x64,
	PrefixClass2_Gs = 0x65,
	PrefixClass2_NoBranch = 0x2e,
	PrefixClass2_Branch = 0x3e,
	PrefixClass3_Opsize = 0x66,
	PrefixClass4_Addrsize = 0x67,
	InternalPrefixTypeInvalid
}InternalPrefixType;

typedef struct REG {
	char*   name;
	REGTYPE type;
	int     id;
	OPTYPE  flags;
}REG, *PREG;

typedef struct TEMPLATE {
	int      mnemonic;
	int      opcode;
	unsigned opcodeExt;
	int      numParms;
	OPTYPE   parms[4];
	int      modifier;
}TEMPLATE, *PTEMPLATE;

typedef struct MODRM {
	int      mode;
	int      reg;
	int      regMem;
}MODRM, *PMODRM;

typedef struct SIB {
	int     scale;
	int     base;
	int     index;
}SIB, *PSIB;

typedef struct ADDRMODE {
	PSYMBOL  sym;
	DISPTYPE type;
	PREG     base;
	PREG     index;
	int      scale;
	int      disp;
}ADDRMODE, *PADDRMODE;

typedef struct INSTR_LIST* PINSTR_LIST;
typedef struct INSTR* PINSTR;

typedef struct INSTR {
	int         op;
	COORD       coord;
	int         opcount;
	OPTYPE      op1;
	PSYMBOL     src1;
	OPTYPE      op2;
	PSYMBOL     src2;
	ADDRMODE    addr;
	int         value;
	PSTRING     label;
//	PXBLOCK     parent;

	// JMP label. after patching,
	// this turns into JMP <target>

//	PXBLOCK     target;

	MODRM       modrm;
	SIB         sib;
	unsigned    modifier; // templ->modifier
	PTEMPLATE   templ;
	PINSTR_LIST list;
	int         segto;
	int         farPtrSeg; // i.e. jmp 7c0:0
	int         prefixCount;
	int         prefix[PREFIX_MAX];
	PINSTR      prev;
	PINSTR      next;
}INSTR, *PINSTR;

typedef struct INSTR_LIST {
	PINSTR first;
	PINSTR last;
}INSTR_LIST, *PINSTR_LIST;

/*
typedef struct XBLOCK {
	PINSTR_LIST  ins;
	unsigned     name;
	unsigned int offset;
	PXBLOCK      next;
}XBLOCK, *PXBLOCK;
*/

typedef enum OUTPUT {
	WRITE_RAWDATA,
	WRITE_ADDRESS,
	WRITE_REL1ADR,
	WRITE_REL2ADR,
	WRITE_REL4ADR
}OUTPUT;

#define PASS0   0
#define PASSGEN 1

#define NO_SEG  0
#define ABS_SEG 0xffff

#define SYM_EXTERN   1
#define SYM_GLOBAL   2

// x86.c

extern PUBLIC PINSTR   XNop(void);
extern PUBLIC PINSTR   XAdd(IN PSYMBOL src1, IN PSYMBOL src2);
extern PUBLIC PINSTR   XSub(IN PSYMBOL src1, IN PSYMBOL src2);
extern PUBLIC PINSTR   XMul(IN PSYMBOL src1);
extern PUBLIC PINSTR   XIMul1(IN PSYMBOL src1);
extern PUBLIC PINSTR   XIMul2(IN PSYMBOL src1, IN PSYMBOL src2);
extern PUBLIC PINSTR   XDiv(IN PSYMBOL src1);
extern PUBLIC PINSTR   XIDiv(IN PSYMBOL src1);
extern PUBLIC PINSTR   XXor(IN PSYMBOL src1, IN PSYMBOL src2);
extern PUBLIC PINSTR   XAnd(IN PSYMBOL src1, IN PSYMBOL src2);
extern PUBLIC PINSTR   XOr(IN PSYMBOL src1, IN PSYMBOL src2);
extern PUBLIC PINSTR   XMov(IN PSYMBOL src1, IN PSYMBOL src2);
extern PUBLIC PINSTR   XMovConst(IN PSYMBOL src1, IN int val);
extern PINSTR XMovInt(IN PSYMBOL src1, IN int val);


extern PUBLIC PINSTR   XJmpc(IN int condcode, IN PSTRING label);
extern PUBLIC PINSTR   XJmp(IN PSTRING label);

//extern PUBLIC PINSTR   XJmpc(IN int condcode, IN PXBLOCK xblock);
//extern PUBLIC PINSTR   XJmp(IN PXBLOCK xblock);

extern PUBLIC PINSTR   XLea(IN PSYMBOL src1, IN PSYMBOL src2);
extern PUBLIC PINSTR   XCmp(IN PSYMBOL src1, IN PSYMBOL src2);
extern PUBLIC PINSTR   XCmpInt(IN PSYMBOL src1, IN int val);
extern PUBLIC PINSTR   XPush(IN PSYMBOL src1);
extern PUBLIC PINSTR   XPop(IN PSYMBOL src1);
extern PUBLIC PINSTR   XRet(void);
extern PUBLIC PINSTR   XCall(IN PSYMBOL src1);
extern PUBLIC PINSTR   XLoad(IN PSYMBOL target, IN PRVAL val, IN PRVAL index, IN int disp);
extern PUBLIC PINSTR   XStore(IN PSYMBOL target, IN PRVAL val, IN PRVAL index, IN int disp);
extern PUBLIC ADDRMODE XAddr(IN REGTYPE base, IN int disp);
extern PUBLIC void     XPrint(IN FILE* out, IN char* format, IN ...);
extern PUBLIC PINSTR   XCoord(IN COORD coord);
extern PUBLIC bool_t   IsXCoord(IN PINSTR ins);
extern PUBLIC PINSTR   XLabel(IN PSTRING label);
extern PUBLIC bool_t   IsXLabel(IN PINSTR ins);

// print.c extension - printing x86 PINSTR

extern PUBLIC void     XVFNPrintIns(IN ADDCHAR out, IN PINSTR ins, IN void* context);

// x86obj.c

extern PUBLIC void     GenerateData(IN PSYMBOL symboL);

// obj.c

extern PUBLIC void     InitCOFF(IN PSTRING src);
extern PUBLIC void     WriteCOFF(IN int segSymIndex,IN uint8_t* data,IN OUTPUT type,IN size_t size,IN int symIndex);
extern PUBLIC void     OutCOFF(IN PSTRING outfile);

// dot.c

extern PUBLIC void     EmitDOT(IN PSYMBOL sym);
extern PUBLIC void     InitDOT(IN FILE* out);
extern PUBLIC void     FlushDOT(void);

#endif

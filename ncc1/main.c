/************************************************************************
*
*	main.c - Neptune C Compiler
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#include "ncc.h"

/* PUBLIC definitions ***************************/

extern TARGET x86IR;
extern TARGET dotIR;
PTARGET       _target;

/**
*	_dump
* 
*	Description : Dumps memory leaks.
*/
PUBLIC void __dump() {

	_CrtDumpMemoryLeaks();
}

/* PRIVATE definitions *************************/

/**
*	Terminate
*
*	Description : Called at process exit or crash.
*/
PRIVATE int Terminate(void) {

	return EXIT_SUCCESS;
}

/**
*	InvokeNASM
*
*	Description : Attempt to call the Neptune Assembler. This may produce
*	an OBJ file and/or LST file with optional debugging support.
*/
PRIVATE void InvokeNASM() {

	system("nasm-2 out_test.txt -f obj -b stabs -o out_test.obj -l out_test.lst");
}

extern PUBLIC void InitListing(void);

typedef enum OPTIONTYPE {
	OPTION_DBGSTABS,
	OPTION_ASSEMBLY,
	OPTION_ASSEMBLYSRC,
	OPTION_INVOKENASM,
	OPTION_IRDOT,
	OPTION_TARGET,
	OPTION_NOLOGO,
	OPTION_HELP
}OPTIONTYPE;

typedef struct OPTION {
	OPTIONTYPE type;
	char* cmd; // case insensitive. e.g. /a, -a, /A, -A are all valid.
	char* msg;
}OPTION, *POPTION;

OPTION _options[] = {
	{ OPTION_INVOKENASM, "c", "Invokes NASM to produce OBJ" },
	{ OPTION_ASSEMBLYSRC, "S", "Output assembly with source code" },
	{ OPTION_ASSEMBLY, "s", "Output assembly only" },
	{ OPTION_TARGET, "o", "Target file" },
	//	{ OPTION_DBGDWARD, "gdwarf", "Produce debug info (dwarf)" },
	{ OPTION_DBGSTABS, "gstabs", "Produce debug info (stabs)" },
	{ OPTION_IRDOT, "E", "Output IR as DOT" },
	{ OPTION_NOLOGO, "NOLOGO", "Suppress copyright message" },
	{ OPTION_HELP, "HELP", "Display brief usage message" },
	{ OPTION_HELP, "?", "Display brief usage message" },
};

unsigned int _globalFlags;

/**
*	main
*
*	Description : Entry point
*/
int main(IN int argc, IN char* argv[]) {

	PPROGRAM     prog;
	PSYMBOL_LIST entry;
	PSYMBOL      func;
	FILE*        outdot;

	outdot = NULL;

	outdot = fopen("out_dot.txt", "w");

	_target = &x86IR;

	atexit(Terminate);

	PushFile(NewStringA("in_test.txt"));

	freopen("out_test.txt", "w", stdout);

	InitTypes(argc, argv);
	InitListing();

	if (_target->Init)
		_target->Init();

	InitDOT(outdot);

	prog = Parse();
	if (_errorCount > 0) {
		Error("Errors detected while parsing");
		return 0;
	}

	// Emit global C strings
	for (entry = prog->cstr; entry; entry = entry->next) {
		OutSymbolSTABS(entry->me);
		_target->Emit(entry->me);
	}

	// Emit globals
	for (entry = prog->gvars; entry; entry = entry->next) {
		OutSymbolSTABS(entry->me);
		_target->Emit(entry->me);
	}

	// Emit functions
	for (entry = prog->functs; entry; entry = entry->next) {

		func = entry->me;

		OutSymbolSTABS(func);



		QPass(func);
		FlowPass(func);
		// DomPass Fast Dominance Algorithm
		// DomFrontierPass
		// SSaPass
		LivenessPass(func);
		// Common subexpression elimination
		// Copy Propogation (should be ran after subexpression)
		// Constant Propogation
		// dead code elimination

		// XCode ----------

		// if Critical Edge Pass...
		// IrLowerPass
		// PhiWebPass
		// SpillPass
		// RegTargetPass (for CALL's)
		ColorPass(func);

		DumpInterfGraph(stderr, func);
		_target->Emit(func);
		EmitDOT(func);
	}

	// only DOT has close
	if (_target->Close)
		_target->Close();

	FlushDOT();
	if (outdot)
		fclose(outdot);

	fflush(stdout);
	return EXIT_SUCCESS;
}



#if 0

// https://k1.spdns.de/Develop/Hardware/AVR/mixed%20docs.../doc/sdccman.html/node180.html
// https://www.diva-portal.org/smash/get/diva2:951540/FULLTEXT01.pdf#page=40&zoom=100,178,94
// https://dl.acm.org/doi/pdf/10.1145/74818.74839
// https://beza1e1.tuxen.de/pdfs/braun11wir.pdf
// https://beza1e1.tuxen.de/pdfs/braun11wir.pdf
// https://scholarship.rice.edu/bitstream/handle/1911/96451/TR95-252.pdf?sequence=1&isAllowed=y
// C. N. Click. Combining Analyses, Combining Optimizations.
// https://www.cs.rice.edu/~keith/EMBED/dom.pdf
// https://vod.video.cornell.edu/media/1_130pq2fh
// https://pages.cs.wisc.edu/~fischer/cs701.f05/lectures/Lecture22.pdf
// https://web.stanford.edu/class/archive/cs/cs143/cs143.1128/lectures/14/Slides14.pdf

// Continuation Passing Style
// https://www.reddit.com/r/Compilers/comments/amd2p4/what_is_the_actual_benefit_of_using_ssa_to/

// Induction variable analysis is trivial in SSA, leading to a lot of useful stuff, like strength reduction, vectorisation, loop fusion, and so on.
// https://www.cs.purdue.edu/homes/suresh/502-Fall2008/papers/kelsey-ssa-cps.pdf

// register interference graph
// https://courses.cs.cornell.edu/cs4120/2022sp/notes.html?id=regalloc
// https://github.com/pyokagan/CS4212-Compiler/blob/bf8191b5c7e8c234cce81528eee1691068610e4f/src/main/java/pyokagan/cs4212/ArmGen.java

// https://citeseerx.ist.psu.edu/document?repid=rep1&type=pdf&doi=347e83b8e0b895f463e6d64d3e258f67aae77527
// peephole optimizer

replace{
	mov %1, a
	mov a, %1
} by{
	mov %1, a
}

#endif


/************************************************************************
*
*	ctype.c - C types
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "ncc.h"
#include <float.h>

PUBLIC PTYPE CharType;                // char
PUBLIC PTYPE WideCharType;            // wchar_t
PUBLIC PTYPE SignedCharType;          // signed char
PUBLIC PTYPE IntType;                 // signed int
PUBLIC PTYPE LongType;                // long
PUBLIC PTYPE LongLongType;            // long long
PUBLIC PTYPE ShortType;               // signed short int
PUBLIC PTYPE FloatType;               // float
PUBLIC PTYPE DoubleType;              // double
PUBLIC PTYPE LongDoubleType;          // long double
PUBLIC PTYPE UnsignedCharType;        // unsigned char
PUBLIC PTYPE UnsignedIntType;         // unsigned int
PUBLIC PTYPE UnsignedLongType;        // unsigned long
PUBLIC PTYPE UnsignedLongLongType;    // unsigned long long
PUBLIC PTYPE UnsignedShortType;       // unsigned short
PUBLIC PTYPE VoidType;                // void
PUBLIC PTYPE ArrayOfCharType;         // ""
PUBLIC PTYPE ArrayOfWideCharType;     // L""
PUBLIC PTYPE EnumeratorType;          // member of enum
PUBLIC PTYPE PointerType;             // T*

PRIVATE unsigned _type;

PRIVATE PTYPE Type(IN OPTIONAL char* name, IN int kind, IN PTYPE type, IN size_t size, IN int align) {

	PTYPE t = Allocz(sizeof(TYPE));

	t->kind = kind;
	t->type = type;
	t->size = size;
	t->align = align;
	if (name) t->name = NewStringA(name);

	switch (kind) {
	case TYPE_INT:
		t->limits.max.i = VALUE_MASK(8 * size) >> 1;
		t->limits.min.i = -t->limits.max.i - 1;
		break;
	case TYPE_UNSIGNED:
		t->limits.max.ui = VALUE_MASK(8 * size);
		t->limits.min.ui = 0;
		break;
	case TYPE_FLOAT:
		if (size == sizeof(float)) {
			t->limits.max.f = FLT_MAX;
			t->limits.min.f = FLT_MIN;
		}
		else if (size == sizeof(double)) {
			t->limits.max.f = DBL_MAX;
			t->limits.min.f = DBL_MIN;
		}
		else if (size == sizeof(long double)) {
			t->limits.max.lf = LDBL_MAX;
			t->limits.min.lf = LDBL_MIN;
		}
		break;
	}
	return t;
}

PUBLIC void InitTypes(IN int argc, IN char* argv[]) {

	IntType               = Type("int",                TYPE_INT,      NULL, _target->intinfo.size,         _target->intinfo.align);
	CharType              = Type("char",               TYPE_INT,      NULL, _target->charinfo.size,        _target->charinfo.align);
	SignedCharType        = Type("signed char",        TYPE_INT,      NULL, _target->charinfo.size,        _target->charinfo.align);
	LongType              = Type("long",               TYPE_INT,      NULL, _target->longinfo.size,        _target->longinfo.align);
	LongLongType          = Type("long long",          TYPE_INT,      NULL, _target->longlonginfo.size,    _target->longlonginfo.align);
	ShortType             = Type("short",              TYPE_INT,      NULL, _target->shortinfo.size,       _target->shortinfo.align);
	FloatType             = Type("float",              TYPE_FLOAT,    NULL, _target->floatinfo.size,       _target->floatinfo.align);
	DoubleType            = Type("double",             TYPE_FLOAT,    NULL, _target->doubleinfo.size,      _target->doubleinfo.align);
	LongDoubleType        = Type("long double",        TYPE_FLOAT,    NULL, _target->longdoubleinfo.size,  _target->longdoubleinfo.align);
	UnsignedCharType      = Type("unsigned char",      TYPE_UNSIGNED, NULL, _target->charinfo.size,        _target->charinfo.align);
	UnsignedIntType       = Type("unsigned int",       TYPE_UNSIGNED, NULL, _target->intinfo.size,         _target->intinfo.align);
	UnsignedLongType      = Type("unsigned long",      TYPE_UNSIGNED, NULL, _target->longinfo.size,        _target->longinfo.align);
	UnsignedLongLongType  = Type("unsigned long long", TYPE_UNSIGNED, NULL, _target->longlonginfo.size,    _target->longlonginfo.align);
	UnsignedShortType     = Type("unsigned short",     TYPE_UNSIGNED, NULL, _target->shortinfo.size,       _target->shortinfo.align);
	EnumeratorType        = Type("enumerator",         TYPE_ENUMERATOR, NULL, _target->longinfo.size, _target->longinfo.align);
	PointerType           = Type("pointer",            TYPE_POINTER, NULL, _target->ptrinfo.size, _target->ptrinfo.align);
	VoidType              = Type("void",               TYPE_VOID, NULL, 0, 0);
	WideCharType          = Type("wchar_t",            TYPE_UNSIGNED, NULL, _target->shortinfo.size, _target->shortinfo.align);
	// specifically used by the Lexer to avoid allocations in tokens:
	ArrayOfCharType       = Array(CharType, 0, 0);
	ArrayOfWideCharType   = Array(WideCharType, 0, 0);
}

PUBLIC PTYPE Pointer(IN PTYPE to){

	return Type(NULL, TYPE_POINTER, to, PointerType->size, PointerType->align);
}

PUBLIC PTYPE Deref(IN PTYPE of) {

	if (IsPointer(of))
		of = of->type;
	else Error("Deref: Pointer expected");
	return of;
}

PUBLIC PTYPE Array(IN PTYPE of, IN int size, IN int align) {

	return Type(NULL, TYPE_ARRAY, of, size, align);
}

PUBLIC PTYPE ArrayToPtr(IN PTYPE ar) {

	if (IsArray(ar))
		return Pointer(ar->type);
	else
		Error("ArrayToPtr: Expected array");
	return Pointer(ar);
}

PUBLIC PTYPE Function(IN PTYPE ret, IN PTYPE* proto, IN bool_t oldStyle) {

	return Type(NULL, TYPE_FUNCTION, ret, 0, 0);
}

PUBLIC PTYPE FunctionRet(IN PTYPE fn) {

	if (IsFunction(fn))
		return fn->type;
	else Error("FunctionRet: Expected function");
	return IntType;
}

PUBLIC PTYPE Struc(IN int kind) {

	return Type(NULL, kind, NULL, 0, 0);
}

PUBLIC PPARAM GetParam(IN PTYPE func, IN index_t idx) {

	PPARAM arg;
	index_t cur;

	cur = 0;
	for (arg = func->args; arg; arg = arg->next)
		if (cur++ == idx)
			return arg;
	return NULL;
}

PUBLIC PFIELD GetField(IN PTYPE struc, IN PSTRING ident) {

	PFIELD field;

	if (!ident) return NULL;
	if (!IsStruct(struc)) return NULL;
	for (field = struc->fields; field; field = field->next)
		if (!strcmp(field->name->val, ident->val))
			return field;
	return NULL;
}

PUBLIC PPARAM AddParam(IN PTYPE func, IN PSTRING ident, IN PTYPE type) {

	PPARAM arg;

	arg = Alloc(sizeof(PARAM));
	arg->name = ident;
	arg->type = type;
	arg->next = NULL;

	if (!func->args)
		func->args = arg;
	if (func->lastarg)
		func->lastarg->next = arg;

	func->lastarg = arg;
	return arg;
}

PUBLIC PFIELD AddField(IN PTYPE struc, IN PSTRING ident, IN PTYPE type, IN short bits) {

	PFIELD field;

	field = Alloc(sizeof(PFIELD));
	field->name = ident;
	field->type = type;
	field->bits = bits;
	field->offset = 0;
	field->next = NULL;

	// struc:
	if (struc->kind == TYPE_STRUCT)
		struc->size += type->size;

	// union:
	else if (type->size > struc->size)
		struc->size = type->size;

	if (!struc->fields)
		struc->fields = field;

	if (struc->lastfield) {

		if (struc->kind == TYPE_STRUCT)
			field->offset = struc->lastfield->offset + struc->lastfield->type->size;

		struc->lastfield->next = field;
	}
	struc->lastfield = field;
	return field;
}

//
// Integer and floating point promotions
// 6.3.1 ~ Arithmitic operands
//
PUBLIC PTYPE Promote(IN PTYPE t) {

	t = Unqual(t);
	if (t->kind == TYPE_INT) {
		if (t->size < IntType->size)
			return IntType;
	}
	else if (t->kind == TYPE_UNSIGNED) {
		if (t->size < IntType->size)
			return IntType;
		if (t->size < UnsignedIntType->size)
			return UnsignedIntType;
	}
	else if (t->kind == TYPE_FLOAT) {
		if (t->size < DoubleType->size)
			return DoubleType;
		if (t->size < LongDoubleType->size)
		  return LongDoubleType;
	}
	return t;
}

//
// Qualifies a type.
//
PUBLIC PTYPE Qual(IN int op, IN PTYPE t) {

	if (IsFunction(t->type))
		Warn("Qualified function types are ignored");
	else if (IsArray(t->type))
		t = Type(NULL, TYPE_ARRAY, Qual(op,t->type), t->size, t->align);
	else{
		if (IsQual(t)) {
			t = t->type;
			op += t->kind;
		}
		t = Type(NULL, op, t, t->size, t->align);
	}
	return t;
}

PUBLIC int TypeToOp(IN PTYPE type) {

	switch (type->kind) {
	case TYPE_CONST: case TYPE_VOLATILE: case TYPE_CONST + TYPE_VOLATILE:
		return TypeToOp(type->type);
	case TYPE_VOID: case TYPE_INT: case TYPE_UNSIGNED: case TYPE_FLOAT:
		return type->kind + SIZEOP(type->size);
	case TYPE_POINTER:
		return P + SIZEOP(PointerType->size);
	case TYPE_FUNCTION:
		return P + SIZEOP(PointerType->size);
	case TYPE_ARRAY: case TYPE_STRUCT: case TYPE_UNION:
		return B;
	case TYPE_ENUM:
		return I + SIZEOP(IntType->size);
	};
}

PUBLIC PTYPE OpToType(IN int op, IN int size) {

#define cc(ty) if (size == (ty)->size) return ty;

	switch (op) {
	case F:
		cc(FloatType);
		cc(DoubleType);
		cc(LongDoubleType);
		__debugbreak(); return NULL;
	case I:
		cc(CharType);
		cc(SignedCharType);
		cc(ShortType);
		cc(IntType);
		cc(LongType);
		cc(LongLongType);
		__debugbreak(); return NULL;
	case U:
		cc(UnsignedCharType);
		cc(UnsignedShortType);
		cc(UnsignedIntType);
		cc(UnsignedLongType);
		cc(UnsignedLongLongType);
		__debugbreak(); return NULL;
	case P:
		cc(PointerType);
		__debugbreak(); return NULL;
	};
#undef cc
	__debugbreak(); return NULL;
}


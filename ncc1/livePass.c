/************************************************************************
*
*	live.c - Liveness Analysis
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

#include "ncc.h"

// Least Fixed Point Algorithm:
// 	https://www.classes.cs.uchicago.edu/archive/2004/spring/22620-1/docs/liveness.pdf

// Sample for using liveness data:
//  https://github.com/pyokagan/CS4212-Compiler/blob/bf8191b5c7e8c234cce81528eee1691068610e4f/src/main/java/pyokagan/cs4212/ColorPass.java

PRIVATE void AppendQuadPred(IN PQUAD to, IN PQUAD from) {

	if (!QuadListFind(to->predlist, from))
		QuadListAdd(&to->predlist, from);
}

PRIVATE void AppendQuadSucc(IN PQUAD to, IN PQUAD from) {

	if (!QuadListFind(to->succlist, from))
		QuadListAdd(&to->succlist, from);
}

PRIVATE void AdjustSuccessorList(IN PBLOCK block) {

	PQUAD_LIST  quadEntry;
	PQUAD_LIST  prevQuadEntry;
	PBLOCK_LIST predListEntry;
	PBLOCK      predBlock;

	// look at the predecessors of this block. The last instruction
	// of each predecessor block has "code->start" as its successor.
	// Likewise, "code->start" is the successor of each.

	quadEntry = block->quads;

	for (predListEntry = block->predlist; predListEntry; predListEntry = predListEntry->next) {

		predBlock = predListEntry->me;
		prevQuadEntry = QuadListGetLast(predBlock->quads);

		AppendQuadPred(quadEntry->me, prevQuadEntry->me);
		AppendQuadSucc(prevQuadEntry->me, quadEntry->me);
	}

	// for all other instructions, we are the successor of the
	// previous instruction:

	for (quadEntry = quadEntry->next; quadEntry; quadEntry = quadEntry->next) {
		AppendQuadPred(quadEntry->me, quadEntry->prev->me);
		AppendQuadSucc(quadEntry->prev->me, quadEntry->me);
	}
}

PRIVATE void AdjustSuccessorLists(IN PSYMBOL func) {

	PBLOCK block;

	for (block = func->code; block; block = block->next)
		AdjustSuccessorList(block);
}

PRIVATE void AddSymbol(IN PSYMBOL_LIST* list, IN PSYMBOL sym) {

	if (sym)
		SymbolListAdd(list, sym);
}

PRIVATE void AdjustDefUseList(IN PQUAD quad) {

	if (OPCODE(quad->op) == QCALL) {

		// all SYMBOL's or TEMP's in the argument list of the
		// function call are being "used" so need to be added to GENLIST.
		// They have already been evaluated by quad.c so should just be
		// SYMBOL or TEMP.

	}
	else if (OPCODE(quad->op) == QASGN) {

		// ASGN is M[x] = k operation. In this case, we
		// are only using "x" and "k" and not changing them.
		// So everything gets added to GENLIST.

		AddSymbol(&quad->uselist, quad->target);

		if (quad->index && quad->index->var)
			AddSymbol(&quad->uselist, quad->index->var);

		if (quad->src1 && quad->src1->var)
			AddSymbol(&quad->uselist, quad->src1->var);

		if (quad->src2 && quad->src2->var)
			AddSymbol(&quad->uselist, quad->src2->var);
	} 
	else{

		// All other operations only change "target".
		// So, add "target" to KILLLIST, all others added
		// to GENLIST.

		if (quad->index && quad->index->var)
			AddSymbol(&quad->uselist, quad->index->var);

		if (quad->src1 && quad->src1->var)
			AddSymbol(&quad->uselist, quad->src1->var);
		if (quad->src2 && quad->src2->var)
			AddSymbol(&quad->uselist, quad->src2->var);
		AddSymbol(&quad->deflist, quad->target);
	}
}

PRIVATE void AdjustDefUseLists(IN PSYMBOL func) {

	PSYMBOL_LIST sym;
	PSYMBOL_LIST use;
	PQUAD_LIST   quad;
	PBLOCK       block;

	for (block = func->code; block; block = block->next) {

		for (quad = QuadListGetLast(block->quads); quad; quad = quad->prev) {

			AdjustDefUseList(quad->me);

			for (sym = quad->me->uselist; sym; sym = sym->next) {
				use = SymbolListAdd(&block->lastUseList, sym->me);
				use->lastUse = quad->me;
			}
		}
	}
}

PRIVATE bool_t AdjustInList(IN PQUAD quad) {

	PSYMBOL_LIST use;
	PSYMBOL_LIST out;
	PSYMBOL_LIST def;
	PSYMBOL_LIST newlist;
	bool_t       repeat;

	// Least Fixed Point Algorithm:
	// This computes IN[n]

	//	in[n] : = use[n] UNION(out[n] - def[n])

	// because "out" can change between iterations,
	// the items to remove may change. So create a new
	// list and compare with original.

	newlist = NULL;
	repeat = FALSE;

	for (use = quad->uselist; use; use = use->next) {
		if (!SymbolListFind(quad->inlist, use->me))
			repeat = TRUE;
		SymbolListAdd(&newlist, use->me);
	}

	for (out = quad->outlist; out; out = out->next) {
		if (SymbolListFind(quad->deflist, out->me))
			continue;
		if (!SymbolListFind(quad->inlist, out->me))
			repeat = TRUE;
		SymbolListAdd(&newlist, out->me);
	}

	// FIXME: Should FREE quad->inlist before assigning the new list.

	quad->inlist = newlist;
	return repeat;
}

PRIVATE bool_t AdjustOutList(IN PQUAD quad) {

	PSYMBOL_LIST in;
	PQUAD_LIST   succEntry;
	bool_t       repeat;

	// Least Fixed Point Algorithm
	// This computes OUT[n]:

	// out[n] : = UNION{ in[s] | s ELEMENT OF succ[n] }

	if (!quad->succlist) return FALSE;
	repeat = FALSE;
	for (succEntry = quad->succlist; succEntry; succEntry = succEntry->next) {
		for (in = succEntry->me->inlist; in; in = in->next) {
			if (!SymbolListFind(quad->outlist, in->me))
				repeat = TRUE;
			SymbolListAdd(&quad->outlist, in->me);
		}
	}
	return repeat;
}

PRIVATE void AdjustInOutLists(IN PSYMBOL func) {

	PBLOCK     block;
	PQUAD_LIST quadEntry;
	bool_t     repeat;

	/* Least Fixed Point Algorithm
	repeat
	for each n
	in [n] : = in[n]; out [n] : = out[n]
	in[n] : = use[n] UNION(out[n] - def[n])
	out[n] : = UNION{ in[s] | s ELEMENT OF succ[n] }
	until in [n] = in[n] and out [n] = out[n] for all n
	*/

	do{
		repeat = FALSE;
		for (block = func->code; block; block = block->next) {
			for (quadEntry = block->quads; quadEntry; quadEntry = quadEntry->next) {
				if (AdjustInList(quadEntry->me)) repeat = TRUE;
				if (AdjustOutList(quadEntry->me)) repeat = TRUE;
			}
		}
	} while (repeat);

	for (block = func->code; block; block = block->next) {
		quadEntry = QuadListGetLast(block->quads);
		block->inlist = block->quads->me->inlist;
		block->outlist = quadEntry->me->outlist;
	}
}

/**
*	Liveness
*
*	Description : Annotate the function code with liveness details
*
*	Input : func - Function
*/
PUBLIC void LivenessPass(IN PSYMBOL func) {

//	BuildPreorderList(func, func->code);
	AdjustSuccessorLists(func);
	AdjustDefUseLists(func);
	AdjustInOutLists(func);
}

PUBLIC PSYMBOL_LIST GetLiveIn(IN PBLOCK block) {

	return block->inlist;
}

PUBLIC PSYMBOL_LIST GetLiveOut(IN PBLOCK block) {

	return block->outlist;
}

PUBLIC PSYMBOL_LIST GetLastUseList(IN PBLOCK block) {

	return block->lastUseList;
}

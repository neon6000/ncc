/************************************************************************
*
*	quad.c
*
*   Copyright (c) BrokenThorn Entertainment Co. All Rights Reserved.
*
************************************************************************/

/* This is a work in progress --------------- */

#include <malloc.h>
#include "ncc.h"

PRIVATE PSYMBOL    _function;
PRIVATE unsigned   _bbcount;
PRIVATE PMAP       _labelToBlock;
PRIVATE PBLOCK     _firstblock;
PRIVATE PBLOCK     _lastblock;

PRIVATE PQUAD_LIST _usedLabels;

#define MAX_BUCKETS 32

PRIVATE unsigned NextBlock(void) {

	return ++_bbcount;
}

PRIVATE PBLOCK NewBlock(void) {

	PBLOCK bb;

	bb = calloc(1, sizeof(BLOCK));
	bb->name = NextBlock();
	bb->fn = _function;

	if (!_lastblock)
		_firstblock = _lastblock = bb;
	else {
		_lastblock->next = bb;
		_lastblock = bb;
	}
	return bb;
}

PRIVATE void AddQuad(IN PQUAD quad) {

	QuadListAdd(&_lastblock->quads, quad);
	quad->block = _lastblock;
}

PRIVATE void AddLabel(IN PQUAD quad) {

	// misses up label: and goto. also, should we do this here anyways?
//	if (!QuadListFind(_usedLabels, quad))
//		return;

	if (_lastblock->quads)
		NewBlock();

	if (_lastblock->label)
		__debugbreak();
	_lastblock->label = quad;
	MapPutA(_labelToBlock, quad->src1->stringlit->val, _lastblock);

	AddQuad(quad);
}

PRIVATE void AppendToPreorderList(IN PSYMBOL func, IN PBLOCK block) {

	PBLOCK blk;

	if (!func->codePreorder) {
		func->codePreorder = block;
		return;
	}
	for (blk = func->codePreorder; blk->preordnext; blk = blk->preordnext)
		;
	blk->preordnext = block;
	block->preorderprev = blk;
}

PRIVATE void AppendToPostorderList(IN PSYMBOL func, IN PBLOCK block) {

	PBLOCK blk;

	if (!block) return;
	if (block->preordvisit) return;

	if (!func->codePostorder) {
		func->codePostorder = block;
		return;
	}
	for (blk = func->codePostorder; blk->postordernext; blk = blk->postordernext)
		;
	blk->postordernext = block;
	block->postorderprev = blk;
}

PRIVATE void BuildPostorderList(IN PSYMBOL func, IN PBLOCK block) {

	if (!block) return;
	if (block->postordervisit) return;

	block->postordervisit = TRUE;

	// prioritize "else"
	BuildPostorderList(func, block->outCond);
	BuildPostorderList(func, block->outDirect);

	AppendToPostorderList(func, block);
}

PRIVATE void BuildPreorderList(IN PSYMBOL func, IN PBLOCK block) {

	PBLOCK blk;

	if (!block) return;
	if (block->preordvisit) return;

	block->preordvisit = TRUE;
	AppendToPreorderList(func, block);

	// prioritize "then"
	BuildPreorderList(func, block->outDirect);
	BuildPreorderList(func, block->outCond);
}

#if 0
PRIVATE PBLOCK_LIST GetLastBlockDfs(void) {

	PBLOCK_LIST item;

	for (item = _blocks; item->next; item = item->next)
		;
	return item;
}

PRIVATE bool_t Visited(IN PBLOCK block) {

	PBLOCK_LIST item;

	item = BlockListFind(_visited, block);
	if (item) return TRUE;
	return FALSE;
}

// reverse post ordering of blocks that prioritize "else" branch

PRIVATE void BlockDfs(IN PBLOCK block) {

	BlockListAdd(&_visited, block);
	if (block->outCond && !Visited(block->outCond))
		BlockDfs(block->outCond);
	if (block->outDirect && !Visited(block->outDirect))
		BlockDfs(block->outDirect);
	BlockListAdd(&_blocks, block);
}
#endif


PUBLIC void FlowPass(IN PSYMBOL funct) {

	PQUAD_LIST  quadEntry;
	PBLOCK      block;
	PBLOCK      target;
	PQUAD       quad;

	_function = funct;
	_bbcount = 0;
	_firstblock = NULL;
	_lastblock = NULL;
	_usedLabels = NULL;

	_labelToBlock = NewMap(FALSE, MAX_BUCKETS);

	// Build used blocks list:

	for (quadEntry = funct->quads; quadEntry; quadEntry = quadEntry->next) {
		if (!quadEntry->me) continue;
		if (OPCODE(quadEntry->me->op) == QJMP) {
			if (!QuadListFind(_usedLabels, quadEntry->me->label))
				QuadListAdd(&_usedLabels, quadEntry->me->label);
		}
	}

	// Create basic blocks:

	funct->code = NewBlock();

	for (quadEntry = funct->quads; quadEntry; quadEntry = quadEntry->next) {
		if (!quadEntry->me) continue;
		if (OPCODE(quadEntry->me->op) == QLABEL)
			AddLabel(quadEntry->me);
		else{
			AddQuad(quadEntry->me);
			if (OPCODE(quadEntry->me->op) == QJMP || OPCODE(quadEntry->me->op) == QRET)
				NewBlock();
		}
	}

	// Output edges:

	for (block = _firstblock; block; block = block->next) {

		if (!block->quads) {

			// if this basic block is empty, add a JMP to next block.
			// this is to keep basic blocks clean:

			QuadListAdd(&block->quads, QJmp(block->next->label));
			block->outDirect = block->next;
			block->outCond = NULL;
			BlockListAdd(&block->succlist, block->next);
		}

		quadEntry = QuadListGetLast(block->quads);
		quad = quadEntry->me;
		if (OPCODE(quad->op) == QJMP) {
			if (!quad->src2) {
				target = MapGetA(_labelToBlock, quad->label->src1->stringlit->val);
				block->outDirect = target;
				block->outCond = NULL;
				BlockListAdd(&block->succlist, target);
			}
			else{
				target = MapGetA(_labelToBlock, quad->label->src1->stringlit->val);
				block->outDirect = block->next;
				block->outCond = target;
				BlockListAdd(&block->succlist, target);
				BlockListAdd(&block->succlist, block->next);
			}
		}
		else if (block->next) {

			// last instruction of this basic block is not a JMP so
			// add a JMP instruction to keep basic blocks clean:

			QuadListAdd(&block->quads, QJmp(block->next->label));
			block->outDirect = block->next;
			block->outCond = NULL;
			BlockListAdd(&block->succlist, block->next);
		}
	}

	// Input edges:

	for (block = _firstblock; block; block = block->next) {

		if (block->outDirect)
			BlockListAdd(&block->outDirect->predlist, block);
		if (block->outCond)
			BlockListAdd(&block->outCond->predlist, block);
	}

	BuildPostorderList(funct, funct->code);
	BuildPreorderList(funct, funct->code);
}
